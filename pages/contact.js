import Head from "next/head";
import { PageInfoBasic } from "@/components/PageInfo/PageInfo";
import React, { useCallback } from "react";

import styles from "../styles/Contact.module.scss";
import { Footer } from "@/components/Footer/Footer";
import SVG404 from "@/svgs/404.svg";
import Link from "next/link";
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";
import SVGLogoImageOnly from "@/svgs/logoImageOnly.svg";

import {
    Form,
    Input,
    Button,
    Radio,
    Row,
    Col,
    Checkbox,
    Select,
    Upload,
} from "antd";
import { HeaderBannerLight } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from 'next-i18next';
import { sendContact } from "@/api";
import router, { useRouter } from "next/router";
import { FancyButton } from "@/components/FancyButton/FancyButton";
const { Option } = Select;
const { TextArea } = Input;

export default function Contact() {
    const { locale } = useRouter();
    const { t } = useTranslation("common");
    const rwd = useRWD();

    const handleFinish = useCallback(
        (data) => {
            sendContact(data, locale).then((res) => {
                if (res.resu) {
                    router.replace("/message?a=contactSuccess");
                }
            });
        },
        [locale]
    );
    return (
        <>
            <Head>
                <title>{t("seo.contact.title")}</title>
                <meta name="description" content={t("seo.contact.desc")} />
            </Head>
            <h1 className="seoH1">{t("h1.contact")}</h1>
            <HeaderBannerLight title={t("contact.headline")} />
            <PageInfoBasic />
            <section className={"basic-section"}>
                <div className={styles.contentContainer}>
                    <div className={styles.contentGroup}>
                        <SVGLogoImageOnly className={styles.logo} />
                        <h3 className={styles.title}>{t("contact.name")}</h3>
                        <p>{t("contact.information")}</p>
                    </div>
                </div>
                <div className={styles.formContainer}>
                    <Form
                        layout="vertical"
                        className={styles.form}
                        onFinish={handleFinish}
                    >
                        <input type="hidden" name="csrf_token" value="no_token" />
                        <Form.Item
                            required
                            label={t("contact.form.name")}
                            name="name"
                            rules={[
                                { required: true, message: "Please input your username!" },
                            ]}
                        >
                            <Input placeholder={t("contact.form.name")} />
                        </Form.Item>
                        <Form.Item
                            required
                            label={t("contact.form.eamil")}
                            name="email"
                            rules={[
                                { required: true, message: "Please input your username!" },
                            ]}
                        >
                            <Input placeholder={t("contact.form.eamil")} />
                        </Form.Item>
                        <Form.Item
                            label={t("contact.form.organization")}
                            name="company"
                            rules={[]}
                        >
                            <Input placeholder={t("contact.form.organization")} />
                        </Form.Item>
                        <Form.Item
                            required
                            label={t("contact.form.message")}
                            name="content"
                            rules={[
                                { required: true, message: "Please input your username!" },
                            ]}
                        >
                            <TextArea showCount maxLength={50} />
                        </Form.Item>
                        <FancyButton className={classNames(styles.button)}>
                            {t("contact.form.submit.button")}
                        </FancyButton>
                    </Form>
                </div>
            </section>
            <Footer />
        </>
    );
}
Contact.headerProps = {
    // title: "聯絡我們",
    // red: true,
};
export async function getStaticProps(context) {
    return {
        props: {
            ...(await serverSideTranslations(context.locale, ["common"])),
        }, // will be passed to the page component as props
    };
}
