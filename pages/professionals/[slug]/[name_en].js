import Head from "next/head";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { useTranslation } from "next-i18next";
import React, { useContext, useMemo } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { collectmIndex, getMemberV3, getSourceItemV3, unitReview } from "@/api";
import partnerPreviewStyles from "@/components/PartnerPreview/PartnerPreview.module.scss";
import styles from "@/styles/ProfessionalsInfo.module.scss";

import { Swiper, SwiperSlide } from "swiper/react";
import IconFb from "@/svgs/socialFb2.svg";
import IconTwitter from "@/svgs/socialTwitter2.svg";
import IconIg from "@/svgs/socialIg.svg";
import IconIn from "@/svgs/socialIn.svg";

import IconWeb from "@/svgs/globe2.svg";
import IconEmail from "@/svgs/email.svg";
import IconTel from "@/svgs/tel.svg";
import IconGeo from "@/svgs/geolocation.svg";
import Registrations from "@/components/Registrations/Registrations";
import classNames from "classnames";
import { useWindowSize } from "react-use";
import { Footer } from "@/components/Footer/Footer";
import WorkCardList from "@/components/WorkCardList/WorkCardList";
import { calcWeb1440Count } from "@/utilits/calcWeb";
import Link from "next/link";
import { useRouter } from "next/router";
import { getLoginData } from "@/utilits/getLoginData";
import { useFavorite } from "@/hooks/useFavorite";
import FavoriteIcon from "@/components/FavoriteIcon/FavoriteIcon";
import { ApplyUnitPopup } from "@/components/ApplyPopup/ApplyPopup";
import { LoadingContext } from "@/context/LoadingContext";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import PreviewBanner from "@/components/PreviewBanner/PreviewBanner";
import IconLocked from "@/svgs/locked.svg";
import { Tooltip } from "antd";
import getConfig from "next/config";

export default function ProfessionalsInfo({
  member,
  sourceItems,
  preview,
  memberFavorites,
  workFavorites,
  isLogin,
  authKey,
  memberId,
  slug,
}) {
  const { publicRuntimeConfig } = useMemo(getConfig, []);
  const { setShowLoading } = useContext(LoadingContext);
  const { t } = useTranslation("common");
  const socialData = [
    {
      icon: <IconFb style={{ width: 30, height: 30 }} />,
      link: member.facebook,
    },
    {
      icon: <IconTwitter style={{ width: 33, height: 23 }} />,
      link: member.twitter,
    },
    {
      icon: <IconIg style={{ width: 30, height: 26 }} />,
      link: member.instagram,
    },
    {
      icon: <IconIn style={{ width: 30, height: 30 }} />,
      link: member.linkedin,
    },
  ].filter((l) => l.link);

  const linksData = [
    {
      icon: <IconWeb />,
      content: member.website,
      // content: "member.website",
      locked: false,
      mt: 5,
    },
    {
      icon: <IconEmail />,
      content: isLogin ? member.email : "***@******",
      mt: 5,
      locked: !isLogin,
      // content: "member.email",
    },
    {
      icon: <IconTel />,
      content: isLogin ? member.tel : "**********",
      mt: 5,
      locked: !isLogin,
      // content: "member.tel",
    },
    {
      icon: <IconGeo />,
      content: member.addressCountry,
      locked: false,
    },
  ].filter((l) => l.content);

  const windowSize = useWindowSize();

  const isMobile = windowSize.width < 960;

  const marginH = (windowSize.width - calcWeb1440Count(1280, windowSize)) / 2;

  const { locale } = useRouter();
  const router = useRouter();

  const memberFavoritesState = useFavorite({
    initFavoriteIds: memberFavorites.map((d) => d.id),
    authKey,
    memberId,
    type: 1,
  });

  const workFavoritesState = useFavorite({
    initFavoriteIds: workFavorites.map((d) => d.id),
    authKey,
    memberId,
    type: 2,
  });

  const handleSuccessApply = async ({ successCb }) => {
    setShowLoading(true);
    const { resu } = await unitReview({
      memberm1_no: memberId,
      auth_key: authKey,
      Unitm1_no: member.Unitm1_no,
    });

    if (resu === 1) {
      successCb(member.Unitm1_no);
    }
    setShowLoading(false);
  };

  return (
    <>
      <Head>
        <title>
          {t("seo.professionalsDetail.title", {
            name:
              locale === "zh"
                ? member.name_zh || member.name_en
                : member.name_en || member.name_zh,
          })}
        </title>
        <meta
          name="description"
          content={
            locale === "zh"
              ? member.intro_zh || member.intro_en
              : member.intro_en || member.intro_zh
          }
        />
        <meta
          property="og:title"
          content={t("seo.professionalsDetail.title", {
            name:
              locale === "zh"
                ? member.name_zh || member.name_en
                : member.name_en || member.name_zh,
          })}
          key="og-title"
        />
        <meta
          property="og:image"
          content={`${publicRuntimeConfig.backendUrl}${member.pic}`}
          key="og-image"
        />
        <meta
          property="og:description"
          content={
            locale === "zh"
              ? member.intro_zh || member.intro_en
              : member.intro_en || member.intro_zh
          }
          key="og-desc"
        />
      </Head>
      {preview ? (
        <PreviewBanner
          previewTitle={t("works.step4.submit1")}
          backText={t("companion.preview.back")}
          action={
            member.isfinish && (
              <ApplyUnitPopup
                onSuccess={handleSuccessApply}
                SubmitBtn={({ text }) => (
                  <FancyButton
                    selected
                    htmlType="button"
                    scaleSize={38}
                    className={classNames(styles.previewBtn)}
                  >
                    {text}
                  </FancyButton>
                )}
              />
            )
          }
        />
      ) : (
        <HeaderBannerNew1 title={t("professionals.headline")} />
      )}

      <div className={styles.container}>
        <div className={styles.grid}>
          <div className={styles.imageContainer}>
            <img src={member.pic} alt="icon" />
          </div>
          <div className={styles.infoContainer}>
            <FavoriteIcon
              tooltip={t("favorite.tooltip")}
              className={styles.favoriteIcon}
              isFavorite={
                memberFavoritesState.idList.indexOf(parseInt(slug)) !== -1
              }
              onClick={(e) => {
                e.stopPropagation();
                e.preventDefault();
                memberFavoritesState.handleFavorite(parseInt(slug));
              }}
              isLogin={isLogin}
            />
            <div>
              <Registrations registration={member.registration} t={t} />
            </div>
            <h4 className={partnerPreviewStyles.titleEng}>
              {member.name_en} {member.lastname_en}
            </h4>
            <h1 className={partnerPreviewStyles.titleZh}>{member.name_zh}</h1>
            <div className={partnerPreviewStyles.establish}>
              {`${sourceItems.mechanism[member.unit]}`}
              {member.year &&
                member.unit !== "4" &&
                ` ${t("professionals.establish")}：${member.year}`}
            </div>
            <div style={{ flex: 1 }}></div>
            <div className={styles.hopeSphere}>
              {member.hopeSphere?.map?.((h) => (
                <div key={h}>{sourceItems.sector[h]}</div>
              ))}
            </div>
            <div className={styles.industry}>
              {member.industry?.map?.((h) => (
                <div key={h}>{sourceItems.industry[h]}</div>
              ))}
            </div>
          </div>
          <div className={styles.socialContainer}>
            {socialData.length !== 0 && (
              <>
                <div className={styles.socialGroup}>
                  {socialData.map((s) => (
                    <a
                      href={s.link}
                      target="_blank"
                      rel="noreferrer"
                      key={s.link}
                    >
                      {s.icon}
                    </a>
                  ))}
                </div>
                <div className={styles.hr}></div>
              </>
            )}
            {linksData.length !== 0 && (
              <>
                <div className={styles.linkGroup}>
                  {linksData.map((s) => (
                    <div key={s.link}>
                      <div className={styles.icon} style={{ marginTop: s.mt }}>
                        {s.icon}
                      </div>
                      <div
                        className={classNames(styles.linkContent, {
                          [styles.lockedContent]: s.locked,
                        })}
                      >
                        {s.content}
                        {s.locked && (
                          <Tooltip title={t("memberOnly")} placement="right">
                            <IconLocked className={styles.lockIcon} />
                          </Tooltip>
                        )}
                      </div>
                    </div>
                  ))}
                </div>
              </>
            )}
          </div>
          <div className={styles.contentContainer}>
            {locale === "en"
              ? member.intro_en
              : member.intro_zh || member.intro_en}
          </div>
        </div>
      </div>
      {/* remove member section when member unit is "4" */}
      {member.unit !== "4" && member.unitt1.length > 0 && (
        <div>
          <div className={classNames(styles.title, styles.sectionContainer)}>
            {t("professionals.members")}
          </div>

          <Swiper
            className={styles.memberList}
            slidesPerView={"auto"}
            spaceBetween={isMobile ? 30 : 50}
            slidesOffsetAfter={isMobile ? 18 : marginH}
            slidesOffsetBefore={isMobile ? 18 : marginH}
            updateOnWindowResize={true}
          >
            {member.unitt1
              ?.map((u) => u.unit)
              .map((u) => (
                <SwiperSlide key={u.id} className={styles.memberCardContainer}>
                  <Link
                    legacyBehavior
                    href={`/professionals/${u.id}/${u.name_en}`}
                  >
                    <a className={styles.memberCard}>
                      <div className={styles.memberImgContainer}>
                        <img src={u.pic_min} alt="icon" />
                      </div>
                      <div>
                        <div className={styles.memberRegistration}>
                          <Registrations registration={u.registration} t={t} />
                        </div>
                        <div className={styles.memberName}>
                          <span>{u.name_en}</span>
                          <span>{u.lastname_en}</span>
                          <span>{u.name_zh}</span>
                        </div>
                      </div>
                      <FavoriteIcon
                        tooltip={t("favorite.tooltip")}
                        className={styles.favoriteIcon}
                        isFavorite={
                          memberFavoritesState.idList.indexOf(u.id) !== -1
                        }
                        onClick={(e) => {
                          e.stopPropagation();
                          e.preventDefault();
                          memberFavoritesState.handleFavorite(u.id);
                        }}
                        isLogin={isLogin}
                      />
                    </a>
                  </Link>
                </SwiperSlide>
              ))}
          </Swiper>
        </div>
      )}

      {(() => {
        const unitt2List = member.unitt2
          .map((u) => u.work)
          .filter((e) => !Array.isArray(e));

        return (
          unitt2List.length > 0 && (
            <div className={styles.memberList}>
              <div
                className={classNames(styles.title, styles.sectionContainer)}
              >
                {`${t("professionals.works")}`}
              </div>
              <WorkCardList
                works={unitt2List}
                sourceItems={sourceItems}
                favoriteIds={workFavoritesState.idList}
                onFavorite={workFavoritesState.handleFavorite}
                favoriteText={t("favorite.tooltip")}
              />
            </div>
          )
        );
      })()}
      <Footer />
    </>
  );
}

export async function getServerSideProps(context) {
  const slug = context.query.slug;
  const preview = context.query.preview;

  const locale = context.locale;
  const { authKey, memberId, isLogin } = getLoginData(context);
  const memberReq = getMemberV3({
    locale,
    id: slug,
    auth_key: authKey,
    memberm1_no: memberId,
    ispublic: preview ? 0 : 1,
  });

  const sourceItemsReq = getSourceItemV3({ locale });

  const memberFavoritesReq = isLogin
    ? collectmIndex({
        auth_key: authKey,
        memberm1_no: memberId,
        ispages: 0,
        type: 1,
        lang: locale,
      }).then((d) => {
        if (d.resu === 1) {
          return d.data;
        }
        return Promise.reject(d);
      })
    : Promise.resolve([]);

  const workFavoritesReq = isLogin
    ? collectmIndex({
        auth_key: authKey,
        memberm1_no: memberId,
        ispages: 0,
        type: 2,
        lang: locale,
      }).then((d) => {
        if (d.resu === 1) {
          return d.data;
        }
        return Promise.reject(d);
      })
    : Promise.resolve([]);

  const [sourceItems, memberRes, memberFavorites, workFavorites] =
    await Promise.all([
      sourceItemsReq,
      memberReq,
      memberFavoritesReq,
      workFavoritesReq,
    ]);

  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
      member: memberRes[0],
      sourceItems: sourceItems.data,
      preview: preview || "",
      memberFavorites,
      workFavorites,
      isLogin,
      authKey,
      memberId,
      slug,
    },
  };
}
