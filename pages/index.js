import Head from "next/head";
import styles from "../styles/Home.module.scss";
import Link from "next/link";
import SVGScroll from "@/svgs/scroll.svg";
import classNames from "classnames";
import { Footer } from "@/components/Footer/Footer";
import { useMeasure } from "react-use";
import { CommonSwiperList } from "@/components/CommonSwiperList/CommonSwiperList";
import { WorkFavCard } from "@/components/WorkCard/WorkCard";
import { auth } from "@/utilits/auth";
import {
  getBanner,
  memberDataCount,
  themeIndex,
  getMemberWork,
  planIndex,
  getArticlev3,
  getArticle001Hotv3,
  sourceItem,
} from "@/api";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useCallback, useRef, useState, useContext } from "react";
import { useRouter } from "next/router";

import { getFullRangeText } from "@/utilits/handleDate";
import { useRWD } from "@/hooks/useRwd";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import animateScrollTo from "animated-scroll-to";
import { ProgramsCard } from "@/components/ProgramsCard/ProgramsCard";
import { LoadingContext } from "@/context/LoadingContext";
import { CardContent } from "@/components/CardContent/CardContent";
import { parseCookies } from "@/utilits/parseCookies";
import CTAContainer from "@/components/CTAContainer/CTAContainer";
import HeroBanner from "@/components/HeroBanner";

export default function Home({
  authKey,
  memberId,
  isLogin,
  banner,
  memberDataCountData,
  themeIndexList,
  futureList,
  collectmDefaultMapping,
  newsList,
  resourceList,
  formItems,
}) {
  const [collectmMapping, setCollectmMapping] = useState(
    collectmDefaultMapping
  );
  const { t } = useTranslation("common");
  const [ref, sizeInfo] = useMeasure();
  const router = useRouter();
  const rwd = useRWD();
  const { locale } = useRouter();
  const { setShowLoading } = useContext(LoadingContext);
  const [currentNewsTab, setCurrentNewsTab] = useState(1);
  const [newsMappingList, setNewsMappingList] = useState({
    1: newsList,
    2: [],
    3: [],
    4: [],
  });

  const handleNewsList = async (subCateid) => {
    if (newsMappingList[subCateid].length > 0) {
      setCurrentNewsTab(subCateid);
    } else {
      setShowLoading(true);

      let resResu = 0;
      let resData = {};
      if (subCateid === 4) {
        const { resu, data } = await planIndex({
          topmost: 1,
          returnAmount: 3,
          ispages: 0,
          page: 1,
          lang: locale,
        });
        resResu = resu;
        resData = data;
      } else {
        const { resu, data } = await getArticlev3({
          topmost: 1,
          returnAmount: 3,
          ispages: 0,
          page: 1,
          locale,
          subCateid,
        });
        resResu = resu;
        resData = data;
      }

      if (resResu === 1) {
        setNewsMappingList((prev) => {
          return {
            ...prev,
            [subCateid]: resData,
          };
        });
        setCurrentNewsTab(subCateid);
      }
      setShowLoading(false);
    }
  };

  const handleBannerClickMore = useCallback(
    (item) => {
      if (item.url) {
        if (item.target === "1") {
          window.open(item.url);
        } else {
          router.push(item.url);
        }
      }
    },
    [router]
  );
  const videoSectionRef = useRef();

  const handleScrollToVideoSection = useCallback(() => {
    animateScrollTo(videoSectionRef.current, {
      verticalOffset: rwd.device === "desktop" ? -100 : -20,
    });
  }, [rwd.device]);

  return (
    <>
      <Head>
        <title>{t("seo.index.title")}</title>
        <meta name="description" content={t("seo.index.desc")} />
      </Head>
      <div className={classNames("canvasHeight", styles.bgContainer)}>
        <div ref={ref} className={styles.banner}>
          <HeroBanner data={banner} onItemClick={handleBannerClickMore} />
          <SVGScroll
            className={classNames(styles.scrollIcon, "desktop-only")}
          />
        </div>

        {/* <div className={styles.countContainer}>
          <div className={classNames(styles.count, styles.count1)}>
            <div className={styles.group}>
              <div className={styles.num}>
                {memberDataCountData.work_ct}
                {locale === "zh" && t("index.count1.piece")}
              </div>
              <div className={styles.name}>{t("index.count1.name")}</div>
            </div>
            <Link href="/content">
              <a className={styles.btn}>
                <FancyButton>{t("index.count1.review")}</FancyButton>
              </a>
            </Link>
          </div>

          <div className={classNames(styles.count, styles.count2)}>
            <div className={styles.group}>
              <div className={styles.num}>
                {memberDataCountData.unit_ct}
                {locale === "zh" && t("index.count2.piece")}
              </div>
              <div className={styles.name}>{t("index.count2.name")}</div>
            </div>
            <Link href="/professionals">
              <a className={styles.btn}>
                <FancyButton>{t("index.count2.review")}</FancyButton>
              </a>
            </Link>
          </div>
        </div> */}
      </div>

      {/* <div className={styles.themeContainer}>
        <ParallaxBanner
          layers={[
            {
              image: "/images/headerBG.png",
              amount: 0.2,
            },
          ]}
          style={{
            height: "auto",
          }}
        >
          <div className={styles.content}>
            <div className="contentContainer">
              <div className={classNames(styles.mainTitle, styles.title)}>
                {t("index.selections.title")}
              </div>
              <div className={styles.cardList}>
                {themeIndexList.map((e) => (
                  <SelectionsCard key={e.id} data={e} formItems={formItems} />
                ))}
              </div>

              <Link href="/selections">
                <a className={styles.btn}>
                  <FancyButton>{t("index.artTags.btn")}</FancyButton>
                </a>
              </Link>
            </div>
          </div>
        </ParallaxBanner>
      </div> */}

      <div className={styles.mainContainer}>
        <div className={styles.artTypeContainer}>
          <div className="contentContainer">
            <div className={styles.titleGroup}>
              <h1 className={classNames(styles.mainTitle, styles.title)}>
                {t("index.artType.title")}
              </h1>
              <div className={styles.tagList}>
                {Object.keys(formItems.artType)
                  .filter((e) => e !== "other")
                  .map((e) => (
                    <Link legacyBehavior href={`/content?gclass=${e}`} key={e}>
                      <a className={styles.tag}>{formItems.artType[e]}</a>
                    </Link>
                  ))}
              </div>
            </div>
          </div>

          <CommonSwiperList
            showFull={true}
            slidesPerView={1.2}
            spaceBetween={30}
            breakpoints={{
              960: {
                slidesPerView: 3,
                slidesPerGroup: 4,
                spaceBetween: "3.1%",
              },
            }}
            dataList={futureList}
            Element={({ data }) =>
              WorkFavCard({
                data,
                collectmMapping,
                setCollectmMapping,
                formItems,
                isLogin,
                authKey,
                memberId,
              })
            }
          />
        </div>

        {/* <div className={styles.artTagsContainer}>
          <div
            className={classNames("contentContainer", styles.contentContainer)}
          >
            <div className={styles.artTagsContent}>
              <div className={styles.titleGroup}>
                <div className={styles.title2}>{t("index.artTags.title2")}</div>
                <div className={classNames(styles.mainTitle, styles.title)}>
                  {t("index.artTags.title1")}
                </div>
              </div>

              {Object.keys(formItems.artTags)
                .filter((e) => e !== "other")
                .map((e) => (
                  <Link href={`/content?gtheme=${e}`} key={e}>
                    <a className={styles.tag}>
                      <div className={styles.icon}>#</div>
                      <div className={classNames("enLH", styles.name)}>{formItems.artTags[e]}</div>
                    </a>
                  </Link>
                ))}

              <Link href="/content">
                <a className={styles.btn}>
                  <FancyButton>{t("index.artTags.btn")}</FancyButton>
                </a>
              </Link>
            </div>
          </div>

          <ParallaxBanner
            layers={[
              {
                image: "/images/headerBG.png",
                amount: 0.2,
              },
            ]}
            style={{
              position: "absolute",
              bottom: 0,
              height: rwd.device === "mobile" ? 420 : calcWeb(450),
            }}
          ></ParallaxBanner>
        </div> */}
      </div>

      <div className={styles.partnerContainer}>
        <div className={styles.content}>
          <div className={classNames(styles.mainTitle, styles.title)}>
            {t("index.partner.title")}
          </div>
          <div className={styles.desc}>{t("index.partner.desc")}</div>
          <Link legacyBehavior href="/professionals">
            <a className={styles.btn}>
              <FancyButton>{t("index.partner.btn")}</FancyButton>
            </a>
          </Link>
        </div>
      </div>

      <div className={styles.listContainer}>
        <div className="contentContainer">
          <div className={styles.listBg}>
            <div className={styles.titleGroup}>
              <div className={classNames(styles.mainTitle, styles.title)}>
                {t("meun.news")}
              </div>

              <div className={styles.btnGroup}>
                <div
                  className={classNames(styles.btn, {
                    [styles.selected]: currentNewsTab === 1,
                  })}
                  onClick={() => handleNewsList(1)}
                >
                  <FancyButton>{t("news.tab1")}</FancyButton>
                </div>
                <div
                  className={classNames(styles.btn, {
                    [styles.selected]: currentNewsTab === 4,
                  })}
                  onClick={() => handleNewsList(4)}
                >
                  <FancyButton>{t("news.tab4")}</FancyButton>
                </div>
                <div
                  className={classNames(styles.btn, {
                    [styles.selected]: currentNewsTab === 2,
                  })}
                  onClick={() => handleNewsList(2)}
                >
                  <FancyButton>{t("news.tab2")}</FancyButton>
                </div>
                <div
                  className={classNames(styles.btn, {
                    [styles.selected]: currentNewsTab === 3,
                  })}
                  onClick={() => handleNewsList(3)}
                >
                  <FancyButton>{t("news.tab3")}</FancyButton>
                </div>
              </div>
            </div>

            <div className={styles.list}>
              {newsMappingList[currentNewsTab].map((a) => {
                if (currentNewsTab === 4) {
                  return (
                    <ProgramsCard key={a.id} data={a} formItems={formItems} />
                  );
                } else {
                  return (
                    <CardContent
                      type="newsBig"
                      key={a.id}
                      title={a.title}
                      image={[a.pic, "/images/card_default.png"]}
                      slug={a.slug}
                      rangeText={getFullRangeText(a)}
                      category={a.subCateid === "2" ? "events" : "news"}
                      clamp={rwd.device === "mobile" ? 2 : 3}
                      subCate={t(`news.tab${a.subCateid}`)}
                    />
                  );
                }
              })}
            </div>

            <Link legacyBehavior href="/news">
              <a className={styles.more}>
                <FancyButton>{t("index.more")}</FancyButton>
              </a>
            </Link>
          </div>

          <div className={styles.listBg}>
            <div className={classNames(styles.mainTitle, styles.title)}>
              {t("meun.resources")}
            </div>

            <div className={styles.list}>
              {resourceList.map((a) => (
                <CardContent
                  type="newsBig"
                  key={a.id}
                  title={a.title}
                  image={[a.pic, "/images/card_default.png"]}
                  slug={a.slug}
                  rangeText={getFullRangeText(a)}
                  category="resources"
                  clamp={rwd.device === "mobile" ? 2 : 3}
                  subCate={t(`resources.tab${a.subCateid}`)}
                />
              ))}
              <Link legacyBehavior href="/industrylinks">
                <a className={styles.industryCard}>
                  <div className={styles.industryTitle}>
                    {t("index.industry.title")}
                  </div>
                  <div className={styles.desc}>{t("index.industry.desc")}</div>
                  <div className={styles.view}>{t("index.industry.view")}</div>
                </a>
              </Link>
            </div>

            <Link legacyBehavior href="/resources">
              <a className={styles.more}>
                <FancyButton>{t("index.more")}</FancyButton>
              </a>
            </Link>
          </div>
        </div>
      </div>

      <div className="homePage">
        <CTAContainer
          desc1={t("index.cta.desc1")}
          desc2={t("index.cta.desc2")}
          // href={"/register"}
          href={"/why"}
          ctaText={t("index.cta.btn")}
        />
      </div>

      <Footer />
    </>
  );
}
Home.headerProps = {};
export async function getServerSideProps(context) {
  const locale = context.locale;
  const cookies = context.req.cookies;
  const { isLogin } = auth(cookies);

  let authKey = "";
  let memberId = "";

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  const handleMemberDataCount = async () => {
    const memberDataCountData = {
      work_ct: 0,
      unit_ct: 0,
    };
    const { resu, data } = await memberDataCount();
    if (resu === 1) {
      memberDataCountData.work_ct = data.work_ct;
      memberDataCountData.unit_ct = data.unit_ct;
    }
    return memberDataCountData;
  };

  const handleThemeIndex = async () => {
    let themeIndexList = [];
    const { resu, data } = await themeIndex({
      topmost: 1,
      returnAmount: 3,
      ispages: 0,
      lang: locale,
    });
    if (resu === 1) {
      themeIndexList = data;
    }
    return themeIndexList;
  };

  const handleMemberWork = async () => {
    let futureList = [];
    let collectmDefaultMapping = {};
    const { resu, data, collectm } = await getMemberWork({
      locale,
      topmost: 1,
      returnAmount: 15,
      ispages: 0,
      ispublic: 1,
      memberm1_no: memberId,
      auth_key: authKey,
    });

    if (resu === 1) {
      futureList = data;
      collectmDefaultMapping = collectm;
    }

    return {
      futureList,
      collectmDefaultMapping,
    };
  };

  const handleGetArticlev3 = async () => {
    let newsList = [];
    const { resu, data } = await getArticlev3({
      topmost: 1,
      returnAmount: 3,
      ispages: 0,
      page: 1,
      locale,
      subCateid: 1,
    });
    if (resu === 1) {
      newsList = data;
    }
    return newsList;
  };

  const handleGetArticle001Hotv3 = async () => {
    let resourceList = [];
    const { resu, data } = await getArticle001Hotv3({
      locale,
    });
    if (resu === 1) {
      resourceList = data;
    }
    return resourceList;
  };

  const handleSourceItem = async () => {
    let formItems = {};
    const { resu, data } = await sourceItem({
      locale: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const [
    bannerRes,
    memberDataCountData,
    themeIndexList,
    { futureList, collectmDefaultMapping },
    newsList,
    resourceList,
    formItems,
  ] = await Promise.all([
    getBanner({ locale }),
    handleMemberDataCount(),
    handleThemeIndex(),
    handleMemberWork(),
    handleGetArticlev3(),
    handleGetArticle001Hotv3(),
    handleSourceItem(),
  ]);

  const banner = (bannerRes?.data || []).map((d) => ({
    titleZh: d.subtitle_zh,
    titleEn: d.subtitle_en,
    image: d.pic,
    imageM: d.pic_min,
    target: d.target,
    url: d.url,
  }));

  return {
    props: {
      authKey,
      memberId,
      isLogin,
      banner: banner,
      memberDataCountData,
      themeIndexList,
      futureList,
      collectmDefaultMapping,
      newsList,
      resourceList,
      formItems,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
