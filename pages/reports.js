import Head from "next/head";
import { Header } from "@/components/Header/Header";
import HeaderBanner from "@/components/HeaderBanner/HeaderBanner";
import PageInfo from "@/components/PageInfo/PageInfo";
import React, { useCallback, useState } from "react";
import dynamic from "next/dynamic";
import styles from "../styles/Reports.module.scss";

import { NextSection } from "@/components/NextSection/NextSection";
import { Footer } from "@/components/Footer/Footer";
import classNames from "classnames";
import { ScrollBoxH } from "@/components/ScrollBoxH/ScrollBoxH";
import { Card } from "@/components/Card/Card";
import { CardContent } from "@/components/CardContent/CardContent";
import { LoadMoreGroup } from "@/components/LoadMoreGroup/LoadMoreGroup";
import { Join } from "@/components/Join/Join";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from 'next-i18next';
import { getArticales } from "@/api";
import { useAsyncFn } from "react-use";
import { getShortDateText } from "@/utilits/handleDate";
import { useRouter } from "next/router";
import { useLoadMoreApi } from "@/utilits/useLoadMoreApi";
import { useRWD } from "@/hooks/useRwd";
import { Loading } from "@/components/Loading/Loading";

export default function Reports({
    reportsNewRes,
    reportAllRes,
    pageSize,
    subCateid,
}) {
    const { t } = useTranslation("common");
    // console.log(reportsNewRes, reportAllRes);

    const loadMoreState = useLoadMoreApi({
        initAllPage: reportAllRes.pages.totalPages,
        initPage: 1,
        initData: reportAllRes.data,
        pageSize: pageSize,
        subCateid: subCateid,
    });

    const rwd = useRWD();

    return (
        <>
            <Head>
                <title>{t("seo.report.title")}</title>
                <meta name="description" content={t("seo.report.desc")} />
            </Head>
            <HeaderBanner
                title={t("reports.headline")}
                numberText={"04"}
                image={"/images/bannerBG04.png"}
            />
            <PageInfo
                title={t("reports.headline")}
                navTitle={t("reports.headline")}
                desc={t("reports.introduction")}
            />

            <section className={styles.focus}>
                <h4>{t("reports.tittle1")}</h4>
                <ScrollBoxH
                    className={styles.scrollboxOuter}
                    innerClassName={styles.scrollboxInner}
                >
                    {reportsNewRes.data.map((a) => (
                        <Card
                            key={a.id}
                            title={a.title}
                            image={a.pic}
                            state={a.status}
                            rangeText={getShortDateText(a)}
                            square
                            year={a.date.split("-")[0]}
                            slug={a.slug}
                            category="reports"
                            clamp={rwd.device === "mobile" ? 3 : 3}
                        />
                    ))}
                    {/* {Array(5)
            .fill()
            .map((_, index) => (
              <Card square key={index} />
            ))} */}
                </ScrollBoxH>
            </section>
            <div className={classNames("contentContainer", styles.contentContainer)}>
                <div className={styles.sectionHeader}>
                    <h4>{t("reports.tittle2")}</h4>
                </div>
                <div className={styles.table}>
                    {/* {Array(8)
            .fill()
            .map((_, i) => (
              <CardContent status="進行中" type="reportSmall" key={i} />
            ))} */}
                    {loadMoreState.data.map((a, i) => {
                        return (
                            <CardContent
                                status={a.status}
                                title={a.title}
                                type="reportSmall"
                                key={i}
                                image={a.pic}
                                slug={a.slug}
                                category="reports"
                            />
                        );
                    })}
                </div>
                {loadMoreState.isLoading && (
                    <div style={{ textAlign: "center", marginBottom: 30 }}>
                        <Loading />
                    </div>
                )}
                {loadMoreState.showLoadMore && (
                    <LoadMoreGroup
                        actionText={t("reports.showmore.button")}
                        leftText={t("event.showmore")}
                        className={styles.loadMoreGroup}
                        onLoadmore={loadMoreState.handleNext}
                    />
                )}
            </div>
            {/* <Join /> */}

            <NextSection
                title={t("meun.resources")}
                prefix={"05."}
                path="/resources"
                image={"/images/bannerBG06.png"}
            />
            <Footer />
        </>
    );
}
Reports.headerProps = {};
export async function getServerSideProps(context) {
    const locale = context.locale;
    const pageSize = 6;
    const subCateid = "002";
    const [reportsNewRes, reportAllRes] = await Promise.all([
        getArticales({
            subCateid: subCateid,
            locale,
            ispages: 0,
            returnAmount: 1,
        }),
        getArticales({
            subCateid: subCateid,
            locale,
            page: 1,
            pagesize: pageSize,
            ispages: 1,
        }),
    ]);
    return {
        props: {
            reportsNewRes,
            reportAllRes,
            pageSize,
            subCateid,
            ...(await serverSideTranslations(context.locale, ["common"])),
        }, // will be passed to the page component as props
    };
}
