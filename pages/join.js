import Head from "next/head";
import { PageInfoBasic } from "@/components/PageInfo/PageInfo";
import React from "react";

import styles from "../styles/Join.module.scss";
import { Footer } from "@/components/Footer/Footer";

import SVGJoinBig from "@/svgs/joinBig.svg";
import SVGStep1 from "@/svgs/step1.svg";
import SVGStep2 from "@/svgs/step2.svg";
import SVGStep3 from "@/svgs/step3.svg";
import SVGStep4 from "@/svgs/step4.svg";
import SVGArrowRight from "@/svgs/ArrowRight.svg";
import classNames from "classnames";
import { useRouter } from "next/router";
import { HeaderBannerLight } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { Trans, useTranslation } from 'next-i18next';
import Link from "next/link";
import { TLink } from "@/components/TLink";
import { FancyButton } from "@/components/FancyButton/FancyButton";

const StepWraper = ({ children, text }) => {
    return (
        <div className={styles.stepWraper}>
            {children}
            <div>{text}</div>
        </div>
    );
};

export default function Join() {
    const router = useRouter();
    const { locale } = router;
    const { t } = useTranslation("common");

    return (
        <>
            <Head>
                <title>{t("seo.join.title")}</title>
                <meta name="description" content={t("seo.join.desc")} />
            </Head>
            <HeaderBannerLight title={t("join.headline")} numberText="01" />
            <PageInfoBasic />

            <div className={styles.contentContainer}></div>
            {/* <Join showBg /> */}
            <section className={styles.section1}>
                <div>
                    <SVGJoinBig className={styles.joinBig} />
                </div>
                <div>
                    <h3>{t("join.looking")}</h3>
                    <div className={styles.desc}>{t("join.looking.content")}</div>
                </div>
            </section>
            <section className={styles.section2}>
                <h3>{t("join.form.introduction.tittle")}</h3>
                {locale == "zh" && <p>{t("join.form.introduction")}</p>}

                <div className={styles.stepImages}>
                    <StepWraper text={t("join.fill")}>
                        <SVGStep1 className={styles.step} />
                    </StepWraper>

                    <SVGArrowRight className={styles.arrow} />
                    <StepWraper text={t("join.fill.explain")}>
                        <SVGStep2 className={styles.step} />
                    </StepWraper>
                    <SVGArrowRight className={styles.arrow} />
                    <StepWraper text={t("join.fill.confirmation")}>
                        <SVGStep3 className={styles.step} />
                    </StepWraper>
                    <SVGArrowRight className={styles.arrow} />
                    <StepWraper text={t("join.fill.Sendout")}>
                        <SVGStep4 className={styles.step} />
                    </StepWraper>
                </div>
                <div className={styles.desc}>
                    <Trans i18nKey="join.form.introduction.remark">
                        ※ 若您想更近一步了解「未來內容共創圈」可以前往
                        <TLink href={"/about"}> 關於我們</TLink>
                        了解詳細內容。
                    </Trans>
                </div>
                <div className={styles.joinActionContainer}>
                    <div className={styles.joinAction}>
                        <div>{t("join.form.tittle")}</div>

                        <FancyButton
                            onClick={() => {
                                router.push("/joinForm");
                            }}
                            // className={"button"}
                            // {...c.props}
                            scaleSize={36}
                        >
                            {t("join.form.start.button")}
                        </FancyButton>
                    </div>
                </div>
            </section>
            <Footer />
        </>
    );
}
Join.headerProps = {
    // red: true,
    // title: "加入共創圈",
    // numberText: "06",
};
export async function getServerSideProps(context) {
    return {
        props: {
            ...(await serverSideTranslations(context.locale, ["common"])),
        }, // will be passed to the page component as props
    };
}
