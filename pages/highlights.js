import News, { getServerSideProps as getServerSidePropsC } from "@/pages/news";

export default News;

export const getServerSideProps = (context) =>
  getServerSidePropsC({ ...context, subCateid: "3" });