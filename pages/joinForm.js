/* eslint-disable @next/next/no-img-element */
import { PageInfoBasic } from "@/components/PageInfo/PageInfo";
import React, {
  useCallback,
  useMemo,
  useRef,
  useState,
  useEffect,
  useContext,
} from "react";

import styles from "../styles/JoinForm.module.scss";
import { Footer } from "@/components/Footer/Footer";
import {
  Form,
  Input,
  Radio,
  Row,
  Col,
  Checkbox,
  Select,
  Upload,
  InputNumber,
} from "antd";
const { Option } = Select;
const { TextArea } = Input;
import classNames from "classnames";
import { categorys } from "@/data/partners";
import { useRWD } from "@/hooks/useRwd";
import Link from "next/link";

import SVGUpload from "@/svgs/upload.svg";
import SVGQustion from "@/svgs/question.svg";
import SVGWarring from "@/svgs/warring.svg";
import SBGSuccess from "@/svgs/joinSuccess.svg";
import FormItemLabel from "antd/lib/form/FormItemLabel";
import { JoinStep } from "@/components/JoinStep/JoinStep";
import HeaderBanner, {
  HeaderBannerLight,
} from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { Trans, useTranslation } from 'next-i18next';
import { getCountry, getFormItems, getSourceItem, insertMember } from "@/api";
import router, { useRouter } from "next/router";
import { places } from "@/data/places";
import { TLink } from "@/components/TLink";
import { ParallaxContext, useController } from "react-scroll-parallax";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { Loading } from "@/components/Loading/Loading";

const usePreview = () => {
  const [previewImage, setPreviewImage] = useState(null);
  const compressUploadImage = (src, fileName, callback) => {
    const baseWidth = 550;
    const quality = 0.8;
    const mime = "image/jpeg";
    const img = new Image();
    img.src = src;
    img.onload = () => {
      const scale = img.width / baseWidth;
      const height = img.height / scale;
      const canvas = document.createElement("canvas");
      canvas.width = baseWidth;
      canvas.height = height;
      const ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0, baseWidth, height);
      const url = ctx.canvas.toDataURL(mime, quality);
      ctx.canvas.toBlob(
        (blob) => {
          const file = new File([blob], fileName, {
            type: mime,
            lastModified: Date.now(),
          });
          if (callback) callback(file, url);
        },
        mime,
        quality
      );
    };
  };

  const previewUploadImage = (img, callback) => {
    const fileName = img.name;
    const reader = new FileReader();
    reader.addEventListener("load", (event) => {
      compressUploadImage(reader.result, fileName, callback);
    });
    reader.readAsDataURL(img);
  };

  const handleUploadImageChange = (info) => {
    previewUploadImage(info.file.originFileObj, (file, url) => {
      setPreviewImage(url);
      // setUploadImage(file);
    });
  };

  return {
    previewImage,
    handleUploadImageChange,
  };
};

const realmsMockBase = [
  "AVMR",
  "動作捕捉",
  "雲端運算",
  "新媒體藝術",
  "顯示科技",
  "容積擷取",
  "360攝影",
  "投影光雕",
  "聲音設計",
  "5G串流",
  "機械裝置",
  "遊戲引擎",
  "影視遊戲特效",
  "異地共演",
  "互動介面",
  "博物館與藝文展覽",
  "角色貼圖",
  "圖文漫畫",
  "文字創作",
  "新創投資",
  "UI/UX",
  "AI",
  "區塊鏈",
  "自媒體創作",
  "app開發",
  "程式設計",
  "銷售通路",
  "OTT",
  "適地性娛樂(LBE)",
  "策展",
  "其他",
];

const service = [
  "IP與創作媒合",
  "投融資諮詢",
  "講座與工作坊",
  "國內外行銷",
  "國際合製",
  "開發支持方案",
];

const targets = ["B2B", "B2C"];

const PublicRadio = ({ name, checkMode }) => {
  const { t } = useTranslation("common");
  return (
    <Form.Item shouldUpdate={() => checkMode} noStyle>
      {({ getFieldValue }) => {
        const currentValue = getFieldValue(name);
        return (
          <Form.Item
            noStyle
            name={name}
            rules={[{ required: true, message: t("join.required.public") }]}
          >
            <Radio.Group disabled={checkMode} optionType="button">
              {checkMode && currentValue !== "public" ? null : (
                <Radio value={1}>{t("join.fill.contact.public")}</Radio>
              )}

              {checkMode && currentValue !== "private" ? null : (
                <Radio value={0}>{t("join.fill.contact.private")}</Radio>
              )}
            </Radio.Group>
          </Form.Item>
        );
      }}
    </Form.Item>
  );
};

const CheckboxGroup = ({
  name,
  label,
  rules,
  checkMode,
  options,
  required,
  className,
}) => {
  return (
    <Form.Item noStyle shouldUpdate={() => checkMode}>
      {({ getFieldValue }) => {
        const currentValue = getFieldValue(name);
        return (
          <Form.Item
            required={required}
            label={label}
            name={name}
            rules={rules}
            className={className}
          >
            <Checkbox.Group
              disabled={checkMode}
              className={styles.checkboxGroup}
              options={options.filter((o) =>
                !checkMode
                  ? true
                  : currentValue?.findIndex((c) => c === o.value) !== -1 || []
              )}
            />
          </Form.Item>
        );
      }}
    </Form.Item>
  );
};

const parseItems = (obj) => {
  const keys = Object.keys(obj).sort(
    (a, b) => parseInt(a, 10) - parseInt(b, 10)
  );

  return keys.reduce((acc, key) => {
    acc.push({
      label: obj[key],
      value: key,
    });
    return acc;
  }, []);
};

const Form1 = ({ rwd, checkMode, onNext, formItems, country }) => {
  const { locale } = useRouter();
  const { t } = useTranslation("common");
  const preview = usePreview();

  // const realmOptions = realmsMockBase.map((title, id) => ({
  //   label: title,
  //   value: `${id}`,
  // }));

  const realmOptions = parseItems(formItems.sphere);
  const areaOptions = parseItems(formItems.continents_position);
  const categorysOptions = parseItems(formItems.clientele);
  const doubleColProps = useMemo(
    () => ({
      span: rwd.device === "mobile" ? 24 : 12,
    }),
    [rwd.device]
  );
  const formRef = useRef();
  // const handleSubmit = () => {
  //   formRef.current.validateFields().then((values) => {
  //     console.log(values);
  //     onNext(values);
  //   });
  // };

  const handleCliclNext = useCallback(
    (values) => {
      onNext(values);
    },
    [onNext]
  );

  return (
    <Form
      ref={formRef}
      layout="vertical"
      onFinish={handleCliclNext}
      scrollToFirstError={true}
      name="step1"
    >
      <input type="hidden" name="csrf_token" value="no_token" />
      <h3 className={styles.title}>{t("join.fill")}</h3>
      <div className={styles.avaterUploadContainer}>
        <Form.Item>
          <Form.Item
            required
            name="pic"
            style={{
              marginBottom: 0,
            }}
            rules={[
              {
                required: true,
                message: "請選擇個人照片/單位LOGO",
              },
            ]}
            noStyle
          >
            <Upload
              beforeUpload={false}
              listType="picture-card"
              className={"avatarUpload"}
              showUploadList={false}
              onChange={preview.handleUploadImageChange}
              disabled={checkMode}
            >
              <div className={styles.uploadDefaultGroup}>
                <img
                  src={
                    preview.previewImage
                      ? preview.previewImage
                      : "/images/uploadAvatar.png"
                  }
                  alt="avatar"
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                    borderRadius: 10,
                  }}
                />
                {!checkMode && <SVGUpload className={styles.iconUpload} />}
              </div>
            </Upload>
          </Form.Item>
          <FormItemLabel
            prefixCls="ant-form"
            required
            label={t("join.fill.photo")}
          />
          {!checkMode && (
            <div className={styles.qusetion}>
              <SVGQustion />
              {t("join.fill.upload.instructions")}
            </div>
          )}
        </Form.Item>
      </div>

      <Row gutter={40}>
        {locale === "zh" && (
          <Col {...doubleColProps}>
            <Form.Item
              required
              label={t("join.fill.name")}
              name="name_zh"
              rules={[
                {
                  required: true,
                  message: t("join.required", { label: t("join.fill.name") }),
                },
              ]}
            >
              <Input disabled={checkMode} />
            </Form.Item>
          </Col>
        )}

        <Col {...doubleColProps}>
          <Form.Item
            label={t("join.fill.name.en")}
            name="name_en"
            required={locale === "en"}
            rules={
              locale === "en"
                ? [
                    {
                      required: true,
                      message: t("join.required", {
                        label: t("join.fill.name.en"),
                      }),
                    },
                  ]
                : []
            }
          >
            <Input disabled={checkMode} />
          </Form.Item>
        </Col>
      </Row>
      {!checkMode && (
        <Form.Item>
          <div className={styles.notice}>
            <span>{t("join.fill.reminder")}</span>
            {t("join.fill.reminder.instructions")}
          </div>
        </Form.Item>
      )}

      <Row gutter={40}>
        {locale === "zh" && (
          <Col {...doubleColProps}>
            <Form.Item
              required
              label={t("join.fill.about")}
              name="intro_zh"
              rules={[
                {
                  required: true,
                  message: t("join.required", { label: t("join.fill.about") }),
                },
              ]}
            >
              <TextArea disabled={checkMode} showCount maxLength={50} />
            </Form.Item>
          </Col>
        )}
        <Col {...doubleColProps}>
          <Form.Item
            label={t("join.fill.about.en")}
            name="intro_en"
            required={locale === "en"}
            rules={
              locale === "en"
                ? [
                    {
                      required: true,
                      message: t("join.required", {
                        label: t("join.fill.about.en"),
                      }),
                    },
                  ]
                : []
            }
          >
            <TextArea disabled={checkMode} showCount maxLength={200} />
          </Form.Item>
        </Col>
      </Row>

      <CheckboxGroup
        required
        label={t("join.fill.category")}
        name="registration"
        rules={[
          {
            required: true,
            message: t("join.required", {
              label: t("join.fill.category"),
            }),
          },
        ]}
        options={categorysOptions}
        checkMode={checkMode}
      />
      <CheckboxGroup
        required
        label={t("join.fill.businesstype")}
        name="sphere"
        rules={[
          {
            required: true,
            message: t("join.required", {
              label: t("join.fill.businesstype"),
            }),
          },
        ]}
        options={realmOptions}
        checkMode={checkMode}
      />

      <h3 className={styles.title}>{t("join.fill.Service")}</h3>
      <Form.Item label={t("join.fill.Service.link")} name="portfolio1">
        <Input disabled={checkMode} placeholder={"網址連結1 http://..."} />
      </Form.Item>
      <Form.Item name="portfolio2">
        <Input disabled={checkMode} placeholder={"網址連結2 http://..."} />
      </Form.Item>
      <CheckboxGroup
        required
        label={t("join.fill.application")}
        name="application"
        rules={[
          {
            required: true,
            message: t("join.required", {
              label: t("join.fill.application"),
            }),
          },
        ]}
        options={realmOptions}
        checkMode={checkMode}
      />
      <Form.Item
        name="goals"
        label={t("join.fill.goals")}
        rules={[
          {
            required: true,
            message: t("join.required", {
              label: t("join.fill.goals"),
            }),
          },
        ]}
      >
        <Input disabled={checkMode} placeholder={"..."} />
      </Form.Item>
      <CheckboxGroup
        required
        label={t("join.fill.exismarket")}
        name="exismarket"
        rules={[
          {
            required: true,
            message: t("join.required", {
              label: t("join.fill.exismarket"),
            }),
          },
        ]}
        options={areaOptions}
        checkMode={checkMode}
      />
      <CheckboxGroup
        required
        label={t("join.fill.futuremarket")}
        name="futuremarket"
        rules={[
          {
            required: true,
            message: t("join.required", {
              label: t("join.fill.futuremarket"),
            }),
          },
        ]}
        options={areaOptions}
        checkMode={checkMode}
      />
      <h3 className={styles.title}>{t("join.fill.contact")}</h3>

      <Form.Item
        required
        label={t("join.fill.contact.email")}
        className={styles.formItemBlock1}
      >
        <div className={styles.inputWithRadioGroup}>
          <Form.Item
            required
            name="email"
            noStyle
            rules={[
              {
                required: true,
                message: t("join.required", {
                  label: t("join.fill.contact.email"),
                }),
              },
              { type: "email" },
            ]}
          >
            <Input disabled={checkMode} placeholder={"email add..."} />
          </Form.Item>
          <PublicRadio name="emailPublish" checkMode={checkMode} />
        </div>
      </Form.Item>
      <Form.Item
        required
        label={t("join.fill.contact.number")}
        className={styles.formItemBlock1}
      >
        <div className={styles.inputWithRadioGroup}>
          <Input.Group compact className={styles.inputGroupConoact}>
            <Form.Item
              required
              noStyle
              name="telcode"
              rules={[
                {
                  required: true,
                  message: t("join.required", {
                    label: "區碼",
                  }),
                },
              ]}
            >
              <Select disabled={checkMode} placeholder="區域" style={{ minWidth: 160 }} showSearch>
              {country.map((c) => (
                  <Option key={c.name} value={c.phoneCode}>
                    {locale === "en" ? `${c.name} ${c.phoneCode}` : `${c.traditionalName} ${c.phoneCode}`}
                  </Option>
                ))}
              </Select>
              {/* <Input style={{ minWidth: 100 }} placeholder="區碼" /> */}
            </Form.Item>
            <Form.Item
              required
              noStyle
              name="tel"
              rules={[
                {
                  required: true,
                  message: t("join.required", {
                    label: t("join.fill.contact.number"),
                  }),
                },
                // { type: "number" },
              ]}
            >
              {/* <InputNumber
                style={{ width: "100%" }}
                disabled={checkMode}
                placeholder={"請輸入電話號碼"}
              /> */}
              <Input />
            </Form.Item>
          </Input.Group>
          <PublicRadio name="telPublish" checkMode={checkMode} />
        </div>
      </Form.Item>

      <Row gutter={40}>
        <Col {...doubleColProps}>
          <Form.Item label={t("join.fill.contact.socialmedia")} name="facebook">
            <Input disabled={checkMode} placeholder={"Facebook"} />
          </Form.Item>
        </Col>
        <Col {...doubleColProps}>
          <Form.Item label={rwd.device === "mobile" ? "" : " "} name="twitter">
            <Input disabled={checkMode} placeholder={"Twitter"} />
          </Form.Item>
        </Col>
        <Col {...doubleColProps}>
          <Form.Item name="linkedin" className={styles.formItemBlock1}>
            <Input disabled={checkMode} placeholder={"Linkedin"} />
          </Form.Item>
        </Col>
      </Row>

      <Form.Item label={t("join.fill.contact.address")} required>
        <div className={styles.inputWithRadioGroup}>
          <Input.Group compact className={styles.inputGroupConoact}>
            <Form.Item
              noStyle
              name="addressCountry"
              rules={[
                {
                  required: true,
                  message: t("join.required", {
                    label: t("join.fill.nation"),
                  }),
                },
              ]}
            >
              <Select
                placeholder={t("join.fill.nation")}
                disabled={checkMode}
                style={{ minWidth: 180 }}
                showSearch
              >
                {country.map((c) => (
                  <Option key={c.name} value={c.name}>
                    {locale === "en" ? c.name : c.traditionalName}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              noStyle
              name="address_zh"
              rules={[
                {
                  required: true,
                  message: t("join.required", {
                    label: t("join.fill.contact.address"),
                  }),
                },
              ]}
            >
              <Input
                disabled={checkMode}
                placeholder={t("join.fill.contact.address")}
              />
            </Form.Item>
          </Input.Group>
          {/* <PublicRadio name="address_public" checkMode={checkMode} /> */}
        </div>
        {!checkMode && (
          <div
            style={{ marginTop: 10, marginBottom: 5 }}
            className={styles.qusetion}
          >
            <SVGQustion />
            {t("join.fill.contact.address.remark")}
          </div>
        )}
      </Form.Item>
      <Form.Item
        name="year"
        label={t("join.fill.founded")}
        rules={[
          {
            required: true,
            message: t("join.required", {
              label: t("join.fill.founded"),
            }),
          },
        ]}
      >
        <InputNumber
          disabled={checkMode}
          placeholder={t("join.fill.year")}
          min={1000}
          max={new Date().getFullYear()}
          style={{ width: "100%" }}
        />
      </Form.Item>

      {!checkMode && (
        <Row gutter={40} className={styles.actionButtonGroup}>
          <Col {...doubleColProps}>
            <FancyButton
              scaleSize={38}
              className={classNames(styles.actionButton)}
              onClick={(e) => {
                e.preventDefault();
                router.back();
              }}
            >
              {t("join.previous")}
            </FancyButton>
          </Col>
          <Col {...doubleColProps}>
            <FancyButton
              // onClick={handleSubmit}
              htmlType="submit"
              scaleSize={38}
              className={classNames(styles.actionButton)}
            >
              {t("join.next")}
            </FancyButton>
          </Col>
        </Row>
      )}
    </Form>
  );
};

const Form2 = ({ rwd, checkMode, onNext, onPrev, formItems }) => {
  const { t } = useTranslation("common");
  // const realmOptions = realmsMockBase.map((title, id) => ({
  //   label: title,
  //   value: `${id}`,
  // }));

  const realmOptions = parseItems(formItems.sphere);
  const servoceOptopns = parseItems(formItems.otherservice);
  const targetsOptions = parseItems(formItems.unittype);
  const categorysOptions = parseItems(formItems.clientele);
  const doubleColProps = useMemo(
    () => ({
      span: rwd.device === "mobile" ? 24 : 12,
    }),
    [rwd.device]
  );

  const handleCliclNext = useCallback(
    (values) => {
      onNext(values);
    },
    [onNext]
  );

  return (
    <Form
      name="step2"
      layout="vertical"
      onFinish={handleCliclNext}
      scrollToFirstError
    >
      <input type="hidden" name="csrf_token" value="no_token" />
      <h3 className={styles.title}>{t("join.fill.explain")}</h3>
      {!checkMode && (
        <div className={classNames(styles.notice, styles.noticeCheck)}>
          <span>{t("join.fill.explain.reminder")}</span>
          {t("join.fill.explain.reminder.instructions")}
        </div>
      )}

      <CheckboxGroup
        required
        label={t("join.fill.explain.target")}
        name="hopeSphere"
        options={targetsOptions}
        checkMode={checkMode}
        rules={[{ required: true, message: "請選擇投入領域/單位業務類別" }]}
        className={styles.formItemBlock}
      />

      <CheckboxGroup
        label={t("join.fill.explain.category")}
        name="hopeObject"
        options={categorysOptions}
        checkMode={checkMode}
        className={styles.formItemBlock}
      />

      <CheckboxGroup
        label={t("join.fill.explain.businesstype")}
        name="hopeBusiness"
        options={realmOptions}
        checkMode={checkMode}
        className={styles.formItemBlock}
      />

      <CheckboxGroup
        label={t("join.fill.explain.otherservice")}
        name="hopeOther"
        options={servoceOptopns}
        checkMode={checkMode}
        className={styles.formItemBlock}
      />

      {!checkMode && (
        <Row gutter={40} className={styles.actionButtonGroup}>
          <Col {...doubleColProps}>
            <FancyButton
              onClick={onPrev}
              scaleSize={38}
              className={classNames(styles.actionButton)}
            >
              {t("join.previous")}
            </FancyButton>
          </Col>
          <Col {...doubleColProps}>
            <FancyButton
              // onClick={onNext}
              htmlType="submit"
              scaleSize={38}
              className={classNames(styles.actionButton)}
            >
              {t("join.next")}
            </FancyButton>
          </Col>
        </Row>
      )}
    </Form>
  );
};

const Form3 = ({ rwd, onNext, onPrev }) => {
  const { t } = useTranslation("common");
  const doubleColProps = useMemo(
    () => ({
      span: rwd.device === "mobile" ? 24 : 12,
    }),
    [rwd.device]
  );

  const handleCliclNext = useCallback(
    (values) => {
      onNext(values);
    },
    [onNext]
  );

  return (
    <Form
      name="step3"
      layout="vertical"
      onFinish={handleCliclNext}
      scrollToFirstError
    >
      <input type="hidden" name="csrf_token" value="no_token" />
      <div className={styles.checkForm}>
        <hr />
        <Form.Item
          name="agreeDisclaimer"
          valuePropName="checked"
          rules={[
            {
              required: true,
              message: t("join.fill.confirmation.disclaimer.required"),
            },
          ]}
          style={{ marginBottom: 0 }}
        >
          <Checkbox>
            <Trans i18nKey={"join.fill.confirmation.disclaimer"}>
              我已詳細閱讀並了解
              <TLink href="/disclaimer" target={"_blank"}>
                「上傳資料免責聲明」
              </TLink>
              之內容
            </Trans>
          </Checkbox>
        </Form.Item>
        <Form.Item
          name="agressProtection"
          valuePropName="checked"
          rules={[
            {
              required: true,
              message: t("join.fill.confirmation.privacypolicy.required"),
            },
          ]}
          style={{ marginBottom: 0 }}
        >
          <Checkbox>
            <Trans i18nKey={"join.fill.confirmation.privacypolicy"}>
              我已詳細閱讀並了解
              <TLink href="/privacy" target={"_blank"}>
                「隱私權保護宣示」
              </TLink>
              之內容
            </Trans>
          </Checkbox>
        </Form.Item>
        <hr />
      </div>
      <Row gutter={40} className={styles.actionButtonGroup}>
        <Col {...doubleColProps}>
          <FancyButton
            onClick={onPrev}
            className={classNames(styles.actionButton)}
          >
            {t("join.previous")}
          </FancyButton>
        </Col>
        <Col {...doubleColProps}>
          <FancyButton
            // onClick={onNext}
            htmlType="submit"
            className={classNames(styles.actionButton)}
          >
            {t("join.fill.confirmation.submit")}
          </FancyButton>
        </Col>
      </Row>
    </Form>
  );
};

const FormSuccess = ({ email }) => {
  const { t } = useTranslation("common");

  return (
    <section
      className={classNames("basic-section reverse", styles.sectionSeccuess)}
    >
      <div>
        <SBGSuccess className={styles.successIcon} />
      </div>
      <div>
        <h3>{t("join.fill.Sendout.reminder")}</h3>
        <div className={classNames("desc", styles.desc)}>
          <Trans
            i18nKey={"join.fill.Sendout.reminder.instructions"}
            values={{ email: email }}
          >
            我們已將您提出的申請內容寄至您的Email， 請前往您所填寫的聯繫Email
            {{ email }} 進行驗證確認。 並請等候審核確認通知，我們將與您聯絡。
            若您無收取驗證信或有任何問題，歡迎來信聯絡我們。
          </Trans>
          <TLink href="mailto://futurecontent@taicca.tw">
            futurecontent@taicca.tw
          </TLink>
        </div>
      </div>
    </section>
  );
};

export default function JoinForm({ formItems, country }) {
  const router = useRouter();
  const locale = router.locale;
  const rwd = useRWD();
  const { t } = useTranslation("common");
  const [step, setStep] = useState(1);
  const activeDelay = 200;

  const parallaxController = useContext(ParallaxContext);

  const jumpToTop = useCallback(() => {
    window.scrollTo({
      top: 0,
      left: 0,
      // behavior: "smooth",
    });
  }, []);

  const dataRef = useRef({});
  const [userEmail, setUserEmail] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const handelSandData = useCallback(
    (data) => {
      setIsLoading(true);
      jumpToTop();

      const countryName = data.addressCountry;
      const continents = country.find((c) => c.name === countryName)[
        "continent"
      ];

      insertMember({ ...data, continents, lang: locale })
        .then((res) => {
          if (res.resu === 1) {
            setUserEmail(data.email);

            setTimeout(() => {
              setStep(4);
              setIsLoading(false);
            }, activeDelay);
          } else {
            setIsLoading(false);
          }
        })
        .catch((e) => {
          setIsLoading(false);
        });
    },
    [jumpToTop, locale]
  );

  useEffect(() => {
    if (parallaxController) {
      parallaxController?.update();
    }
  }, [parallaxController, step]);

  return (
    <>
      <HeaderBannerLight title={t("join.headline")} numberText={"01"} />
      <PageInfoBasic />

      <JoinStep
        step={step}
        isMoble={rwd.device === "mobile"}
        className={styles.steps}
        t={t}
      />

      <div style={{ display: !isLoading ? "block" : "none" }}>
        <div
          style={{ display: step === 1 || step === 3 ? "block" : "none" }}
          className={styles.contentContainer}
        >
          <Form1
            rwd={rwd}
            checkMode={step === 3}
            formItems={formItems}
            country={country}
            onNext={(data) => {
              dataRef.current = { ...dataRef.current, ...data };
              jumpToTop();
              setTimeout(() => setStep(2), activeDelay);
            }}
          />
        </div>

        <div
          style={{ display: step === 2 || step === 3 ? "block" : "none" }}
          className={styles.contentContainer}
        >
          <Form2
            rwd={rwd}
            checkMode={step === 3}
            formItems={formItems}
            onNext={(data) => {
              jumpToTop();
              // setStep(3);
              setTimeout(() => setStep(3), activeDelay);
              dataRef.current = { ...dataRef.current, ...data };
            }}
            onPrev={(e) => {
              e.preventDefault();
              jumpToTop();
              setTimeout(() => setStep(1), activeDelay);
            }}
          />
        </div>

        <div
          style={{ display: step === 3 ? "block" : "none" }}
          className={styles.contentContainer}
        >
          <Form3
            rwd={rwd}
            onPrev={(e) => {
              e.preventDefault();
              jumpToTop();
              setTimeout(() => {
                setStep(2);
              }, activeDelay);
            }}
            onNext={(data) => {
              dataRef.current = { ...dataRef.current, ...data };
              handelSandData(dataRef.current);
            }}
          />
        </div>
      </div>
      <div
        style={{
          display: isLoading ? "flex" : "none",
          alignItems: "center",
          justifyContent: "center",
          height: 300,
          marginBottom: 150,
        }}
      >
        <Loading />
      </div>
      {step === 4 && <FormSuccess email={userEmail} />}

      <Footer />
    </>
  );
}
JoinForm.headerProps = {
  // red: true,
  // title: "加入共創圈",
  // numberText: "06",
};
export async function getServerSideProps(context) {
  const { locale } = context;
  const formItems = await getFormItems({ locale });

  const countryRes = await getCountry({
    locale,
  });

  const country = countryRes.data.country;

  return {
    props: {
      formItems,
      country,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
