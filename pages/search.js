import Head from "next/head";
import { PageInfoBasic, PageInfoSearch } from "@/components/PageInfo/PageInfo";
import React, { useCallback, useMemo, useState } from "react";

import styles from "../styles/Search.module.scss";
import { Footer } from "@/components/Footer/Footer";

import Link from "next/link";
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";
import { HeaderBannerLight } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import SVGSearch from "@/svgs/searchDC.svg";
import { useRouter } from "next/router";
import { useAsync, useUpdateEffect } from "react-use";
import { searchApi } from "@/api";
import Highlighter from "react-highlight-words";
import ReactPaginate from "react-paginate";
import { usePageApi } from "@/hooks/usePageApi";
import { useLoadMoreApi2 } from "@/utilits/useLoadMoreApi";
import { LoadMoreGroup } from "@/components/LoadMoreGroup/LoadMoreGroup";
import { Trans, Translation, useTranslation } from 'next-i18next';
import { Loading } from "@/components/Loading/Loading";

const getStart = (text, key) => {
    const max = 100;
    const index = text.indexOf(key);

    // return index;

    for (let i = 0; i < max; i++) {
        const checkIndex = index - i;
        if (checkIndex === 0) {
            return 0;
        }
        const char = text[index - i];
        if (",.，。 \n\r、".indexOf(char) !== -1) {
            return index - i + 1;
        }
    }
    return 0;
};

const searchPageNumber = 5;

export default function Search({ initText, initRes }) {
    const rwd = useRWD();
    const router = useRouter();
    const queryText = router.query.text;
    const locale = router.locale;

    const [inputValue, setInputValue] = useState(initText);

    // const dataState = useAsync(async () => {
    //   const res = await searchApi(queryText, router.locale);

    //   return res;
    // }, [queryText]);
    useUpdateEffect(() => {
        setInputValue(initText);
    }, [initText]);
    const api = useCallback(
        ({ page }) => {
            return searchApi({
                text: queryText,
                lang: locale,
                pagesize: searchPageNumber,
                page: page,
            });
        },
        [locale, queryText]
    );

    const dataState = usePageApi({
        initAllPage: initRes.pages.totalPages,
        initData: initRes.data,
        initPage: 1,
        initTotalCount: initRes.pages.totalCount,
        api: api,
    });

    const loadMoreState = useLoadMoreApi2({
        initAllPage: initRes.pages.totalPages,
        initData: initRes.data,
        initPage: 1,
        initTotalCount: initRes.pages.totalCount,
        api: api,
    });

    const { t } = useTranslation("common");

    const isLoading = (rwd.device === "mobile" ? loadMoreState : dataState)
        .isLoading;

    const target = rwd.device === "mobile" ? loadMoreState : dataState;
    const isEmpty = !target.isLoading && target.totalCount === 0;

    return (
        <>
            <Head>
                <title>{t("seo.search.title")}</title>
                <meta name="description" content={t("seo.search.desc")} />
            </Head>
            <h1 className="seoH1">{t("h1.search")}</h1>
            <HeaderBannerLight title={t("search.headline")} />
            <PageInfoSearch>
                <div className={styles.searchInputContainer}>
                    <input
                        value={inputValue}
                        onChange={(e) => setInputValue(e.target.value)}
                        className={styles.searchInput}
                        onKeyPress={(e) => {
                            if (event.key === "Enter") {
                                router.push(`/search?text=${inputValue}`, undefined, {
                                    shallow: true,
                                });
                            }
                        }}
                    />
                    <SVGSearch
                        onClick={() => {
                            router.push(`/search?text=${inputValue}`, undefined, {
                                shallow: true,
                            });
                        }}
                        viewBox="0 0 21 20"
                        className={styles.seaechIcon}
                    />
                </div>
            </PageInfoSearch>

            <div className={"contentContainer"}>
                <section className={styles.section1}>
                    {dataState.data && (
                        <>
                            <div className={styles.searchResultInfo}>
                                <Trans
                                    i18nKey={"search.results"}
                                    values={{ num: isLoading ? "-" : dataState.totalCount }}
                                >
                                    搜尋結果
                                    <span className={styles.highlighter}>0筆資料</span>
                                </Trans>
                            </div>

                            {(rwd.device === "mobile" || !isLoading) &&
                                (rwd.device === "mobile" ? loadMoreState : dataState).data.map(
                                    (a) => {
                                        let path = "/";
                                        if (a.type === "plan") {
                                            path = `/programs/${a.slug}`;
                                        } else if (a.type === "resources") {
                                            path = `/resources/${a.slug}`;
                                        } else if  (a.type === "news") {
                                            if (a.category === "2") {
                                                path = `/events/${a.slug}`;
                                            } else {
                                                path = `/news/${a.slug}`;
                                            }
                                        } else if  (a.type === "unit") {
                                            path = `/professionals/${a.id}/${a.slug}`;
                                        } else if  (a.type === "work") {
                                            path = `/content/${a.id}/${a.slug}`;
                                        }
                                        return (
                                            <Link
                                                legacyBehavior
                                                key={a.id}
                                                href={path}
                                                passHref
                                            >
                                                <div className={styles.searchResultItem}>
                                                    <h4>{a.title}</h4>
                                                    <div>{`index${path}`}</div>
                                                    {/* <p>{a.content}</p> */}
                                                    {/* <div>{getStart(a.content, queryText)}</div> */}
    
                                                    <Highlighter
                                                        highlightClassName={styles.highlighter}
                                                        searchWords={[queryText]}
                                                        autoEscape={true}
                                                        textToHighlight={a.content.slice(
                                                            getStart(a.content, queryText)
                                                        )}
                                                        className={styles.content}
                                                    />
                                                </div>
                                            </Link>
                                        )
                                    }
                                )}
                            {!isLoading && isEmpty && (
                                <div
                                    className={classNames(styles.isEmpty, {
                                        [styles.zh]: locale === "zh",
                                    })}
                                    key={queryText}
                                >
                                    <Trans
                                        i18nKey={"search.results.wrong"}
                                        values={{ text: queryText }}
                                    />
                                </div>
                            )}
                            {!isEmpty && (
                                <div className={classNames(styles.paginate, "desktop-only")}>
                                    <ReactPaginate
                                        previousLabel={"<"}
                                        nextLabel={">"}
                                        breakLabel={"..."}
                                        breakClassName={"break-me"}
                                        // initialPage={state.page - 1}
                                        initialPage={0}
                                        pageCount={dataState.allPage}
                                        forcePage={dataState.currentPage - 1}
                                        // pageCount={state.allPage}
                                        marginPagesDisplayed={2}
                                        pageRangeDisplayed={4}
                                        onPageChange={dataState.handlePageClick}
                                        containerClassName={classNames("pagination")}
                                        activeClassName={"active"}
                                    />
                                </div>
                            )}

                            {loadMoreState.showLoadMore && (
                                <LoadMoreGroup
                                    actionText={t("event.showmore.button")}
                                    leftText={t("event.showmore")}
                                    className={classNames("mobile-only", styles.loadMoreGroup)}
                                    onLoadmore={loadMoreState.handleNext}
                                />
                            )}

                            {isLoading && (
                                <div style={{ textAlign: "center" }}>
                                    <Loading />
                                </div>
                            )}
                        </>
                    )}
                </section>
            </div>
            <Footer />
        </>
    );
}
Search.headerProps = {
    // red: true,
    search: false,
};
export async function getServerSideProps(context) {
    const text = context.query.text || null;

    const initRes = await searchApi({
        text,
        lang: context.locale,
        pagesize: searchPageNumber,
    });

    return {
        props: {
            ...(await serverSideTranslations(context.locale, ["common"])),
            initText: text,
            initRes,
        }, // will be passed to the page component as props
    };
}
