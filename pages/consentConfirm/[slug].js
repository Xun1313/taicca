import Link from "next/link";

import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import React, { useContext, useMemo, useState } from "react";
import styles from "@/styles/ConsentConfirm.module.scss";

import { Footer } from "@/components/Footer/Footer";
import classNames from "classnames";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { planContent } from "@/api";
import { Trans, useTranslation } from "next-i18next";

import { useRouter } from "next/router";
import { ParallaxBanner } from "react-scroll-parallax";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { auth } from "@/utilits/auth";

export default function ConsentConfirm({ slug, article }) {
  const { t } = useTranslation("common");

  const router = useRouter();
  const { locale } = useRouter();
  const CustomLink = ({ children }) => (
    <Link legacyBehavior href={`/user/myAccount`}>
      <a>{children}</a>
    </Link>
  );

  return (
    <>
      <HeaderBannerNew1
        title={t("news.headline")}
        navList={[
          {
            text: t("news.headline"),
            link: "/news",
          },
          {
            text: t("news.tab4"),
            link: "/programs",
          },
          {
            text: article.title,
          },
        ]}
      />

      <div className={classNames("contentContainer", styles.contentContainer)}>
        <div className={styles.status}>{t("consentConfirm.status")}</div>

        <div className={styles.title}>{article.title}</div>

        <div className={styles.content}>
          <Link legacyBehavior href={`/programs/${slug}`}>
            <a>
              {">"} {t("consentConfirm.content")}
            </a>
          </Link>
        </div>

        <div className={styles.notice}>
          <Trans
            i18nKey={t("consentConfirm.notice")}
            transKeepBasicHtmlNodesFor={["li", "ul", "b", "h3", "a"]}
            components={{
              link_member: <CustomLink />,
            }}
          />
        </div>

        <div className={styles.consentBtn}>
          <FancyButton onClick={() => router.push(`/user/consent/${slug}`)}>
            {t("consentConfirm.btn")}
          </FancyButton>
        </div>
      </div>

      <Footer />
    </>
  );
}

export async function getServerSideProps(context) {
  const cookies = context.req.cookies;
  const locale = context.locale;
  const slug = context.query.slug;

  if (!slug) {
    return {
      notFound: true,
    };
  }

  const { isLogin, redirect } = auth(cookies);
  if (!isLogin) return redirect;

  const handleArticleContent = async () => {
    let article = {};
    const { data } = await planContent({
      locale,
      slug,
    });
    article = data;
    return article;
  };

  const [article] = await Promise.all([handleArticleContent()]);

  if (!article.openjoin) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      slug,
      article,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
