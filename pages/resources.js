import Head from "next/head";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import React, { useCallback, useState } from "react";
import styles from "../styles/Resources.module.scss";

import { Footer } from "@/components/Footer/Footer";
import classNames from "classnames";
import { ScrollBoxH } from "@/components/ScrollBoxH/ScrollBoxH";
import { CardContent } from "@/components/CardContent/CardContent";
import { LoadMoreGroup } from "@/components/LoadMoreGroup/LoadMoreGroup";
import ReactPaginate from "react-paginate";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from 'next-i18next';
import { getArticle001v3 } from "@/api";
import { useAsyncFn } from "react-use";
import { getFullRangeText } from "@/utilits/handleDate";
import { useRouter } from "next/router";
import { useLoadMoreApi } from "@/utilits/useLoadMoreApi";
import { useRWD } from "@/hooks/useRwd";
import { Loading } from "@/components/Loading/Loading";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { TLink } from "@/components/TLink";

const usePageApi = ({
  initPage,
  initAllPage,
  initData,
  subCateid,
  pageSize,
}) => {
  const [state, setState] = useState({
    data: initData,
    page: initPage,
    allPage: initAllPage,
  });
  const { locale } = useRouter();

  const [apiState, fetch] = useAsyncFn(async (page) => {
    const res = await getArticle001v3({
      topmost: subCateid ? 0 : 2,
      pagesize: pageSize,
      ispages: 1,
      page: page,
      locale,
      subCateid: subCateid,
    });

    setState({
      data: res.data,
      page: page,
      allPage: res.pages.totalPages,
    });
  }, []);

  const handlePageClick = useCallback(
    (val) => {
      setState((prev) => ({
        ...prev,
        page: val.selected + 1,
      }));
      fetch(val.selected + 1);
    },
    [fetch]
  );

  return {
    handlePageClick,
    currentPage: state.page,
    allPage: state.allPage,
    data: state.data,
    isLoading: apiState.loading,
  };
};

export default function Resources({
  resourcesHotRes,
  resourcesListRes,
  subCateid,
  pageSize,
}) {
  const { t } = useTranslation("common");
  const { locale } = useRouter();

  const pageControl = usePageApi({
    initAllPage: resourcesListRes.pages.totalPages,
    initPage: 1,
    initData: resourcesListRes.data,
    subCateid: subCateid,
    pageSize: pageSize,
  });

  const loadMoreState = useLoadMoreApi({
    initAllPage: resourcesListRes.pages.totalPages,
    initPage: 1,
    initData: resourcesListRes.data,
    pageSize: pageSize,
    subCateid: subCateid,
  });

  const rwd = useRWD();
  return (
    <>
      <Head>
        <title>{t(`seo.resources${subCateid}.title`)}</title>
        <meta
          name="description"
          content={t(`seo.resources${subCateid}.desc`)}
        />
      </Head>
      <HeaderBannerNew1
        title={t("resources.headline")}
        navList={[
          {
            text: t("resources.headline"),
          },
          {
            text: [t(`resources.tab${subCateid}`)],
          },
        ].slice(0, subCateid === "" ? 1 : 2)}
        desc={t("resources.desc1")}
      />
      <div className={styles.categoryList}>
        <a href={`${locale === "zh" ? "" : "/en"}/resources`}>
          <FancyButton selected={subCateid === ""}>
            {t("resources.tab")}
            <h1 className="seoH1">{t("h1.reports")}</h1>
          </FancyButton>
        </a>
        <a href={`${locale === "zh" ? "" : "/en"}/articles`}>
          <FancyButton selected={subCateid === "002"}>
            {t("resources.tab002")}
            <h1 className="seoH1">{t("h1.articles")}</h1>
          </FancyButton>
        </a>
        <a href={`${locale === "zh" ? "" : "/en"}/opencall`}>
          <FancyButton selected={subCateid === "004"}>
            {t("resources.tab004")}
            <h1 className="seoH1">{t("h1.opencall")}</h1>
          </FancyButton>
        </a>
        <TLink href="/industrylinks">
          <FancyButton>{t("resources.tab1")}</FancyButton>
        </TLink>
      </div>

      {subCateid === "" && (
        <section className={styles.focus}>
          <h4>{t("resources.tittle1")}</h4>
          <ScrollBoxH
            className={styles.scrollboxOuter}
            innerClassName={styles.scrollboxInner}
          >
            {resourcesHotRes.data.map((a) => (
              <CardContent
                type="newsBig"
                key={a.id}
                title={a.title}
                image={[a.pic, "/images/card_default.png"]}
                slug={a.slug}
                rangeText={getFullRangeText(a)}
                category="resources"
                clamp={rwd.device === "mobile" ? 2 : 3}
                subCate={t(`resources.tab${a.subCateid}`)}
              />
            ))}
            {/* {Array(5)
            .fill()
            .map((_, index) => (
              <CardContent type="newsBig" status="進行中" key={index} />
            ))} */}
          </ScrollBoxH>
        </section>
      )}

      <div className={"contentContainer"}>
        <div className={styles.sectionHeader}></div>
        <div className={styles.table}>
          {(rwd.device === "desktop"
            ? pageControl.data
            : loadMoreState.data
          ).map((a) => (
            <CardContent
              type="newsSmall"
              title={a.title}
              image={[a.pic, "/images/card_default.png"]}
              key={a.id}
              slug={a.slug}
              rangeText={getFullRangeText(a)}
              category="resources"
              clamp={3}
              subCate={t(`resources.tab${a.subCateid}`)}
            />
          ))}
        </div>

        {/* {pageControl.isLoading && (
          <div style={{ textAlign: "center" }}>
            <Loading />
          </div>
        )} */}

        <div className={classNames(styles.paginate, "desktop-only")}>
          <ReactPaginate
            previousLabel={"<"}
            nextLabel={">"}
            breakLabel={"..."}
            breakClassName={"break-me"}
            // initialPage={state.page - 1}
            initialPage={0}
            pageCount={pageControl.allPage}
            // pageCount={state.allPage}
            marginPagesDisplayed={2}
            pageRangeDisplayed={4}
            onPageChange={pageControl.handlePageClick}
            containerClassName={classNames("pagination")}
            activeClassName={"active"}
          />
        </div>
        {loadMoreState.isLoading && (
          <div style={{ textAlign: "center", marginBottom: 30 }}>
            <Loading />
          </div>
        )}
        {loadMoreState.showLoadMore && (
          <LoadMoreGroup
            actionText={t("event.showmore.button")}
            leftText={t("event.showmore")}
            className={classNames("mobile-only", styles.loadMoreGroup)}
            onLoadmore={loadMoreState.handleNext}
          />
        )}
      </div>
      {/* <SVGJoin className={classNames("desktop-only", "joinIcon")} /> */}
      {/* <SVGJoinM className={classNames("mobile-only", "joinIcon")} /> */}

      {/* <Join className={styles.join} /> */}

      <Footer />
    </>
  );
}
Resources.headerProps = {};
export async function getServerSideProps(context) {
  const locale = context.locale;
  const subCateid = context.subCateid || "";
  const pageSize = 8;

  const getArticleHot = async () => {
    if (subCateid) {
      return [];
    } else {
      return await getArticle001v3({
        topmost: 1,
        returnAmount: 3,
        ispages: 0,
        page: 1,
        locale,
      });
    }
  };

  const [resourcesHotRes, resourcesListRes] = await Promise.all([
    getArticleHot(),
    getArticle001v3({
      topmost: subCateid ? 0 : 2,
      pagesize: pageSize,
      ispages: 1,
      page: 1,
      locale,
      subCateid,
    }),
  ]);

  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
      resourcesHotRes,
      resourcesListRes,
      pageSize,
      subCateid,
    }, // will be passed to the page component as props
  };
}
