import Head from "next/head";
import React, {
  useCallback,
  useMemo,
  useState,
  useContext,
  useRef,
  useEffect,
} from "react";
import moment from "moment";
import styles from "../../styles/MyAccount.module.scss";
import { Footer } from "@/components/Footer/Footer";
import SVGLBX from "@/svgs/close.svg";
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";
import SVGAccount from "@/svgs/account.svg";
import SVGCompanion from "@/svgs/companion.svg";
import SVGContent from "@/svgs/content.svg";
import SVGQustion2 from "@/svgs/question2.svg";
import SVGEdit from "@/svgs/edit.svg";
import SVGRemove from "@/svgs/remove.svg";
import SVGAdd from "@/svgs/add.svg";
import SVGEyeOpen from "@/svgs/eyeOpen.svg";
import SVGEyeClose from "@/svgs/eyeClose.svg";
import SVGArrow from "@/svgs/arrow2.svg";
import SVGMatch from "@/svgs/match.svg";
import SVGFavorite from "@/svgs/favorite.svg";
import SVGActivity from "@/svgs/activity.svg";
import SVGResources from "@/svgs/resources.svg";
import SVGAlert from "@/svgs/alert.svg";
import SVGLogout from "@/svgs/logout.svg";
import { auth } from "@/utilits/auth";
import { parseItems } from "@/utilits/parseItems";
import { parseCookies } from "@/utilits/parseCookies";

import { Form, Input, Modal, Row, Col, Checkbox, Radio, Popover } from "antd";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import {
  unitList,
  unitNew,
  unitIsshow,
  unitDelete,
  sourceItem,
  accWorksNew,
  accWorksIsshow,
  accWorksList,
  accWorksDelete,
  logout,
} from "@/api";
import router, { useRouter } from "next/router";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { LoadingContext } from "@/context/LoadingContext";
import { CommonLightBox } from "@/components/CommonLightBox/CommonLightBox";
import { CommonMsg } from "@/components/CommonMsg/CommonMsg";
import { TLink } from "@/components/TLink";
// 1.如何拿到這頁的內容 api or from login api?
// 2.串夥伴頁面列表，公司、展會，狀態也是從 mapping api，狀態的 tag 顏色確認
// 3.format 時間格式
// 4.功能點點點有四個功能，好像只有審核通過後才能自行設定"公開"或"關閉"
// 5.新增的 popup
export default function MyAccount({
  email,
  identity,
  memberId,
  authKey,
  unitDataList,
  accWorksDataList,
  formItems,
}) {
  const { locale } = useRouter();
  const router = useRouter();
  const { t } = useTranslation("common");
  const rwd = useRWD();
  const { setShowLoading } = useContext(LoadingContext);
  const removeItem = useRef(null);

  const [companionModal, setCompanionModal] = useState(false);
  const [errorCompanionModal, setErrorCompanionModal] = useState("");
  const [worksModal, setWorksModal] = useState(false);
  const [errorWorksModal, setErrorWorksModal] = useState("");

  useEffect(() => {
    switch (router.query.openPopup) {
      case "1":
        setCompanionModal(true);
        break;
      case "2": {
        if (identity === "1") {
          setWorksModal(true);
        }
        break;
      }
      default:
        break;
    }
  }, [router.query.openPopup, identity]);

  const [removeAlertModal, setRemoveAlertModal] = useState(false);
  const doubleColProps = useMemo(
    () => ({
      span: rwd.device === "mobile" ? 24 : 12,
    }),
    [rwd.device]
  );
  const mechanismOptions = parseItems(formItems.mechanism);

  const handleAddCompanionFinish = useCallback(
    (data) => {
      console.log(data);
      setShowLoading(true);
      unitNew({
        auth_key: authKey,
        memberm1_no: memberId,
        ...data,
      }).then((res) => {
        if (res.resu === 1) {
          router.push(`/user/companion/${res.data.Unitm1_no}`);
        } else {
          setErrorCompanionModal(t(`signin.error.apiMsg${res.error}`));
        }
        setShowLoading(false);
      });
    },
    [locale]
  );

  const handleAddWorkFinish = useCallback(
    (data) => {
      console.log(data);
      setShowLoading(true);
      accWorksNew({
        auth_key: authKey,
        memberm1_no: memberId,
        ...data,
      }).then((res) => {
        if (res.resu === 1) {
          router.push(`/user/works/${res.data.id}`);
        } else {
          setErrorWorksModal(t(`signin.error.apiMsg${res.error}`));
        }
        setShowLoading(false);
      });
    },
    [locale]
  );

  const handleDeleteCompanion = async (id) => {
    setShowLoading(true);
    const { resu } = await unitDelete({
      auth_key: authKey,
      memberm1_no: memberId,
      Unitm1_no: id,
    });
    if (resu === 1) {
      router.replace(router.pathname, undefined, { scroll: false });
    }
    setShowLoading(false);
    setRemoveAlertModal(false);
    CommonMsg({
      type: "success",
      msg: t("myAccount.remove.success"),
    });
  };

  const handleDeleteWork = async (id) => {
    setShowLoading(true);
    const { resu } = await accWorksDelete({
      auth_key: authKey,
      memberm1_no: memberId,
      workm1_no: id,
    });
    if (resu === 1) {
      router.replace(router.pathname, undefined, { scroll: false });
    }
    setShowLoading(false);
    setRemoveAlertModal(false);
    CommonMsg({
      type: "success",
      msg: t("myAccount.remove.success"),
    });
  };

  const handleIsshowCompanion = async (id, isshow) => {
    setShowLoading(true);
    const { resu } = await unitIsshow({
      auth_key: authKey,
      memberm1_no: memberId,
      Unitm1_no: id,
      isshow,
    });
    if (resu === 1) {
      if (isshow === "8") {
        CommonMsg({
          type: "success",
          msg: t("myAccount.public.desc1"),
        });
      } else {
        CommonMsg({
          type: "success",
          msg: t("myAccount.public.desc2"),
        });
      }
      router.replace(router.pathname, undefined, { scroll: false });
    }
    setShowLoading(false);
  };

  const handleIsshowWork = async (id, isshow) => {
    setShowLoading(true);
    const { resu } = await accWorksIsshow({
      auth_key: authKey,
      memberm1_no: memberId,
      workm1_no: id,
      isshow,
    });
    if (resu === 1) {
      if (isshow === "8") {
        CommonMsg({
          type: "success",
          msg: t("myAccount.public.desc1"),
        });
      } else {
        CommonMsg({
          type: "success",
          msg: t("myAccount.public.desc2"),
        });
      }
      router.replace(router.pathname, undefined, { scroll: false });
    }
    setShowLoading(false);
  };

  const handleLogout = async () => {
    setShowLoading(true);
    document.cookie = `taiccaAuth=; expires=${new Date(
      "1950 01-01"
    ).toUTCString()}; path=/`;

    await logout();
    router.replace("/signin");
    setShowLoading(false);
  };

  return (
    <>
      {/* <Head>
        <title>{t("seo.contact.title")}</title>
        <meta name="description" content={t("seo.contact.desc")} />
      </Head> */}
      <HeaderBannerNew1 title={t("meun.memberCenter")} />

      <section className={classNames(styles.contentContainer, "myAccountPage")}>
        <div className={classNames(styles.head)}>
          <div className={classNames(styles.titleBlock)}>
            <div className={classNames(styles.title2)}>
              {t("myAccount.title")}
            </div>
          </div>

          <div className={classNames(styles.email)}>
            {email}
            <Row align="middle" onClick={handleLogout}>
              <SVGLogout />
              {t("myAccount.btn6")}
            </Row>
          </div>
        </div>

        <Row gutter={[0, rwd.device === "mobile" ? 35 : 45]}>
          <Col span={24}>
            <div className={classNames(styles.menuContainer)}>
              <div className={classNames(styles.menuTitle)}>
                <Row align="middle" wrap={false}>
                  <SVGAccount />
                  &nbsp;
                  <span className={classNames(styles.title)}>
                    {t("myAccount.field1.title")}
                  </span>
                </Row>
              </div>
              <div
                className={classNames(styles.menuContent, styles.account)}
                style={{ cursor: "pointer" }}
                onClick={() => router.push("/user/accountSetting")}
              >
                {t("myAccount.field1.content")}
              </div>
            </div>
          </Col>

          <Col span={24}>
            <div className={classNames(styles.menuContainer)}>
              <div className={classNames(styles.menuTitle)}>
                <Row align="middle" wrap={false}>
                  <SVGCompanion />
                  &nbsp;
                  <span className={classNames(styles.title)}>
                    {t("myAccount.field2.title")}
                  </span>
                </Row>
              </div>
              {unitDataList.map((e) => (
                <div className={classNames(styles.menuContent)} key={e.id}>
                  <div className={classNames(styles.first)}>
                    <div className={classNames(styles.subTitle)}>
                      {formItems.mechanism[e.unit]}
                    </div>
                    <div className={classNames(styles.mainTitle)}>{`${
                      e.name_en || ""
                    } ${e.lastname_en || ""} ${e.name_zh || ""}`}</div>
                  </div>

                  <div className={classNames(styles.last)}>
                    <div className={classNames(styles.statusContainer)}>
                      <div
                        className={classNames(
                          styles.status,
                          styles[`status${e.status}`]
                        )}
                      >
                        {formItems.unitStatus[e.status]}
                      </div>
                      {e.audit && (
                        <Popover
                          overlayClassName={classNames("customPopover")}
                          content={
                            <div>
                              <div
                                style={{
                                  fontSize: 18,
                                  marginBottom: 15,
                                }}
                              >
                                {t("myAccount.audit.title")}
                              </div>
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: e.audit,
                                }}
                              ></div>
                            </div>
                          }
                          trigger="hover"
                          placement="topLeft"
                        >
                          <SVGAlert style={{ marginLeft: 7 }} />
                        </Popover>
                      )}
                    </div>

                    <div className={classNames(styles.subTitle, styles.time)}>
                      {e.date &&
                        moment(e.date.replace(" ", "T")).format("DD/M/YYYY")}
                    </div>

                    {rwd.device === "mobile" ? (
                      <div className={classNames(styles.more)}>
                        {e.status !== 3 && e.status !== 6 && (
                          <SVGEdit
                            onClick={() =>
                              router.push(`/user/companion/${e.id}`)
                            }
                          />
                        )}
                        {e.status == 7 && (
                          <SVGEyeOpen
                            onClick={() => handleIsshowCompanion(e.id, "8")}
                          />
                        )}
                        {e.status == 8 && (
                          <SVGEyeClose
                            onClick={() => handleIsshowCompanion(e.id, "7")}
                          />
                        )}
                        <SVGRemove
                          onClick={() => {
                            removeItem.current = {
                              category: "companion",
                              id: e.id,
                            };
                            setRemoveAlertModal(true);
                          }}
                        />
                      </div>
                    ) : (
                      <Popover
                        overlayClassName={classNames("customPopover")}
                        content={
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "column",
                              alignItems: "center",
                            }}
                          >
                            {e.status !== 3 && e.status !== 6 && (
                              <SVGEdit
                                onClick={() =>
                                  router.push(`/user/companion/${e.id}`)
                                }
                              />
                            )}
                            {e.status == 7 && (
                              <SVGEyeOpen
                                onClick={() => handleIsshowCompanion(e.id, "8")}
                              />
                            )}
                            {e.status == 8 && (
                              <SVGEyeClose
                                onClick={() => handleIsshowCompanion(e.id, "7")}
                              />
                            )}
                            <SVGRemove
                              onClick={() => {
                                removeItem.current = {
                                  category: "companion",
                                  id: e.id,
                                };
                                setRemoveAlertModal(true);
                              }}
                            />
                          </div>
                        }
                        trigger="click"
                        placement="right"
                      >
                        <div className={classNames(styles.more)}>&hellip;</div>
                      </Popover>
                    )}
                  </div>
                </div>
              ))}

              <Row
                justify={
                  rwd.device === "mobile" && unitDataList.length > 0
                    ? "end"
                    : "end"
                }
                align="middle"
                className={classNames({
                  [styles.menuContent]: unitDataList.length === 0,
                })}
                style={{
                  marginTop: unitDataList.length > 0 ? 10 : 0,
                  cursor: "pointer",
                }}
                onClick={() => setCompanionModal(true)}
              >
                <Col>
                  <SVGAdd />
                </Col>

                <Col style={{ marginLeft: 10 }}>
                  {t("myAccount.field2.add1")}
                </Col>
              </Row>
            </div>
          </Col>

          {identity === "1" && (
            <Col span={24}>
              <div className={classNames(styles.menuContainer)}>
                <div className={classNames(styles.menuTitle)}>
                  <Row align="middle" wrap={false}>
                    <SVGContent />
                    &nbsp;
                    <span className={classNames(styles.title)}>
                      {t("myAccount.field3.title")}
                    </span>
                  </Row>
                </div>
                {accWorksDataList.map((e) => (
                  <div className={classNames(styles.menuContent)} key={e.id}>
                    <div className={classNames(styles.first)}>
                      <div className={classNames(styles.mainTitle)}>{`${
                        e.name_en || ""
                      } ${e.name_zh || ""}`}</div>
                    </div>

                    <div className={classNames(styles.last)}>
                      <div className={classNames(styles.statusContainer)}>
                        <div
                          className={classNames(
                            styles.status,
                            styles[`status${e.status}`]
                          )}
                        >
                          {formItems.workStatus[e.status]}
                        </div>
                        {e.audit && (
                          <Popover
                            overlayClassName={classNames("customPopover")}
                            content={
                              <div>
                                <div
                                  style={{
                                    fontSize: 18,
                                    marginBottom: 15,
                                  }}
                                >
                                  {t("myAccount.audit.title")}
                                </div>
                                <div
                                  dangerouslySetInnerHTML={{
                                    __html: e.audit,
                                  }}
                                ></div>
                              </div>
                            }
                            trigger="hover"
                            placement="topLeft"
                          >
                            <SVGAlert style={{ marginLeft: 7 }} />
                          </Popover>
                        )}
                      </div>

                      <div className={classNames(styles.subTitle, styles.time)}>
                        {e.date &&
                          moment(e.date.replace(" ", "T")).format("DD/M/YYYY")}
                      </div>

                      {rwd.device === "mobile" ? (
                        <div className={classNames(styles.more)}>
                          {e.status !== 3 && e.status !== 6 && (
                            <SVGEdit
                              onClick={() => router.push(`/user/works/${e.id}`)}
                            />
                          )}
                          {e.status == 7 && (
                            <SVGEyeOpen
                              onClick={() => handleIsshowWork(e.id, "8")}
                            />
                          )}
                          {e.status == 8 && (
                            <SVGEyeClose
                              onClick={() => handleIsshowWork(e.id, "7")}
                            />
                          )}
                          <SVGRemove
                            onClick={() => {
                              removeItem.current = {
                                category: "work",
                                id: e.id,
                              };
                              setRemoveAlertModal(true);
                            }}
                          />
                        </div>
                      ) : (
                        <Popover
                          overlayClassName={classNames("customPopover")}
                          content={
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "center",
                              }}
                            >
                              {e.status !== 3 && e.status !== 6 && (
                                <SVGEdit
                                  onClick={() =>
                                    router.push(`/user/works/${e.id}`)
                                  }
                                />
                              )}
                              {e.status == 7 && (
                                <SVGEyeOpen
                                  onClick={() => handleIsshowWork(e.id, "8")}
                                />
                              )}
                              {e.status == 8 && (
                                <SVGEyeClose
                                  onClick={() => handleIsshowWork(e.id, "7")}
                                />
                              )}
                              <SVGRemove
                                onClick={() => {
                                  removeItem.current = {
                                    category: "work",
                                    id: e.id,
                                  };
                                  setRemoveAlertModal(true);
                                }}
                              />
                            </div>
                          }
                          trigger="click"
                          placement="right"
                        >
                          <div className={classNames(styles.more)}>
                            &hellip;
                          </div>
                        </Popover>
                      )}
                    </div>
                  </div>
                ))}

                <Row
                  justify={
                    rwd.device === "mobile" && accWorksDataList.length > 0
                      ? "end"
                      : "end"
                  }
                  align="middle"
                  className={classNames({
                    [styles.menuContent]: accWorksDataList.length === 0,
                  })}
                  style={{
                    marginTop: accWorksDataList.length > 0 ? 10 : 0,
                    cursor: "pointer",
                  }}
                  onClick={() => setWorksModal(true)}
                >
                  <Col>
                    <SVGAdd />
                  </Col>

                  <Col style={{ marginLeft: 10 }}>
                    {t("myAccount.field2.add2")}
                  </Col>
                </Row>
              </div>
            </Col>
          )}
        </Row>

        <div className={classNames(styles.menuContainer3)}>
          <Row gutter={[40, rwd.device === "mobile" ? 15 : 20]}>
            <Col {...doubleColProps}>
              <TLink href="/user/match">
                <div className={classNames(styles.menu)}>
                  <SVGMatch />
                  <div>{t("myAccount.btn1")}</div>
                  <SVGArrow />
                </div>
              </TLink>
            </Col>

            <Col {...doubleColProps}>
              <TLink href="/user/favorite">
                <div className={classNames(styles.menu)}>
                  <SVGFavorite />
                  <div>{t("myAccount.btn2")}</div>
                  <SVGArrow />
                </div>
              </TLink>
            </Col>

            <Col {...doubleColProps}>
              <TLink href="/user/activity">
                <div className={classNames(styles.menu)}>
                  <SVGActivity />
                  <div>{t("myAccount.btn4")}</div>
                  <SVGArrow />
                </div>
              </TLink>
            </Col>

            <Col {...doubleColProps}>
              <TLink href="/user/informationProvided">
                <div className={classNames(styles.menu)}>
                  <SVGResources />
                  <div>{t("myAccount.btn5")}</div>
                  <SVGArrow />
                </div>
              </TLink>
            </Col>
          </Row>
        </div>

        <Modal
          visible={companionModal}
          footer={null}
          onCancel={() => {
            setCompanionModal(false);
          }}
          width={800}
          closeIcon={<SVGLBX className={styles.lightboxClose} />}
        >
          <div className={styles.noticeContainer}>
            <div className={styles.noticeTitle}>{t("companion.add.title")}</div>
            <Form layout="vertical" onFinish={handleAddCompanionFinish}>
              <Form.Item noStyle shouldUpdate>
                {({ setFields }) => {
                  return (
                    <Form.Item
                      required
                      label={t("companion.add.field1")}
                      name="unit"
                      initialValue={"1"}
                      rules={[]}
                    >
                      <Radio.Group
                        style={{ width: "100%" }}
                        onChange={() =>
                          setFields([
                            {
                              name: "name_en",
                              errors: [],
                            },
                            {
                              name: "name_zh",
                              errors: [],
                            },
                          ])
                        }
                      >
                        <Row gutter={[0, 10]}>
                          {mechanismOptions.map((e) => (
                            <Col
                              span={rwd.device === "mobile" ? 8 : 6}
                              key={e.value}
                            >
                              <Radio value={e.value}>{e.label}</Radio>
                            </Col>
                          ))}
                        </Row>
                      </Radio.Group>
                    </Form.Item>
                  );
                }}
              </Form.Item>
              <Form.Item noStyle shouldUpdate>
                {({ getFieldValue }) => {
                  const unitValue = getFieldValue("unit");
                  {
                    if (
                      unitValue === "1" ||
                      unitValue === "2" ||
                      unitValue === "3"
                    ) {
                      return (
                        <>
                          <Form.Item
                            required
                            label={t("companion.add.identity1.field2")}
                          >
                            <Form.Item
                              name="name_en"
                              rules={[
                                {
                                  required: true,
                                  message: t("companion.add.required", {
                                    label: t("companion.add.identity1.field2"),
                                  }),
                                },
                              ]}
                              noStyle
                            >
                              <Input />
                            </Form.Item>
                            <div className={classNames(styles.emailNotice)}>
                              <SVGQustion2 />
                              {t("companion.add.identity1.field2.memo")}
                            </div>
                          </Form.Item>

                          <Form.Item
                            label={t("companion.add.identity1.field3")}
                            name="name_zh"
                            rules={[]}
                          >
                            <Input />
                          </Form.Item>
                        </>
                      );
                    } else {
                      return (
                        <>
                          <Form.Item
                            required
                            label={t("companion.add.identity2.field2")}
                            name="name_en"
                            rules={[
                              {
                                required: true,
                                message: t("companion.add.required", {
                                  label: t(
                                    "companion.add.identity2.field2.placeholder1"
                                  ),
                                }),
                              },
                            ]}
                            shouldUpdate
                          >
                            <Input
                              placeholder={t(
                                "companion.add.identity2.field2.placeholder1"
                              )}
                            />
                          </Form.Item>

                          <Form.Item
                            required
                            name="lastname_en"
                            rules={[
                              {
                                required: true,
                                message: t("companion.add.required", {
                                  label: t(
                                    "companion.add.identity2.field2.placeholder2"
                                  ),
                                }),
                              },
                            ]}
                          >
                            <Input
                              placeholder={t(
                                "companion.add.identity2.field2.placeholder2"
                              )}
                            />
                          </Form.Item>

                          {locale === "zh" && (
                            <Form.Item
                              label={t("companion.add.identity2.field3")}
                              name="name_zh"
                              rules={[]}
                            >
                              <Input
                                placeholder={t(
                                  "companion.add.identity2.field3.placeholder"
                                )}
                              />
                            </Form.Item>
                          )}
                        </>
                      );
                    }
                  }
                }}
              </Form.Item>
              <div className={classNames(styles.customSubmit)}>
                <div className={classNames(styles.submit)}>
                  {errorCompanionModal && (
                    <div style={{ color: "red" }}>{errorCompanionModal}</div>
                  )}
                  <FancyButton>{t("companion.add.submit")}</FancyButton>
                </div>
              </div>
            </Form>

            <div className={classNames(styles.noticeBlock)}>
              <div
                className={classNames(styles.noticeTag)}
                style={{
                  left: locale === "zh" ? "-55px" : "-90px",
                }}
              >
                {t("companion.add.desc.tip")}
              </div>
              <ol className={classNames(styles.noticeContent)}>
                <li>{t("companion.add.desc1")}</li>
                <li>{t("companion.add.desc2")}</li>
              </ol>
            </div>
          </div>
        </Modal>

        <Modal
          visible={worksModal}
          footer={null}
          onCancel={() => {
            setWorksModal(false);
          }}
          width={800}
          closeIcon={<SVGLBX className={styles.lightboxClose} />}
        >
          <div className={styles.noticeContainer}>
            <div className={styles.noticeTitle}>{t("works.add.title")}</div>
            <Form layout="vertical" onFinish={handleAddWorkFinish}>
              <Form.Item
                required
                name="name_en"
                label={t("works.add.field1")}
                rules={[
                  {
                    required: true,
                    message: t("works.add.required", {
                      label: t("works.add.field1"),
                    }),
                  },
                  {
                    message: t("works.add.error.msg1"),
                    max: 60,
                  },
                ]}
              >
                <Input placeholder={t("works.add.field1")} />
              </Form.Item>

              <Form.Item
                label={t("works.add.field2")}
                name="name_zh"
                rules={[
                  {
                    message: t("works.add.error.msg1"),
                    max: 25,
                  },
                ]}
              >
                <Input placeholder={t("works.add.field2")} />
              </Form.Item>
              <div className={classNames(styles.customSubmit)}>
                <div className={classNames(styles.submit)}>
                  {errorWorksModal && (
                    <div style={{ color: "red" }}>{errorWorksModal}</div>
                  )}
                  <FancyButton>{t("works.add.submit")}</FancyButton>
                </div>
              </div>
            </Form>
          </div>
        </Modal>
      </section>

      <CommonLightBox
        className={classNames(styles.removeLightBox)}
        descClass={classNames(styles.desc)}
        button2Class={classNames(styles.button2)}
        desc={t("myAccount.remove.desc")}
        button1={t("myAccount.remove.submit1")}
        showButton2={true}
        button2={t("myAccount.remove.submit2")}
        button1Class={classNames(styles.button1)}
        showModal={removeAlertModal}
        setShowModal={setRemoveAlertModal}
        onClose2={() => {
          if (removeItem.current.category === "companion") {
            handleDeleteCompanion(removeItem.current.id);
          } else {
            handleDeleteWork(removeItem.current.id);
          }
        }}
      />

      <Footer />
    </>
  );
}
MyAccount.headerProps = {};
export async function getServerSideProps(context) {
  const cookies = context.req.cookies;
  const { isLogin } = auth(cookies);
  if (!isLogin) {
    return {
      redirect: {
        destination: "/signin",
        permanent: false,
      },
    };
  }

  let email = "";
  let identity = "";
  let authKey = "";
  let memberId = "";

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    email = authData.email;
    identity = authData.identity;
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  const handleUnitList = async () => {
    let unitDataList = [];

    const { resu, data } = await unitList({
      auth_key: authKey,
      memberm1_no: memberId,
      type: 1,
    });
    if (resu === 1) {
      unitDataList = data;
    }
    return unitDataList;
  };

  const handleAccWorksList = async () => {
    let accWorksDataList = [];

    const { resu, data } = await accWorksList({
      auth_key: authKey,
      memberm1_no: memberId,
    });
    if (resu === 1) {
      accWorksDataList = data;
    }
    return accWorksDataList;
  };

  const handleSourceItem = async () => {
    let formItems = {};
    const { resu, data } = await sourceItem({
      locale: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const [formItems, unitDataList, accWorksDataList] = await Promise.all([
    handleSourceItem(),
    handleUnitList(),
    handleAccWorksList(),
  ]);

  return {
    props: {
      email,
      identity,
      memberId,
      authKey,
      unitDataList,
      accWorksDataList,
      formItems,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
