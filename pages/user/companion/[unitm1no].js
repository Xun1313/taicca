/* eslint-disable @next/next/no-img-element */
import React, {
  useCallback,
  useMemo,
  useRef,
  useState,
  useEffect,
  useContext,
} from "react";

import Cropper from "react-cropper";
import "cropperjs/dist/cropper.min.css";
import styles from "@/styles/Companion.module.scss";
import { Footer } from "@/components/Footer/Footer";
import {
  Form,
  Input,
  Radio,
  Row,
  Col,
  Checkbox,
  Select,
  Upload,
  InputNumber,
  Tabs,
  Tag,
  Button,
  message,
  Alert,
} from "antd";

const { Option } = Select;
const { TextArea } = Input;
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";

import SVGClose from "@/svgs/close.svg";
import SVGQustion2 from "@/svgs/question2.svg";
import SVGFacebook from "@/svgs/facebook.svg";
import SVGLinkedin from "@/svgs/linkedin.svg";
import SVGTwitter from "@/svgs/twitter.svg";
import SVGMemberDefaultIcon from "@/svgs/memberDefaultIcon.svg";
import SVGAlert2 from "@/svgs/alert2.svg";
import { UserStep } from "@/components/UserStep/UserStep";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import {
  unitList,
  unitOne,
  unitModify,
  unitModify2,
  unitWorks,
  getCountry,
  sourceItem,
  unitAddpartner,
  unitPickworks,
} from "@/api";
import router, { useRouter } from "next/router";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { Loading } from "@/components/Loading/Loading";
import { FormSuccess } from "@/components/FormSuccess/FormSuccess";
import { auth } from "@/utilits/auth";
import { parseCookies } from "@/utilits/parseCookies";
import { parseItems, parseItems2 } from "@/utilits/parseItems";
import { LoadingContext } from "@/context/LoadingContext";
import { CommonLightBox } from "@/components/CommonLightBox/CommonLightBox";
import { CommonMsg } from "@/components/CommonMsg/CommonMsg";
import { ApplyUnitPopup } from "@/components/ApplyPopup/ApplyPopup";

const usePreview = () => {
  const [previewImage, setPreviewImage] = useState(null);
  const compressUploadImage = (src, fileName, callback) => {
    const baseWidth = 550;
    const quality = 0.8;
    const mime = "image/jpeg";
    const img = new Image();
    img.src = src;
    img.onload = () => {
      const scale = img.width / baseWidth;
      const height = img.height / scale;
      const canvas = document.createElement("canvas");
      canvas.width = baseWidth;
      canvas.height = height;
      const ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0, baseWidth, height);
      const url = ctx.canvas.toDataURL(mime, quality);
      ctx.canvas.toBlob(
        (blob) => {
          const file = new File([blob], fileName, {
            type: mime,
            lastModified: Date.now(),
          });
          if (callback) callback(file, url);
        },
        mime,
        quality
      );
    };
  };

  const previewUploadImage = (img, callback) => {
    const fileName = img.name;
    const reader = new FileReader();
    reader.addEventListener("load", (event) => {
      compressUploadImage(reader.result, fileName, callback);
    });
    reader.readAsDataURL(img);
  };

  const handleUploadImageChange = (info) => {
    previewUploadImage(info.file.originFileObj, (file, url) => {
      setPreviewImage(url);
      // setUploadImage(file);
    });
  };

  return {
    previewImage,
    handleUploadImageChange,
  };
};

const MultipleSelect1 = ({
  rwd,
  name,
  label,
  rules,
  className,
  required,
  checkMode,
  placeholder,
  options,
  limit,
  showCount,
  initialValue,
}) => {
  const [value, setValue] = useState(initialValue); // { value: "1", label: "創作方" }
  const closeList = useRef({});

  const handleChange = (option) => {
    setValue(option);
    // if (value.find((e) => e.value === option[option.length - 1].value)) {
    //   setValue((prev) => prev.filter((e) => e.value !== option[option.length - 1].value));
    // } else {
    //   if (value.length < limit) {
    //     setValue((prev) => [...prev, option[option.length - 1]]);
    //   } else {

    //   }
    // }
  };

  // const handleChange = (option) => {
  //   setValue((prev) => {
  //     if (prev.length < limit) {
  //       if (prev.find((e) => e.value === option.value)) {
  //         return prev;
  //       } else {
  //         return [...prev, option];
  //       }
  //     } else {
  //       return prev;
  //     }
  //   });
  // };

  const onTagClose = (event, option) => {
    closeList.current[option.value]();
    // event.preventDefault();
    setValue((prev) => prev.filter((e) => e.value !== option.value));
  };

  // return (
  //   <Form.Item
  //     required={required}
  //     label={label}
  //     name={name}
  //     rules={rules}
  //     className={className}
  //   >
  //     <Select
  //       options={options}
  //       value={[]}
  //     />
  //   </Form.Item>
  // );
  const tagRender = (props) => {
    const { label, value, closable, onClose } = props;
    closeList.current[value] = onClose;
    const onPreventMouseDown = (event) => {
      event.preventDefault();
      event.stopPropagation();
    };
    /* <Tag
        className={"customTag"}
        onMouseDown={onPreventMouseDown}
        closable={true}
        onClose={onClose}
        style={{
          marginRight: 3,
        }}
      >
        {label}
      </Tag> */
    return <div></div>;
  };
  return (
    <Row gutter={[33, 8]} align="bottom" style={{ marginBottom: 24 }}>
      <Col span={rwd.device === "mobile" ? 24 : 10}>
        <Form.Item noStyle shouldUpdate={() => checkMode}>
          {({ getFieldValue }) => {
            const currentValue = getFieldValue(name);
            return (
              <>
                <Form.Item
                  required={required}
                  label={label}
                  name={name}
                  rules={rules}
                  className={className}
                  initialValue={value}
                  style={{ marginBottom: 0 }}
                >
                  <Select
                    mode="multiple"
                    showSearch
                    showArrow
                    disabled={checkMode}
                    onChange={handleChange}
                    value={value}
                    tagRender={tagRender}
                    placeholder={placeholder}
                    optionFilterProp="label"
                    optionLabelProp="label"
                    labelInValue={true}
                    open={value.length === limit ? false : undefined}
                    options={options.filter((o) =>
                      !checkMode
                        ? true
                        : currentValue?.findIndex((c) => c === o.value) !==
                            -1 || []
                    )}
                  />
                  {/* 從 disabled 去擋其他選項 */}
                  {/* 一樣用 tagRender custom */}
                  {/* {
                    if (有在 value 裡) {
                      disabled : false
                    } else {
                      if (還在 Limit 之內) {
                        disabled: false
                      } else {
                        disabled: true
                      }
                    }
                  } */}
                </Form.Item>
              </>
            );
          }}
        </Form.Item>
      </Col>

      <Col span={rwd.device === "mobile" ? 24 : 14}>
        <Row align="middle" wrap={false}>
          <div>
            {value.map((e) => (
              <Tag
                className={"customTag"}
                closable
                key={e.value}
                onClose={(event) => onTagClose(event, e)}
              >
                {e.label}
              </Tag>
            ))}
          </div>

          {showCount && (
            <div>
              <Tag color={value.length === limit && "#e53c2b"}>
                &nbsp;{value.length}&nbsp;/&nbsp;{limit}&nbsp;
              </Tag>
            </div>
          )}
        </Row>
      </Col>
    </Row>
  );
};

const MultipleSelect2 = ({
  rwd,
  name,
  label,
  label2,
  rules,
  className,
  required,
  checkMode,
  placeholder,
  options,
  limit,
  initialValue,
}) => {
  const doubleColProps = useMemo(
    () => ({
      span: rwd.device === "mobile" ? 24 : 12,
    }),
    [rwd.device]
  );

  const [value, setValue] = useState(initialValue);
  const closeList = useRef({});

  const handleChange = (option) => {
    console.log(option);
    setValue(option);
  };

  const onItemClose = (option) => {
    closeList.current[option.value]();
    setValue((prev) => prev.filter((e) => e.value !== option.value));
  };

  const tagRender = (props) => {
    const { label, value, closable, onClose } = props;
    closeList.current[value] = onClose;
    const onPreventMouseDown = (event) => {
      event.preventDefault();
      event.stopPropagation();
    };
    return (
      // <Tag
      //   className={"customTag"}
      //   onMouseDown={onPreventMouseDown}
      //   closable={true}
      //   onClose={onClose}
      //   style={{
      //     marginRight: 3,
      //   }}
      // >
      //   {label}
      // </Tag>
      <div></div>
    );
  };

  return (
    <Row>
      <Col span={rwd.device === "mobile" ? 24 : 16}>
        <Form.Item
          required={required}
          label={
            <div
              style={{
                display: rwd.device === "mobile" ? "block" : "flex",
                alignItems: "center",
              }}
            >
              <div style={{ marginRight: 8 }}>{label}</div>
              <div
                style={{ color: "#999999", fontSize: 13, fontWeight: "normal" }}
              >
                {label2}
              </div>
            </div>
          }
          name={name}
          rules={rules}
          className={className}
          initialValue={value}
        >
          <Select
            mode="multiple"
            showSearch
            showArrow
            disabled={checkMode}
            onChange={handleChange}
            value={value}
            tagRender={tagRender}
            placeholder={placeholder}
            labelInValue={true}
            optionFilterProp="title"
            open={value.length === limit ? false : undefined}
          >
            {options.map((e) => (
              <Option
                title={`${e.name_en} ${e.lastname_en} ${e.name_zh}`}
                value={e.id}
                key={e.value}
              >
                <Row align="middle">
                  {e.pic ? (
                    <img
                      src={e.pic}
                      alt="icon"
                      style={{
                        width: 50,
                        height: "auto",
                        marginRight: 15,
                        borderRadius: "50%",
                      }}
                    />
                  ) : (
                    <SVGMemberDefaultIcon
                      style={{ width: 50, height: 50, marginRight: 15 }}
                    />
                  )}
                  {`${e.name_en || ""} ${e.lastname_en || ""} ${
                    e.name_zh || ""
                  }`}
                </Row>
              </Option>
            ))}
          </Select>
        </Form.Item>
      </Col>

      <Col span={24}></Col>

      <Col span={24}>
        <Row gutter={[40, 20]}>
          {value.map((e, i) => (
            <Col {...doubleColProps} key={i}>
              <div className={styles.memberItem}>
                <SVGClose
                  style={{ cursor: "pointer" }}
                  onClick={() => onItemClose(e)}
                />
                <Row gutter={18} align="middle">
                  <Col span={"none"}>
                    {e.label.props.children[0].props.src ? (
                      <img
                        src={e.label.props.children[0].props.src}
                        alt="icon"
                        style={{
                          width: 50,
                          height: "auto",
                          borderRadius: "50%",
                        }}
                      />
                    ) : (
                      <SVGMemberDefaultIcon
                        style={{ width: 50, height: 50, marginRight: 15 }}
                      />
                    )}
                  </Col>
                  <Col span={"none"}>{e.label.props.children[1]}</Col>
                </Row>
              </div>
            </Col>
          ))}

          {Array.from(Array(limit - value.length), (e) => (
            <Col {...doubleColProps} key={e}>
              <div className={styles.memberItem}>
                <Row gutter={18}>
                  <Col span={"none"}></Col>
                  <Col span={"none"}></Col>
                </Row>
              </div>
            </Col>
          ))}

          <Col span={24}>
            <Row justify="end">
              <Tag color={value.length === limit && "#e53c2b"}>
                &nbsp;{value.length}&nbsp;/&nbsp;{6}&nbsp;
              </Tag>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

const CheckboxGroup = ({
  name,
  label,
  rules,
  checkMode,
  options,
  required,
  className,
  initialValue,
}) => {
  return (
    <Form.Item noStyle shouldUpdate={() => checkMode}>
      {({ getFieldValue }) => {
        const currentValue = getFieldValue(name);
        return (
          <Form.Item
            required={required}
            label={label}
            name={name}
            rules={rules}
            className={className}
            initialValue={initialValue}
          >
            <Checkbox.Group
              disabled={checkMode}
              className={styles.checkboxGroup}
              options={options.filter((o) =>
                !checkMode
                  ? true
                  : currentValue?.findIndex((c) => c === o.value) !== -1 || []
              )}
            />
          </Form.Item>
        );
      }}
    </Form.Item>
  );
};

const CropperImg = ({ unitOneData }) => {
  const { t } = useTranslation("common");
  const rwd = useRWD();

  const doubleColProps = useMemo(
    () => ({
      span: rwd.device === "mobile" ? 24 : 12,
    }),
    [rwd.device]
  );
  const maxWidth = 600;
  const maxHeight = 600;

  const [cropperDone, setCropperDone] = useState(!!unitOneData.pic_url);
  const [cropperFile, setCropperFile] = useState(!!unitOneData.pic_url);
  const [cropperUploading, setCropperUploading] = useState(false);
  const cropperDoneRef = useRef(unitOneData.pic_url || null); // for api
  const cropperRef = useRef(null);
  const onCrop = () => {
    const cropper = cropperRef.current?.cropper;
    cropperDoneRef.current = cropper
      .getCroppedCanvas()
      // .toDataURL("image/jpeg", 1);
      .toDataURL();
  };

  const handleChange = (info) => {
    if (info.file.status === "uploading") {
      setCropperUploading(true);
    } else if (info.file.status === "done") {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj);
    } else if (info.file.status === "error") {
      setCropperUploading(false);
    }
  };

  const getBase64 = (img) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      const image = new Image();
      image.src = reader.result;
      image.onload = function () {
        const canvas = document.createElement("canvas");
        const ctx = canvas.getContext("2d");

        let width = 0;
        let height = 0;

        let widthPercent = maxWidth / image.width; // ex: 1920 / 2010  0.955223 (v) -> it means the maximum we can reduce
        let heightPercent = maxHeight / image.height; // ex: 1080 / 1500  0.72

        if (widthPercent >= heightPercent) {
          width = image.width * widthPercent;
          height = image.height * widthPercent;
        } else {
          width = image.width * heightPercent;
          height = image.height * heightPercent;
        }

        canvas.width = width;
        canvas.height = height;

        ctx.drawImage(image, 0, 0, width, height);

        // const dataURL = canvas.toDataURL("image/jpeg", 0.8);
        const dataURL = canvas.toDataURL();
        setCropperFile(dataURL);
        setCropperUploading(false);
      };
    });
    reader.readAsDataURL(img);
  };

  const beforeUpload = (file) => {
    const isImg =
      file.type === "image/jpeg" ||
      file.type === "image/png" ||
      file.type === "image/webp";
    if (!isImg) {
      CommonMsg({
        type: "error",
        msg: t("companion.step1.field7.error1"),
      });
      return false;
    }

    // maybe need to trigger error msg

    // const isLt2M = file.size / 1024 / 1024 < 10;
    // if (!isLt2M) return false;
    // maybe need to trigger error msg

    return true;
  };

  return (
    <>
      <Form.Item noStyle shouldUpdate>
        {({ setFieldsValue, getFieldValue }) => {
          return (
            <>
              <Form.Item
                required
                name={
                  unitOneData.pic_url && cropperDone && cropperFile
                    ? getFieldValue("pic")
                      ? "pic"
                      : "pic_url"
                    : "pic"
                }
                label={t("companion.step1.field7")}
                rules={[]}
                initialValue={unitOneData.pic_url}
              >
                <div className={styles.cropperContainer}>
                  <Row
                    wrap={rwd.device === "mobile"}
                    align="middle"
                    gutter={38}
                  >
                    <Col
                      span={rwd.device === "mobile" ? 24 : 7}
                      order={rwd.device === "mobile" ? 2 : 1}
                      className={styles.descContainer}
                    >
                      <div className={styles.desc1}>
                        {t("companion.step1.field3")}
                      </div>
                      <div className={styles.desc2}>
                        {t("companion.step1.field7.desc")}
                      </div>
                    </Col>

                    <Col
                      span={rwd.device === "mobile" ? 24 : 17}
                      order={rwd.device === "mobile" ? 1 : 2}
                      className={styles.cropperBlock}
                    >
                      {/* 1. 600 * 600 */}
                      {/* 2. 上傳前 UI */}
                      {/* 3. 裁切圖像按鈕按下去後長怎樣? */}
                      {
                        // 有檔案 -> 1. 正在裁切, 2. 裁切完畢
                        // 沒檔案 -> 1. 還沒傳, 2. 上傳中
                      }
                      {cropperFile ? (
                        cropperDone ? (
                          <div className={styles.cropperBox}>
                            <img
                              className={styles.cropperDoneImg}
                              src={cropperDoneRef.current}
                              alt="icon"
                            />
                          </div>
                        ) : (
                          /* crop={onCrop} */
                          <Cropper
                            className={styles.cropperBox}
                            src={cropperFile}
                            ref={cropperRef}
                            autoCropArea={1}
                            aspectRatio={1 / 1}
                            zoomable={false}
                            zoomOnWheel={false}
                            zoomOnTouch={false}
                            movable={false}
                          />
                        )
                      ) : (
                        <Upload
                          className="customUpload"
                          accept="image/png,image/jpeg,image/webp"
                          showUploadList={false}
                          customRequest={({ onSuccess, onError, file }) => {
                            const img = new Image();
                            img.src = URL.createObjectURL(file);
                            img.onload = () => {
                              setFieldsValue({ pic: null });
                              if (
                                img.width < maxWidth ||
                                img.height < maxHeight
                              ) {
                                CommonMsg({
                                  type: "error",
                                  msg: t("companion.step1.field7.error2"),
                                });
                                URL.revokeObjectURL(img.src);
                                onError();
                              } else {
                                URL.revokeObjectURL(img.src);
                                onSuccess();
                              }
                            };
                          }}
                          beforeUpload={beforeUpload}
                          onChange={handleChange}
                        >
                          <div>
                            {cropperUploading ? (
                              <Loading />
                            ) : (
                              <Tag color="#999999">
                                {t("companion.step1.field3.btn1")}
                              </Tag>
                            )}
                          </div>
                        </Upload>
                      )}

                      {cropperFile && (
                        <div className={styles.btnGroup}>
                          <Button
                            onClick={() => {
                              setCropperDone(false);
                              setCropperFile(null);
                              cropperRef.current?.cropper?.destroy();
                              cropperDoneRef.current = null;
                              setFieldsValue({ pic: null });
                            }}
                          >
                            {t("companion.step1.field3.btn2")}
                          </Button>
                          {!cropperDone && (
                            <Button
                              onClick={() => {
                                onCrop();
                                cropperRef.current.cropper?.destroy();
                                setCropperDone(true);
                                setFieldsValue({
                                  pic: cropperDoneRef.current,
                                });
                              }}
                              style={{
                                backgroundColor: "#999999",
                                color: "white",
                              }}
                            >
                              {t("companion.step1.field3.btn3")}
                            </Button>
                          )}
                        </div>
                      )}
                    </Col>
                  </Row>
                </div>
              </Form.Item>
              {cropperFile && (
                <Row gutter={[22, 14]} className={styles.imgFields}>
                  <Col {...doubleColProps}>
                    <Form.Item
                      required
                      name="pic_title"
                      rules={[]}
                      initialValue={unitOneData.pic_title}
                      style={{ marginBottom: 0 }}
                    >
                      <Input
                        placeholder={t("companion.step1.field3.placeholder1")}
                      />
                    </Form.Item>
                  </Col>

                  <Col {...doubleColProps}>
                    <Form.Item
                      required
                      name="pic_alt"
                      rules={[]}
                      initialValue={unitOneData.pic_alt}
                    >
                      <Input
                        placeholder={t("companion.step1.field3.placeholder2")}
                      />
                    </Form.Item>
                  </Col>
                </Row>
              )}
            </>
          );
        }}
      </Form.Item>
    </>
  );
};

const Form1 = ({
  rwd,
  setShowLoading,
  showSaveMsgPopup,
  setShowSaveMsgPopup,
  saveStep,
  checkMode,
  onNext,
  formItems,
  unitOneData,
  memberId,
  authKey,
  unitm1no,
  SaveMsgPopup,
}) => {
  const router = useRouter();
  const { t } = useTranslation("common");
  const preview = usePreview();
  const [form1] = Form.useForm();

  const yearOptions = useMemo(() => {
    let i = new Date().getFullYear();
    let list = [];
    while (i > 1949) {
      list.push(i + "");
      i--;
    }
    return list;
  }, []);
  const industryOptions = parseItems(formItems.industry);
  const sectorOptions = parseItems(formItems.sector);
  const clienteleOptions = parseItems(formItems.clientele);
  const industryInitailOptions = parseItems2(
    unitOneData.industry,
    formItems.industry
  );
  const hopeSphereInitailOptions = parseItems2(
    unitOneData.hopeSphere,
    formItems.sector
  );

  const formRef = useRef();

  const handleCliclNext = useCallback(
    (values) => {
      const fieldData = JSON.parse(JSON.stringify(values));
      fieldData.industry = fieldData.industry.map((e) => e.value);
      fieldData.hopeSphere = fieldData.hopeSphere.map((e) => e.value);
      console.log(fieldData);

      setShowSaveMsgPopup(false);
      setShowLoading(true);
      const calcRequiredField = () => {
        let total = 0;
        if (!fieldData.intro_en) total = total + 1;
        if (!fieldData.pic && !fieldData.pic_url) total = total + 1;
        if (!fieldData.registration) total = total + 1;
        if (fieldData.industry.length === 0) total = total + 1;
        if (fieldData.hopeSphere.length === 0) total = total + 1;

        return Math.floor(((5 - total) / 5) * 100);
      };
      unitModify({
        auth_key: authKey,
        memberm1_no: memberId,
        Unitm1_no: unitm1no,
        step1: calcRequiredField(),
        ...fieldData,
      }).then((res) => {
        if (res.resu === 1) {
          onNext(res.data);
        } else {
        }

        CommonMsg({
          type: "success",
          msg: t("companion.message.save"),
        });

        const currentStep = showSaveMsgPopup ? saveStep.current : 2;
        setShowLoading(false);
        router.push(
          `${router.pathname.replace(
            "[unitm1no]",
            router.query.unitm1no
          )}?step=${currentStep}`
        );
      });
    },
    [onNext]
  );

  return (
    <>
      <Form
        ref={formRef}
        layout="vertical"
        onFinish={handleCliclNext}
        onFinishFailed={({ values, errorFields }) => {
          console.log(values, errorFields, formRef.current, 45465);
        }}
        scrollToFirstError={true}
        name="step1"
        form={form1}
      >
        <Row>
          <Col span={rwd.device === "mobile" ? 24 : 12}>
            <Form.Item
              name="year"
              label={t("companion.step1.field1")}
              rules={[]}
              initialValue={unitOneData.year}
            >
              <Select disabled={checkMode} placeholder="Year">
                {yearOptions.map((yearOption) => (
                  <Option key={yearOption} value={yearOption}>
                    {yearOption}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Form.Item required label={t("companion.step1.field2")}>
          <Tabs className={"customTab"}>
            <Tabs.TabPane tab={t("companion.step1.field2.tab1")} key="1">
              <Form.Item
                name="intro_en"
                required
                rules={[]}
                initialValue={unitOneData.intro_en}
              >
                <TextArea
                  disabled={checkMode}
                  showCount
                  maxLength={1000}
                  autoSize={{ minRows: 12 }}
                />
              </Form.Item>
            </Tabs.TabPane>

            <Tabs.TabPane tab={t("companion.step1.field2.tab2")} key="2">
              <Form.Item
                name="intro_zh"
                rules={[]}
                initialValue={unitOneData.intro_zh}
              >
                <TextArea
                  disabled={checkMode}
                  showCount
                  maxLength={500}
                  autoSize={{ minRows: 12 }}
                />
              </Form.Item>
            </Tabs.TabPane>
          </Tabs>
        </Form.Item>

        <CropperImg unitOneData={unitOneData} />

        <CheckboxGroup
          required
          label={t("companion.step1.field4")}
          name="registration"
          rules={[
            {
              type: "array",
              max: 2,
              message: t("companion.step1.required", {
                label: t("companion.step1.field4"),
              }),
            },
          ]}
          options={clienteleOptions}
          checkMode={checkMode}
          initialValue={unitOneData.registration}
        />

        <MultipleSelect1
          rwd={rwd}
          required
          label={t("companion.step1.field5")}
          name="industry"
          rules={
            [
              // {
              //   message: t("companion.step1.required", {
              //     label: t("companion.step1.field5"),
              //   }),
              //   validator: (_, value) => {
              //     if (value.length > 2) {
              //       return Promise.reject("Some message here");
              //     } else {
              //       return Promise.resolve();
              //     }
              //   },
              // },
            ]
          }
          options={industryOptions}
          checkMode={checkMode}
          placeholder={t("companion.step1.field5.placeholder")}
          limit={2}
          showCount={true}
          initialValue={industryInitailOptions}
        />

        <MultipleSelect1
          rwd={rwd}
          required
          label={t("companion.step1.field6")}
          name="hopeSphere"
          rules={
            [
              // {
              //   message: t("companion.step1.required", {
              //     label: t("companion.step1.field6"),
              //   }),
              //   validator: (_, value) => {
              //     if (value.length > 2) {
              //       return Promise.reject("Some message here");
              //     } else {
              //       return Promise.resolve();
              //     }
              //   },
              // },
            ]
          }
          options={sectorOptions}
          checkMode={checkMode}
          placeholder={t("companion.step1.field6.placeholder")}
          limit={4}
          showCount={true}
          initialValue={hopeSphereInitailOptions}
        />

        <div className={classNames(styles.customSubmit)}>
          <div className={classNames(styles.submit)}>
            <FancyButton htmlType="submit" scaleSize={38}>
              {t("companion.step1.submit")}
            </FancyButton>
          </div>
        </div>
      </Form>
    </>
  );
};

const Form2 = ({
  rwd,
  setShowLoading,
  showSaveMsgPopup,
  setShowSaveMsgPopup,
  saveStep,
  checkMode,
  onNext,
  onPrev,
  formItems,
  country,
  unitOneData,
  memberId,
  authKey,
  unitm1no,
  SaveMsgPopup,
}) => {
  const { locale } = useRouter();
  const { t } = useTranslation("common");
  const [form2] = Form.useForm();

  // const realmOptions = realmsMockBase.map((title, id) => ({
  //   label: title,
  //   value: `${id}`,
  // }));

  // const realmOptions = parseItems(formItems.sphere);
  // const servoceOptopns = parseItems(formItems.otherservice);
  // const targetsOptions = parseItems(formItems.unittype);
  // const categorysOptions = parseItems(formItems.clientele);
  const doubleColProps = useMemo(
    () => ({
      span: rwd.device === "mobile" ? 24 : 12,
    }),
    [rwd.device]
  );

  const handleCliclNext = useCallback(
    (values) => {
      console.log(values);

      setShowSaveMsgPopup(false);
      setShowLoading(true);
      const calcRequiredField = () => {
        let total = 0;
        if (!values.email) total = total + 1;
        if (!values.address) total = total + 1;
        if (!values.addressCountry) total = total + 1;

        return Math.floor(((3 - total) / 3) * 100);
      };

      unitModify2({
        auth_key: authKey,
        memberm1_no: memberId,
        Unitm1_no: unitm1no,
        step2: calcRequiredField(),
        ...values,
      }).then((res) => {
        if (res.resu === 1) {
          onNext(res.data);
        } else {
        }
        CommonMsg({
          type: "success",
          msg: t("companion.message.save"),
        });

        const currentStep = showSaveMsgPopup
          ? saveStep.current
          : unitOneData.unit === "4"
          ? 4
          : 3;
        setShowLoading(false);
        router.push(
          `${router.pathname.replace(
            "[unitm1no]",
            router.query.unitm1no
          )}?step=${currentStep}`
        );
      });
    },
    [onNext]
  );

  return (
    <>
      <Form
        name="step2"
        layout="vertical"
        onFinish={handleCliclNext}
        onFinishFailed={({ values, errorFields }) => {
          console.log(values, errorFields);
        }}
        scrollToFirstError
        form={form2}
      >
        <Row gutter={40}>
          <Col {...doubleColProps}>
            <Form.Item
              label={t("companion.step2.field1")}
              name="website"
              rules={[]}
              initialValue={unitOneData.website}
            >
              <Input placeholder={"https://"} />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item label={t("companion.step2.field2")}>
          <Row gutter={20}>
            <Col span={rwd.device === "mobile" ? 24 : 12}>
              <Form.Item name="facebook" initialValue={unitOneData.facebook}>
                <Input placeholder={`https://`} prefix={<SVGFacebook />} />
              </Form.Item>
            </Col>

            <Col span={rwd.device === "mobile" ? 24 : 12}>
              <Form.Item name="linkedin" initialValue={unitOneData.linkedin}>
                <Input placeholder={`https://`} prefix={<SVGLinkedin />} />
              </Form.Item>
            </Col>

            <Col span={rwd.device === "mobile" ? 24 : 12}>
              <Form.Item name="twitter" initialValue={unitOneData.twitter}>
                <Input placeholder={`https://`} prefix={<SVGTwitter />} />
              </Form.Item>
            </Col>
          </Row>
        </Form.Item>

        <Row gutter={20}>
          <Col span={rwd.device === "mobile" ? 24 : 12}>
            <Form.Item
              required
              label={t("companion.step2.field3")}
              name="email"
              rules={[{ type: "email", message: t("signin.error.msg7") }]}
              initialValue={unitOneData.email}
            >
              <Input placeholder={"Email"} />
            </Form.Item>
            <div className={classNames(styles.emailNotice)}>
              <SVGAlert2 />
              {t("companion.step2.msg1")}
            </div>
          </Col>
        </Row>

        <Form.Item label={t("companion.step2.field4")}>
          <Row gutter={20}>
            <Col span={rwd.device === "mobile" ? 24 : 8}>
              <Form.Item name="telcode" initialValue={unitOneData.telcode}>
                <Select
                  disabled={checkMode}
                  placeholder={t("companion.step2.field4.placeholder1")}
                  style={{ width: "100%" }}
                  showSearch
                  optionFilterProp="title"
                >
                  {country.map((c) => (
                    <Option
                      key={c.name}
                      value={c.phoneCode}
                      title={
                        locale === "en"
                          ? `${c.name} ${c.phoneCode}`
                          : `${c.traditionalName} ${c.phoneCode}`
                      }
                    >
                      {locale === "en"
                        ? `${c.name} ${c.phoneCode}`
                        : `${c.traditionalName} ${c.phoneCode}`}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
              <div className={classNames(styles.emailNotice)}>
                <SVGAlert2 />
                {t("companion.step2.msg2")}
              </div>
            </Col>

            <Col span={rwd.device === "mobile" ? 24 : 8}>
              <Form.Item name="tel" initialValue={unitOneData.tel}>
                <Input placeholder={t("companion.step2.field4.placeholder2")} />
              </Form.Item>
            </Col>
          </Row>
        </Form.Item>

        <Form.Item required label={t("companion.step2.field5")}>
          <Row gutter={20}>
            <Col span={rwd.device === "mobile" ? 24 : 8}>
              <Form.Item
                required
                name="addressCountry"
                rules={[]}
                initialValue={unitOneData.addressCountry}
              >
                <Select
                  disabled={checkMode}
                  placeholder={t("companion.step2.field5.placeholder1")}
                  style={{ width: "100%" }}
                  showSearch
                  optionFilterProp="title"
                >
                  {country.map((c) => (
                    <Option
                      key={c.name}
                      value={c.name}
                      title={locale === "en" ? c.name : c.traditionalName}
                    >
                      {locale === "en" ? c.name : c.traditionalName}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>

            <Col span={rwd.device === "mobile" ? 24 : 24}>
              <Form.Item
                required
                name="address"
                rules={[]}
                initialValue={unitOneData.address}
              >
                <Input placeholder={t("companion.step2.field5.placeholder3")} />
              </Form.Item>
              <div className={classNames(styles.emailNotice)}>
                <SVGQustion2 />
                {t("companion.step2.desc2")}
              </div>
            </Col>
          </Row>
        </Form.Item>

        <div className={classNames(styles.customSubmit)}>
          <div className={classNames(styles.submit)}>
            <FancyButton htmlType="submit" scaleSize={38}>
              {t("companion.step2.submit")}
            </FancyButton>
          </div>
        </div>
      </Form>
    </>
  );
};

const Form3 = ({
  rwd,
  setShowLoading,
  showSaveMsgPopup,
  setShowSaveMsgPopup,
  saveStep,
  onNext,
  onPrev,
  checkMode,
  formItems,
  unitOneData,
  memberId,
  authKey,
  unitm1no,
  SaveMsgPopup,
}) => {
  const { t } = useTranslation("common");
  const [unitDataList, setUnitDataList] = useState([]);
  const [isLoadingUnitDataList, setIsLoadingUnitDataList] = useState(true);
  const [form3] = Form.useForm();

  useEffect(() => {
    (async () => {
      setShowLoading(true);
      const { resu, data } = await unitList({
        memberm1_no: memberId,
        auth_key: authKey,
        unit: 4,
        type: 0,
      });
      if (resu === 1) {
        setUnitDataList(data);
      }
      setIsLoadingUnitDataList(false);
      setShowLoading(false);
    })();
  }, [authKey, memberId]);

  const initialValue = useMemo(() => {
    return unitOneData.unitt1.map((e) => {
      const data = unitDataList.find((f) => e.unitm1_no === f.id);
      if (data) {
        return {
          value: data.id,
          // label: [
          //   { props: { src: data.pic, alt: null } },
          //   `${data.name_en} ${data.lastname_en} ${data.name_zh}`,
          // ],
          label: {
            props: {
              children: [
                {
                  props: {
                    src: data.pic_min,
                  },
                },
                `${data.name_en || ""} ${data.lastname_en || ""} ${
                  data.name_zh || ""
                }`,
              ],
            },
          },
        };
      } else {
        return {
          value: null,
          // label: [{ props: { src: null, alt: null } }, null],
          label: {
            props: {
              children: [
                {
                  props: {
                    src: null,
                  },
                },
                null,
              ],
            },
          },
        };
      }
    });
  }, [unitOneData.unitt1, unitDataList]);

  const handleCliclNext = useCallback(
    (values) => {
      console.log(values);

      setShowSaveMsgPopup(false);
      setShowLoading(true);
      unitAddpartner({
        auth_key: authKey,
        memberm1_no: memberId,
        Unitm1_no: unitm1no,
        id: values.id.map((e) => e.value),
        step3: 100,
      }).then((res) => {
        if (res.resu === 1) {
          onNext();
        } else {
        }
        CommonMsg({
          type: "success",
          msg: t("companion.message.save"),
        });
        const currentStep = showSaveMsgPopup ? saveStep.current : 4;
        setShowLoading(false);
        router.push(
          `${router.pathname.replace(
            "[unitm1no]",
            router.query.unitm1no
          )}?step=${currentStep}`
        );
        //onNext(values);
      });
    },
    [onNext]
  );

  return (
    <>
      <Form
        name="step3"
        layout="vertical"
        onFinish={handleCliclNext}
        onFinishFailed={({ values, errorFields }) => {
          console.log(values, errorFields);
        }}
        scrollToFirstError
        form={form3}
      >
        {!isLoadingUnitDataList && (
          <MultipleSelect2
            rwd={rwd}
            required={false}
            label={t("companion.step3.field1")}
            label2={t("companion.step3.field1-2")}
            name="id"
            rules={[]}
            options={unitDataList}
            checkMode={checkMode}
            placeholder={t("companion.step3.field1.placeholder")}
            limit={6}
            initialValue={initialValue}
          />
        )}

        <div className={classNames(styles.customSubmit)}>
          <div className={classNames(styles.submit)}>
            <FancyButton htmlType="submit" scaleSize={38}>
              {t("companion.step3.submit")}
            </FancyButton>
          </div>
        </div>
      </Form>
    </>
  );
};

const Form4 = ({
  rwd,
  setShowLoading,
  showSaveMsgPopup,
  setShowSaveMsgPopup,
  saveStep,
  onSend,
  onPrev,
  checkMode,
  formItems,
  unitOneData,
  t,
  authKey,
  memberId,
  unitm1no,
  SaveMsgPopup,
}) => {
  const router = useRouter();

  const [unitWorksList, setUnitWorksList] = useState([]);
  const [form4] = Form.useForm();

  useEffect(() => {
    (async () => {
      setShowLoading(true);
      const { resu, data } = await unitWorks({
        auth_key: authKey,
        memberm1_no: memberId,
        Unitm1_no: unitm1no,
      });
      if (resu === 1) {
        setUnitWorksList(data);
      }
      setShowLoading(false);
    })();
  }, []);

  const initialValue = useMemo(
    () => unitWorksList.filter((e) => e.check).map((e) => e.workm1_no),
    [unitWorksList]
  );

  const handleCliclNext = useCallback(
    (values) => {
      console.log(values);

      setShowSaveMsgPopup(false);
      setShowLoading(true);
      unitPickworks({
        auth_key: authKey,
        memberm1_no: memberId,
        Unitm1_no: unitm1no,
        step4: 100,
        ...values,
      }).then((res) => {
        if (res.resu === 1) {
          if (values.review === "1") {
            router.push(
              `${router.pathname.replace(
                "[unitm1no]",
                router.query.unitm1no
              )}?step=5`
            );
          } else {
            CommonMsg({
              type: "success",
              msg: t("companion.message.save"),
            });
            const currentStep = showSaveMsgPopup ? saveStep.current : 4;
            router.replace(
              `${router.pathname.replace(
                "[unitm1no]",
                router.query.unitm1no
              )}?step=${currentStep}`
            );
          }
          onSend(values);
        }
        setShowLoading(false);
        //onNext(values);
      });
    },
    [onSend]
  );

  const handleSuccessApply = () => {
    form4.setFieldsValue({
      review: "1",
    });
    handleCliclNext(form4.getFieldValue());
  };

  return (
    <>
      <Form
        name="step4"
        layout="vertical"
        onFinish={handleCliclNext}
        scrollToFirstError
        className={classNames(styles.contentContainer2, "companionPage")}
        form={form4}
      >
        {unitWorksList.length > 0 ? (
          <>
            <div className={classNames(styles.mainTitle)}>
              {t("companion.step4.field1")}
            </div>

            <Form.Item name="workm1_no" initialValue={initialValue}>
              <Checkbox.Group style={{ width: "100%" }}>
                <Row gutter={[42, rwd.device === "mobile" ? 30 : 42]}>
                  {unitWorksList.map((e) => (
                    <Col
                      span={rwd.device === "mobile" ? 24 : 8}
                      key={e.workm1_no}
                    >
                      <Form.Item noStyle shouldUpdate>
                        {({ getFieldValue }) => {
                          const checkedList = getFieldValue("workm1_no");

                          return (
                            <div
                              className={classNames(styles.card, {
                                [styles.selected]: checkedList.includes(
                                  e.workm1_no
                                ),
                              })}
                            >
                              <img src={e.pic} alt="icon" />
                              <div className={classNames(styles.cardContent)}>
                                <div className={classNames(styles.cardTitle)}>
                                  {e.name_en}
                                </div>
                                <div
                                  className={classNames(styles.cardSubTitle)}
                                >
                                  {e.name_zh}
                                </div>
                                <Row justify={"end"}>
                                  <Checkbox value={e.workm1_no} />
                                </Row>
                              </div>
                            </div>
                          );
                        }}
                      </Form.Item>
                    </Col>
                  ))}
                </Row>
              </Checkbox.Group>
            </Form.Item>
          </>
        ) : (
          <>
            {rwd.device !== "mobile" && (
              <div className={classNames(styles.step4Title1)}>
                {t("companion.step4.field2")}
              </div>
            )}
            <Row gutter={[48, 40]}>
              {Array.from(Array(rwd.device === "mobile" ? 1 : 6), (e) => (
                <Col span={rwd.device === "mobile" ? 24 : 8} key={e}>
                  <div className={classNames(styles.step4DefaultBlock)}>
                    {rwd.device === "mobile" && (
                      <div className={classNames(styles.step4Title1)}>
                        {t("companion.step4.field2")}
                      </div>
                    )}
                  </div>
                </Col>
              ))}
            </Row>
          </>
        )}

        <Row gutter={[18, 18]} className={styles.step4BtnGroup}>
          <Col
            span={rwd.device === "mobile" ? 24 : unitOneData.isfinish ? 6 : 12}
          >
            <FancyButton
              scaleSize={38}
              htmlType="button"
              className={classNames(styles.step4Btn)}
              onClick={() =>
                router.push(
                  `/professionals/${unitOneData.Unitm1_no}/${unitOneData.name_en}?preview=1`
                )
              }
            >
              {t("companion.step4.btn1")}
            </FancyButton>
          </Col>

          <Col
            span={rwd.device === "mobile" ? 24 : unitOneData.isfinish ? 6 : 12}
          >
            <Form.Item name="review" noStyle>
              <Form.Item
                noStyle
                shouldUpdate={(prevValues, curValues) =>
                  prevValues.review !== curValues.review
                }
              >
                {({ setFieldsValue }) => {
                  return (
                    <FancyButton
                      htmlType="submit"
                      scaleSize={38}
                      className={classNames(styles.step4Btn)}
                      onClick={() => {
                        setFieldsValue({
                          review: null,
                        });
                      }}
                    >
                      {t("companion.step4.btn3")}
                    </FancyButton>
                  );
                }}
              </Form.Item>
            </Form.Item>
          </Col>

          {unitOneData.isfinish && (
            <Col span={rwd.device === "mobile" ? 24 : 12}>
              <ApplyUnitPopup
                onSuccess={handleSuccessApply}
                SubmitBtn={({ text }) => (
                  <FancyButton
                    selected
                    htmlType="button"
                    scaleSize={38}
                    className={classNames(styles.step4Btn)}
                  >
                    {text}
                  </FancyButton>
                )}
              />
            </Col>
          )}
        </Row>
      </Form>
    </>
  );
};

const FormSuccessTitle = (t) => {
  return (
    <>
      <div>{t("companion.success.title1")}</div>
      <div>{t("companion.success.title2")}</div>
    </>
  );
};

export default function Companion({
  formItems,
  country,
  authKey,
  memberId,
  identity,
  unitOneData,
  unitm1no,
  step,
}) {
  console.log(unitOneData);
  const router = useRouter();
  const locale = router.locale;
  const rwd = useRWD();
  const { t } = useTranslation("common");
  const { setShowLoading } = useContext(LoadingContext);
  const [showSaveMsgPopup, setShowSaveMsgPopup] = useState(false);
  const saveStep = useRef(1);
  const handleProfileIcon = () => {
    if (rwd.device !== "mobile") {
      if (unitOneData.pic_min) {
        return (
          <img
            src={unitOneData.pic_min}
            width="80px"
            height="80px"
            alt="icon"
          />
        );
      } else {
        return <SVGMemberDefaultIcon style={{ width: 80, height: 80 }} />;
      }
    }
  };

  const SaveMsgPopup = ({ handleSubmit }) => {
    return (
      <CommonLightBox
        containerClass={classNames(styles.container)}
        titleClass={classNames(styles.title)}
        descClass={classNames(styles.desc)}
        button2Class={classNames(styles.button2)}
        desc={t("companion.changes.desc1")}
        button1={t("companion.changes.btn1")}
        showButton2={true}
        button2={t("companion.changes.btn2")}
        showModal={showSaveMsgPopup}
        setShowModal={setShowSaveMsgPopup}
        onClose2={() => {
          //setShowSaveMsgPopup(false);
          handleSubmit();
        }}
      />
    );
  };

  const handleClickStep = (value) => {
    setShowSaveMsgPopup(true);
    saveStep.current = value;
    console.log(value);
  };

  return (
    <>
      <HeaderBannerNew1
        title={t("meun.memberCenter")}
        showBack={true}
        backLink="/user/myAccount"
        backText={t("myAccount.title")}
      />

      {step !== 5 && (
        <div className={classNames(styles.contentContainer, "companionPage")}>
          <div className={classNames(styles.head)}>
            {handleProfileIcon()}
            <div className={classNames(styles.headBlock)}>
              <div className={classNames(styles.desc)}>
                {formItems.mechanism[unitOneData.unit]}
              </div>
              <div className={classNames(styles.name)}>
                {`${unitOneData.name_en || ""} ${
                  unitOneData.lastname_en || ""
                } ${unitOneData.name_zh || ""}`}
              </div>
            </div>
          </div>

          <UserStep
            step={step}
            isMoble={rwd.device === "mobile"}
            className={styles.steps}
            t={t}
            stepList={[
              {
                title: t("companion.step1.progress"),
                show: true,
                progress: unitOneData.step1,
                link: `${router.pathname.replace(
                  "[unitm1no]",
                  router.query.unitm1no
                )}?step=1`,
                onClick: handleClickStep,
              },
              {
                title: t("companion.step2.progress"),
                show: true,
                progress: unitOneData.step2,
                link: `${router.pathname.replace(
                  "[unitm1no]",
                  router.query.unitm1no
                )}?step=2`,
                onClick: handleClickStep,
              },
              {
                title: t("companion.step3.progress"),
                show: unitOneData.unit !== "4",
                progress: unitOneData.step3,
                link: `${router.pathname.replace(
                  "[unitm1no]",
                  router.query.unitm1no
                )}?step=3`,
                onClick: handleClickStep,
              },
              {
                title: t("companion.step4.progress"),
                show: true, //identity === "1"
                progress: unitOneData.step4,
                link: `${router.pathname.replace(
                  "[unitm1no]",
                  router.query.unitm1no
                )}?step=4`,
                onClick: handleClickStep,
              },
            ]}
          />

          <div>
            {step === 1 && (
              <Form1
                rwd={rwd}
                setShowLoading={setShowLoading}
                showSaveMsgPopup={showSaveMsgPopup}
                setShowSaveMsgPopup={setShowSaveMsgPopup}
                SaveMsgPopup={SaveMsgPopup}
                saveStep={saveStep}
                checkMode={step === 3}
                formItems={formItems}
                unitOneData={unitOneData}
                authKey={authKey}
                memberId={memberId}
                unitm1no={unitm1no}
                onNext={() => {}}
              />
            )}

            {step === 2 && (
              <Form2
                rwd={rwd}
                setShowLoading={setShowLoading}
                showSaveMsgPopup={showSaveMsgPopup}
                setShowSaveMsgPopup={setShowSaveMsgPopup}
                SaveMsgPopup={SaveMsgPopup}
                saveStep={saveStep}
                checkMode={step === 3}
                formItems={formItems}
                unitOneData={unitOneData}
                authKey={authKey}
                memberId={memberId}
                unitm1no={unitm1no}
                country={country}
                onNext={() => {}}
              />
            )}

            {step === 3 && (
              <Form3
                rwd={rwd}
                setShowLoading={setShowLoading}
                showSaveMsgPopup={showSaveMsgPopup}
                setShowSaveMsgPopup={setShowSaveMsgPopup}
                SaveMsgPopup={SaveMsgPopup}
                saveStep={saveStep}
                checkMode={false}
                formItems={formItems}
                unitOneData={unitOneData}
                authKey={authKey}
                memberId={memberId}
                unitm1no={unitm1no}
                onNext={() => {}}
              />
            )}
          </div>
        </div>
      )}

      {step === 4 && (
        <Form4
          rwd={rwd}
          setShowLoading={setShowLoading}
          showSaveMsgPopup={showSaveMsgPopup}
          setShowSaveMsgPopup={setShowSaveMsgPopup}
          SaveMsgPopup={SaveMsgPopup}
          saveStep={saveStep}
          checkMode={false}
          t={t}
          authKey={authKey}
          memberId={memberId}
          unitOneData={unitOneData}
          unitm1no={unitm1no}
          onSend={() => {}}
        />
      )}

      {step === 5 && (
        <FormSuccess
          title={FormSuccessTitle(t)}
          desc={t("companion.success.desc")}
        />
      )}

      <Footer />
    </>
  );
}
Companion.headerProps = {};
export async function getServerSideProps(context) {
  const { locale, query } = context;
  console.log(query, "query");

  const cookies = context.req.cookies;
  const { isLogin, redirect } = auth(cookies);
  if (!isLogin) return redirect;

  let email = "";
  let identity = "";
  let authKey = "";
  let memberId = "";
  const unitm1no = context.params.unitm1no;

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    email = authData.email;
    identity = authData.identity;
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  const handleSourceItem = async () => {
    let formItems = {};
    const { resu, data } = await sourceItem({
      locale: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const handleUnitOne = async () => {
    let unitOneData = {};

    const { resu, data } = await unitOne({
      auth_key: authKey,
      memberm1_no: memberId,
      Unitm1_no: unitm1no,
    });
    if (resu === 1) {
      unitOneData = data;
    }
    return unitOneData;
  };

  const handleGetCountry = async () => {
    let country = {};

    const { resu, data } = await getCountry({
      locale,
    });
    if (resu === 1) {
      country = data.country;
    }
    return country;
  };

  const [formItems, unitOneData, country] = await Promise.all([
    handleSourceItem(),
    handleUnitOne(),
    handleGetCountry(),
  ]);

  return {
    props: {
      formItems,
      country,
      authKey,
      memberId,
      identity,
      unitOneData,
      unitm1no,
      step: Number(query.step) || 1,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
