/* eslint-disable @next/next/no-img-element */
import { PageInfoBasic } from "@/components/PageInfo/PageInfo";
import React, {
  useCallback,
  useMemo,
  useRef,
  useState,
  useEffect,
  useContext,
  useReducer,
} from "react";

import Cropper from "react-cropper";
import "cropperjs/dist/cropper.min.css";
import styles from "@/styles/Works.module.scss";
import { Footer } from "@/components/Footer/Footer";
import {
  Form,
  Input,
  Radio,
  Row,
  Col,
  Checkbox,
  Select,
  Upload,
  InputNumber,
  Tabs,
  Tag,
  Button,
  Popover,
  Modal,
} from "antd";

const { Option } = Select;
const { TextArea } = Input;
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";

import SVGClose from "@/svgs/close.svg";
import SVGAdd from "@/svgs/add.svg";
import SVGEdit from "@/svgs/edit.svg";
import SVGRemove from "@/svgs/remove.svg";
import SVGMemberDefaultIcon from "@/svgs/memberDefaultIcon.svg";
import SVGFilesFolders from "@/svgs/filesFolders.svg";
import SVGPdfFile from "@/svgs/pdfFile.svg";
import SVGWorkEdit from "@/svgs/workEdit.svg";
import SVGLBX from "@/svgs/close.svg";
import SVGAlert2 from "@/svgs/alert2.svg";
import { UserStep } from "@/components/UserStep/UserStep";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import {
  accWorksOne,
  getCountry,
  sourceItem,
  sourceIndex,
  accWorksRename,
  accWorksModifyst2,
  accWorksModifyst3,
  accWorksModifyst4,
  accWorksModifyst5,
  unitList,
} from "@/api";
import router, { useRouter } from "next/router";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { Loading } from "@/components/Loading/Loading";
import { FormSuccess } from "@/components/FormSuccess/FormSuccess";
import { auth } from "@/utilits/auth";
import { parseCookies } from "@/utilits/parseCookies";
import { parseItems, parseItems2 } from "@/utilits/parseItems";
import { LoadingContext } from "@/context/LoadingContext";
import { CommonLightBox } from "@/components/CommonLightBox/CommonLightBox";
import { CommonMsg } from "@/components/CommonMsg/CommonMsg";
import { ApplyWorkPopup } from "@/components/ApplyPopup/ApplyPopup";

const MultipleSelect1 = ({
  rwd,
  name,
  label,
  rules,
  className,
  required,
  checkMode,
  placeholder,
  options,
  limit,
  showCount,
  initialValue,
  mode = "multiple",
  filterSort = () => 0,
  optionFilterProp = "label",
  optionLabelProp = "label",
  labelInValue = true,
  children,
  tagChildren = () => null,
}) => {
  const [value, setValue] = useState(initialValue); // { value: "1", label: "創作方" }
  const closeList = useRef({});

  const handleChange = (option, setFieldsValue) => {
    // if (Array.isArray(option)) {
    //   if (typeof option[option.length - 1]?.value === "string") {
    //     if (option[option.length - 1].value.trim() === "") {
    //       setFieldsValue({
    //         [name]: option.slice(0, option.length - 1),
    //       });
    //     } else {
    //       setValue(option);
    //     }
    //   } else {
    //     setValue(option);
    //   }
    // } else {
    //   setValue(option);
    // }
    setValue(option);
    console.log(option);
  };

  const onTagClose = (event, option) => {
    closeList.current[option.value]();
    // event.preventDefault();
    setValue((prev) => prev.filter((e) => e.value !== option.value));
  };

  const tagRender = (props) => {
    const { label, value, closable, onClose } = props;
    closeList.current[value] = onClose;
    const onPreventMouseDown = (event) => {
      event.preventDefault();
      event.stopPropagation();
    };

    return <div></div>;
  };

  const searchTest = useRef("");
  return (
    <Row gutter={[33, 8]} align="bottom" style={{ marginBottom: 24 }}>
      <Col span={rwd.device === "mobile" ? 24 : 10}>
        <Form.Item noStyle shouldUpdate={() => checkMode}>
          {({ getFieldValue, setFieldsValue }) => {
            const currentValue = getFieldValue(name);
            return (
              <>
                <Form.Item
                  required={required}
                  label={label}
                  name={name}
                  rules={rules}
                  className={className}
                  initialValue={value}
                  style={{ marginBottom: 0 }}
                >
                  <Select
                    mode={mode}
                    showSearch
                    showArrow
                    disabled={checkMode}
                    onSearch={(inputValue) => (searchTest.current = inputValue)}
                    onChange={(option) => handleChange(option, setFieldsValue)}
                    value={value}
                    filterSort={(option1, option2) =>
                      filterSort(option1, option2, searchTest)
                    }
                    tagRender={tagRender}
                    placeholder={placeholder}
                    optionFilterProp={optionFilterProp}
                    optionLabelProp={optionLabelProp}
                    labelInValue={labelInValue}
                    open={value.length === limit ? false : undefined}
                    options={
                      options &&
                      options.filter((o) =>
                        !checkMode
                          ? true
                          : currentValue?.findIndex((c) => c === o.value) !==
                              -1 || []
                      )
                    }
                  >
                    {children}
                  </Select>
                </Form.Item>
              </>
            );
          }}
        </Form.Item>
      </Col>

      <Col span={rwd.device === "mobile" ? 24 : 14}>
        <Row align="middle" wrap={false}>
          <div>
            {value.map((e) => (
              <Tag
                className={"customTag"}
                closable
                key={e.value}
                onClose={(event) => onTagClose(event, e)}
              >
                {tagChildren(e) || e.label}
              </Tag>
            ))}
          </div>

          {showCount && (
            <div>
              <Tag color={value.length === limit && "#e53c2b"}>
                &nbsp;{value.length}&nbsp;/&nbsp;{limit}&nbsp;
              </Tag>
            </div>
          )}
        </Row>
      </Col>
    </Row>
  );
};

const MultipleSelect2 = ({
  rwd,
  t,
  locale,
  name,
  label1,
  label2,
  rules,
  className,
  required,
  checkMode,
  placeholder1,
  placeholder2,
  options1,
  optionsOriginFormat1,
  options2,
  limit,
  showCount,
  initialValue,
  mode1 = "multiple",
  mode2 = "multiple",
  totalAmount,
  optionFilterProp = "label",
  optionLabelProp = "label",
  labelInValue = true,
  children2 = () => null,
  tagChildren = () => null,
  formatWorkctDetail,
}) => {
  const [value, setValue] = useState(initialValue); // { value: "1", label: "創作方" }
  const closeList = useRef({});

  const handleChange1 = (option, getFieldValue, setFieldsValue) => {
    console.log(option, "option");

    // const formData = getFieldValue(name);
    //   console.log(formData, name);
    const existedData = getFieldValue(name).find(
      (e) => e.title === option.value
    );
    if (existedData) {
      // there's been a data of existed title
      // set previous data to option of "name"

      setFieldsValue({
        [`${name}name`]: [
          ...existedData.Unitm1_no.map((e) => {
            return {
              label: "",
              value: e,
            };
          }),
          ...existedData.name.map((e) => {
            return {
              label: e,
              value: e,
            };
          }),
        ],
      });
    } else {
      // clear unrelated data as initial
      // because it's been setField by default after change event
      const fieldTitle = getFieldValue(`${name}title`);
      if (fieldTitle) {
        setFieldsValue({
          [`${name}name`]: undefined,
        });
      }
    }

    // const fieldNameOld = getFieldValue(`${name}name`);
    // console.log(fieldNameOld, `${name}name`, 666666666666);
    // if (fieldNameOld) {
    // have data of name now

    // the data of after switching
    // it means we can pass data to tag, need to set form data to be as loop
    // const fieldNameNew = getFieldValue(`${name}name`);

    // if (fieldNameNew) {
    //   setFieldsValue({
    //     [name]: [
    //       ...formData.filter((e) => e.title !== option.value),
    //       // {
    //       //   title: option.value,
    //       //   Unitm1_no: currentData
    //       //   ? [...currentData.Unitm1_no, fieldName.value]
    //       //   : [fieldName.value],
    //       //   name: currentData
    //       //   ? [...currentData.name, fieldName.label]
    //       //   : [fieldName.label],
    //       // },
    //       {
    //         title: option.value,
    //         Unitm1_no: fieldNameNew
    //           .filter((e) => typeof e.value !== "string")
    //           .map((e) => e.value),
    //         name: fieldNameNew
    //           .filter((e) => typeof e.value === "string")
    //           .map((e) => e.value),
    //       },
    //     ],
    //   });
    // }
    // }
    // setValue((prevValue) => {
    //   setNameList((prevName) => {
    //     return {
    //       ...prevName,
    //       [option.value]: prevName[option.value] || [],
    //     };
    //   });
    //   return [...prevValue, option];
    // });
  };

  const handleChange2 = (option, getFieldValue, setFieldsValue) => {
    console.log(option, "option");

    const fieldTitle = getFieldValue(`${name}title`);
    console.log(fieldTitle, `${name}title`);
    if (fieldTitle) {
      const newData = {
        title: fieldTitle.value,
        Unitm1_no: option
          .filter((e) => typeof e.value !== "string")
          .map((e) => e.value),
        name: option
          .filter((e) => typeof e.value === "string")
          .map((e) => e.value),
      };

      if (newData.Unitm1_no.length > 0 || newData.name.length > 0) {
        console.log(123123);
        // setFieldsValue({
        //   [name]: [
        //     ...getFieldValue(name).filter((e) => e.title !== fieldTitle.value),
        //     newData,
        //   ],
        // });
        const formData = JSON.parse(JSON.stringify(getFieldValue(name)));
        console.log(formData);
        const index = formData.findIndex((e) => e.title === fieldTitle.value);

        if (index > -1) {
          formData[index] = newData;
          setFieldsValue({
            [name]: formData,
          });
        } else {
          setFieldsValue({
            [name]: [
              ...getFieldValue(name).filter(
                (e) => e.title !== fieldTitle.value
              ),
              newData,
            ],
          });
        }
      } else {
        setFieldsValue({
          [name]: getFieldValue(name).filter(
            (e) => e.title !== fieldTitle.value
          ),
        });
      }
    }
    // setValue((prevValue) => {
    //   setNameList((prevName) => {
    //     return {
    //       ...prevName,
    //       [option.value]: prevName[option.value] || [],
    //     };
    //   });
    //   return [...prevValue, option];
    // });
  };

  const onTagClose = (
    currentData,
    fieldName,
    data,
    getFieldValue,
    setFieldsValue
  ) => {
    const newData = {
      title: currentData.title,
      Unitm1_no:
        fieldName === "Unitm1_no"
          ? currentData.Unitm1_no.filter((e) => e !== data)
          : currentData.Unitm1_no,
      name:
        fieldName === "name"
          ? currentData.name.filter((e) => e !== data)
          : currentData.name,
    };

    if (newData.Unitm1_no.length > 0 || newData.name.length > 0) {
      const formData = JSON.parse(JSON.stringify(getFieldValue(name)));
      console.log(formData);
      const index = formData.findIndex((e) => e.title === currentData.title);

      if (index > -1) {
        formData[index] = newData;
        setFieldsValue({
          [name]: formData,
        });
      }
    } else {
      setFieldsValue({
        [name]: getFieldValue(name).filter(
          (e) => e.title !== currentData.title
        ),
      });
    }

    if (currentData.title === getFieldValue(`${name}title`).value) {
      setFieldsValue({
        [`${name}name`]: [
          ...newData.Unitm1_no.map((e) => {
            return {
              label: "",
              value: e,
            };
          }),
          ...newData.name.map((e) => {
            return {
              label: e,
              value: e,
            };
          }),
        ],
      });
    }

    // closeList.current[`${getFieldValue(`${name}title`).value}${option}`]();
    // clear {} if no data rest
    // event.preventDefault();
    // setValue((prev) => prev.filter((e) => e.value !== option.value));
  };

  const tagRender = (props) => {
    // const { label, value, closable, onClose } = props;
    // console.log(label, value, closable, onClose);
    // console.log(getFieldValue(`${name}title`));
    // closeList.current[value] = onClose;
    // closeList.current[`${getFieldValue(`${name}title`).value}${value}`] =
    //   onClose;
    // console.log(closeList.current);
    // const onPreventMouseDown = (event) => {
    //   event.preventDefault();
    //   event.stopPropagation();
    // };
    return <div></div>;
  };

  return (
    <Row gutter={[33, 8]} align="middle" style={{ marginBottom: 24 }}>
      <Form.Item noStyle shouldUpdate={() => checkMode}>
        {({ getFieldValue, setFieldsValue }) => {
          const currentValue = getFieldValue(name);
          return (
            <>
              <Col span={rwd.device === "mobile" ? 24 : 10}>
                <Form.Item
                  required={required}
                  label={label1}
                  rules={rules}
                  className={className}
                  name={`${name}title`}
                  style={{
                    marginBottom: 8,
                  }}
                >
                  <Select
                    showSearch
                    showArrow
                    disabled={checkMode}
                    onChange={(option) =>
                      handleChange1(option, getFieldValue, setFieldsValue)
                    }
                    placeholder={placeholder1}
                    optionFilterProp="label"
                    optionLabelProp="label"
                    labelInValue={true}
                    options={
                      options1 &&
                      options1.filter((o) =>
                        !checkMode
                          ? true
                          : currentValue?.findIndex((c) => c === o.value) !==
                              -1 || []
                      )
                    }
                  ></Select>
                  {/* open={totalAmount === limit ? false : undefined} */}
                </Form.Item>

                <Form.Item
                  required
                  label={label2}
                  name={`${name}name`}
                  rules={[]}
                  style={{
                    marginBottom: 0,
                  }}
                >
                  {/* value={value} */}
                  <Select
                    mode={mode2}
                    showSearch
                    showArrow
                    disabled={checkMode}
                    onChange={(option) =>
                      handleChange2(option, getFieldValue, setFieldsValue)
                    }
                    tagRender={tagRender}
                    placeholder={placeholder1}
                    optionFilterProp="title"
                    optionLabelProp="children"
                    labelInValue={true}
                    open={totalAmount === limit ? false : undefined}
                  >
                    {children2()}
                  </Select>
                </Form.Item>
              </Col>

              <Col span={rwd.device === "mobile" ? 24 : 14}>
                <div>
                  {getFieldValue(name).map((e, i) => (
                    <Row
                      key={e.title}
                      align="middle"
                      style={{ display: rwd.device === "mobile" && "block" }}
                    >
                      <div style={{ marginRight: 10 }}>
                        {optionsOriginFormat1[e.title]}:
                      </div>
                      <div>
                        {e.Unitm1_no.map((f) => {
                          const unitm1Data = formatWorkctDetail(f);
                          return (
                            <Tag
                              className={"customTag"}
                              closable
                              key={f + i}
                              onClose={() =>
                                onTagClose(
                                  e,
                                  "Unitm1_no",
                                  f,
                                  getFieldValue,
                                  setFieldsValue
                                )
                              }
                            >
                              {unitm1Data.pic ? (
                                <img
                                  src={unitm1Data.pic}
                                  alt="icon"
                                  style={{
                                    width: 25,
                                    height: 25,
                                    marginRight: 15,
                                    verticalAlign: "middle",
                                    borderRadius: "50%",
                                  }}
                                />
                              ) : (
                                <SVGMemberDefaultIcon
                                  style={{
                                    width: 25,
                                    height: 25,
                                    marginRight: 15,
                                    verticalAlign: "middle",
                                    borderRadius: "50%",
                                  }}
                                />
                              )}

                              {`${unitm1Data.name_en || ""} ${
                                unitm1Data.lastname_en || ""
                              } ${unitm1Data.name_zh || ""}`}
                            </Tag>
                          );
                        })}
                        {e.name.map((f) => (
                          <Tag
                            className={"customTag"}
                            closable
                            key={f + i}
                            onClose={() =>
                              onTagClose(
                                e,
                                "name",
                                f,
                                getFieldValue,
                                setFieldsValue
                              )
                            }
                          >
                            <SVGMemberDefaultIcon
                              style={{
                                width: 25,
                                height: 25,
                                marginRight: 15,
                                verticalAlign: "middle",
                                borderRadius: "50%",
                              }}
                            />
                            {f}
                          </Tag>
                        ))}
                      </div>
                    </Row>
                  ))}
                </div>

                {showCount && (
                  <div>
                    <Tag color={totalAmount === limit && "#e53c2b"}>
                      &nbsp;{totalAmount}&nbsp;/&nbsp;{limit}&nbsp;
                    </Tag>
                  </div>
                )}
              </Col>
            </>
          );
        }}
      </Form.Item>
    </Row>
  );
};

const CropperImg = ({ accWorksOneData }) => {
  const { t } = useTranslation("common");
  const rwd = useRWD();

  const doubleColProps = useMemo(
    () => ({
      span: rwd.device === "mobile" ? 24 : 12,
    }),
    [rwd.device]
  );
  const maxWidth = 1920;
  const maxHeight = 1080;

  const [cropperDone, setCropperDone] = useState(!!accWorksOneData.pic_url);
  const [cropperFile, setCropperFile] = useState(!!accWorksOneData.pic_url);
  const [cropperUploading, setCropperUploading] = useState(false);
  const cropperDoneRef = useRef(accWorksOneData.pic_url || null); // for api
  const cropperRef = useRef(null);
  const onCrop = () => {
    const cropper = cropperRef.current?.cropper;
    cropperDoneRef.current = cropper
      .getCroppedCanvas()
      // .toDataURL("image/jpeg", 1);
      .toDataURL();
  };

  const handleChange = (info) => {
    if (info.file.status === "uploading") {
      setCropperUploading(true);
    } else if (info.file.status === "done") {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj);
    } else if (info.file.status === "error") {
      setCropperUploading(false);
    }
  };

  const getBase64 = (img) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      const image = new Image();
      image.src = reader.result;
      image.onload = function () {
        const canvas = document.createElement("canvas");
        const ctx = canvas.getContext("2d");

        let width = 0;
        let height = 0;

        let widthPercent = maxWidth / image.width; // ex: 1920 / 2010  0.955223 (v) -> it means the maximum we can reduce
        let heightPercent = maxHeight / image.height; // ex: 1080 / 1500  0.72

        if (widthPercent >= heightPercent) {
          width = image.width * widthPercent;
          height = image.height * widthPercent;
        } else {
          width = image.width * heightPercent;
          height = image.height * heightPercent;
        }

        canvas.width = width;
        canvas.height = height;

        ctx.drawImage(image, 0, 0, width, height);

        // const dataURL = canvas.toDataURL("image/jpeg", 0.8);
        const dataURL = canvas.toDataURL();
        setCropperFile(dataURL);
        setCropperUploading(false);
      };
    });
    reader.readAsDataURL(img);
  };

  const beforeUpload = (file) => {
    const isImg =
      file.type === "image/jpeg" ||
      file.type === "image/png" ||
      file.type === "image/webp";
    if (!isImg) {
      CommonMsg({
        type: "error",
        msg: t("works.step1.field6.desc2"),
      });
      return false;
    }
    // maybe need to trigger error msg

    // const isLt2M = file.size / 1024 / 1024 < 10;
    // if (!isLt2M) return false;
    // maybe need to trigger error msg

    return true;
  };

  return (
    <>
      <Form.Item noStyle shouldUpdate>
        {({ setFieldsValue, getFieldValue }) => {
          return (
            <>
              <Form.Item
                required
                name={
                  accWorksOneData.pic_url && cropperDone && cropperFile
                    ? getFieldValue("pic")
                      ? "pic"
                      : "pic_url"
                    : "pic"
                }
                label={t("works.step1.field6")}
                rules={[]}
                initialValue={accWorksOneData.pic_url}
              >
                <div className={styles.cropperContainer}>
                  <Row
                    wrap={rwd.device === "mobile"}
                    align="middle"
                    gutter={38}
                  >
                    <Col
                      span={rwd.device === "mobile" ? 24 : 8}
                      order={rwd.device === "mobile" ? 2 : 1}
                      className={styles.descContainer}
                    >
                      <div className={styles.desc1}>
                        {t("works.step1.field6.desc1")}
                      </div>
                      <div className={styles.desc2}>
                        {t("works.step1.field6.desc2")}
                      </div>
                    </Col>

                    <Col
                      span={rwd.device === "mobile" ? 24 : 16}
                      order={rwd.device === "mobile" ? 1 : 2}
                      className={styles.cropperBlock}
                    >
                      {
                        // 有檔案 -> 1. 正在裁切, 2. 裁切完畢
                        // 沒檔案 -> 1. 還沒傳, 2. 上傳中
                      }
                      {cropperFile ? (
                        cropperDone ? (
                          <div className={styles.cropperBox}>
                            <img
                              className={styles.cropperDoneImg}
                              src={cropperDoneRef.current}
                              alt="icon"
                            />
                          </div>
                        ) : (
                          /* crop={onCrop} */
                          <Cropper
                            className={styles.cropperBox}
                            src={cropperFile}
                            ref={cropperRef}
                            autoCropArea={1}
                            aspectRatio={16 / 9}
                            zoomable={false}
                            zoomOnWheel={false}
                            zoomOnTouch={false}
                            movable={false}
                          />
                        )
                      ) : (
                        <Upload
                          className="customUpload"
                          accept="image/png,image/jpeg,image/webp"
                          showUploadList={false}
                          customRequest={({ onSuccess, onError, file }) => {
                            const img = new Image();
                            img.src = URL.createObjectURL(file);
                            img.onload = () => {
                              setFieldsValue({ pic: null });
                              if (
                                img.width < maxWidth ||
                                img.height < maxHeight
                              ) {
                                CommonMsg({
                                  type: "error",
                                  msg: t("works.step1.field6.error2"),
                                });
                                URL.revokeObjectURL(img.src);
                                onError();
                              } else {
                                URL.revokeObjectURL(img.src);
                                onSuccess();
                              }
                            };
                          }}
                          beforeUpload={beforeUpload}
                          onChange={handleChange}
                          zoomable={false}
                        >
                          <div>
                            {cropperUploading ? (
                              <Loading />
                            ) : (
                              <Tag color="#999999">
                                {t("works.step1.field6.desc3")}
                              </Tag>
                            )}
                          </div>
                        </Upload>
                      )}

                      {cropperFile && (
                        <div className={styles.btnGroup}>
                          <Button
                            onClick={() => {
                              setCropperDone(false);
                              setCropperFile(null);
                              cropperRef.current?.cropper?.destroy();
                              cropperDoneRef.current = null;
                              setFieldsValue({ pic: null });
                            }}
                          >
                            {t("works.step1.field6.btn1")}
                          </Button>
                          {!cropperDone && (
                            <Button
                              onClick={() => {
                                onCrop();
                                cropperRef.current.cropper?.destroy();
                                setCropperDone(true);
                                setFieldsValue({
                                  pic: cropperDoneRef.current,
                                });
                              }}
                              style={{
                                backgroundColor: "#999999",
                                color: "white",
                              }}
                            >
                              {t("works.step1.field6.btn2")}
                            </Button>
                          )}
                        </div>
                      )}
                    </Col>
                  </Row>
                </div>
              </Form.Item>
              {cropperFile && (
                <Row gutter={[22, 14]} className={styles.imgFields}>
                  <Col {...doubleColProps}>
                    <Form.Item
                      required
                      name="pic_title"
                      rules={[]}
                      initialValue={accWorksOneData.pic_title}
                      style={{ marginBottom: 0 }}
                    >
                      <Input
                        placeholder={t("works.step1.field6.placeholder1")}
                      />
                    </Form.Item>
                  </Col>

                  <Col {...doubleColProps}>
                    <Form.Item
                      required
                      name="pic_alt"
                      rules={[]}
                      initialValue={accWorksOneData.pic_alt}
                    >
                      <Input
                        placeholder={t("works.step1.field6.placeholder2")}
                      />
                    </Form.Item>
                  </Col>
                </Row>
              )}
            </>
          );
        }}
      </Form.Item>
    </>
  );
};

const Form1 = ({
  rwd,
  setShowLoading,
  showSaveMsgPopup,
  setShowSaveMsgPopup,
  saveStep,
  checkMode,
  onNext,
  formItems,
  accWorksOneData,
  memberId,
  authKey,
  workm1no,
  SaveMsgPopup,
}) => {
  const { locale } = useRouter();
  const { t } = useTranslation("common");
  const [form1] = Form.useForm();

  const yearOptions = useMemo(() => {
    let i = new Date().getFullYear();
    let list = [];
    while (i > 1949) {
      list.push(i + "");
      i--;
    }
    return list;
  }, []);
  const workTypeOptions = parseItems(formItems.workType);

  const handleCliclNext = useCallback(
    (values) => {
      console.log(values);

      setShowSaveMsgPopup(false);
      setShowLoading(true);
      const calcRequiredField = () => {
        let totalAmount = 0;
        let total = 0;
        if (values.work_status === "1") {
          totalAmount = 5;
          if (!values.final_month) total = total + 1;
          if (!values.final_year) total = total + 1;
        } else if (values.work_status === "2") {
          totalAmount = 4;
          if (!values.year) total = total + 1;
        } else {
          totalAmount = 5;
          total = total + 3;
        }

        if (!values.intro_en) total = total + 1;
        if (!values.pic && !values.pic_url) total = total + 1;

        return Math.floor(((totalAmount - total) / totalAmount) * 100);
      };

      accWorksModifyst2({
        auth_key: authKey,
        memberm1_no: memberId,
        workm1_no: workm1no,
        step1: calcRequiredField(),
        ...values,
      }).then((res) => {
        if (res.resu === 1) {
          onNext();

          CommonMsg({
            type: "success",
            msg: t("works.message.save"),
          });
          const currentStep = showSaveMsgPopup ? saveStep.current : 2;
          setShowLoading(false);
          router.push(
            `${router.pathname.replace(
              "[workm1no]",
              router.query.workm1no
            )}?step=${currentStep}`
          );
        } else {
        }
      });
    },
    [onNext]
  );

  const formRef = useRef();

  return (
    <>
      <Form
        ref={formRef}
        layout="vertical"
        onFinish={handleCliclNext}
        onFinishFailed={({ values, errorFields }) => {
          console.log(values, errorFields, formRef.current, 45465);
        }}
        scrollToFirstError={true}
        name="step1"
        form={form1}
      >
        <Form.Item
          noStyle
          shouldUpdate={(prevValues, curValues) =>
            prevValues.work_status !== curValues.work_status
          }
        >
          {({ getFieldValue }) => {
            const currentValue =
              getFieldValue("work_status") || accWorksOneData.work_status;

            return (
              <>
                <Form.Item
                  required
                  label={t("works.step1.field1")}
                  name="work_status"
                  rules={[]}
                  initialValue={accWorksOneData.work_status}
                >
                  <Radio.Group
                    className={classNames("customRadioGroup")}
                    options={workTypeOptions}
                  />
                </Form.Item>

                {currentValue === "1" ? (
                  <Form.Item
                    label={t("works.step1.field2")}
                    style={{ marginTop: rwd.device === "mobile" ? 0 : 0 }}
                  >
                    <Row gutter={[18, 12]}>
                      <Col span={rwd.device === "mobile" ? 24 : 8}>
                        <Form.Item
                          required
                          name="final_month"
                          rules={[]}
                          initialValue={accWorksOneData.final_month}
                        >
                          <Select
                            placeholder={t("works.step1.field2.placeholder1")}
                          >
                            {Array.from(Array(12), (e, i) => (
                              <Option key={e} value={i + 1 + ""}>
                                {i + 1 + ""}
                              </Option>
                            ))}
                          </Select>
                        </Form.Item>
                      </Col>

                      <Col span={rwd.device === "mobile" ? 24 : 8}>
                        <Form.Item
                          required
                          name="final_year"
                          rules={[]}
                          initialValue={accWorksOneData.final_year}
                        >
                          <Select
                            placeholder={t("works.step1.field2.placeholder2")}
                          >
                            {Array.from(
                              { length: 2035 - 2024 + 1 },
                              (_, i) => 2024 + i
                            ).map((yearOption) => (
                              <Option
                                key={yearOption + ""}
                                value={yearOption + ""}
                              >
                                {yearOption + ""}
                              </Option>
                            ))}
                          </Select>
                        </Form.Item>
                      </Col>
                    </Row>
                  </Form.Item>
                ) : currentValue === "2" ? (
                  <Row
                    gutter={[18, 12]}
                    style={{ marginTop: rwd.device === "mobile" ? 0 : 0 }}
                  >
                    <Col span={rwd.device === "mobile" ? 24 : 8}>
                      <Form.Item
                        required
                        name="year"
                        label={t("works.step1.field3")}
                        rules={[]}
                        initialValue={accWorksOneData.year}
                      >
                        <Select
                          placeholder={t("works.step1.field3.placeholder")}
                        >
                          {yearOptions.map((yearOption) => (
                            <Option key={yearOption} value={yearOption}>
                              {yearOption}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                    </Col>

                    <Col span={rwd.device === "mobile" ? 24 : 8}>
                      <Form.Item
                        name="platform"
                        label={t("works.step1.field4")}
                        rules={[]}
                        initialValue={accWorksOneData.platform}
                      >
                        <Input placeholder={"https://"}></Input>
                      </Form.Item>
                    </Col>
                  </Row>
                ) : null}
              </>
            );
          }}
        </Form.Item>

        <Form.Item required label={t("works.step1.field5")}>
          <Tabs className={"customTab"}>
            <Tabs.TabPane tab={t("works.step1.field5.tab1")} key="1">
              <Form.Item
                name="intro_en"
                required
                rules={[]}
                initialValue={accWorksOneData.intro_en}
              >
                <TextArea
                  disabled={checkMode}
                  showCount
                  maxLength={1000}
                  placeholder={t("works.step1.field5.placeholder", {
                    maximum: 1000,
                  })}
                  autoSize={{ minRows: 12 }}
                />
              </Form.Item>
            </Tabs.TabPane>

            <Tabs.TabPane tab={t("works.step1.field5.tab2")} key="2">
              <Form.Item
                name="intro_zh"
                rules={[]}
                initialValue={accWorksOneData.intro_zh}
              >
                <TextArea
                  disabled={checkMode}
                  showCount
                  maxLength={500}
                  placeholder={t("works.step1.field5.placeholder", {
                    maximum: 500,
                  })}
                  autoSize={{ minRows: 12 }}
                />
              </Form.Item>
            </Tabs.TabPane>
          </Tabs>
        </Form.Item>

        <CropperImg accWorksOneData={accWorksOneData} />

        <div className={classNames(styles.customSubmit)}>
          <div className={classNames(styles.submit)}>
            <FancyButton htmlType="submit" scaleSize={38}>
              {t("works.step1.submit")}
            </FancyButton>
          </div>
        </div>
      </Form>
    </>
  );
};

const Form2 = ({
  rwd,
  setShowLoading,
  showSaveMsgPopup,
  setShowSaveMsgPopup,
  saveStep,
  checkMode,
  onNext,
  onPrev,
  formItems,
  country,
  accWorksOneData,
  memberId,
  authKey,
  workm1no,
  SaveMsgPopup,
}) => {
  const { locale } = useRouter();
  const { t } = useTranslation("common");
  const [form2] = Form.useForm();

  const industryOptions = parseItems(formItems.industry);
  const langOptions = parseItems(formItems.lang);
  const screentextOptions = parseItems(formItems.lang_captions);
  const artTypeOptions = parseItems(formItems.artType);
  const artTagsOptions = parseItems(formItems.artTags);
  const artformOptions = parseItems(formItems.artform);
  const industryInitailOptions = parseItems2(
    accWorksOneData.industry,
    formItems.industry
  );
  const langInitailOptions = parseItems2(
    accWorksOneData.language,
    formItems.lang
  );
  const screentextInitailOptions = parseItems2(
    accWorksOneData.screentext,
    formItems.lang
  );
  const artTypeInitailOptions = parseItems2(
    accWorksOneData.gclass,
    formItems.artType
  );
  const artTagsInitailOptions = parseItems2(
    accWorksOneData.gtheme,
    formItems.artTags
  );
  const artformInitailOptions = parseItems2(
    accWorksOneData.workform,
    formItems.artform
  );

  const doubleColProps = useMemo(
    () => ({
      span: rwd.device === "mobile" ? 24 : 12,
    }),
    [rwd.device]
  );

  const handleCliclNext = useCallback(
    (values) => {
      console.log(values);
      const fieldData = JSON.parse(JSON.stringify(values));
      const selectList = [
        "gclass",
        "gtheme",
        "language",
        "screentext",
        "workform",
      ];
      selectList.forEach(
        (e) => (fieldData[e] = fieldData[e].map((f) => f.value))
      );

      setShowSaveMsgPopup(false);
      setShowLoading(true);
      const calcRequiredField = () => {
        let total = 0;

        if (fieldData.language.length === 0) total = total + 1;
        if (fieldData.screentext.length === 0) total = total + 1;
        if (!fieldData.mv_length) total = total + 1;
        if (!fieldData.ex_method) total = total + 1;
        if (fieldData.gclass.length === 0) total = total + 1;
        if (fieldData.gtheme.length === 0) total = total + 1;
        if (fieldData.workform.length === 0) total = total + 1;

        return Math.floor(((7 - total) / 7) * 100);
      };

      accWorksModifyst3({
        auth_key: authKey,
        memberm1_no: memberId,
        workm1_no: workm1no,
        step2: calcRequiredField(),
        ...fieldData,
      }).then((res) => {
        if (res.resu === 1) {
          onNext();
          CommonMsg({
            type: "success",
            msg: t("companion.message.save"),
          });
          const currentStep = showSaveMsgPopup ? saveStep.current : 3;
          setShowLoading(false);
          router.push(
            `${router.pathname.replace(
              "[workm1no]",
              router.query.workm1no
            )}?step=${currentStep}`
          );
        } else {
        }
      });
    },
    [onNext]
  );

  return (
    <>
      <Form
        name="step2"
        layout="vertical"
        onFinish={handleCliclNext}
        onFinishFailed={({ values, errorFields }) => {
          console.log(values, errorFields);
        }}
        scrollToFirstError
        form={form2}
      >
        <MultipleSelect1
          rwd={rwd}
          required
          label={t("works.step2.field1")}
          name="language"
          rules={
            [
              // {
              //   message: t("companion.step1.required", {
              //     label: t("companion.step1.field5"),
              //   }),
              //   validator: (_, value) => {
              //     if (value.length > 2) {
              //       return Promise.reject("Some message here");
              //     } else {
              //       return Promise.resolve();
              //     }
              //   },
              // },
            ]
          }
          options={langOptions}
          checkMode={checkMode}
          placeholder={t("works.step2.field1.placeholder")}
          limit={null}
          showCount={false}
          initialValue={langInitailOptions}
        />

        <MultipleSelect1
          rwd={rwd}
          required
          label={t("works.step2.field2")}
          name="screentext"
          rules={
            [
              // {
              //   message: t("companion.step1.required", {
              //     label: t("companion.step1.field5"),
              //   }),
              //   validator: (_, value) => {
              //     if (value.length > 2) {
              //       return Promise.reject("Some message here");
              //     } else {
              //       return Promise.resolve();
              //     }
              //   },
              // },
            ]
          }
          options={screentextOptions}
          checkMode={checkMode}
          placeholder={t("works.step2.field2.placeholder")}
          limit={null}
          showCount={false}
          initialValue={screentextInitailOptions}
        />

        <Row gutter={60}>
          <Col span={rwd.device === "mobile" ? 24 : 12}>
            <Form.Item
              required
              label={t("works.step2.field3")}
              name="mv_length"
              rules={[]}
              initialValue={accWorksOneData.mv_length}
            >
              <Input placeholder={t("works.step2.field3.placeholder")} />
            </Form.Item>
          </Col>

          <Col span={rwd.device === "mobile" ? 24 : 6}>
            <Form.Item
              required
              label={t("works.step2.field5")}
              name="ex_method"
              rules={[]}
              initialValue={accWorksOneData.ex_method}
            >
              <Checkbox.Group style={{ whiteSpace: "nowrap" }}>
                <Row gutter={40}>
                  <Col span={12}>
                    <Checkbox value={"1"}>
                      {t("works.step2.field5.radio1")}
                    </Checkbox>
                  </Col>

                  <Col span={12}>
                    <Checkbox value={"2"}>
                      {t("works.step2.field5.radio2")}
                    </Checkbox>
                  </Col>
                </Row>
              </Checkbox.Group>
            </Form.Item>
          </Col>

          <Col span={rwd.device === "mobile" ? 24 : 6}>
            <Form.Item
              label={t("works.step2.field4")}
              name="ex_number"
              rules={[]}
              initialValue={accWorksOneData.ex_number}
            >
              <Checkbox.Group style={{ whiteSpace: "nowrap" }}>
                <Row gutter={40}>
                  <Col span={12}>
                    <Checkbox value={"1"}>
                      {t("works.step2.field4.radio1")}
                    </Checkbox>
                  </Col>

                  <Col span={12}>
                    <Checkbox value={"2"}>
                      {t("works.step2.field4.radio2")}
                    </Checkbox>
                  </Col>
                </Row>
              </Checkbox.Group>
            </Form.Item>
          </Col>
        </Row>

        <Form.Item
          label={
            <div>
              {t("works.step2.field9-1")}
              <span style={{ fontSize: 13, color: "#666666" }}>
                {t("works.step2.field9-2")}
              </span>
            </div>
          }
          name="youtube"
          rules={[]}
          initialValue={accWorksOneData.youtube}
        >
          <Input placeholder={"https://"} />
        </Form.Item>

        <MultipleSelect1
          rwd={rwd}
          required
          label={t("works.step2.field6")}
          name="gclass"
          rules={
            [
              // {
              //   message: t("companion.step1.required", {
              //     label: t("companion.step1.field5"),
              //   }),
              //   validator: (_, value) => {
              //     if (value.length > 2) {
              //       return Promise.reject("Some message here");
              //     } else {
              //       return Promise.resolve();
              //     }
              //   },
              // },
            ]
          }
          options={artTypeOptions}
          checkMode={checkMode}
          placeholder={t("works.step2.field6.placeholder")}
          limit={3}
          showCount={true}
          initialValue={artTypeInitailOptions}
        />

        <MultipleSelect1
          rwd={rwd}
          required
          label={t("works.step2.field7")}
          name="gtheme"
          rules={
            [
              // {
              //   message: t("companion.step1.required", {
              //     label: t("companion.step1.field5"),
              //   }),
              //   validator: (_, value) => {
              //     if (value.length > 2) {
              //       return Promise.reject("Some message here");
              //     } else {
              //       return Promise.resolve();
              //     }
              //   },
              // },
            ]
          }
          options={artTagsOptions}
          checkMode={checkMode}
          placeholder={t("works.step2.field7.placeholder")}
          limit={3}
          showCount={true}
          initialValue={artTagsInitailOptions}
        />

        <MultipleSelect1
          rwd={rwd}
          required
          label={t("works.step2.field8")}
          name="workform"
          rules={
            [
              // {
              //   message: t("companion.step1.required", {
              //     label: t("companion.step1.field5"),
              //   }),
              //   validator: (_, value) => {
              //     if (value.length > 2) {
              //       return Promise.reject("Some message here");
              //     } else {
              //       return Promise.resolve();
              //     }
              //   },
              // },
            ]
          }
          options={artformOptions}
          checkMode={checkMode}
          placeholder={t("works.step2.field8.placeholder")}
          limit={3}
          showCount={true}
          initialValue={artformInitailOptions}
        />

        <div className={classNames(styles.customSubmit)}>
          <div className={classNames(styles.submit)}>
            <FancyButton htmlType="submit" scaleSize={38}>
              {t("works.step2.submit")}
            </FancyButton>
          </div>
        </div>
      </Form>
    </>
  );
};

const Form3 = ({
  rwd,
  setShowLoading,
  showSaveMsgPopup,
  setShowSaveMsgPopup,
  saveStep,
  checkMode,
  onNext,
  onPrev,
  formItems,
  country,
  accWorksOneData,
  memberId,
  authKey,
  workm1no,
  SaveMsgPopup,
}) => {
  const { locale } = useRouter();
  const { t } = useTranslation("common");
  const [modal1Form] = Form.useForm();
  const [modal2Form] = Form.useForm();
  const [modal1, setModal1] = useState(false);
  const [modal2, setModal2] = useState(false);
  const [form3] = Form.useForm();
  // const [sourceIndexList, setSourceIndexList] = useState({
  //   zh: [],
  //   en: [],
  // });
  const [sourceIndexList, setSourceIndexList] = useState([]);
  const modalForm1Reducer = (state, action) => {
    switch (action.type) {
      // 年份新的放下面
      case "add": {
        // 不會有 workt1_no

        // filter those fields which have data
        const currentData = {};
        Object.entries(action.data)
          .filter((e) => e[1] !== undefined)
          .forEach((e) => (currentData[e[0]] = e[1]));
        return [
          ...state,
          {
            ...currentData,
            workt1_no: `#${new Date() / 1}`,
          },
        ].sort((prev, next) => prev.year - next.year);
      }
      case "update": {
        // 用 workt1_no 送出

        // filter those fields which have data
        const currentData = {};
        Object.entries(action.data)
          .filter((e) => e[1] !== undefined)
          .forEach((e) => (currentData[e[0]] = e[1]));
        return [
          ...state.filter((e) => e.workt1_no !== action.data.workt1_no),
          currentData,
        ].sort((prev, next) => prev.year - next.year);
      }
      case "delete":
        // 用 workt1_no 刪除
        return state.filter((e) => e.workt1_no !== action.data.workt1_no);
      default:
        break;
    }
  };

  const modalForm2Reducer = (state, action) => {
    switch (action.type) {
      // 年份新的放下面
      case "add": {
        // 不會有 workt2_no

        // filter those fields which have data
        const currentData = {};
        Object.entries(action.data)
          .filter((e) => e[1] !== undefined)
          .forEach((e) => (currentData[e[0]] = e[1]));
        return [
          ...state,
          {
            ...currentData,
            workt2_no: `#${new Date() / 1}`,
          },
        ].sort((prev, next) => prev.year - next.year);
      }
      case "update": {
        // 用 workt2_no 送出

        // filter those fields which have data
        const currentData = {};
        Object.entries(action.data)
          .filter((e) => e[1] !== undefined)
          .forEach((e) => (currentData[e[0]] = e[1]));
        return [
          ...state.filter((e) => e.workt2_no !== action.data.workt2_no),
          currentData,
        ].sort((prev, next) => prev.year - next.year);
      }

      case "delete":
        // 用 workt2_no 刪除
        var a = state.filter((e) => e.workt2_no !== action.data.workt2_no);
        return a;
      default:
        break;
    }
  };

  useEffect(() => {
    (async () => {
      setShowLoading(true);
      const { resu, data } = await sourceIndex({
        category: "Festival,Exposition",
      });
      if (resu === 1) {
        // setSourceIndexList({
        //   zh: data.map((e) => e.title_zh),
        //   en: data.map((e) => e.title_en),
        // });
        setSourceIndexList(data);
      }
      setShowLoading(false);
    })();
  }, []);

  const [modalForm1, dispatchModal1Form] = useReducer(
    modalForm1Reducer,
    accWorksOneData.workt1
  );
  const [errorModal1, setErrorModal1] = useState("");

  const [modalForm2, dispatchModal2Form] = useReducer(
    modalForm2Reducer,
    accWorksOneData.workt2
  );
  const [errorModal2, setErrorModal2] = useState("");

  const yearOptions = useMemo(() => {
    let i = new Date().getFullYear();
    let list = [];
    while (i > 1949) {
      list.push(i + "");
      i--;
    }
    return list;
  }, []);

  const formatNation = (name) => country.find((e) => e.name === name) || {};

  const formatUnit = (value, lang) => {
    const sourceIndexData = sourceIndexList.find((e) => e.id === value);
    return sourceIndexData ? sourceIndexData[`title_${lang}`] : value;
  };

  const handleUnitSelect = (
    fieldName,
    value,
    option,
    setFieldsValue,
    getFieldValue
  ) => {
    if (!option?.value) {
      // New tag entered
      if (value.trim() === "") {
        setFieldsValue({
          [fieldName]: [],
        });
      } else {
        setFieldsValue({
          [fieldName]: value,
        });
      }
    } else {
      // Existing option selected
      setFieldsValue({
        [fieldName]: getFieldValue(fieldName).filter(
          (e, i, arr) => i === arr.length - 1
        )[0],
      });

      if (fieldName === "unit_en") {
        // set chinese
        setFieldsValue({
          unit_zh: getFieldValue(fieldName),
        });
      }
    }
  };

  const unitTagRender = (props) => {
    const { label, value, closable, onClose } = props;
    return <div>{label}</div>;
  };

  const beforeUpload = (file) => {
    if (file.type !== "application/pdf") {
      CommonMsg({
        type: "error",
        msg: t("works.step3.error.msg2"),
      });
      return false;
    }
    // if (file.size / 1024 / 1024 < 5) return false;
    // maybe need to trigger error msg
    return true;
  };

  const handleChange = (info, setFieldsValue, fieldName) => {
    if (info.file.status === "done") {
      setFieldsValue({ [fieldName]: info.file.originFileObj });
    } else {
      setFieldsValue({ [fieldName]: null });
    }
  };

  const handleModal1Finish = useCallback(
    (data) => {
      console.log(data);
      const { action, ...rest } = data;

      if (Object.values(rest).every((e) => e === undefined)) {
        if (action === "update") {
          dispatchModal1Form({
            type: "delete",
            data: rest,
          });
        }
      } else {
        dispatchModal1Form({
          type: data.action,
          data: {
            ...rest,
          },
        });
      }
      setModal1(false);
    },
    [locale]
  );

  const handleModal2Finish = useCallback(
    (data) => {
      console.log(data);
      const { action, ...rest } = data;

      if (Object.values(rest).every((e) => e === undefined)) {
        if (action === "update") {
          dispatchModal2Form({
            type: "delete",
            data: rest,
          });
        }
      } else {
        dispatchModal2Form({
          type: data.action,
          data: {
            ...rest,
          },
        });
      }
      setModal2(false);
    },
    [locale]
  );

  const handleCliclNext = (values) => {
    console.log(values);

    setShowSaveMsgPopup(false);
    setShowLoading(true);
    accWorksModifyst4({
      auth_key: authKey,
      memberm1_no: memberId,
      workm1_no: workm1no,
      ...values,
      workt1: modalForm1,
      workt2: modalForm2,
      step3: 100,
    }).then((res) => {
      if (res.resu === 1) {
        //onNext();
        CommonMsg({
          type: "success",
          msg: t("works.message.save"),
        });
        const currentStep = showSaveMsgPopup ? saveStep.current : 4;
        setShowLoading(false);
        router.push(
          `${router.pathname.replace(
            "[workm1no]",
            router.query.workm1no
          )}?step=${currentStep}`
        );
      } else {
      }
    });
  };

  return (
    <>
      <Form
        name="step3"
        layout="vertical"
        onFinish={handleCliclNext}
        onFinishFailed={({ values, errorFields }) => {
          console.log(values, errorFields);
        }}
        scrollToFirstError
        form={form3}
        requiredMark={false}
      >
        <Form.Item
          label={
            <Row align="middle">
              {t("works.step3.field1")}
              {modalForm1.length < 5 && (
                <Row
                  align="middle"
                  onClick={() => {
                    setModal1(true);
                    modal1Form.resetFields();
                    modal1Form.setFieldsValue({
                      action: "add",
                    });
                  }}
                  style={{
                    marginLeft: rwd.device === "mobile" ? 20 : 28,
                    cursor: "pointer",
                  }}
                >
                  <SVGAdd />
                  <div style={{ marginLeft: 5 }}>{t("works.step3.btn1")}</div>
                </Row>
              )}
            </Row>
          }
        >
          <Row className={styles.listGroup}>
            <Col span={24} style={{ cursor: "pointer" }}></Col>
            {modalForm1.map((e) => (
              <Col span={24} className={styles.list} key={e.workt1_no}>
                <Row align="middle">
                  <Col span={rwd.device === "mobile" ? 23 : 23}>
                    <Row align="middle" gutter={[0, 10]}>
                      <Col span={rwd.device === "mobile" ? 24 : 3}>
                        <div className={styles.subTitle}>{e.year}</div>
                      </Col>
                      <Col span={rwd.device === "mobile" ? 24 : 11}>
                        <div>{formatUnit(e.unit_en, "en")}</div>
                        <div>{formatUnit(e.unit_zh, "zh")}</div>
                      </Col>
                      <Col span={rwd.device === "mobile" ? 24 : 10}>
                        <div>{e.title_en}</div>
                        <div>{e.title_zh}</div>
                      </Col>
                    </Row>
                  </Col>

                  <Col span={rwd.device === "mobile" ? 1 : 1}>
                    <Popover
                      overlayClassName={classNames("customPopover")}
                      content={
                        <>
                          <SVGEdit
                            onClick={() => {
                              setModal1(true);
                              modal1Form.setFieldsValue({
                                action: "update",
                                ...e,
                              });
                            }}
                          />
                          <SVGRemove
                            onClick={() => {
                              dispatchModal1Form({
                                type: "delete",
                                data: e,
                              });
                            }}
                          />
                        </>
                      }
                      trigger="click"
                      placement="right"
                    >
                      <div className={classNames(styles.more)}>&hellip;</div>
                    </Popover>
                  </Col>
                </Row>
              </Col>
            ))}

            <Col span={24} style={{ marginTop: 10 }}>
              <Row justify="end">
                <Tag>
                  &nbsp;{modalForm1.length}&nbsp;/&nbsp;{5}&nbsp;
                </Tag>
              </Row>
            </Col>
          </Row>
        </Form.Item>

        <Form.Item
          label={
            <Row align="middle">
              {t("works.step3.field7")}
              {modalForm2.length < 5 && (
                <Row
                  align="middle"
                  onClick={() => {
                    setModal2(true);
                    modal2Form.resetFields();
                    modal2Form.setFieldsValue({
                      action: "add",
                    });
                  }}
                  style={{
                    marginLeft: rwd.device === "mobile" ? 20 : 28,
                    cursor: "pointer",
                  }}
                >
                  <SVGAdd />
                  <div style={{ marginLeft: 5 }}>{t("works.step3.btn1")}</div>
                </Row>
              )}
            </Row>
          }
        >
          <Row className={styles.listGroup}>
            <Col span={24} style={{ cursor: "pointer" }}></Col>
            {modalForm2.map((e) => (
              <Col span={24} className={styles.list} key={e.workt2_no}>
                <Row align="middle">
                  <Col span={rwd.device === "mobile" ? 23 : 23}>
                    <Row align="middle" gutter={[0, 10]}>
                      <Col span={rwd.device === "mobile" ? 24 : 8}>
                        <Row align="middle" gutter={[0, 10]}>
                          <Col span={12}>
                            <div className={styles.subTitle}>{e.year}</div>
                          </Col>
                          <Col span={12}>
                            <div className={styles.subTitle}>
                              {locale === "zh"
                                ? formatNation(e.nation).traditionalName
                                : formatNation(e.nation).name}
                            </div>
                          </Col>
                        </Row>
                      </Col>

                      <Col span={rwd.device === "mobile" ? 24 : 16}>
                        <div>{e.name_en}</div>
                        <div>{e.name_zh}</div>
                      </Col>
                    </Row>
                  </Col>

                  <Col span={rwd.device === "mobile" ? 1 : 1}>
                    <Popover
                      overlayClassName={classNames("customPopover")}
                      content={
                        <>
                          <SVGEdit
                            onClick={() => {
                              setModal2(true);
                              modal2Form.setFieldsValue({
                                action: "update",
                                ...e,
                              });
                            }}
                          />
                          <SVGRemove
                            onClick={() => {
                              dispatchModal2Form({
                                type: "delete",
                                data: e,
                              });
                            }}
                          />
                        </>
                      }
                      trigger="click"
                      placement="right"
                    >
                      <div className={classNames(styles.more)}>&hellip;</div>
                    </Popover>
                  </Col>
                </Row>
              </Col>
            ))}

            <Col span={24} style={{ marginTop: 10 }}>
              <Row justify="end">
                <Tag>
                  &nbsp;{modalForm2.length}&nbsp;/&nbsp;{5}&nbsp;
                </Tag>
              </Row>
            </Col>
          </Row>
        </Form.Item>

        <Form.Item label={t("works.step3.field12")}>
          <Row gutter={44}>
            <Col span={rwd.device === "mobile" ? 24 : 12}>
              <Form.Item
                noStyle
                shouldUpdate={(prevValues, curValues) =>
                  prevValues.pdf_en !== curValues.pdf_en ||
                  prevValues.pdf_en_url !== curValues.pdf_en_url ||
                  prevValues.pdf_title_en !== curValues.pdf_title_en
                }
              >
                {({ setFieldsValue, getFieldValue }) => {
                  return (
                    <>
                      {/* required={getFieldValue("pdf_title_en")} */}
                      <Form.Item
                        label={t("works.step3.field13")}
                        name={
                          accWorksOneData.pdf_en_url
                            ? getFieldValue("pdf_en")
                              ? "pdf_en"
                              : "pdf_en_url"
                            : "pdf_en"
                        }
                        rules={[
                          {
                            required: getFieldValue("pdf_title_en"),
                            message: t("works.step1.required", {
                              label: t("works.step3.field13"),
                            }),
                          },
                        ]}
                        initialValue={accWorksOneData.pdf_en}
                      >
                        <Upload
                          className="customUpload1"
                          accept="application/pdf"
                          showUploadList={false}
                          maxCount={1}
                          customRequest={({ onSuccess, onError, file }) => {
                            setFieldsValue({ pdf_en_url: "" });
                            if (file.size / 1024 / 1024 > 5) {
                              CommonMsg({
                                type: "error",
                                msg: t("works.step3.error.msg1"),
                              });
                              onError();
                            } else {
                              if (!getFieldValue("pdf_title_en"))
                                setFieldsValue({
                                  pdf_title_en: file.name.replace(".pdf", ""),
                                });
                              onSuccess();
                            }
                          }}
                          onChange={(info) =>
                            handleChange(info, setFieldsValue, "pdf_en")
                          }
                          beforeUpload={beforeUpload}
                        >
                          <div className="uploadContainer">
                            {/*
                            判斷方式:
                              欄位有值
                                1. 有預設(string)就放上PDF icon, fileName, remove icon
                                  這時候按remove icon(after clicking then remove default path from api)
                                  這時候按送出
                                2. 沒預設(File Object)正常上傳(File Object)
                              欄位沒值
                                1. 初始化 UI
                            */}

                            {getFieldValue("pdf_en_url") ? (
                              <>
                                <SVGPdfFile />
                                <div className="fileName">
                                  {accWorksOneData.pdf_title_en}
                                </div>
                                <div className="operate">
                                  <SVGRemove
                                    onClick={(e) => {
                                      e.stopPropagation();
                                      setFieldsValue({ pdf_en_url: "" });
                                    }}
                                  />
                                </div>
                              </>
                            ) : getFieldValue("pdf_en") ? (
                              <>
                                <SVGPdfFile />
                                <div className="fileName">
                                  {getFieldValue("pdf_en").name}
                                </div>
                                <div className="operate">
                                  <SVGRemove
                                    onClick={(e) => {
                                      e.stopPropagation();
                                      setFieldsValue({ pdf_en: null });
                                    }}
                                  />
                                </div>
                              </>
                            ) : (
                              <>
                                <SVGFilesFolders />
                                <div className="desc1">
                                  {t("works.step3.field12.desc1")}
                                </div>
                                <div className="desc2">
                                  {t("works.step3.field12.desc2")}
                                </div>
                              </>
                            )}
                          </div>
                        </Upload>
                      </Form.Item>

                      <Form.Item
                        required={
                          getFieldValue("pdf_en") || getFieldValue("pdf_en_url")
                        }
                        name="pdf_title_en"
                        rules={[
                          {
                            required:
                              getFieldValue("pdf_en") ||
                              getFieldValue("pdf_en_url"),
                            message: t("works.step1.required", {
                              label: t("works.step3.field12.placeholder1"),
                            }),
                          },
                        ]}
                        initialValue={accWorksOneData.pdf_title_en}
                      >
                        <Input
                          placeholder={t("works.step3.field12.placeholder1")}
                        ></Input>
                      </Form.Item>
                    </>
                  );
                }}
              </Form.Item>
            </Col>

            <Col span={rwd.device === "mobile" ? 24 : 12}>
              <Form.Item label={t("works.step3.field12")} noStyle>
                <Form.Item
                  noStyle
                  shouldUpdate={(prevValues, curValues) =>
                    prevValues.pdf_zh !== curValues.pdf_zh ||
                    prevValues.pdf_zh_url !== curValues.pdf_zh_url ||
                    prevValues.pdf_title_zh !== curValues.pdf_title_zh
                  }
                >
                  {({ setFieldsValue, getFieldValue }) => {
                    return (
                      <>
                        {/* required={getFieldValue("pdf_title_zh")} */}
                        <Form.Item
                          label={t("works.step3.field14")}
                          name={
                            accWorksOneData.pdf_zh_url
                              ? getFieldValue("pdf_zh")
                                ? "pdf_zh"
                                : "pdf_zh_url"
                              : "pdf_zh"
                          }
                          rules={[
                            {
                              required: getFieldValue("pdf_title_zh"),
                              message: t("works.step1.required", {
                                label: t("works.step3.field14"),
                              }),
                            },
                            // {
                            //   validator: (rule, file) => {
                            //     if (file) {
                            //       if (typeof file === "string") {
                            //         return Promise.resolve();
                            //       } else {
                            //         file = file.file || file;
                            //         if (file.size / 1024 / 1024 < 5) {
                            //           return Promise.resolve();
                            //         } else {
                            //           return Promise.reject(
                            //             new Error(t("works.step3.error.msg1"))
                            //           );
                            //         }
                            //       }
                            //     } else {
                            //       return Promise.resolve();
                            //     }
                            //   },
                            // },
                          ]}
                          initialValue={accWorksOneData.pdf_zh_url}
                        >
                          <Upload
                            className="customUpload1"
                            accept="application/pdf"
                            showUploadList={false}
                            maxCount={1}
                            customRequest={({ onSuccess, onError, file }) => {
                              setFieldsValue({ pdf_zh_url: "" });
                              if (file.size / 1024 / 1024 > 5) {
                                CommonMsg({
                                  type: "error",
                                  msg: t("works.step3.error.msg1"),
                                });
                                onError();
                              } else {
                                if (!getFieldValue("pdf_title_zh"))
                                  setFieldsValue({
                                    pdf_title_zh: file.name.replace(".pdf", ""),
                                  });
                                onSuccess();
                              }
                            }}
                            onChange={(info) =>
                              handleChange(info, setFieldsValue, "pdf_zh")
                            }
                            beforeUpload={beforeUpload}
                          >
                            <div className="uploadContainer">
                              {getFieldValue("pdf_zh_url") ? (
                                <>
                                  <SVGPdfFile />
                                  <div className="fileName">
                                    {accWorksOneData.pdf_title_zh}
                                  </div>
                                  <div className="operate">
                                    <SVGRemove
                                      onClick={(e) => {
                                        e.stopPropagation();
                                        setFieldsValue({ pdf_zh_url: "" });
                                      }}
                                    />
                                  </div>
                                </>
                              ) : getFieldValue("pdf_zh") ? (
                                <>
                                  <SVGPdfFile />
                                  <div className="fileName">
                                    {getFieldValue("pdf_zh").name}
                                  </div>
                                  <div className="operate">
                                    <SVGRemove
                                      onClick={(e) => {
                                        e.stopPropagation();
                                        setFieldsValue({ pdf_zh: null });
                                      }}
                                    />
                                  </div>
                                </>
                              ) : (
                                <>
                                  <SVGFilesFolders />
                                  <div className="desc1">
                                    {t("works.step3.field12.desc1")}
                                  </div>
                                  <div className="desc2">
                                    {t("works.step3.field12.desc2")}
                                  </div>
                                </>
                              )}
                            </div>
                          </Upload>
                        </Form.Item>

                        <Form.Item
                          required={
                            getFieldValue("pdf_zh") ||
                            getFieldValue("pdf_zh_url")
                          }
                          name="pdf_title_zh"
                          rules={[
                            {
                              required:
                                getFieldValue("pdf_zh") ||
                                getFieldValue("pdf_zh_url"),
                              message: t("works.step1.required", {
                                label: t("works.step3.field12.placeholder2"),
                              }),
                            },
                          ]}
                          initialValue={accWorksOneData.pdf_title_zh}
                        >
                          <Input
                            placeholder={t("works.step3.field12.placeholder2")}
                          ></Input>
                        </Form.Item>
                      </>
                    );
                  }}
                </Form.Item>
              </Form.Item>
            </Col>
            <div className={classNames(styles.emailNotice2)}>
              <SVGAlert2 />
              {t("works.step3.msg1")}
            </div>
          </Row>
        </Form.Item>

        <div className={classNames(styles.customSubmit)}>
          <div className={classNames(styles.submit)}>
            <FancyButton htmlType="submit" scaleSize={38}>
              {t("works.step2.submit")}
            </FancyButton>
          </div>
        </div>
      </Form>

      <Modal
        visible={modal1}
        footer={null}
        onCancel={() => {
          setModal1(false);
        }}
        width={800}
        closeIcon={<SVGClose className={styles.lightboxClose} />}
      >
        <div className={styles.noticeContainer}>
          <div className={styles.noticeTitle}>
            {t("works.step3.add.title2")}
          </div>
          <Form
            layout="vertical"
            form={modal1Form}
            onFinish={handleModal1Finish}
          >
            <Row>
              <Col span={24}>
                <Form.Item
                  label={t("works.step3.field2")}
                  name="year"
                  rules={[]}
                >
                  <Select placeholder="Year">
                    {yearOptions.map((yearOption) => (
                      <Option key={yearOption} value={yearOption}>
                        {yearOption}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  noStyle
                  shouldUpdate={(prevValues, curValues) =>
                    prevValues.unit_en !== curValues.unit_en
                  }
                >
                  {({ setFieldsValue, getFieldValue }) => {
                    return (
                      <Form.Item
                        label={t("works.step3.field3")}
                        name="unit_en"
                        rules={[]}
                      >
                        <Select
                          mode="tags"
                          showSearch
                          showArrow
                          optionFilterProp="title"
                          placeholder={t("works.step3.field3.placeholder")}
                          tagRender={unitTagRender}
                          onSelect={(value, option) =>
                            handleUnitSelect(
                              "unit_en",
                              value,
                              option,
                              setFieldsValue,
                              getFieldValue
                            )
                          }
                        >
                          {sourceIndexList.map((e) => (
                            <Option
                              value={e.id}
                              title={`${e.title_en} ${e.title_zh}`}
                              key={e.title_en}
                            >
                              {e.title_en}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                    );
                  }}
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  label={t("works.step3.field4")}
                  name="title_en"
                  rules={[]}
                >
                  <Input placeholder={t("works.step3.field4.placeholder")} />
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  noStyle
                  shouldUpdate={(prevValues, curValues) =>
                    prevValues.unit_zh !== curValues.unit_zh
                  }
                >
                  {({ setFieldsValue, getFieldValue }) => {
                    return (
                      <Form.Item
                        label={t("works.step3.field5")}
                        name="unit_zh"
                        rules={[]}
                      >
                        <Select
                          mode="tags"
                          showSearch
                          showArrow
                          optionFilterProp="title"
                          placeholder={t("works.step3.field5.placeholder")}
                          tagRender={unitTagRender}
                          onSelect={(value, option) =>
                            handleUnitSelect(
                              "unit_zh",
                              value,
                              option,
                              setFieldsValue,
                              getFieldValue
                            )
                          }
                        >
                          {sourceIndexList.map((e) => (
                            <Option
                              value={e.id}
                              title={`${e.title_en} ${e.title_zh}`}
                              key={e.title_zh}
                            >
                              {e.title_zh}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                    );
                  }}
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  label={t("works.step3.field6")}
                  name="title_zh"
                  rules={[]}
                >
                  <Input placeholder={t("works.step3.field6.placeholder")} />
                </Form.Item>
              </Col>

              <Form.Item name="workt1_no" hidden />
              <Form.Item name="action" hidden />
            </Row>
            <div className={classNames(styles.submit)}>
              {errorModal1 && <div style={{ color: "red" }}>{errorModal1}</div>}
              <FancyButton>{t("works.step3.add.submit")}</FancyButton>
            </div>
          </Form>
        </div>
      </Modal>

      <Modal
        visible={modal2}
        footer={null}
        onCancel={() => {
          setModal2(false);
        }}
        width={800}
        closeIcon={<SVGClose className={styles.lightboxClose} />}
      >
        <div className={styles.noticeContainer}>
          <div className={styles.noticeTitle}>
            {t("works.step3.add.title1")}
          </div>
          <Form
            layout="vertical"
            form={modal2Form}
            onFinish={handleModal2Finish}
          >
            <Row>
              <Col span={24}>
                <Form.Item
                  label={t("works.step3.field2")}
                  name="year"
                  rules={[]}
                >
                  <Select placeholder="Year">
                    {yearOptions.map((yearOption) => (
                      <Option key={yearOption} value={yearOption}>
                        {yearOption}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  label={t("works.step3.field9")}
                  name="nation"
                  rules={[]}
                >
                  <Select
                    showSearch
                    showArrow
                    placeholder={t("works.step3.field9.placeholder")}
                    optionFilterProp="title"
                  >
                    {country.map((c) => (
                      <Option
                        key={c.name}
                        value={c.name}
                        title={`${c.name} ${c.traditionalName}`}
                      >
                        {locale === "en" ? c.name : c.traditionalName}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  label={t("works.step3.field10")}
                  name="name_en"
                  rules={[]}
                >
                  <Input placeholder={t("works.step3.field10.placeholder")} />
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                  label={t("works.step3.field11")}
                  name="name_zh"
                  rules={[]}
                >
                  <Input placeholder={t("works.step3.field11.placeholder")} />
                </Form.Item>
              </Col>

              <Form.Item name="workt2_no" hidden />
              <Form.Item name="action" hidden />
            </Row>
            <div className={classNames(styles.submit)}>
              {errorModal2 && <div style={{ color: "red" }}>{errorModal2}</div>}
              <FancyButton>{t("works.step3.add.submit")}</FancyButton>
            </div>
          </Form>
        </div>
      </Modal>
    </>
  );
};

const Form4 = ({
  rwd,
  setShowLoading,
  showSaveMsgPopup,
  setShowSaveMsgPopup,
  saveStep,
  checkMode,
  onSend,
  onPrev,
  formItems,
  country,
  accWorksOneData,
  memberId,
  authKey,
  workm1no,
  SaveMsgPopup,
}) => {
  const { locale } = useRouter();
  const { t } = useTranslation("common");
  const [unitDataAllList, setUnitDataAllList] = useState([]);
  const [form4] = Form.useForm();

  const unitData4List = useMemo(
    () => unitDataAllList.filter((e) => e.unit === "4"),
    [unitDataAllList]
  );

  const madecoutryOptions = country.map((e) => {
    return {
      value: `${e.name} ${e.traditionalName}`,
      label: `${e.name} ${e.traditionalName}`,
    };
  });

  const workct1InitailOptions = accWorksOneData.workct1.map((e, i) => {
    if (e.Unitm1_no) {
      return {
        value: e.Unitm1_no,
        label: e.name,
      };
    } else {
      return {
        value: new Date() / 1 + i,
        label: e.name,
      };
    }
  });
  const madecoutryInitailOptions = accWorksOneData.madecoutry.map((e) => {
    return {
      value: e,
      label: e,
    };
  });
  const rolecreationOptions = parseItems(formItems.rolecreation);
  const roleproductionOptions = parseItems(formItems.roleproduction);

  const workct2Initial = accWorksOneData.workct2.filter(
    (e) => e.name.length > 0 || e.Unitm1_no.length > 0
  );
  const workct3Initial = accWorksOneData.workct3.filter(
    (e) => e.name.length > 0 || e.Unitm1_no.length > 0
  );

  const formatWorkctDetail = (value, listName) => {
    const data = listName.find((e) => e.id === value);
    if (data) {
      return data;
    } else {
      return {};
    }
  };

  useEffect(() => {
    (async () => {
      setShowLoading(true);
      const { resu, data } = await unitList({
        auth_key: authKey,
        memberm1_no: memberId,
        type: 0,
      });
      if (resu === 1) {
        setUnitDataAllList(data);
      }
      setShowLoading(false);
    })();
  }, []);

  const handleCliclNext = useCallback(
    (values) => {
      console.log(values);
      const { workct2title, workct2name, workct3title, workct3name, ...rest } =
        values;

      setShowLoading(true);
      const calcRequiredField = () => {
        let total = 0;

        if (values.workct1.length === 0) total = total + 1;
        if (values.madecoutry.length === 0) total = total + 1;
        if (values.workct2.length === 0) total = total + 1;
        if (values.workct3.length === 0) total = total + 1;
        if (!values.email) total = total + 1;
        if (!values.contact_en) total = total + 1;
        if (!values.lastname_en) total = total + 1;

        return Math.floor(((7 - total) / 7) * 100);
      };

      setShowSaveMsgPopup(false);
      accWorksModifyst5({
        auth_key: authKey,
        memberm1_no: memberId,
        workm1_no: workm1no,
        ...rest,
        madecoutry: rest.madecoutry.map((e) => e.value),
        workct1: rest.workct1.map((e) => {
          const isFormatById = typeof e.label === "object" || e.label === "";
          return {
            Unitm1_no: isFormatById ? e.value : null,
            name: isFormatById ? "" : e.label || e.value,
          };
        }),
        step4: calcRequiredField(),
        agree1: rest.agree1 ? 1 : 0,
      }).then((res) => {
        if (res.resu === 1) {
          onSend(values);
          if (values.review === "1") {
            router.push(
              `${router.pathname.replace(
                "[workm1no]",
                router.query.workm1no
              )}?step=5`
            );
          } else {
            CommonMsg({
              type: "success",
              msg: t("works.message.save"),
            });
            const currentStep = showSaveMsgPopup ? saveStep.current : 4;

            router.replace(
              `${router.pathname.replace(
                "[workm1no]",
                router.query.workm1no
              )}?step=${currentStep}`
            );
          }
        } else {
        }
        setShowLoading(false);
      });
    },
    [onSend]
  );

  const handleSuccessApply = () => {
    form4.setFieldsValue({
      review: "1",
    });
    handleCliclNext(form4.getFieldValue());
  };

  return (
    <>
      <Form
        name="step4"
        layout="vertical"
        onKeyDown={(e) => (e.keyCode == 13 ? e.preventDefault() : "")}
        onFinish={handleCliclNext}
        onFinishFailed={({ values, errorFields }) => {
          console.log(values, errorFields);
        }}
        scrollToFirstError
        form={form4}
      >
        <Form.Item
          noStyle
          shouldUpdate={(prevValues, curValues) =>
            prevValues.workct1 !== curValues.workct1
          }
        >
          {({ getFieldValue }) => {
            return (
              <Form.Item label={t("works.step4.field1")}>
                <MultipleSelect1
                  rwd={rwd}
                  required
                  label={t("works.step4.field2")}
                  name="workct1"
                  rules={[]}
                  options={undefined}
                  checkMode={checkMode}
                  placeholder={t("works.step4.field2.placeholder")}
                  limit={3}
                  showCount={true}
                  mode={
                    getFieldValue("workct1")
                      ? getFieldValue("workct1").length === 3
                        ? "multiple"
                        : "tags"
                      : "multiple"
                  }
                  optionFilterProp="title"
                  optionLabelProp="children"
                  initialValue={workct1InitailOptions}
                  tagChildren={(tagData) => {
                    // 1. could be selected in option now
                    // key: "16"
                    // label: {$$typeof: Symbol(react.element), type: 'div', key: null, ref: null, props: {…}, …}
                    // title: "TEST_A測試者A"
                    // value: 16

                    // 抓 id

                    // 2. could be manullay input in option now
                    // key: undefined
                    // label: undefined
                    // title: undefined
                    // value: "ffffffffff"

                    // 空的方式

                    // 3. initial with existing
                    // key: "16"
                    // label: ""
                    // title: "TEST_A測試者A"
                    // value: 16

                    // 抓 id

                    // 4. initial with manual
                    // key: undefined
                    // label: "aaa123"
                    // title: undefined
                    // value: 1713412754395

                    return typeof tagData.label === "object" ||
                      tagData.label === "" ? (
                      <>
                        {formatWorkctDetail(tagData.value, unitData4List)
                          .pic_min ? (
                          <img
                            src={
                              formatWorkctDetail(tagData.value, unitData4List)
                                .pic_min
                            }
                            alt="icon"
                            style={{
                              width: 25,
                              height: 25,
                              marginRight: 15,
                              verticalAlign: "middle",
                              borderRadius: "50%",
                            }}
                          />
                        ) : (
                          <SVGMemberDefaultIcon
                            style={{
                              width: 25,
                              height: 25,
                              marginRight: 15,
                              verticalAlign: "middle",
                              borderRadius: "50%",
                            }}
                          />
                        )}

                        {`${
                          formatWorkctDetail(tagData.value, unitData4List)
                            .name_en || ""
                        } ${
                          formatWorkctDetail(tagData.value, unitData4List)
                            .lastname_en || ""
                        } ${
                          formatWorkctDetail(tagData.value, unitData4List)
                            .name_zh || ""
                        }`}
                      </>
                    ) : (
                      <>
                        <SVGMemberDefaultIcon
                          style={{
                            width: 25,
                            height: 25,
                            marginRight: 15,
                            verticalAlign: "middle",
                            borderRadius: "50%",
                          }}
                        />

                        {tagData.label || tagData.value}
                      </>
                    );
                  }}
                >
                  {unitData4List.map((e) => (
                    <Option
                      title={`${e.name_en || ""}${e.lastname_en || ""}${
                        e.name_zh || ""
                      }`}
                      value={e.id}
                      key={e.id}
                    >
                      <div style={{ display: "inline-block" }}>
                        {e.pic_min ? (
                          <img
                            src={e.pic_min}
                            alt="icon"
                            style={{
                              width: 25,
                              height: "auto",
                              marginRight: 15,
                              verticalAlign: "middle",
                              borderRadius: "50%",
                            }}
                          />
                        ) : (
                          <SVGMemberDefaultIcon
                            style={{
                              width: 25,
                              height: 25,
                              marginRight: 15,
                              verticalAlign: "middle",
                              borderRadius: "50%",
                            }}
                          />
                        )}
                        <span style={{ verticalAlign: "middle" }}>{`${
                          e.name_en || ""
                        } ${e.lastname_en || ""} ${e.name_zh || ""}`}</span>
                      </div>
                    </Option>
                  ))}
                </MultipleSelect1>
              </Form.Item>
            );
          }}
        </Form.Item>

        <MultipleSelect1
          rwd={rwd}
          required
          label={t("works.step4.field3")}
          name="madecoutry"
          rules={[]}
          options={madecoutryOptions}
          checkMode={checkMode}
          placeholder={t("works.step4.field3.placeholder")}
          limit={null}
          showCount={false}
          filterSort={(option1, option2, searchTest) => {
            if (searchTest.current) {
              if (
                option1.label
                  .slice(0, searchTest.current.length)
                  .toUpperCase() === searchTest.current.toUpperCase()
              ) {
                return -1;
              } else if (
                option2.label
                  .slice(0, searchTest.current.length)
                  .toUpperCase() === searchTest.current.toUpperCase()
              ) {
                return 1;
              } else {
                return option1.label
                  .toUpperCase()
                  .localeCompare(option2.label.toUpperCase());
              }
            } else {
              return 0;
            }
          }}
          initialValue={madecoutryInitailOptions}
        />

        <Form.Item
          noStyle
          shouldUpdate={(prevValues, curValues) =>
            prevValues.workct2 !== curValues.workct2
          }
        >
          {({ getFieldValue }) => {
            const limit = 10;
            let total = 0;
            (function handleTotalAmount() {
              if (getFieldValue("workct2")) {
                getFieldValue("workct2").forEach(
                  (e) => (total = total + e.Unitm1_no.length + e.name.length)
                );
              }
            })();

            return (
              <Form.Item
                label={t("works.step4.field4")}
                name="workct2"
                rules={[]}
                initialValue={workct2Initial}
              >
                <MultipleSelect2
                  rwd={rwd}
                  locale={locale}
                  t={t}
                  required
                  name="workct2"
                  label1={t("works.step4.field5")}
                  label2={t("works.step4.field6")}
                  rules={[]}
                  options1={rolecreationOptions}
                  optionsOriginFormat1={formItems.rolecreation}
                  options2={undefined}
                  mode2={total === limit ? "multiple" : "tags"}
                  totalAmount={total}
                  checkMode={checkMode}
                  placeholder1={t("works.step4.field5.placeholder")}
                  placeholder2={t("works.step4.field6.placeholder")}
                  limit={limit}
                  showCount={true}
                  initialValue={[]}
                  formatWorkctDetail={(value) =>
                    formatWorkctDetail(value, unitDataAllList)
                  }
                  children2={() => {
                    return unitDataAllList.map((e) => (
                      <Option
                        title={`${e.name_en || ""}${e.lastname_en || ""}${
                          e.name_zh || ""
                        }`}
                        value={e.id}
                        key={e.id}
                      >
                        <div style={{ display: "inline-block" }}>
                          {e.pic_min ? (
                            <img
                              src={e.pic_min}
                              alt="icon"
                              style={{
                                width: 25,
                                height: "auto",
                                marginRight: 15,
                                verticalAlign: "middle",
                                borderRadius: "50%",
                              }}
                            />
                          ) : (
                            <SVGMemberDefaultIcon
                              style={{
                                width: 25,
                                height: 25,
                                marginRight: 15,
                                verticalAlign: "middle",
                                borderRadius: "50%",
                              }}
                            />
                          )}
                          <span style={{ verticalAlign: "middle" }}>{`${
                            e.name_en || ""
                          } ${e.lastname_en || ""} ${e.name_zh || ""}`}</span>
                        </div>
                      </Option>
                    ));
                  }}
                />
              </Form.Item>
            );
          }}
        </Form.Item>

        <Form.Item
          noStyle
          shouldUpdate={(prevValues, curValues) =>
            prevValues.workct3 !== curValues.workct3
          }
        >
          {({ getFieldValue }) => {
            let total = 0;
            const limit = 3;
            (function handleTotalAmount() {
              if (getFieldValue("workct3")) {
                getFieldValue("workct3").forEach(
                  (e) => (total = total + e.Unitm1_no.length + e.name.length)
                );
              }
            })();

            return (
              <Form.Item
                label={t("works.step4.field7")}
                name="workct3"
                rules={[]}
                initialValue={workct3Initial}
              >
                <MultipleSelect2
                  rwd={rwd}
                  locale={locale}
                  t={t}
                  required
                  name="workct3"
                  label1={t("works.step4.field5")}
                  label2={t("works.step4.field6")}
                  rules={[]}
                  options1={roleproductionOptions}
                  optionsOriginFormat1={formItems.roleproduction}
                  options2={undefined}
                  mode2={total === limit ? "multiple" : "tags"}
                  totalAmount={total}
                  checkMode={checkMode}
                  placeholder1={t("works.step4.field5.placeholder")}
                  placeholder2={t("works.step4.field6.placeholder")}
                  limit={limit}
                  showCount={true}
                  initialValue={[]}
                  formatWorkctDetail={(value) =>
                    formatWorkctDetail(value, unitDataAllList)
                  }
                  children2={() => {
                    return unitDataAllList.map((e) => (
                      <Option
                        title={`${e.name_en} ${e.lastname_en} ${e.name_zh}`}
                        value={e.id}
                        key={e.id}
                      >
                        <div style={{ display: "inline-block" }}>
                          {e.pic_min ? (
                            <img
                              src={e.pic_min}
                              alt="icon"
                              style={{
                                width: 25,
                                height: "auto",
                                marginRight: 15,
                                verticalAlign: "middle",
                                borderRadius: "50%",
                              }}
                            />
                          ) : (
                            <SVGMemberDefaultIcon
                              style={{
                                width: 25,
                                height: 25,
                                marginRight: 15,
                                verticalAlign: "middle",
                                borderRadius: "50%",
                              }}
                            />
                          )}
                          <span style={{ verticalAlign: "middle" }}>{`${
                            e.name_en || ""
                          } ${e.lastname_en || ""} ${e.name_zh || ""}`}</span>
                        </div>
                      </Option>
                    ));
                  }}
                />
              </Form.Item>
            );
          }}
        </Form.Item>

        <Form.Item label={t("works.step4.field10")}>
          <Row>
            <Col span={rwd.device === "mobile" ? 24 : 18}>
              <Form.Item
                required
                label={t("works.step4.field11")}
                name="email"
                rules={[{ type: "email", message: t("signin.error.msg7") }]}
                initialValue={accWorksOneData.email}
              >
                <Input placeholder="Email" />
              </Form.Item>
            </Col>

            <Col span={24}>
              <Row gutter={18}>
                <Col span={rwd.device === "mobile" ? 24 : 8}>
                  <Form.Item
                    required
                    label={t("works.step4.field12")}
                    name="contact_en"
                    rules={[]}
                    initialValue={accWorksOneData.contact_en}
                  >
                    <Input placeholder="First name" />
                  </Form.Item>
                </Col>

                <Col span={rwd.device === "mobile" ? 24 : 8}>
                  <Form.Item
                    required
                    label={" "}
                    name="lastname_en"
                    rules={[]}
                    initialValue={accWorksOneData.lastname_en}
                  >
                    <Input placeholder="Last name" />
                  </Form.Item>
                </Col>

                <Col span={rwd.device === "mobile" ? 24 : 8}>
                  <Form.Item
                    label={t("works.step4.field13")}
                    name="contact_zh"
                    rules={[]}
                    initialValue={accWorksOneData.contact_zh}
                  >
                    <Input />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
          </Row>
        </Form.Item>

        <Row gutter={[18, 18]} className={styles.step4BtnGroup}>
          <Col
            span={
              rwd.device === "mobile" ? 24 : accWorksOneData.isfinish ? 6 : 12
            }
          >
            <FancyButton
              htmlType="button"
              scaleSize={38}
              className={classNames(styles.step4Btn)}
              onClick={() =>
                router.push(
                  `/content/${accWorksOneData.workm1_no}/${accWorksOneData.name_en}?preview=1`
                )
              }
            >
              {t("works.step4.submit1")}
            </FancyButton>
          </Col>

          <Col
            span={
              rwd.device === "mobile" ? 24 : accWorksOneData.isfinish ? 6 : 12
            }
          >
            <Form.Item name="review" noStyle>
              <Form.Item
                noStyle
                shouldUpdate={(prevValues, curValues) =>
                  prevValues.review !== curValues.review
                }
              >
                {({ setFieldsValue }) => {
                  return (
                    <FancyButton
                      htmlType="submit"
                      scaleSize={38}
                      className={classNames(styles.step4Btn)}
                      onClick={() => {
                        setFieldsValue({
                          review: null,
                        });
                      }}
                    >
                      {t("works.step4.submit2")}
                    </FancyButton>
                  );
                }}
              </Form.Item>
            </Form.Item>
          </Col>

          {accWorksOneData.isfinish && (
            <Col span={rwd.device === "mobile" ? 24 : 12}>
              <ApplyWorkPopup
                onSuccess={handleSuccessApply}
                SubmitBtn={({ text }) => (
                  <FancyButton
                    selected
                    htmlType="button"
                    scaleSize={38}
                    className={classNames(styles.step4Btn)}
                  >
                    {text}
                  </FancyButton>
                )}
              />
            </Col>
          )}
        </Row>
      </Form>
    </>
  );
};

const FormSuccessTitle = (t) => {
  return (
    <>
      <div>{t("companion.success.title1")}</div>
      <div>{t("companion.success.title2")}</div>
    </>
  );
};

export default function Works({
  formItems,
  country,
  authKey,
  memberId,
  identity,
  accWorksOneData,
  workm1no,
  step,
}) {
  const router = useRouter();
  const locale = router.locale;
  const rwd = useRWD();
  const { setShowLoading } = useContext(LoadingContext);
  const [showSaveMsgPopup, setShowSaveMsgPopup] = useState(false);
  const saveStep = useRef(1);
  const { t } = useTranslation("common");
  const [renameModal, setRenameModal] = useState(false);

  const handleAcWorksRenameFinish = useCallback(
    (data) => {
      console.log(data);
      setShowLoading(true);
      accWorksRename({
        auth_key: authKey,
        memberm1_no: memberId,
        workm1_no: workm1no,
        ...data,
      }).then((res) => {
        if (res.resu === 1) {
          console.log(router);

          router.replace(
            `${router.pathname.replace(
              "[workm1no]",
              router.query.workm1no
            )}?step=${step}`,
            undefined,
            { scroll: false }
          );
          setRenameModal(false);
        }
        setShowLoading(false);
      });
    },
    [step]
  );

  const handleProfileIcon = () => {
    if (rwd.device !== "mobile") {
      if (accWorksOneData.pic_min) {
        return <img src={accWorksOneData.pic_min} alt="icon" />;
      } else {
        return <SVGMemberDefaultIcon style={{ width: 80, height: 80 }} />;
      }
    }
  };

  const SaveMsgPopup = ({ handleSubmit }) => {
    return (
      <CommonLightBox
        containerClass={classNames(styles.container)}
        titleClass={classNames(styles.title)}
        descClass={classNames(styles.desc)}
        button2Class={classNames(styles.button2)}
        desc={t("companion.changes.desc1")}
        button1={t("companion.changes.btn1")}
        showButton2={true}
        button2={t("companion.changes.btn2")}
        showModal={showSaveMsgPopup}
        setShowModal={setShowSaveMsgPopup}
        onClose2={() => {
          //setShowSaveMsgPopup(false);
          handleSubmit();
        }}
      />
    );
  };

  const handleClickStep = (value) => {
    setShowSaveMsgPopup(true);
    saveStep.current = value;
    console.log(value);
  };

  return (
    <>
      <HeaderBannerNew1
        title={t("meun.memberCenter")}
        showBack={true}
        backLink="/user/myAccount"
        backText={t("myAccount.title")}
      />

      {step !== 5 ? (
        <div className={classNames(styles.contentContainer, "companionPage")}>
          <div className={classNames(styles.head)}>
            {handleProfileIcon()}
            <div className={classNames(styles.headBlock)}>
              <div className={classNames(styles.nameEn)}>
                {accWorksOneData.name_en}
              </div>
              <Row className={classNames(styles.nameZh)}>
                {accWorksOneData.name_zh}
                <div
                  className={classNames(styles.edit)}
                  onClick={() => setRenameModal(true)}
                >
                  <SVGWorkEdit style={{ cursor: "pointer" }} />
                </div>
              </Row>
            </div>
          </div>

          <UserStep
            step={step}
            isMoble={rwd.device === "mobile"}
            className={styles.steps}
            t={t}
            stepList={[
              {
                title: t("works.step1.progress"),
                show: true,
                progress: accWorksOneData.step1,
                link: `${router.pathname.replace(
                  "[workm1no]",
                  router.query.workm1no
                )}?step=1`,
                onClick: handleClickStep,
              },
              {
                title: t("works.step2.progress"),
                show: true,
                progress: accWorksOneData.step2,
                link: `${router.pathname.replace(
                  "[workm1no]",
                  router.query.workm1no
                )}?step=2`,
                onClick: handleClickStep,
              },
              {
                title: t("works.step3.progress"),
                show: true,
                progress: accWorksOneData.step3,
                link: `${router.pathname.replace(
                  "[workm1no]",
                  router.query.workm1no
                )}?step=3`,
                onClick: handleClickStep,
              },
              {
                title: t("works.step4.progress"),
                show: true,
                progress: accWorksOneData.step4,
                link: `${router.pathname.replace(
                  "[workm1no]",
                  router.query.workm1no
                )}?step=4`,
                onClick: handleClickStep,
              },
            ]}
          />

          <div>
            {step === 1 && (
              <Form1
                rwd={rwd}
                setShowLoading={setShowLoading}
                showSaveMsgPopup={showSaveMsgPopup}
                setShowSaveMsgPopup={setShowSaveMsgPopup}
                SaveMsgPopup={SaveMsgPopup}
                saveStep={saveStep}
                checkMode={step === 3}
                formItems={formItems}
                accWorksOneData={accWorksOneData}
                authKey={authKey}
                memberId={memberId}
                workm1no={workm1no}
                onNext={() => {}}
              />
            )}

            {step === 2 && (
              <Form2
                rwd={rwd}
                setShowLoading={setShowLoading}
                showSaveMsgPopup={showSaveMsgPopup}
                setShowSaveMsgPopup={setShowSaveMsgPopup}
                SaveMsgPopup={SaveMsgPopup}
                saveStep={saveStep}
                checkMode={step === 3}
                formItems={formItems}
                accWorksOneData={accWorksOneData}
                authKey={authKey}
                memberId={memberId}
                workm1no={workm1no}
                country={country}
                onNext={() => {}}
              />
            )}

            {step === 3 && (
              <Form3
                rwd={rwd}
                setShowLoading={setShowLoading}
                showSaveMsgPopup={showSaveMsgPopup}
                setShowSaveMsgPopup={setShowSaveMsgPopup}
                SaveMsgPopup={SaveMsgPopup}
                saveStep={saveStep}
                checkMode={false}
                formItems={formItems}
                accWorksOneData={accWorksOneData}
                authKey={authKey}
                memberId={memberId}
                workm1no={workm1no}
                country={country}
                onNext={() => {}}
              />
            )}

            {step === 4 && (
              <Form4
                rwd={rwd}
                setShowLoading={setShowLoading}
                showSaveMsgPopup={showSaveMsgPopup}
                setShowSaveMsgPopup={setShowSaveMsgPopup}
                SaveMsgPopup={SaveMsgPopup}
                saveStep={saveStep}
                checkMode={false}
                formItems={formItems}
                accWorksOneData={accWorksOneData}
                authKey={authKey}
                memberId={memberId}
                workm1no={workm1no}
                country={country}
                onSend={(data) => {
                  // dataRef.current = { ...dataRef.current, ...data };
                  // handelSandData(dataRef.current);
                }}
              />
            )}
          </div>
        </div>
      ) : (
        <FormSuccess
          title={FormSuccessTitle(t)}
          desc={t("companion.success.desc")}
        />
      )}

      <Modal
        visible={renameModal}
        footer={null}
        onCancel={() => {
          setRenameModal(false);
        }}
        width={800}
        closeIcon={<SVGLBX className={styles.lightboxClose} />}
      >
        <div className={styles.noticeContainer}>
          <div className={styles.noticeTitle}>{t("works.edit.title")}</div>
          <Form layout="vertical" onFinish={handleAcWorksRenameFinish}>
            <Form.Item
              required
              name="name_en"
              label={t("works.add.field1")}
              rules={[
                {
                  required: true,
                  message: t("works.add.required", {
                    label: t("works.add.field1"),
                  }),
                },
                {
                  message: t("works.add.error.msg1"),
                  max: 60,
                },
              ]}
              initialValue={accWorksOneData.name_en}
            >
              <Input placeholder={t("works.add.field1")} />
            </Form.Item>

            <Form.Item
              label={t("works.add.field2")}
              name="name_zh"
              rules={[
                {
                  message: t("works.add.error.msg1"),
                  max: 25,
                },
              ]}
              initialValue={accWorksOneData.name_zh}
            >
              <Input placeholder={t("works.add.field2")} />
            </Form.Item>
            <div className={classNames(styles.customSubmit)}>
              <div className={classNames(styles.submit)}>
                <FancyButton>{t("works.edit.submit")}</FancyButton>
              </div>
            </div>
          </Form>
        </div>
      </Modal>

      <Footer />
    </>
  );
}
Works.headerProps = {};
export async function getServerSideProps(context) {
  const { locale, query } = context;
  console.log(query, "query");

  const cookies = context.req.cookies;
  const { isLogin, redirect } = auth(cookies);
  if (!isLogin) return redirect;

  let email = "";
  let identity = "";
  let authKey = "";
  let memberId = "";
  const workm1no = context.params.workm1no;

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    email = authData.email;
    identity = authData.identity;
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  const handleSourceItem = async () => {
    let formItems = {};
    const { resu, data } = await sourceItem({
      locale: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const handleAccWorksOne = async () => {
    let accWorksOneData = {};

    const { resu, data } = await accWorksOne({
      auth_key: authKey,
      memberm1_no: memberId,
      workm1_no: workm1no,
    });
    if (resu === 1) {
      accWorksOneData = data;
    }
    return accWorksOneData;
  };

  const handleGetCountry = async () => {
    let country = {};

    const { resu, data } = await getCountry({
      locale,
    });
    if (resu === 1) {
      country = data.country;
    }
    return country;
  };

  const [formItems, accWorksOneData, country] = await Promise.all([
    handleSourceItem(),
    handleAccWorksOne(),
    handleGetCountry(),
  ]);

  return {
    props: {
      formItems,
      country,
      authKey,
      memberId,
      identity,
      accWorksOneData,
      workm1no,
      step: Number(query.step) || 1,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
