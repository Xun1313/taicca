/* eslint-disable @next/next/no-img-element */
import React, {
  useCallback,
  useMemo,
  useRef,
  useState,
  useEffect,
  useContext,
} from "react";

import styles from "@/styles/Match.module.scss";
import { Footer } from "@/components/Footer/Footer";
import {
  Form,
  Input,
  Radio,
  Row,
  Col,
  Checkbox,
  Select,
  Upload,
  InputNumber,
  Tabs,
  Tag,
  Button,
  message,
  Alert,
} from "antd";

const { Option } = Select;
const { TextArea } = Input;
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";

import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from 'next-i18next';
import { matchIndex, matchModify, sourceMatch } from "@/api";
import router, { useRouter } from "next/router";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { auth } from "@/utilits/auth";
import { parseCookies } from "@/utilits/parseCookies";
import { parseItems } from "@/utilits/parseItems";
import { LoadingContext } from "@/context/LoadingContext";
import { CommonLightBox } from "@/components/CommonLightBox/CommonLightBox";
import { CommonMsg } from "@/components/CommonMsg/CommonMsg";

const CheckboxGroup = ({
  name,
  label,
  rules,
  options,
  required,
  className,
  initialValue,
  showOtherInput = false,
  otherInputName,
  otherInputInitialValue,
}) => {
  return (
    <Form.Item
      required={required}
      label={label}
      name={name}
      rules={rules}
      className={className}
      initialValue={initialValue}
    >
      <Checkbox.Group className={styles.checkboxGroup}>
        {options.map((e) => (
          <Checkbox value={e.value} key={e.value}>
            <span>{e.label}</span>

            {showOtherInput && e.value === "other" && (
              <Form.Item
                className={styles.otherInput}
                name={otherInputName}
                initialValue={otherInputInitialValue}
              >
                <Input />
              </Form.Item>
            )}
          </Checkbox>
        ))}
      </Checkbox.Group>
      {/* options={options.filter((o) =>
                !checkMode
                  ? true
                  : currentValue?.findIndex((c) => c === o.value) !== -1 || []
              )} */}
    </Form.Item>
  );
};

export default function Match({
  formItems,
  matchIndexData,
  authKey,
  memberId,
}) {
  const router = useRouter();
  const locale = router.locale;
  const rwd = useRWD();
  const { t } = useTranslation("common");
  const { setShowLoading } = useContext(LoadingContext);
  const unittypeOptions = parseItems(formItems.unittype);
  const clienteleOptions = parseItems(formItems.clientele);
  const industryOptions = parseItems(formItems.industry);
  const sphereOptions = parseItems(formItems.sphere);
  const otherserviceOptions = parseItems(formItems.otherservice);
  const continentsDirectOptions = parseItems(formItems.continents_direct);

  const parseInitial = (value) => (value ? value.split(",") : []);

  const hopeSphereInitialOptions = parseInitial(matchIndexData.hopeSphere);
  const hopeObjectInitialOptions = parseInitial(matchIndexData.hopeObject);
  const applicationInitialOptions = parseInitial(matchIndexData.application);
  const hopeBusinessInitialOptions = parseInitial(matchIndexData.hopeBusiness);
  const hopeOtherInitialOptions = parseInitial(matchIndexData.hopeOther);
  const exismarketInitialOptions = parseInitial(matchIndexData.exismarket);
  const futuremarketInitialOptions = parseInitial(matchIndexData.futuremarket);
  const goalsInitial = matchIndexData.goals || "";
  const exismarketOtherInitial = matchIndexData.exismarket_other || "";
  const futuremarketOtherInitial = matchIndexData.futuremarket_other || "";

  const handleCliclNext = useCallback((values) => {
    const { goals, exismarket_other, futuremarket_other, ...rest } = values;
    rest.exismarket = rest.exismarket.filter((e) => {
      if (e === "other") {
        if (exismarket_other) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    });

    rest.futuremarket = rest.futuremarket.filter((e) => {
      if (e === "other") {
        if (futuremarket_other) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    });
    const data = {};
    for (const key in rest) {
      data[key] = rest[key].join(",");
    }

    setShowLoading(true);
    matchModify({
      auth_key: authKey,
      memberm1_no: memberId,
      goals,
      ...data,
      exismarket_other: data.exismarket.includes("other")
        ? exismarket_other
        : "",
      futuremarket_other: data.futuremarket.includes("other")
        ? futuremarket_other
        : "",
    }).then((res) => {
      if (res.resu === 1) {
        CommonMsg({
          type: "success",
          msg: t("match.message.save"),
        });
      } else {
      }
      setShowLoading(false);
    });
  }, []);

  return (
    <>
      <HeaderBannerNew1
        title={t("meun.memberCenter")}
        showBack={true}
        backLink="/user/myAccount"
        backText={t("myAccount.title")}
      />
      <div className={styles.contentContainer}>
        <div className={styles.titleGroup}>
          <div className={styles.title}>{t("match.title")}</div>
          <div className={styles.desc}>{t("match.desc1")}</div>
        </div>
        <Form
          layout="vertical"
          onFinish={handleCliclNext}
          onFinishFailed={({ values, errorFields }) => {
            console.log(values, errorFields);
          }}
          scrollToFirstError
        >
          <CheckboxGroup
            label={t("match.field1")}
            name="hopeSphere"
            options={unittypeOptions}
            initialValue={hopeSphereInitialOptions}
          />

          <CheckboxGroup
            label={t("match.field2")}
            name="hopeObject"
            options={clienteleOptions}
            initialValue={hopeObjectInitialOptions}
          />

          <CheckboxGroup
            label={
              <>
                {t("match.field3")}
                <span style={{ fontSize: 15, marginLeft: 5 }}>
                  {t("match.max", { amount: 3 })}
                </span>
              </>
            }
            name="application"
            rules={[
              {
                type: "array",
                max: 3,
                message: t("match.max", {
                  amount: 3,
                }),
              },
            ]}
            options={industryOptions}
            initialValue={applicationInitialOptions}
          />

          <CheckboxGroup
            label={
              <>
                {t("match.field4")}
                <span style={{ fontSize: 15, marginLeft: 5 }}>
                  {t("match.max", { amount: 5 })}
                </span>
              </>
            }
            name="hopeBusiness"
            rules={[
              {
                type: "array",
                max: 5,
                message: t("match.max", {
                  amount: 5,
                }),
              },
            ]}
            options={sphereOptions}
            initialValue={hopeBusinessInitialOptions}
          />

          <CheckboxGroup
            label={
              <>
                {t("match.field5")}
                <span style={{ fontSize: 15, marginLeft: 5 }}>
                  {t("match.max", { amount: 5 })}
                </span>
              </>
            }
            name="hopeOther"
            rules={[
              {
                type: "array",
                max: 5,
                message: t("match.max", {
                  amount: 5,
                }),
              },
            ]}
            options={otherserviceOptions}
            initialValue={hopeOtherInitialOptions}
          />

          <CheckboxGroup
            label={
              <>
                {t("match.field6")}
                <span style={{ fontSize: 15, marginLeft: 5 }}>
                  {t("match.max", { amount: 3 })}
                </span>
              </>
            }
            name="exismarket"
            rules={[
              {
                type: "array",
                max: 3,
                message: t("match.max", {
                  amount: 3,
                }),
              },
            ]}
            options={continentsDirectOptions}
            initialValue={exismarketInitialOptions}
            showOtherInput={true}
            otherInputName="exismarket_other"
            otherInputInitialValue={exismarketOtherInitial}
          />

          <CheckboxGroup
            label={
              <>
                {t("match.field7")}
                <span style={{ fontSize: 15, marginLeft: 5 }}>
                  {t("match.max", { amount: 3 })}
                </span>
              </>
            }
            name="futuremarket"
            rules={[
              {
                type: "array",
                max: 3,
                message: t("match.max", {
                  amount: 3,
                }),
              },
            ]}
            options={continentsDirectOptions}
            initialValue={futuremarketInitialOptions}
            showOtherInput={true}
            otherInputName="futuremarket_other"
            otherInputInitialValue={futuremarketOtherInitial}
          />

          <Form.Item
            label={t("match.field8")}
            name="goals"
            initialValue={goalsInitial}
          >
            <TextArea showCount maxLength={700} autoSize={{ minRows: 12 }} />
          </Form.Item>

          <div className={classNames(styles.submit)}>
            <FancyButton htmlType="submit" scaleSize={38}>
              {t("match.btn1")}
            </FancyButton>
          </div>
        </Form>
      </div>

      <Footer />
    </>
  );
}
Match.headerProps = {};
export async function getServerSideProps(context) {
  const cookies = context.req.cookies;
  const { isLogin, redirect } = auth(cookies);
  if (!isLogin) return redirect;

  let authKey = "";
  let memberId = "";

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  const handleSourceMatch = async () => {
    let formItems = {};
    const { resu, data } = await sourceMatch({
      lang: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const handleMatchIndex = async () => {
    let matchIndexData = {};
    const { resu, data } = await matchIndex({
      auth_key: authKey,
      memberm1_no: memberId,
    });
    if (resu === 1) {
      matchIndexData = data || {};
    }
    return matchIndexData;
  };

  const [formItems, matchIndexData] = await Promise.all([
    handleSourceMatch(),
    handleMatchIndex(),
  ]);

  return {
    props: {
      formItems,
      matchIndexData,
      authKey,
      memberId,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
