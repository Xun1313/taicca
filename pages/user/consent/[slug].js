/* eslint-disable @next/next/no-img-element */
import React, { useState, useContext, useMemo } from "react";

import styles from "@/styles/ConsentContent.module.scss";
import { Footer } from "@/components/Footer/Footer";
import { Form, Checkbox, Switch } from "antd";
import classNames from "classnames";
import SVGInformationLink from "@/svgs/informationLink.svg";

import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import { planAgree, planJoin, sourceItem } from "@/api";
import { useRouter } from "next/router";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { auth } from "@/utilits/auth";
import { parseCookies } from "@/utilits/parseCookies";
import { LoadingContext } from "@/context/LoadingContext";
import { CommonLightBox } from "@/components/CommonLightBox/CommonLightBox";
import { CommonMsg } from "@/components/CommonMsg/CommonMsg";
import { UnitCustomCard1 } from "@/components/UnitCard/UnitCard";
import { WorkCustomCard1 } from "@/components/WorkCard/WorkCard";
import SVGInfo from "@/svgs/info.svg";
import moment from "moment";

export default function ConsentContent({
  authKey,
  memberId,
  planJoinList,
  formItems,
  slug,
  host,
  identity,
}) {
  console.log(planJoinList, 222);
  const router = useRouter();
  const locale = router.locale;
  const { t } = useTranslation("common");
  const { setShowLoading } = useContext(LoadingContext);
  const [form] = Form.useForm();
  const isPreview = !!planJoinList.plant2;
  const [confirmLightBox, setConfirmLightBox] = useState(false);

  const agreeUnitInitial = isPreview
    ? planJoinList.plant2.unit
    : planJoinList.unit.map((e) => e.id);
  const agreeWorkInitial = isPreview
    ? planJoinList.plant2.work
    : planJoinList.work.map((e) => e.id);

  const handleCliclNext = (values) => {
    console.log(values);
    const { check1, check2, ...rest } = values;
    if (!check1 && !check2) return;
    if (!confirmLightBox) return setConfirmLightBox(true);

    setShowLoading(true);
    planAgree({
      auth_key: authKey,
      memberm1_no: memberId,
      slug,
      ...rest,
    }).then((res) => {
      if (res.resu === 1) {
        CommonMsg({
          type: "success",
          msg: t("consent.success.msg"),
        });

        router.push("/user/activity");
      } else {
      }
      setShowLoading(false);
    });
  };

  return (
    <>
      <HeaderBannerNew1
        title={t("meun.memberCenter")}
        showBack={true}
        backLink="/user/myAccount"
        backText={t("myAccount.title")}
      />
      <div className={classNames("contentContainer", styles.contentContainer)}>
        <div className={styles.title}>
          {planJoinList.plan.title}

          <a
            href={`/programs/${slug}`}
            target="_blank"
            rel="noreferrer"
            className={styles.link}
          >
            <SVGInformationLink />
          </a>
        </div>

        <div className={styles.subTitle}>{planJoinList.plan.subtitle}</div>
        <div className={styles.time}>
          {t("consent.time")}
          {`${moment(new Date(planJoinList.plan.receipt_date) / 1).format(
            "YYYY/MM/DD"
          )}-${moment(new Date(planJoinList.plan.receipt_dateed) / 1).format(
            "YYYY/MM/DD"
          )}`}
        </div>

        <div className={styles.desc}>
          <div>{t("consent.desc1")}</div>
          <div>
            <span>{t("consent.desc2")}</span>
            {t("consent.desc3")}
            <br />
            <span>{t("consent.desc2")}</span>
            {t("consent.desc4")}
            <br />
            {t("consent.desc5")}
            <a href="mailto://futurecontent@taicca.tw">
              futurecontent@taicca.tw
            </a>
          </div>
        </div>

        <Form
          className={styles.form}
          layout="vertical"
          onFinish={handleCliclNext}
          onFinishFailed={({ values, errorFields }) => {
            console.log(values, errorFields);
          }}
          scrollToFirstError
          form={form}
        >
          {planJoinList.unit.length > 0 || planJoinList.work.length > 0 ? (
            <>
              <div className={styles.listContainer1}>
                <Form.Item name="agreeUnit" initialValue={agreeUnitInitial}>
                  <div className={styles.list1}>
                    {planJoinList.unit.map((e) => (
                      <UnitCustomCard1
                        key={e.id}
                        data={e}
                        formItems={formItems}
                        customChildren1={
                          <Switch
                            className={styles.switch}
                            defaultChecked={
                              isPreview
                                ? planJoinList.plant2.unit.includes(e.id)
                                : true
                            }
                            onClick={(checked, event) => {
                              form.setFieldsValue({
                                agreeUnit: checked
                                  ? [...form.getFieldValue("agreeUnit"), e.id]
                                  : form
                                      .getFieldValue("agreeUnit")
                                      .filter((f) => f !== e.id),
                              });
                              event.preventDefault();
                            }}
                          />
                        }
                      />
                    ))}
                  </div>
                </Form.Item>
              </div>

              <div className={styles.listContainer2}>
                <Form.Item name="agreeWork" initialValue={agreeWorkInitial}>
                  <div className={styles.list2}>
                    {planJoinList.work.map((e) => (
                      <WorkCustomCard1
                        key={e.id}
                        data={e}
                        formItems={formItems}
                        customChildren1={
                          <Switch
                            className={styles.switch}
                            defaultChecked={
                              isPreview
                                ? planJoinList.plant2.work.includes(e.id)
                                : true
                            }
                            onClick={(checked, event) => {
                              form.setFieldsValue({
                                agreeWork: checked
                                  ? [...form.getFieldValue("agreeWork"), e.id]
                                  : form
                                      .getFieldValue("agreeWork")
                                      .filter((f) => f !== e.id),
                              });
                              event.preventDefault();
                            }}
                          />
                        }
                      />
                    ))}
                  </div>
                </Form.Item>
              </div>

              {planJoinList.showAgreeBtn && (
                <>
                  <div className={styles.checkGroup}>
                    <div className={styles.check}>
                      <Form.Item
                        name="check1"
                        valuePropName="checked"
                        rules={[
                          {
                            validator: (rule, value, callback) =>
                              value ? callback() : callback(new Error()),
                            message: t("consent.error.check"),
                          },
                        ]}
                        style={{ marginBottom: 0 }}
                      >
                        <Checkbox>{t("consent.check1")}</Checkbox>
                      </Form.Item>
                    </div>

                    <div className={styles.check}>
                      <Form.Item
                        name="check2"
                        valuePropName="checked"
                        rules={[
                          {
                            validator: (rule, value, callback) =>
                              value ? callback() : callback(new Error()),
                            message: t("consent.error.check"),
                          },
                        ]}
                        style={{ marginBottom: 0 }}
                      >
                        <Checkbox>{t("consent.check2")}</Checkbox>
                      </Form.Item>
                    </div>
                  </div>

                  <div className={styles.submitBtn}>
                    <FancyButton>{t("consent.submit")}</FancyButton>
                  </div>
                </>
              )}
            </>
          ) : (
            <div className={styles.emptyBlock}>
              <div className={styles.titleBlock}>
                <div>
                  <SVGInfo />
                </div>
                <div>{t("consent.empty")}</div>
              </div>

              <div className={styles.btnGroup}>
                <FancyButton
                  htmlType="button"
                  onClick={() => router.push("/user/myAccount?openPopup=1")}
                >
                  {t("myAccount.field2.add1")}
                </FancyButton>
                {identity === "1" && (
                  <FancyButton
                    htmlType="button"
                    onClick={() => router.push("/user/myAccount?openPopup=2")}
                  >
                    {t("myAccount.field2.add2")}
                  </FancyButton>
                )}
              </div>
            </div>
          )}
        </Form>
      </div>

      <CommonLightBox
        className={classNames(styles.confirmLightBox)}
        descClass={classNames(styles.desc)}
        button2Class={classNames(styles.button2)}
        desc={t("consent.popup.msg")}
        button1={t("consent.popup.btn1")}
        showButton2={true}
        button2={t("consent.popup.btn2")}
        button1Class={classNames(styles.button1)}
        showModal={confirmLightBox}
        setShowModal={setConfirmLightBox}
        onClose2={() => {
          form.submit();
        }}
      />

      <Footer />
    </>
  );
}
ConsentContent.headerProps = {};
export async function getServerSideProps(context) {
  const cookies = context.req.cookies;
  const slug = context.query.slug;

  const { isLogin, redirect } = auth(cookies);
  if (!isLogin) return redirect;

  let identity = "";
  let authKey = "";
  let memberId = "";

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    identity = authData.identity;
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  const handleSourceItem = async () => {
    let formItems = {};
    const { resu, data } = await sourceItem({
      locale: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const handlePlanJoin = async () => {
    let planJoinList = [];

    const { resu, data } = await planJoin({
      auth_key: authKey,
      memberm1_no: memberId,
      slug,
    });
    if (resu === 1) {
      planJoinList = data;
    }
    return planJoinList;
  };

  const [planJoinList, formItems] = await Promise.all([
    handlePlanJoin(),
    handleSourceItem(),
  ]);

  return {
    props: {
      identity,
      authKey,
      memberId,
      slug,
      formItems,
      planJoinList,
      host: context.req.headers.host,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
