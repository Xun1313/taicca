/* eslint-disable @next/next/no-img-element */
import React, {
  useCallback,
  useMemo,
  useRef,
  useState,
  useEffect,
  useContext,
} from "react";
import dynamic from "next/dynamic";

import styles from "@/styles/Activity.module.scss";
import { Footer } from "@/components/Footer/Footer";
import {
  Form,
  Input,
  Radio,
  Row,
  Col,
  Checkbox,
  Select,
  Upload,
  InputNumber,
  Tabs,
  Tag,
  Button,
  message,
  Alert,
} from "antd";

const { Option } = Select;
const { TextArea } = Input;
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";
import SVGInformationLink from "@/svgs/informationLink.svg";

import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import { memberTheme, memberLiveEvent } from "@/api";
import { useRouter } from "next/router";
import { auth } from "@/utilits/auth";
import { parseCookies } from "@/utilits/parseCookies";
import { LoadingContext } from "@/context/LoadingContext";
import moment from "moment";
import { EmptyDataBlock } from "@/components/EmptyDataBlock/EmptyDataBlock";

export default function Activity({
  memberThemeList,
  memberLiveEventList,
  host,
}) {
  console.log(memberThemeList, 555);
  const router = useRouter();
  const locale = router.locale;
  const rwd = useRWD();
  const { t } = useTranslation("common");

  return (
    <>
      <HeaderBannerNew1
        title={t("meun.memberCenter")}
        showBack={true}
        backLink="/user/myAccount"
        backText={t("myAccount.title")}
      />
      <div className={styles.contentContainer}>
        <div className={styles.titleGroup}>
          <div className={styles.title}>{t("activity.title")}</div>
        </div>

        <div className={styles.listContainer}>
          <div className={styles.listTitle}>{t("activity.field1")}</div>

          {memberLiveEventList.length > 0 ? (
            <div className={styles.list}>
              {memberLiveEventList.map((e) => (
                <div className={styles.post} key={e.id}>
                  <div className={styles.time}>{e.eventdate}</div>

                  <div className={styles.title}>
                    <div>{locale === "zh" ? e.title_zh : e.title_en}</div>
                    {e.link && rwd.device === "mobile" && (
                      <a
                        href={e.link}
                        target="_blank"
                        rel="noreferrer"
                        className={styles.link}
                      >
                        <SVGInformationLink />
                      </a>
                    )}
                  </div>

                  {e.link && rwd.device !== "mobile" ? (
                    <a
                      href={e.link}
                      target="_blank"
                      rel="noreferrer"
                      className={styles.link}
                    >
                      <SVGInformationLink />
                    </a>
                  ) : (
                    <div className={styles.link}></div>
                  )}
                </div>
              ))}
            </div>
          ) : (
            <EmptyDataBlock />
          )}
        </div>

        <div className={styles.listContainer}>
          <div className={styles.listTitle}>{t("activity.field2")}</div>

          {memberThemeList.length > 0 ? (
            <div className={styles.list}>
              {memberThemeList.map((e) => (
                <div className={styles.post} key={e.id}>
                  <div className={styles.time}>{e.datest}</div>

                  <div className={styles.title}>
                    <div>{e.title}</div>
                    {(e.status === "1" ||
                      e.status === "2" ||
                      e.status === "4") &&
                      rwd.device === "mobile" && (
                        <a
                          href={`/user/consent/${e.slug}`}
                          target="_blank"
                          rel="noreferrer"
                          className={styles.link}
                        >
                          <SVGInformationLink />
                        </a>
                      )}
                    {e.status === "3" && rwd.device === "mobile" && (
                      <a
                        href={`/selections/${e.slug}`}
                        target="_blank"
                        rel="noreferrer"
                        className={styles.link}
                      >
                        <SVGInformationLink />
                      </a>
                    )}
                  </div>
                  <div
                    className={classNames(
                      styles.status,
                      styles[`status${e.status}`]
                    )}
                  >
                    {t(`activity.status${e.status}`)}
                  </div>
                  {(e.status === "1" || e.status === "2" || e.status === "4") &&
                    rwd.device !== "mobile" && (
                      <a
                        href={`/user/consent/${e.slug}`}
                        target="_blank"
                        rel="noreferrer"
                        className={styles.link}
                      >
                        <SVGInformationLink />
                      </a>
                    )}
                  {e.status === "3" && rwd.device !== "mobile" && (
                    <a
                      href={`/selections/${e.slug}`}
                      target="_blank"
                      rel="noreferrer"
                      className={styles.link}
                    >
                      <SVGInformationLink />
                    </a>
                  )}
                </div>
              ))}
            </div>
          ) : (
            <EmptyDataBlock />
          )}
        </div>
      </div>

      <Footer />
    </>
  );
}
Activity.headerProps = {};
export async function getServerSideProps(context) {
  const cookies = context.req.cookies;
  const { isLogin, redirect } = auth(cookies);
  if (!isLogin) return redirect;

  let authKey = "";
  let memberId = "";

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  const handleMemberLiveEvent = async () => {
    let memberLiveEventList = [];

    const { resu, data } = await memberLiveEvent({
      auth_key: authKey,
      memberm1_no: memberId,
    });
    if (resu === 1) {
      memberLiveEventList = data;
    }
    return memberLiveEventList;
  };

  const handleMemberTheme = async () => {
    let memberThemeList = [];

    const { resu, data } = await memberTheme({
      auth_key: authKey,
      memberm1_no: memberId,
    });
    if (resu === 1) {
      memberThemeList = data;
    }
    return memberThemeList;
  };

  const [memberThemeList, memberLiveEventList] = await Promise.all([
    handleMemberTheme(),
    handleMemberLiveEvent(),
  ]);
  return {
    props: {
      memberThemeList,
      memberLiveEventList,
      host: context.req.headers.host,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
