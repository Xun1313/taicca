import Head from "next/head";
import React, { useCallback, useMemo, useState, useContext } from "react";
import styles from "../../styles/AccountSetting.module.scss";
import { Footer } from "@/components/Footer/Footer";
import { CommonLightBox } from "@/components/CommonLightBox/CommonLightBox";
import SVGLBX from "@/svgs/close.svg";
import { TLink } from "@/components/TLink";
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";
import { auth } from "@/utilits/auth";
import { parseItems } from "@/utilits/parseItems";
import { parseCookies } from "@/utilits/parseCookies";

import {
  Form,
  Input,
  Modal,
  Row,
  Col,
  Checkbox,
  Radio,
  Tag,
  Select,
} from "antd";
const { Option } = Select;
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from 'next-i18next';
import { getCountry, memberModify, memberResetpwd, memberReemail } from "@/api";
import router, { useRouter } from "next/router";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { LoadingContext } from "@/context/LoadingContext";

export default function AccountSetting({
  email,
  nation,
  name_zh,
  name_en,
  lastname_en,
  memberId,
  authKey,
  unitDataList,
  accWorksDataList,
  country,
}) {
  const { locale } = useRouter();
  const { t } = useTranslation("common");
  const rwd = useRWD();
  const { setShowLoading } = useContext(LoadingContext);

  const [errorAccountSetting, setErrorAccountSetting] = useState("");
  const [passwordModal, setPasswordModal] = useState(false);
  const [errorPasswordModal, setErrorPasswordModal] = useState("");
  const [passwordSuccessModal, setPasswordSuccessModal] = useState(false);

  const [emailModal, setEmailModal] = useState(false);
  const [errorEmailModal, setErrorEmailModal] = useState("");
  const [emailSuccessModal, setEmailSuccessModal] = useState(false);

  const [passwordModalForm] = Form.useForm();

  const doubleColProps = useMemo(
    () => ({
      span: rwd.device === "mobile" ? 24 : 12,
    }),
    [rwd.device]
  );

  const handlePasswordFinish = useCallback(
    (data) => {
      console.log(data);
      setShowLoading(true);
      memberResetpwd({
        auth_key: authKey,
        memberm1_no: memberId,
        ...data,
      }).then((res) => {
        if (res.resu === 1) {
          setPasswordSuccessModal(true);
        } else {
          setErrorPasswordModal(
            t(`accountSetting.changePassword.error.apiMsg${res.error}`)
          );
        }
        setShowLoading(false);
      });
    },
    [locale]
  );

  const handleReemailFinish = useCallback(
    (data) => {
      console.log(data);
      setShowLoading(true);
      memberReemail({
        auth_key: authKey,
        memberm1_no: memberId,
        oldemail: email,
        ...data,
      }).then((res) => {
        if (res.resu === 1) {
          setEmailSuccessModal(true);
        } else {
          setErrorEmailModal(
            t(`accountSetting.changeEmail.error.apiMsg${res.error}`)
          );
        }
        setShowLoading(false);
      });
    },
    [locale]
  );

  const handleModifyFinish = useCallback(
    (data) => {
      console.log(data);
      setShowLoading(true);
      memberModify({
        auth_key: authKey,
        memberm1_no: memberId,
        ...data,
      }).then((res) => {
        if (res.resu === 1) {
          document.cookie = `taiccaAuth=${encodeURIComponent(
            JSON.stringify(res.data)
          )}; expires=${new Date("2050 01-01").toUTCString()}; path=/`;
        } else {
          // setErrorWorksModal(t(`signin.error.apiMsg${res.error}`));
        }
        setShowLoading(false);
      });
    },
    [locale]
  );

  return (
    <>
      {/* <Head>
        <title>{t("seo.contact.title")}</title>
        <meta name="description" content={t("seo.contact.desc")} />
      </Head> */}
      <HeaderBannerNew1 title={t("meun.memberCenter")} showBack={true} />

      <section className={classNames(styles.contentContainer)}>
        <div className={classNames(styles.head)}>
          <h3 className={classNames("customTitle1")}>
            {t("accountSetting.title")}
          </h3>
        </div>

        <Form layout="vertical" onFinish={handleModifyFinish}>
          <Form.Item required label={t("accountSetting.field1")}>
            <Row wrap={false} align="middle">
              <div>{email}</div>
              <Tag
                style={{ marginLeft: 18, cursor: "pointer" }}
                onClick={() => setEmailModal(true)}
              >
                {t("accountSetting.field1.btn")}
              </Tag>
            </Row>
          </Form.Item>

          <Form.Item required label={t("accountSetting.field2")}>
            <Row wrap={false} align="middle">
              <div>***************</div>
              <Tag
                style={{ marginLeft: 18, cursor: "pointer" }}
                onClick={() => setPasswordModal(true)}
              >
                {t("accountSetting.field2.btn")}
              </Tag>
            </Row>
          </Form.Item>

          <Form.Item
            required
            label={t("accountSetting.field5")}
            name="nation"
            rules={[
              {
                required: true,
                message: t("accountSetting.changePassword.required", {
                  label: t("accountSetting.field5"),
                }),
              },
            ]}
            initialValue={nation}
          >
            <Select
              placeholder="Taiwan 台灣"
              showSearch
              optionFilterProp="title"
            >
              {country.map((c) => (
                <Option
                  key={c.name}
                  value={c.name}
                  title={`${c.name} ${c.traditionalName}`}
                >
                  {c.name} {c.traditionalName}
                </Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            required
            label={t("accountSetting.field3")}
            name="name_zh"
            rules={[
              {
                required: true,
                message: t("accountSetting.changePassword.required", {
                  label: t("accountSetting.field3"),
                }),
              },
            ]}
            initialValue={name_zh}
          >
            <Input placeholder={t("accountSetting.field3.placeholder")} />
          </Form.Item>

          <Row gutter={20}>
            <Col span={rwd.device === "mobile" ? 24 : 12}>
              <Form.Item
                required
                label={t("accountSetting.field4")}
                name="name_en"
                rules={[
                  {
                    required: true,
                    message: t("accountSetting.changePassword.required", {
                      label: t("accountSetting.field4.placeholder1"),
                    }),
                  },
                ]}
                initialValue={name_en}
              >
                <Input placeholder={t("accountSetting.field4.placeholder1")} />
              </Form.Item>
            </Col>

            <Col span={rwd.device === "mobile" ? 24 : 12}>
              <Form.Item
                required
                name="lastname_en"
                label={" "}
                rules={[
                  {
                    required: true,
                    message: t("accountSetting.changePassword.required", {
                      label: t("accountSetting.field4.placeholder2"),
                    }),
                  },
                ]}
                initialValue={lastname_en}
              >
                <Input placeholder={t("accountSetting.field4.placeholder2")} />
              </Form.Item>
            </Col>
          </Row>

          <div className={classNames(styles.customSubmit)}>
            <div className={classNames(styles.submit)}>
              {errorAccountSetting && (
                <div style={{ color: "red" }}>{errorAccountSetting}</div>
              )}
              <FancyButton>{t("accountSetting.field4.submit")}</FancyButton>
            </div>
          </div>
        </Form>

        <Modal
          visible={passwordModal}
          footer={null}
          onCancel={() => {
            setPasswordModal(false);
          }}
          width={800}
          closeIcon={<SVGLBX className={styles.lightboxClose} />}
        >
          <div className={styles.noticeContainer}>
            <div className={styles.noticeTitle}>
              {t("accountSetting.changePassword.title")}
            </div>
            <Form layout="vertical" onFinish={handlePasswordFinish}>
              <Form.Item
                required
                label={t("accountSetting.changePassword.field1")}
                name="password"
                rules={[
                  {
                    required: true,
                    message: t("signin.error.msg6"),
                    pattern: new RegExp(
                      /^(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\`~!@#$%^&*_<>.,()\-+=|\\?\/{}\[\]]).{8,}$/
                    ),
                  },
                ]}
              >
                <Input.Password placeholder="***************" />
              </Form.Item>

              <Form.Item
                required
                label={t("accountSetting.changePassword.field2")}
                name="confirmpassword"
                rules={[
                  {
                    required: true,
                    message: t("signin.error.msg6"),
                    pattern: new RegExp(
                      /^(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\`~!@#$%^&*_<>.,()\-+=|\\?\/{}\[\]]).{8,}$/
                    ),
                  },
                ]}
              >
                <Input.Password placeholder="***************" />
              </Form.Item>
              <div>
                {errorPasswordModal && (
                  <div style={{ color: "red" }}>{errorPasswordModal}</div>
                )}
                <div className={classNames(styles.submit)}>
                  <FancyButton style={{ width: "100%" }}>
                    {t("accountSetting.changePassword.submit")}
                  </FancyButton>
                </div>
              </div>
            </Form>
          </div>
        </Modal>

        <Modal
          visible={emailModal}
          footer={null}
          onCancel={() => {
            setEmailModal(false);
          }}
          width={800}
          closeIcon={<SVGLBX className={styles.lightboxClose} />}
        >
          <div className={styles.noticeContainer}>
            <div className={styles.noticeTitle}>
              {t("accountSetting.changeEmail.title")}
            </div>
            <Form layout="vertical" onFinish={handleReemailFinish}>
              <Form.Item
                required
                label={t("accountSetting.changeEmail.field1")}
                name="email"
                rules={[
                  {
                    required: true,
                    message: t("accountSetting.changeEmail.required", {
                      label: t("accountSetting.changeEmail.field1"),
                    }),
                  },
                  { type: "email", message: t("signin.error.msg7") },
                ]}
              >
                <Input placeholder="XXX@xxx.com" />
              </Form.Item>

              <Form.Item
                required
                label={t("accountSetting.changeEmail.field2")}
                name="confirmemail"
                rules={[
                  {
                    required: true,
                    message: t("accountSetting.changeEmail.required", {
                      label: t("accountSetting.changeEmail.field2"),
                    }),
                  },
                  { type: "email", message: t("signin.error.msg7") },
                ]}
              >
                <Input placeholder="XXX@xxx.com" />
              </Form.Item>

              <div>
                {errorEmailModal && (
                  <div style={{ color: "red" }}>{errorEmailModal}</div>
                )}
                <div className={classNames(styles.submit)}>
                  <FancyButton style={{ width: "100%" }}>
                    {t("accountSetting.changeEmail.submit")}
                  </FancyButton>
                </div>
              </div>
            </Form>
          </div>
        </Modal>

        <CommonLightBox
          title={t("accountSetting.changePassword.success.title")}
          desc={t("accountSetting.changePassword.success.desc")}
          button1={t("accountSetting.changePassword.success.submit")}
          showModal={passwordSuccessModal}
          setShowModal={setPasswordSuccessModal}
          onClose1={() => {
            document.cookie = `taiccaAuth=; expires=${new Date(
              "1950 01-01"
            ).toUTCString()}; path=/`;

            router.replace(`/signin`);
          }}
        />

        <CommonLightBox
          title={t("accountSetting.changeEmail.success.title")}
          desc={t("accountSetting.changeEmail.success.desc")}
          button1={t("accountSetting.changeEmail.success.submit")}
          showModal={emailSuccessModal}
          setShowModal={setEmailSuccessModal}
          onClose1={() => {
            document.cookie = `taiccaAuth=; expires=${new Date(
              "1950 01-01"
            ).toUTCString()}; path=/`;

            router.replace(`/signin`);
          }}
        />
      </section>
      <Footer />
    </>
  );
}
AccountSetting.headerProps = {};
export async function getServerSideProps(context) {
  const { locale } = context;
  const cookies = context.req.cookies;
  const { isLogin } = auth(cookies);
  if (!isLogin) {
    return {
      redirect: {
        destination: "/signin",
        permanent: false,
      },
    };
  }

  let email = "";
  let nation = "";
  let name_zh = "";
  let name_en = "";
  let lastname_en = "";
  let authKey = "";
  let memberId = "";

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    email = authData.email;
    nation = authData.nation;
    name_zh = authData.name_zh;
    name_en = authData.name_en;
    lastname_en = authData.lastname_en;
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  let country = {};
  const { resu, data } = await getCountry({
    locale,
  });
  if (resu === 1) {
    country = data.country;
  }

  return {
    props: {
      email,
      nation,
      name_zh,
      name_en,
      lastname_en,
      memberId,
      authKey,
      country,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
