/* eslint-disable @next/next/no-img-element */
import React, {
  useCallback,
  useMemo,
  useRef,
  useState,
  useEffect,
  useContext,
} from "react";

import styles from "@/styles/Favorite.module.scss";
import { Footer } from "@/components/Footer/Footer";
import {
  Form,
  Input,
  Radio,
  Row,
  Col,
  Checkbox,
  Select,
  Upload,
  InputNumber,
  Tabs,
  Tag,
  Button,
  message,
  Alert,
  Tooltip,
} from "antd";

import classNames from "classnames";

import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import { collectmIndex, sourceItem } from "@/api";
import { useRouter } from "next/router";
import { auth } from "@/utilits/auth";
import { parseCookies } from "@/utilits/parseCookies";
import { UnitFavCard } from "@/components/UnitCard/UnitCard";
import { WorkFavCard } from "@/components/WorkCard/WorkCard";
import { EmptyDataBlock } from "@/components/EmptyDataBlock/EmptyDataBlock";

export default function Favorite({
  collecList1,
  collecList2,
  collectmDefaultMapping,
  formItems,
  authKey,
  memberId,
}) {
  const router = useRouter();
  const locale = router.locale;
  const { t } = useTranslation("common");
  const [collectmMapping, setCollectmMapping] = useState(
    collectmDefaultMapping
  );

  return (
    <>
      <HeaderBannerNew1
        title={t("meun.memberCenter")}
        showBack={true}
        backLink="/user/myAccount"
        backText={t("myAccount.title")}
      />

      <div className={classNames(styles.contentContainer, "favoritePage")}>
        <div className={styles.title}>{t("favorite.title")}</div>

        <Tabs className={"customTab"}>
          <Tabs.TabPane tab={t("favorite.tab1")} key="1">
            {collecList1.length > 0 ? (
              <div className={styles.tabList1}>
                {collecList1.map((u) => (
                  <UnitFavCard
                    key={u.id}
                    data={u}
                    collectmMapping={collectmMapping}
                    setCollectmMapping={setCollectmMapping}
                    formItems={formItems}
                    isLogin={true}
                    authKey={authKey}
                    memberId={memberId}
                  />
                ))}
              </div>
            ) : (
              <EmptyDataBlock />
            )}
          </Tabs.TabPane>

          <Tabs.TabPane tab={t("favorite.tab2")} key="2">
            {collecList2.length > 0 ? (
              <div className={styles.tabList2}>
                {collecList2.map((u) => (
                  <WorkFavCard
                    key={u.id}
                    data={u}
                    collectmMapping={collectmMapping}
                    setCollectmMapping={setCollectmMapping}
                    formItems={formItems}
                    isLogin={true}
                    authKey={authKey}
                    memberId={memberId}
                  />
                ))}
              </div>
            ) : (
              <EmptyDataBlock />
            )}
          </Tabs.TabPane>
        </Tabs>
      </div>

      <Footer />
    </>
  );
}
Favorite.headerProps = {};
export async function getServerSideProps(context) {
  const locale = context.locale;
  const cookies = context.req.cookies;
  const { isLogin, redirect } = auth(cookies);
  if (!isLogin) return redirect;

  let authKey = "";
  let memberId = "";

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  const collectmDefaultMapping = {
    unit: [],
    work: [],
  };

  const handleCollectmIndex1 = async () => {
    let collectmIndexList = [];
    const { resu, data } = await collectmIndex({
      auth_key: authKey,
      memberm1_no: memberId,
      ispages: 0,
      type: 1,
      lang: locale,
    });
    if (resu === 1) {
      collectmIndexList = data;
      data.forEach((e) => collectmDefaultMapping.unit.push(e.id + ""));
    }
    return collectmIndexList;
  };

  const handleCollectmIndex2 = async () => {
    let collectmIndexList = [];
    const { resu, data } = await collectmIndex({
      auth_key: authKey,
      memberm1_no: memberId,
      ispages: 0,
      type: 2,
      lang: locale,
    });
    if (resu === 1) {
      collectmIndexList = data;
      data.forEach((e) => collectmDefaultMapping.work.push(e.id + ""));
    }
    return collectmIndexList;
  };

  const handleSourceItem = async () => {
    let formItems = {};
    const { resu, data } = await sourceItem({
      locale: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const [collecList1, collecList2, formItems] = await Promise.all([
    handleCollectmIndex1(),
    handleCollectmIndex2(),
    handleSourceItem(),
  ]);

  return {
    props: {
      collecList1,
      collecList2,
      collectmDefaultMapping,
      formItems,
      authKey,
      memberId,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
