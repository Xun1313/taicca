/* eslint-disable @next/next/no-img-element */
import React, {
  useCallback,
  useMemo,
  useRef,
  useState,
  useEffect,
  useContext,
} from "react";
import dynamic from "next/dynamic";

import styles from "@/styles/InformationProvided.module.scss";
import { Footer } from "@/components/Footer/Footer";
import {
  Form,
  Input,
  Radio,
  Row,
  Col,
  Checkbox,
  Select,
  Upload,
  InputNumber,
  Tabs,
  Tag,
  Button,
  message,
  Alert,
} from "antd";

const { Option } = Select;
const { TextArea } = Input;
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";
import SVGInformationPost from "@/svgs/informationPost.svg";
import SVGInformationLink from "@/svgs/informationLink.svg";

import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import { memNewsIndex, memNewsAdd } from "@/api";
import { useRouter } from "next/router";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { auth } from "@/utilits/auth";
import { parseCookies } from "@/utilits/parseCookies";
import { LoadingContext } from "@/context/LoadingContext";
import { CommonMsg } from "@/components/CommonMsg/CommonMsg";
import moment from "moment";
import { EmptyDataBlock } from "@/components/EmptyDataBlock/EmptyDataBlock";

const CustomEditor = dynamic(
  async () => {
    const { CustomEditor } = await import(
      "@/components/CustomEditor/CustomEditor"
    );
    return CustomEditor;
  },
  { ssr: false }
);

export default function InformationProvided({
  authKey,
  memberId,
  memNewsIndexList,
  host,
}) {
  const router = useRouter();
  const locale = router.locale;
  const rwd = useRWD();
  const { t } = useTranslation("common");
  const { setShowLoading } = useContext(LoadingContext);
  const [form] = Form.useForm();
  const [isPosting, setIsPosting] = useState(false);

  const categoryMapping = {
    1: t("information.field1.option1"),
    2: t("information.field1.option2"),
  };

  const langOptions = [
    {
      label: "中文",
      value: "zh",
    },
    {
      label: "English",
      value: "en",
    },
  ];

  const getBase64 = (img) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      form.setFieldsValue({ pic: reader.result });
    });
    reader.readAsDataURL(img);
  };

  const handleChange = (info) => {
    if (info.file.status === "uploading") {
    } else if (info.file.status === "done") {
      getBase64(info.file.originFileObj);
    } else if (info.file.status === "error") {
      form.setFieldsValue({ pic: null });
    } else if (info.file.status === "removed") {
      form.setFieldsValue({ pic: null });
    }
  };

  const handleCliclNext = useCallback((values) => {
    const { agree, ...rest } = values;
    setShowLoading(true);
    memNewsAdd({
      auth_key: authKey,
      memberm1_no: memberId,
      ...rest,
    }).then((res) => {
      if (res.resu === 1) {
        CommonMsg({
          type: "success",
          msg: t("information.success.msg"),
        });
        router.replace(router.asPath);
        setIsPosting(false);
      } else {
      }
      setShowLoading(false);
    });
  }, []);

  return (
    <>
      <HeaderBannerNew1
        title={t("meun.memberCenter")}
        showBack={true}
        backLink="/user/myAccount"
        backText={t("myAccount.title")}
      />
      <div className={styles.contentContainer}>
        <div className={styles.titleGroup}>
          <div className={styles.title}>{t("information.title")}</div>
          <div className={styles.desc}>
            <div>{t("information.desc1")}</div>
            <span style={{ color: "#E53C2B" }}>{t("information.desc2")}</span>
            {t("information.desc3")}
          </div>
        </div>

        {isPosting ? (
          <Form
            className={styles.form}
            layout="vertical"
            onFinish={handleCliclNext}
            onFinishFailed={({ values, errorFields }) => {
              console.log(values, errorFields);
            }}
            scrollToFirstError
            form={form}
          >
            <Row gutter={[80]}>
              <Col span={10}>
                <Form.Item
                  required
                  name="category"
                  label={t("information.field1")}
                  rules={[
                    {
                      required: true,
                      message: t("information.required", {
                        label: t("information.field1"),
                      }),
                    },
                  ]}
                >
                  <Select placeholder={t("information.field1.placeholder")}>
                    {Object.keys(categoryMapping).map((e) => (
                      <Option key={e} value={e}>
                        {categoryMapping[e]}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col span={10}>
                <Form.Item
                  name="lang"
                  label={t("information.field2")}
                  initialValue={locale}
                  valuePropName="value"
                >
                  <Radio.Group options={langOptions} />
                </Form.Item>
              </Col>
            </Row>

            <Form.Item
              required
              name="title"
              label={t("information.field3")}
              rules={[
                {
                  required: true,
                  message: t("information.required", {
                    label: t("information.field3"),
                  }),
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              required
              name="content"
              label={t("information.field4")}
              rules={[
                {
                  required: true,
                  message: t("information.required", {
                    label: t("information.field4"),
                  }),
                },
              ]}
            >
              <CustomEditor
                setData={(ckEditorData) =>
                  form.setFieldsValue({
                    content: ckEditorData,
                  })
                }
              />
            </Form.Item>

            <Form.Item
              required
              name="pic"
              label={t("information.field5")}
              rules={[
                {
                  required: true,
                  message: t("information.required", {
                    label: t("information.field5"),
                  }),
                },
              ]}
            >
              <Upload
                accept="image/png,image/jpeg,image/webp"
                maxCount={1}
                customRequest={({ onSuccess, onError, file }) => {
                  const isImg =
                    file.type === "image/jpeg" ||
                    file.type === "image/png" ||
                    file.type === "image/webp";
                  if (!isImg) {
                    CommonMsg({
                      type: "error",
                      msg: t("information.field5.error1"),
                    });
                    form.setFieldsValue({ pic: null });
                    return onError();
                  }

                  const img = new Image();
                  img.src = URL.createObjectURL(file);
                  img.onload = () => {
                    if (img.width < 1920) {
                      CommonMsg({
                        type: "error",
                        msg: t("information.field5.error2"),
                      });
                      URL.revokeObjectURL(img.src);
                      onError();
                    } else {
                      URL.revokeObjectURL(img.src);
                      onSuccess();
                    }
                    form.setFieldsValue({ pic: null });
                  };
                }}
                onChange={handleChange}
              >
                <Button>{t("information.field5.btn")}</Button>
              </Upload>
            </Form.Item>

            <Form.Item name="source" label={t("information.field6")}>
              <Input />
            </Form.Item>

            <Form.Item name="sourceurl" label={t("information.field7")}>
              <Input placeholder="https://" />
            </Form.Item>

            <Form.Item
              className={styles.privacy}
              name="agree"
              required
              valuePropName="checked"
              rules={[
                {
                  required: true,
                  message: t("information.check.error"),
                },
              ]}
            >
              <Checkbox>
                <div>{t("information.check1")}</div>
                <div>{t("information.check2")}</div>
              </Checkbox>
            </Form.Item>

            <div className={styles.btnGroup}>
              <FancyButton onClick={() => setIsPosting(false)}>
                {t("information.btn2")}
              </FancyButton>
              <FancyButton>{t("information.btn1")}</FancyButton>
            </div>
          </Form>
        ) : (
          <div className={styles.postedPage}>
            <div className={styles.postBtn}>
              <FancyButton selected onClick={() => setIsPosting(true)}>
                <SVGInformationPost />
                {t("information.postBtn")}
              </FancyButton>
            </div>

            {memNewsIndexList.length > 0 ? (
              <div className={styles.postList}>
                {memNewsIndexList.map((e) => (
                  <div className={styles.post} key={e.id}>
                    <div className={styles.time}>
                      {moment(e.datest).format("DD/M/YYYY")}
                    </div>
                    <div className={styles.category}>
                      {categoryMapping[e.category]}
                    </div>
                    <div className={styles.title}>
                      <div>{e.title}</div>
                      {e.status === "2" && rwd.device === "mobile" && (
                        <a
                          href={`/news/${e.slug}`}
                          target="_blank"
                          rel="noreferrer"
                          className={styles.link}
                        >
                          <SVGInformationLink />
                        </a>
                      )}
                    </div>
                    <div
                      className={classNames(
                        styles.status,
                        styles[`status${e.status}`]
                      )}
                    >
                      {t(`information.status${e.status}`)}
                    </div>
                    {e.status === "2" && rwd.device !== "mobile" ? (
                      <a
                        href={`/news/${e.slug}`}
                        target="_blank"
                        rel="noreferrer"
                        className={styles.link}
                      >
                        <SVGInformationLink />
                      </a>
                    ) : (
                      <div className={styles.link}></div>
                    )}
                  </div>
                ))}
              </div>
            ) : (
              <EmptyDataBlock />
            )}
          </div>
        )}
      </div>

      <Footer />
    </>
  );
}
InformationProvided.headerProps = {};
export async function getServerSideProps(context) {
  const cookies = context.req.cookies;
  const { isLogin, redirect } = auth(cookies);
  if (!isLogin) return redirect;

  let authKey = "";
  let memberId = "";

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  const handleMemNewsIndex = async () => {
    let memNewsIndexList = [];

    const { resu, data } = await memNewsIndex({
      auth_key: authKey,
      memberm1_no: memberId,
    });
    if (resu === 1) {
      memNewsIndexList = data;
    }
    return memNewsIndexList;
  };

  const [memNewsIndexList] = await Promise.all([handleMemNewsIndex()]);
  return {
    props: {
      authKey,
      memberId,
      memNewsIndexList,
      host: context.req.headers.host,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
