import Head from "next/head";
import { PageInfoBasic } from "@/components/PageInfo/PageInfo";
import React, { useEffect, useState } from "react";

import styles from "@/styles/message.module.scss";
import { Footer } from "@/components/Footer/Footer";
import { useRWD } from "@/hooks/useRwd";
import { HeaderBannerLight } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { Trans, useTranslation } from 'next-i18next';
import SBGSuccess from "@/svgs/joinSuccess.svg";
import { useRouter } from "next/router";
import { useAsyncFn } from "react-use";
import { memberVerify } from "@/api";
import { Loading } from "@/components/Loading/Loading";
import { FancyButton } from "@/components/FancyButton/FancyButton";

const messages = {
  confirm: {
    title: "emailconfirmed.headline",
    message1: "emailconfirmed.success.title",
    message2: "emailconfirmed.success.desc",
  },
};

export default function EmailConfirmed() {
  const rwd = useRWD();
  const { t } = useTranslation("common");
  const router = useRouter();
  const { mail, token } = router.query;
  const [isFinish, setIsFinish] = useState(false);
  const [error, setError] = useState(null);

  const message = messages["confirm"];

  const [apiState, fetch] = useAsyncFn(async (mail, token) => {
    const res = await memberVerify({ mail, token });
    if (res.resu === 1) {
      setIsFinish(true);
    } else if (res.resu === 0) {
      setError(res.errorcode);
    }
  }, []);

  useEffect(() => {
    if (token && mail) {
      fetch(mail, token);
    }
  }, [fetch, mail, token]);
  return (
    <>
      <Head>
        <title>{t("seo.emailconfirmed.title")}</title>
        <meta name="description" content={t("seo.emailconfirmed.desc")} />
      </Head>
      <h1 className="seoH1">{t("h1.emailconfirmed")}</h1>
      <HeaderBannerLight title={isFinish ? t(message.title) : ""} />
      <PageInfoBasic />
      <div className={"contentContainer"}>
        {!isFinish && !error ? (
          <div
            style={{
              height: "50vh",
              minHeight: 350,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Loading />
          </div>
        ) : error ? (
          <section className={styles.section1}>
            <div>
              <SBGSuccess className={styles.successIcon} />
            </div>
            <div>
              <div className={styles.desc}>
                <Trans i18nKey={error} />
              </div>
            </div>
          </section>
        ) : (
          <section className={styles.section1}>
            <div>
              <h3>
                {t("accountSetting.verifyEmail.success.title")}
              </h3>
              <div className={styles.desc} dangerouslySetInnerHTML={{
                __html: t("accountSetting.verifyEmail.success.desc")
              }}>
              </div>
              <div className={styles.button}>
                <FancyButton onClick={() => router.push("/signin")}>{t("signin.login")}</FancyButton>
              </div>
            </div>
            <div>
              <SBGSuccess className={styles.successIcon} />
            </div>
          </section>
        )}
      </div>
      <Footer />
    </>
  );
}
EmailConfirmed.headerProps = {
  // red: true,
};
export async function getStaticProps(context) {
  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
