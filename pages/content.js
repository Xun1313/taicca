import Head from "next/head";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import React, { useCallback, useEffect, useRef, useState, useMemo } from "react";
import getConfig from "next/config";
import styles from "@/styles/Content.module.scss";
import FilterIcon from "@/svgs/filter.svg";
import { Collapse } from "react-collapse";
import FilterOpenIcon from "@/svgs/filterOpen.svg";
import FilterCloseIcon from "@/svgs/filterClose.svg";
import classNames from "classnames";
import { FilterTree } from "@/components/FilterTree/FilterTree";
import { sourceItem, getMemberWork, sourceItemFeature } from "@/api";

import Form, { Field } from "rc-field-form";
import { useAsyncFn, usePrevious } from "react-use";
import { useRouter } from "next/router";
import { useLoadMoreApiCommon } from "@/utilits/useLoadMoreApi";
import ReactPaginate from "react-paginate";
import { Loading } from "@/components/Loading/Loading";
import { LoadMoreGroup } from "@/components/LoadMoreGroup/LoadMoreGroup";
import { useRWD } from "@/hooks/useRwd";
import { Footer } from "@/components/Footer/Footer";
import { auth } from "@/utilits/auth";
import { parseCookies } from "@/utilits/parseCookies";
import { WorkFavCard } from "@/components/WorkCard/WorkCard";
import { ComingSoon } from "@/components/ComingSoon/ComingSoon";

const pageSize = 12;

function Content({
  resourcesRes,
  formItems,
  sourceItemFeatureData,
  isLogin,
  authKey,
  memberId,
  filterDefaultQuery,
}) {
  const { t } = useTranslation("common");
  const [form] = Form.useForm();
  const { locale } = useRouter();
  const [filterOpenControl, setFilterOpenControl] = useState(null);
  const { publicRuntimeConfig } = useMemo(getConfig, []);
  
  const FilterOption = ({
    spec,
    value = [],
    onChange,
    isCollapsed,
    open,
    close,
  }) => {
    const headerTitle = spec.title;
    // const [isCollapsed, setIsCollapsed] = useState(false);

    return (
      <div className={styles.bodyBox}>
        <div
          onClick={isCollapsed ? close : open}
          className={styles.optionHeader}
        >
          <div>{headerTitle}</div>
          {isCollapsed ? <FilterOpenIcon /> : <FilterCloseIcon />}
        </div>
        <Collapse isOpened={isCollapsed}>
          <FilterTree
            spec={spec.options}
            value={value}
            onChange={onChange}
            idPrefix={spec.key}
          />
        </Collapse>
      </div>
    );
  };

  const Filter = ({ filterSprcs, onReset }) => {
    const { t } = useTranslation("common");
    return (
      <div className={styles.filter}>
        <div className={styles.header}>
          <div>
            <FilterIcon />
            {t("content.filter")}
          </div>
          <div
            onClick={() => {
              onReset();
            }}
          >
            {t("content.clearFilter")}
          </div>
        </div>
        <div className={styles.body}>
          {filterSprcs.map((spec) => (
            <Field name={spec.key} key={spec.key}>
              <FilterOption
                spec={spec}
                isCollapsed={filterOpenControl === spec.key}
                open={() => setFilterOpenControl(spec.key)}
                close={() => setFilterOpenControl(null)}
              />
            </Field>
          ))}
        </div>
      </div>
    );
  };

  const usePageApi = ({
    initPage,
    initAllPage,
    initData,
    initCount,
    initCollectm,
    pageSize,
    memberId,
    authKey,
    otherQuery = {},
  }) => {
    const otherQueryRef = useRef(otherQuery);
    otherQueryRef.current = otherQuery;
    const [state, setState] = useState({
      collectm: initCollectm,
      data: initData,
      page: initPage,
      allPage: initAllPage,
      count: initCount,
    });
    const { locale } = useRouter();

    const [apiState, fetch] = useAsyncFn(async (page) => {
      const res = await getMemberWork({
        page: page,
        pagesize: pageSize,
        ispublic: 1,
        locale,
        memberm1_no: memberId,
        auth_key: authKey,
        ...otherQueryRef.current,
      });

      setState({
        collectm: res.collectm,
        data: res.data,
        page: page,
        allPage: res.pages.totalPages,
        count: res.pages.totalCount,
      });
    }, []);

    const handlePageClick = useCallback(
      (val) => {
        setState((prev) => ({
          ...prev,
          page: val.selected + 1,
        }));
        fetch(val.selected + 1);
      },
      [fetch]
    );

    const handleQueryChange = useCallback(() => {
      setState({
        data: [],
        page: 1,
        allPage: 1,
        count: 0,
      });

      fetch(1);
    }, [fetch]);

    const handleFavoriteChange = useCallback((func) => {
      return setState((prev) => {
        return {
          ...prev,
          collectm: func(prev.collectm),
        };
      });
    }, []);

    const prevQuery = usePrevious(otherQuery);

    useEffect(() => {
      if (typeof prevQuery !== "undefined" && prevQuery !== otherQuery) {
        handleQueryChange();
      }
    }, [otherQuery]);

    return {
      handlePageClick,
      currentPage: state.page,
      allPage: state.allPage,
      data: state.data,
      isLoading: apiState.loading,
      count: state.count,
      handleQueryChange,
      handleFavoriteChange,
      collectm: state.collectm,
    };
  };

  const artTypeList = Object.entries(formItems.artType)
    .map(([key, title]) => {
      return {
        key,
        title,
        amount: sourceItemFeatureData.gclass[key],
      };
    })
    .filter((e) => e.amount > 0);

  const artformList = Object.entries(formItems.artform)
    .map(([key, title]) => {
      return {
        key,
        title,
        amount: sourceItemFeatureData.workform[key],
      };
    })
    .filter((e) => e.amount > 0);

  const workTypeList = Object.entries(formItems.workType)
    .map(([key, title]) => {
      return {
        key,
        title,
        amount: sourceItemFeatureData.work_status[key],
      };
    })
    .filter((e) => e.amount > 0);

  const artTagsList = Object.entries(formItems.artTags)
    .map(([key, title]) => {
      return {
        key,
        title,
        amount: sourceItemFeatureData.gtheme[key],
      };
    })
    .filter((e) => e.amount > 0);

  const langList = Object.entries(formItems.lang)
    .map(([key, title]) => {
      return {
        key,
        title,
        amount: sourceItemFeatureData.language[key],
      };
    })
    .filter((e) => e.amount > 0);

  const langList2 = langList.slice(0, 8);

  if (langList.length > 8) {
    langList2.push({
      key: "other",
      title: locale === "zh" ? "其他" : "Other",
      amount: langList.length - 8 + "",
    });
  }
  const rwd = useRWD();

  const filterSprcs = [
    {
      title: t("content.filter.title1"),
      key: "gclass",
      options: artTypeList,
    },
    {
      title: t("content.filter.title2"),
      key: "workform",
      options: artformList,
    },
    {
      title: t("content.filter.title3"),
      key: "work_status",
      options: workTypeList,
    },
    {
      title: t("content.filter.title4"),
      key: "gtheme",
      options: artTagsList,
    },
    {
      title: t("content.filter.title5"),
      key: "language",
      options: langList2,
    },
  ];

  const [filterQuery, setFilterQuery] = useState({
    memberm1_no: memberId,
    auth_key: authKey,
    ispublic: 1,
    ...filterDefaultQuery,
  });

  const pageControl = usePageApi({
    initAllPage: resourcesRes.pages.totalPages,
    initPage: 1,
    initData: resourcesRes.data,
    pageSize: pageSize,
    otherQuery: filterQuery,
    initCount: resourcesRes.pages.totalCount,
    initCollectm: resourcesRes.collectm,
    memberId,
    authKey,
  });
  const loadMoreState = useLoadMoreApiCommon({
    initAllPage: resourcesRes.pages.totalPages,
    initPage: 1,
    initData: resourcesRes.data,
    pageSize: pageSize,
    api: getMemberWork,
    otherQuery: filterQuery,
    initCount: resourcesRes.pages.totalCount,
    initCollectm: resourcesRes.collectm,
  });

  const control = rwd.device === "mobile" ? loadMoreState : pageControl;

  return (
    <>
      <Head>
        <title>{t("seo.content.title")}</title>
        <meta name="description" content={t("seo.content.desc")} />
        <link
          rel="canonical" 
          href={`${publicRuntimeConfig.shareBaseUrl}${locale === "zh" ? "" : "/en"}/content`}
        />
      </Head>
      <h1 className="seoH1">{t("h1.content")}</h1>
      <HeaderBannerNew1
        title={t("content.headline")}
        navList={[
          {
            text: t("content.headline"),
          },
        ]}
        desc={t("content.desc1")}
      />

      {control.data.length > 0 ? (
        <div className={classNames("contentContainer", styles.content)}>
          <Form
            form={form}
            onValuesChange={(changedValues, values) => {
              setFilterQuery((prev) => {
                return {
                  ...prev,
                  ...values,
                };
              });
            }}
            initialValues={filterDefaultQuery}
          >
            <input type="hidden" name="csrf_token" value="no_token" />
            <Filter
              filterSprcs={filterSprcs}
              onReset={() => {
                form.resetFields();
                const filterDefaultQuery2 = filterDefaultQuery;
                Object.keys(filterDefaultQuery2).forEach(
                  (e) => (filterDefaultQuery2[e] = [])
                );
                form.setFieldsValue(filterDefaultQuery2);

                setFilterQuery({
                  memberm1_no: memberId,
                  auth_key: authKey,
                  ispublic: 1,
                });
              }}
            />
          </Form>
          <div className={styles.contentInner}>
            {control.isLoading && control.data.length === 0 && (
              <div className={styles.loading}>
                <Loading />
              </div>
            )}
            {!(control.isLoading && control.data.length === 0) && (
              <div className={styles.count}>
                {t("totalCount", {
                  num: control.count,
                })}
              </div>
            )}

            <div className={styles.grid}>
              {control.data.map((u) => {
                return (
                  <WorkFavCard
                    key={u.id}
                    data={u}
                    collectmMapping={control.collectm}
                    setCollectmMapping={control.handleFavoriteChange}
                    formItems={formItems}
                    isLogin={isLogin}
                    authKey={authKey}
                    memberId={memberId}
                  />
                );
              })}
            </div>

            <div className={classNames(styles.paginate, "desktop-only")}>
              <ReactPaginate
                previousLabel={"<"}
                nextLabel={">"}
                breakLabel={"..."}
                breakClassName={"break-me"}
                // initialPage={state.page - 1}
                initialPage={0}
                pageCount={pageControl.allPage}
                // pageCount={state.allPage}
                marginPagesDisplayed={2}
                pageRangeDisplayed={4}
                onPageChange={pageControl.handlePageClick}
                containerClassName={classNames("pagination")}
                activeClassName={"active"}
              />
            </div>
            {loadMoreState.isLoading && (
              <div style={{ textAlign: "center", marginBottom: 30 }}>
                <Loading />
              </div>
            )}
            {loadMoreState.showLoadMore && (
              <LoadMoreGroup
                actionText={t("event.showmore.button")}
                leftText={t("event.showmore")}
                className={classNames("mobile-only", styles.loadMoreGroup)}
                onLoadmore={loadMoreState.handleNext}
              />
            )}
          </div>
        </div>
      ) : (
        <ComingSoon />
      )}
      <Footer />
    </>
  );
}

export default Content;
export async function getServerSideProps(context) {
  const cookies = context.req.cookies;
  const { isLogin } = auth(cookies);

  let authKey = "";
  let memberId = "";

  if (isLogin) {
    try {
      const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
      authKey = authData.auth_key;
      memberId = authData.id;
    } catch (error) {}
  }

  const handleSourceItem = async () => {
    let formItems = {};
    const { resu, data } = await sourceItem({
      locale: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const filterDefaultQuery = {};
  let filterDefaultQuery2 = [];
  Object.keys(context.query).forEach((e) => {
    try {
      filterDefaultQuery[e] = context.query[e].split(",");
    } catch (error) {
      error;
    }
  });
  Object.values(filterDefaultQuery).forEach(
    (e) => (filterDefaultQuery2 = [...filterDefaultQuery2, ...e])
  );

  const handleMemberWork = async () => {
    const resourcesRes = await getMemberWork({
      locale: context.locale,
      page: 1,
      pagesize: pageSize,
      ispublic: 1,
      memberm1_no: memberId,
      auth_key: authKey,
      ...filterDefaultQuery,
    });

    return resourcesRes;
  };

  const handleSourceItemFeature = async () => {
    let sourceItemFeatureData = {};
    const { resu, data } = await sourceItemFeature();
    if (resu === 1) {
      sourceItemFeatureData = data;
    }
    return sourceItemFeatureData;
  };

  const [formItems, resourcesRes, sourceItemFeatureData] = await Promise.all([
    handleSourceItem(),
    handleMemberWork(),
    handleSourceItemFeature(),
  ]);

  return {
    props: {
      resourcesRes,
      formItems,
      sourceItemFeatureData,
      isLogin,
      authKey,
      memberId,
      filterDefaultQuery,
      ...(await serverSideTranslations(context.locale, ["common"])),
    },
  };
}
