import Head from "next/head";
import Link from "next/link";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import styles from "../styles/Selections.module.scss";
import { Footer } from "@/components/Footer/Footer";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import { themeIndex } from "@/api";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { CommonSwiperList } from "@/components/CommonSwiperList/CommonSwiperList";
import { SelectionsCard } from "@/components/SelectionsCard/SelectionsCard";
import classNames from "classnames";
import { ParallaxBanner } from "react-scroll-parallax";
import CTAContainer from "@/components/CTAContainer/CTAContainer";
import { ComingSoon } from "@/components/ComingSoon/ComingSoon";

export default function Selections({ themeList }) {
  const { t } = useTranslation("common");
  const CardPc = ({ data }) => (
    <div className={styles.card}>
      <div className={styles.tag}>{data.datest}</div>
      <div className={styles.cardTitle}>{data.title}</div>
      <Link legacyBehavior href={`/selections/${data.slug}`}>
        <a className={styles.btn}>
          <FancyButton>{t("selections.more")}</FancyButton>
        </a>
      </Link>
    </div>
  );

  return (
    <>
      <Head>
        <title>{t("seo.selections.title")}</title>
        <meta name="description" content={t("seo.selections.desc")} />
      </Head>
      <h1 className="seoH1">{t("h1.selections")}</h1>
      <HeaderBannerNew1
        title={t("selections.headline")}
        navList={[
          {
            text: t("selections.headline"),
          },
        ]}
        desc={t("selections.desc1")}
        descClass={styles.headerDesc}
      />

      {themeList.length > 0 ? (
        <>
          <div className={styles.block1}>
            <div className="contentContainer">
              {themeList[0] && (
                <>
                  <div className={styles.blockPc}>
                    <CardPc data={themeList[0]} />
                    <img src={themeList[0].pic} alt={themeList[0].title} />
                  </div>

                  <div className={styles.blockMobile}>
                    <SelectionsCard data={themeList[0]} />
                  </div>
                </>
              )}
            </div>
          </div>

          {themeList[1] && (
            <div className={styles.block2}>
              <ParallaxBanner
                layers={[
                  {
                    image: "/images/headerBG.png",
                    amount: 0.2,
                  },
                ]}
                style={{
                  height: "auto",
                }}
              >
                <div className={styles.content}>
                  <div className="contentContainer">
                    {/* <div className={classNames(styles.mainTitle, styles.title)}>
                      {t("selections.title2")}
                    </div> */}

                    <div className={styles.blockPc}>
                      <img src={themeList[1].pic} alt={themeList[1].title} />
                      <CardPc data={themeList[1]} />
                    </div>

                    <div className={styles.blockMobile}>
                      <SelectionsCard data={themeList[1]} />
                    </div>
                  </div>
                </div>
              </ParallaxBanner>
            </div>
          )}

          {themeList.slice(2).length > 0 && (
            <div className={styles.block3}>
              <div className="contentContainer">
                {/* <div className={styles.subTitle}>{t("selections.title3-1")}</div> */}
                <div className={classNames(styles.mainTitle, styles.title)}>
                  {t("selections.title3-2")}
                </div>
              </div>

              <CommonSwiperList
                dataList={themeList.slice(2)}
                Element={SelectionsCard}
                showFull={true}
                slidesPerView={1.2}
                spaceBetween={30}
                breakpoints={{
                  960: {
                    slidesPerView: 3,
                    slidesPerGroup: 4,
                    spaceBetween: "3.1%",
                  },
                }}
              />
            </div>
          )}

          <CTAContainer
            desc1={t("index.cta.desc1")}
            desc2={t("index.cta.desc2")}
            href={"/why"}
            ctaText={t("index.cta.btn")}
          />
        </>
      ) : (
        <ComingSoon />
      )}

      <Footer />
    </>
  );
}
Selections.headerProps = {};
export async function getServerSideProps(context) {
  const locale = context.locale;

  const handleThemeIndex = async () => {
    let themeList = [];
    const { resu, data } = await themeIndex({
      ispages: 0,
      lang: locale,
    });
    if (resu === 1) {
      themeList = data;
    }
    return themeList;
  };

  const [themeList] = await Promise.all([handleThemeIndex(1)]);

  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
      themeList,
    }, // will be passed to the page component as props
  };
}
