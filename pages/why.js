import Head from "next/head";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import React from "react";
import styles from "../styles/Why.module.scss";
import { Footer } from "@/components/Footer/Footer";
import { Trans, useTranslation } from 'next-i18next';
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import CTAContainer from "@/components/CTAContainer/CTAContainer";
import { categorys } from "@/data/partners";
import { partnersIcons } from "@/data/partnersIcon";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import Link from "next/link";

export default function Why() {
  const { t } = useTranslation("common");
  const CustomLink = ({ children, href }) => (
    <a href={`${locale === "zh" ? "" : "/en"}${href}`} target="_blank" rel="noreferrer">
        {children}
    </a>
);
  return (
    <>
      <Head>
        <title>{t("seo.why.title")}</title>
        <meta name="description" content={t("seo.why.desc")} />
        <meta property="og:description" content={t("seo.why.desc")} key="og-desc" />
      </Head>
      <h1 className="seoH1">{t("h1.why")}</h1>
      <HeaderBannerNew1
        title={t("why.headline")}
        navList={[
          {
            text: t("why.headline")
          }
        ]}
        desc={t("why.introduction")}
        action={
          <Link legacyBehavior href={"/register"}>
            <a>
              <FancyButton className={styles.bannerAction}>
                {t("index.cta.btn")}
              </FancyButton>
            </a>
          </Link>
        }
      />

      <section className={styles.section}>
        <div className={styles.content}>
          <h3>{t("why.title1")}</h3>
          <div className={styles.table}>
            {categorys.map((c, index) => {
              const Icon = partnersIcons[`icon0${index + 1}`];
              return (
                <div key={index} className={styles.categoryItem}>
                  <Icon />

                  <h4>{`${t(c.title)}`}</h4>
                  <p>{t(c.text)}</p>
                </div>
              );
            })}
          </div>

          <h3>{t("why.title2")}</h3>
          <div className={styles.grid}>
            <ul>
              <li>{t("why.content_li_1")}</li>
              <li>{t("why.content_li_2")}</li>
              <li>{t("why.content_li_3")}</li>
              <li>{t("why.content_li_4")}</li>
            </ul>
            {/* <p className={styles.aboutContent}>
              {`• 將喜愛的夥伴或內容加入收藏
• 建立一或多個夥伴頁面，讓國內外的業者找到你
• 立即聯繫潛在合作夥伴
• 分享你的產業活動或新聞資訊`}
            </p> */}
            <p className={styles.aboutContent}>
              {`如果你是臺灣文化科技產業業者：

建立作品頁面，進一步為作品找展演機會，或是為開發中的製作案找合製夥伴。`}
            </p>
          </div>
        </div>
      </section>
      <CTAContainer
        // desc1={t("why.about")}
        href={"/register"}
        ctaText={t("index.cta.btn")}
      />
      <Footer />
    </>
  );
}
Why.headerProps = {};
export async function getServerSideProps(context) {
  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
