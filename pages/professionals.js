import Head from "next/head";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import React, { useEffect, useMemo, useState } from "react";
import dynamic from "next/dynamic";
import styles from "@/styles/Professionals.module.scss";
import { categorys } from "@/data/partners";

import { Footer } from "@/components/Footer/Footer";
import classNames from "classnames";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from 'next-i18next';
import { getFormItems, getMembersV3, getSourceItemV3 } from "@/api";
import { useRouter } from "next/router";
import Droupdown from "@/components/Droupdown/Droupdown";
import { SearchInput } from "@/components/SearchInput/SearchInput";
import { CardContent } from "@/components/CardContent/CardContent";
import Form, { Field } from "rc-field-form";
import { AnimatePresence, motion } from "framer-motion";
import { places } from "@/data/places";
import { useRef } from "react";
import { useRWD } from "@/hooks/useRwd";
import PartnerPreview from "@/components/PartnerPreview/PartnerPreview";
import { useWindowSize } from "react-use";
import animateScrollTo from "animated-scroll-to";

const DynamicGlobe = dynamic(() => import("@/components/Globe"), {
  ssr: false,
});

export default function Professionals({ members, clientele, sphere, sourceItems }) {
  const { t } = useTranslation("common");
  const { locale } = useRouter();

  const [selectPartner, setSelectPartner] = useState(null);
  const [focusPartner, setFocusPartner] = useState(null);
  const [filterQuery, setFilterQuery] = useState({});
  const gobeRef = useRef();

  const continents = useMemo(() => {
    return places.map((p) => ({
      key: p.key,
      title: t(p.title),
    }));
  }, []);

  const displayMembers = useMemo(() => {
    let res = members;
    if (filterQuery.clientele && filterQuery.clientele.length > 0) {
      res = res.filter((c) =>
        filterQuery.clientele.some((s) => s - 0 === c.registration - 1)
      );
    }
    if (filterQuery.sphere && filterQuery.sphere.length > 0) {
      res = res.filter((c) =>
        filterQuery.sphere.some((s) => c.sphere.indexOf(s) !== -1)
      );
    }

    if (filterQuery.continents && filterQuery.continents.length > 0) {
      res = res.filter((c) =>
        filterQuery.continents.some(
          (s) => c.continents && c.continents?.indexOf(s) !== -1
        )
      );
    }
    if (filterQuery.search) {
      res = res.filter(
        (c) => Object.values(c).join(",").indexOf(filterQuery.search) !== -1
      );
    }

    return res;
  }, [filterQuery, members]);

  useEffect(() => {
    if (displayMembers === members) {
      // gobeRef.current?.setFocus(null);

      return;
    }

    const continentsSum = Object.entries(
      displayMembers.reduce((acc, val) => {
        const continents = val.continents;

        if (!continents) {
          return acc;
        }

        if (typeof acc[continents] === "undefined") {
          acc[continents] = 1;
        } else {
          acc[continents] = acc[continents] + 1;
        }

        return acc;
      }, {})
    ).sort((a, b) => b[1] - a[1]);

    const displayContinentNumber = continentsSum?.[0]?.[1] || 0;
    let displayContinent = continentsSum?.[0]?.[0];

    const asiaCount = continentsSum.find((v) => v[0] === "Asia")?.[1] || 0;

    if (asiaCount === displayContinentNumber) {
      displayContinent = "Asia";
    }

    const coordinates = places.find(
      (v) => v.key === displayContinent
    ).coordinates;

    gobeRef.current?.setFocus(coordinates, 4);
  }, [displayMembers, filterQuery]);

  const isCardAnimationPlayingRef = useRef(false);
  const windowSize = useWindowSize();
  const isMobile = windowSize.width < 960;

  return (
    <>
      <Head>
        <title>{t("seo.professionals.title")}</title>
        <meta name="description" content={t("seo.professionals.desc")} />
      </Head>
      <h1 className="seoH1">{t("h1.professionals")}</h1>
      <HeaderBannerNew1
        title={t("professionals.headline")}
        navList={[
          {
            text: t("professionals.headline"),
          },
        ]}
        desc={t("professionals.desc1")}
      />
      <div className={""}>
        <div className={styles.mapView}>
          <div
            className={styles.mapFilter}
            // style={{
            //   height: rwd.device === "mobile" ? 470 : calcWeb(900),
            // }}
          >
            <Form
              onValuesChange={(changedValues, values) => {
                setFilterQuery(values);
              }}
            >
              <input type="hidden" name="csrf_token" value="no_token" />
              <div className={styles.itemSearch}>
                <Field name={"search"}>
                  <SearchInput className={styles.searchInput} />
                </Field>
              </div>
              <div className={styles.filters}>
                <Field name={"continents"}>
                  <Droupdown
                    // placeholder="所在地區"
                    placeholder={t("professionals.area")}
                    spec={continents}
                    inputClassName={styles.filterDroupdownInput}
                    double
                  />
                </Field>
                <Field name={"clientele"}>
                  <Droupdown
                    placeholder={t("professionals.category")}
                    // idPrefix="clientele"
                    spec={clientele.map((v, index) => ({
                      key: `${index}`,
                      title: v,
                    }))}
                    inputClassName={styles.filterDroupdownInput}
                  />
                </Field>
                <Field name={"sphere"}>
                  <Droupdown
                    // placeholder="領域類別"
                    placeholder={t("professionals.soursphere")}
                    idPrefix="sphere"
                    spec={sphere.map((v) => ({ key: v, title: v }))}
                    double
                    inputClassName={styles.filterDroupdownInput}
                  />
                </Field>
              </div>
            </Form>
            <div className={styles.itemList}>
              {displayMembers.map((m) => (
                <div
                  onClick={() => {
                    if (isMobile) {
                      const target = document.querySelector(`#card-${m.id}`);
                      animateScrollTo(target, { verticalOffset: -100 });

                      return;
                    }
                    setFocusPartner(m);
                    gobeRef.current?.setFocus?.(
                      [m.latitude - 0, m.longitude - 0],
                      13
                    );

                    if (selectPartner) {
                      setSelectPartner(null);
                    }
                  }}
                  key={m.name_zh}
                  className={classNames(styles.item, {
                    [styles.focus]: m.id == focusPartner?.id,
                  })}
                >
                  {(() => {
                    let currentLocale = "";
                    let name = "";
                    if (locale === "en") {
                      currentLocale = "en";
                      name = m.name_en;
                    } else {
                      if (m.name_zh) {
                        currentLocale = "zh";
                        name = m.name_zh;
                      } else if (m.name_en) {
                        currentLocale = "en";
                        name = m.name_en;
                      }
                    }
                    return (
                      name && (
                        <>
                          {name}
                          {currentLocale === "en" && (
                            <span className="lastname">{m.lastname_en}</span>
                          )}
                        </>
                      )
                    );
                  })()}
                </div>
              ))}
            </div>
          </div>

          <div className={styles.contentDisplay}>
            <div
              className={styles.globe}
            >
              <DynamicGlobe
                locale={locale}
                t={t}
                members={displayMembers}
                selectPartner={selectPartner}
                setSelectPartner={setSelectPartner}
                focusPartner={focusPartner}
                actionRef={gobeRef}
                showBigTitle={false}
              ></DynamicGlobe>
            </div>

            <div className={styles.grid}>
              {displayMembers.map((m) => (
                <CardContent
                  key={m.id}
                  id={`card-${m.id}`}
                  type="partner"
                  title={m[`name_${locale}`]}
                  image={m.pic}
                  subTitle={m[`intro_${locale}`]}
                  sphere={m.registration?.map((d) => {
                    const category = categorys[d - 1];
                    return (
                      <span className={styles.sphere} key={category.title}>
                        <dot style={{ backgroundColor: category.color }} />
                        {t(category.title)}
                      </span>
                    );
                  })}
                  onClick={() => {
                    // setSelectPartner(m);
                    window.location.href = `/professionals/${m.id}/${m.name_en}`;
                  }}
                />
              ))}
            </div>

            <AnimatePresence>
              {selectPartner && !isMobile && (
                <motion.div
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  exit={{ opacity: 0 }}
                  onAnimationStart={() => {
                    isCardAnimationPlayingRef.current = true;
                  }}
                  onAnimationComplete={() => {
                    isCardAnimationPlayingRef.current = false;
                  }}
                >
                  <PartnerPreview
                    data={selectPartner}
                    locale={locale}
                    textEstablish={t("professionals.establish")}
                    textMore={t("professionals.more")}
                    registration={
                      <div>
                        {selectPartner.registration.map((d) => {
                          const category = categorys[d - 1];

                          return (
                            <span
                              className={styles.sphere}
                              key={category.title}
                            >
                              <dot
                                style={{ backgroundColor: category.color }}
                              />
                              {t(category.title)}
                            </span>
                          );
                        })}
                      </div>
                    }
                    sourceItems={sourceItems}
                    onClose={() => {
                      setSelectPartner(null);
                    }}
                  />
                </motion.div>
              )}
            </AnimatePresence>
          </div>
        </div>
      </div>
      {/* <div className={"contentContainer"}>
        <h3 className={styles.title}>{t("professionals.headline.looking")}</h3>
        <div className={styles.table}>
          {categorys.map((c, index) => {
            const Icon = partnersIcons[`icon0${index + 1}`];
            return (
              <div key={index} className={styles.categoryItem}>
                <Icon />
                <h4>{`0${index + 1} | ${t(c.title)}`}</h4>
                <p>{t(c.text)}</p>
              </div>
            );
          })}
        </div>
      </div> */}
      {/* <Join className={styles.join} /> */}
      <Footer />
    </>
  );
}
Professionals.headerProps = {};
export async function getServerSideProps(context) {
  const membersRes = await getMembersV3({ locale: context.locale });
  const fromItems = await getFormItems({ locale: context.locale });
  const sourceItems = await getSourceItemV3({ locale: context.locale });

  // const sourceItemsRes = await getSourceItem(context.locale);

  // const areaRaw = Object.entries(sourceItemsRes.data.continents_position).map(
  //   ([key, value]) => ({ key, title: value })
  // );

  // const area = areaRaw.map((a) =>
  //   sourceItemsRes.data.nation[a.key]
  //     ? {
  //         ...a,
  //         options: Object.entries(sourceItemsRes.data.nation[a.key]).map(
  //           ([key, value]) => ({ key, title: value })
  //         ),
  //       }
  //     : a
  // );

  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
      members: membersRes,
      fromItems: fromItems,

      clientele: Object.values(fromItems.clientele),
      sphere: Object.values(fromItems.sphere),
      sourceItems: sourceItems.data,
    }, // will be passed to the page component as props
  };
}
