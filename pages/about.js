import Head from "next/head";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import React from "react";
import styles from "../styles/About.module.scss";
import { Footer } from "@/components/Footer/Footer";
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import CTAContainer from "@/components/CTAContainer/CTAContainer";

export default function About() {
  const { t } = useTranslation("common");

  return (
    <>
      <Head>
        <title>{t("seo.about.title")}</title>
        <meta name="description" content={t("seo.about.desc")} />
        <meta
          property="og:description"
          content={t("seo.about.desc")}
          key="og-desc"
        />
      </Head>
      <h1 className="seoH1">{t("h1.about")}</h1>
      <HeaderBannerNew1
        title={t("about.headline")}
        navList={[
          {
            text: t("about.headline")
          }
        ]}
        desc={t("about.introduction")}
      />

      <section className={styles.section}>
        <div className={styles.content}>
          <h3>{t("about.content_title1")}</h3>
          <p>{t("about.content_p1")}</p>

          <div className={styles.grid}>
            <div className={styles.aboutContent}>
              <h4>{t("about.content_title1_1")}</h4>
              <p>{t("about.content_p1_1")}</p>
            </div>
            <div className={styles.aboutContent}>
              <h4>{t("about.content_title1_2")}</h4>
              <p>{t("about.content_p1_2")}</p>
            </div>
          </div>
          <div className={styles.grid2}>
            <div className={styles.aboutContent}>
                <h4>{t("about.content_title1_3")}</h4>
                <p>{t("about.content_p1_3")}</p>
            </div>
          </div>
          <h3>{t("about.content_title2")}</h3>
          <div className={styles.grid}>
            <div className={styles.aboutContent}>
              <h4>{t("about.content_title2_1")}</h4>
              <p>{t("about.content_p2_1")}
                <br />
                <br />
                <a href={t("about.content_link")} target="_blank" rel="noreferrer">
                  {t("about.content_link")}
                </a>
              </p>
            </div>
            <div className={styles.aboutContent}>
              <h4>{t("about.content_title2_2")}</h4>
              <ul>
                <li>{t("about.content_li_1")}</li>
                <li>{t("about.content_li_2")}</li>
                <li>{t("about.content_li_3")}</li>
                <li>{t("about.content_li_4")}</li>
                <li>{t("about.content_li_5")}</li>
                <li>{t("about.content_li_6")}</li>
            </ul>
              {/* <p>
                {`• 推動內容產業投資、融資及引入多元資金
• 根據內容產業現況，多元方式協助拓展國際市場
• 以戰代訓，促成更多具市場性、國際性、IP延伸應用價值的作品及產業關鍵角色
• 推動文化科技產業化
• 提供務實有用的內容產業調查研究
• 提供產業化之重要基礎建設`}
              </p> */}
            </div>
          </div>
        </div>
      </section>
      <CTAContainer
        desc1={t("index.cta.desc1")}
        desc2={t("index.cta.desc2")}
        href={"/why"}
        ctaText={t("index.cta.btn")}
      />
      <Footer />
    </>
  );
}
About.headerProps = {};
export async function getServerSideProps(context) {
  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
