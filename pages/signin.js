import Head from "next/head";
import React, { useCallback, useState, useContext } from "react";

import styles from "@/styles/Signin.module.scss";
import { Recaptcha } from "@/components/Recaptcha/Recaptcha";
import { Footer } from "@/components/Footer/Footer";
import { TLink } from "@/components/TLink";
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";
import SVGJoinBig from "@/svgs/joinBig.svg";
import { auth } from "@/utilits/auth";
import { parseCookies } from "@/utilits/parseCookies";

import { Form, Input, Button, Row, Col, Checkbox, Select } from "antd";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from 'next-i18next';
import { login } from "@/api";
import { useRouter } from "next/router";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { LoadingContext } from "@/context/LoadingContext";

export default function SignIn({ defaultUsername }) {
  const { locale } = useRouter();
  const router = useRouter();
  const { t } = useTranslation("common");
  const rwd = useRWD();
  const [error, setError] = useState("");
  const { setShowLoading } = useContext(LoadingContext);
  const handleFinish = useCallback(
    (data) => {
      setShowLoading(true);
      const { rememberMe, username } = data;
      login({ ...data, locale }).then((res) => {
        if (res.resu === 1) {
          if (rememberMe) {
            document.cookie = `username=${username}; expires=${new Date(
              "2050 01-01"
            ).toUTCString()}; path=/`;
          } else {
            document.cookie = `username=; expires=${new Date(
              "1970 01-01"
            ).toUTCString()}; path=/`;
          }

          document.cookie = `taiccaAuth=${encodeURIComponent(
            JSON.stringify(res.data)
          )}; expires=${new Date("2050 01-01").toUTCString()}; path=/`;

          if (router.query.redirectUrl) {
            router.replace(router.query.redirectUrl);
          } else {
            router.replace("/user/myAccount");
          }

        } else {
          setError(t(`signin.error.apiMsg${res.error}`));
        }
        setShowLoading(false);
      });
    },
    [locale]
  );
  return (
    <>
      <Head>
        <title>{t("seo.signin.title")}</title>
        <meta name="description" content={t("seo.signin.desc")} />
      </Head>
      <h1 className="seoH1">{t("h1.signin")}</h1>
      <HeaderBannerNew1 title={t("meun.memberCenter")} showBack={true} />

      {rwd.device === "mobile" && (
        <h3 className={classNames("customTitle1", styles.titlePc)}>會員登入</h3>
      )}
      <section className={"basic-section"}>
        <div>
          <SVGJoinBig className={styles.logo} />
        </div>

        <div className={styles.formContainer}>
          <Form
            layout="vertical"
            className={styles.form}
            onFinish={handleFinish}
          >
            <input type="hidden" name="csrf_token" value="no_token" />
            <h3 className={classNames("customTitle1", styles.titleMobile)}>
              {t("signin.login")}
            </h3>
            <Form.Item
              required
              label={t("signin.email")}
              name="username"
              rules={[
                {
                  required: true,
                  message: t("signin.email.required", {
                    label: t("signin.email"),
                  }),
                },
                { type: "email", message: t("signin.error.msg7") },
              ]}
              initialValue={defaultUsername}
            >
              <Input placeholder={t("signin.email.placehoder")} />
            </Form.Item>
            <Form.Item
              label={t("signin.password")}
              name="password"
              rules={[
                {
                  required: true,
                  message: t("signin.error.msg6"),
                  pattern: new RegExp(
                    /^(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\`~!@#$%^&*_<>.,()\-+=|\\?\/{}\[\]]).{8,}$/
                  ),
                },
              ]}
            >
              <Input.Password placeholder="***************" />
            </Form.Item>

            <div style={{ marginTop: "34px" }}>
              {error && <div style={{ color: "red" }}>{error}</div>}
              <FancyButton className={classNames(styles.button)}>
                {t("signin.submit")}
              </FancyButton>
            </div>

            <Row justify="space-between" align="middle">
              <Form.Item
                name="rememberMe"
                valuePropName="checked"
                initialValue={true}
                style={{ marginBottom: 0 }}
              >
                <Checkbox>{t("signin.rememberMe")}</Checkbox>
              </Form.Item>

              <div className={styles.desc1}>
                <TLink href={"/forgotPassword"}>
                  {t("signin.forgotPassword")}
                </TLink>
              </div>
            </Row>

            <div className={styles.desc2}>
              {t("signin.desc1")}
              <TLink href={"/register"}> {t("signin.desc2")}</TLink>
            </div>
          </Form>
        </div>
      </section>
      <Footer />
      <Recaptcha />
    </>
  );
}
SignIn.headerProps = {};
export async function getServerSideProps(context) {
  const cookies = context.req.cookies;

  const { isLogin, redirect } = auth(cookies);
  if (isLogin) return redirect;

  const defaultUsername = parseCookies(cookies, "username");

  return {
    props: {
      defaultUsername,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
