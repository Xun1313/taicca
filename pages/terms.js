import Head from "next/head";
// import { HeaderBannerLight } from "@/components/HeaderBanner/HeaderBanner";
import { PageInfoBasic } from "@/components/PageInfo/PageInfo";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import React from "react";
import styles from "@/styles/ArticleContent.module.scss";

import { Footer } from "@/components/Footer/Footer";
import classNames from "classnames";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { Trans, useTranslation } from 'next-i18next';
import { useRWD } from "@/hooks/useRwd";
import { useRouter } from "next/router";

export default function Terms({ article }) {
    const { t } = useTranslation("common");
    const { locale } = useRouter();
    const rwd = useRWD();

    const CustomLink = ({ children, href }) => (
        <a href={`${locale === "zh" ? "" : "/en"}${href}`} target="_blank" rel="noreferrer">
            {children}
        </a>
    );

    return (
        <>
            <Head>
                <title>{t("seo.terms.title")}</title>
                <meta name="description" content={t("seo.terms.desc")} />
            </Head>
            <h1 className="seoH1">{t("h1.terms")}</h1>
            <HeaderBannerNew1
                title={t("terms.headline")}
                navList={[
                {
                    text: t("terms.headline")
                }
                ]}
                desc={t("terms.introduction")}
            />

            <div className={styles.contentContainer}>
                {/* <div
          className={"article"}
          dangerouslySetInnerHTML={{ __html: article.content }}
        ></div> */}
                <div className={"article-simple"}>
                    <Trans
                        i18nKey={t("terms.content")}
                        transKeepBasicHtmlNodesFor={["li", "ul", "b", "h3", "a"]}
                        components={{
                            link_privacy: <CustomLink href="/privacy" />,
                        }}
                    />
                </div>
            </div>

            <Footer />
        </>
    );
}
Terms.headerProps = {
    // red: true,
    // title: "活動公告",
    // numberText: "02",
};
export async function getServerSideProps(context) {
    const locale = context.locale;

    return {
        props: {
            ...(await serverSideTranslations(context.locale, ["common"])),
        }, // will be passed to the page component as props
    };
}
