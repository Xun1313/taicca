import Content, {
  getServerSideProps as getServerSidePropsC,
} from "../article/[slug]";

export default Content;

export const getServerSideProps = getServerSidePropsC;
