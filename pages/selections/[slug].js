import Head from "next/head";
import { Header } from "@/components/Header/Header";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import React, { useContext, useMemo, useState } from "react";
import dynamic from "next/dynamic";
import styles from "@/styles/SelectionsContent.module.scss";

import { Footer } from "@/components/Footer/Footer";
import classNames from "classnames";
import SVGScoialFB from "@/svgs/socialFB.svg";
import SVGScoialTwitter from "@/svgs/socialTwitter.svg";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { themeContent, sourceItem } from "@/api";
import getConfig from "next/config";
import { useTranslation } from "next-i18next";
import { useRWD } from "@/hooks/useRwd";
import moment from "moment";

import { ParallaxContext } from "react-scroll-parallax";
import { useRouter } from "next/router";
import { ParallaxBanner } from "react-scroll-parallax";
import { CommonSwiperList } from "@/components/CommonSwiperList/CommonSwiperList";
import { UnitFavCard } from "@/components/UnitCard/UnitCard";
import { WorkFavCard } from "@/components/WorkCard/WorkCard";
import { auth } from "@/utilits/auth";
import { parseCookies } from "@/utilits/parseCookies";

export default function SelectionsContent({
  article,
  formItems,
  collectmDefaultMapping,
  isLogin,
  authKey,
  memberId,
}) {
  const { publicRuntimeConfig } = useMemo(getConfig, []);
  const { t } = useTranslation("common");

  const [collectmMapping, setCollectmMapping] = useState(
    collectmDefaultMapping
  );

  const router = useRouter();
  const { locale } = useRouter();

  const dataRow = 3;
  const subDataList = [];
  const dataLength = Math.ceil(article.sub.length / dataRow);
  for (let i = 0; i < dataLength; i++) {
    subDataList.push(article.sub.slice(i * dataRow, dataRow * (i + 1)));
  }

  const CardList = ({ listIndex }) => (
    <CommonSwiperList
      dataList={article.sub[listIndex].data}
      Element={({ data, index }) => {
        if (index === 0) {
          return (
            <div
              className={classNames(
                styles.cardFirst,
                styles[`cardFirst${article.sub[listIndex].category}`]
              )}
            >
              {article.sub[listIndex].subtitle}
            </div>
          );
        } else {
          const Element =
            article.sub[listIndex].category === "1" ? UnitFavCard : WorkFavCard;
          return Element({
            data,
            collectmMapping,
            setCollectmMapping,
            formItems,
            isLogin,
            authKey,
            memberId,
          });
        }
      }}
    />
  );

  SelectionsContent.headerProps = {
    customLangLink: () => {
      const toLocale = locale === "zh" ? "en" : "zh";
      if (article.langTheme) {
        router.push(`/selections/${article.langTheme}`, undefined, {
          locale: toLocale,
        });
      } else {
        router.push("/404", undefined, {
          locale: toLocale,
        });
      }
    },
  };
  return (
    <>
      <Head>
        <title>
          {t("seo.articleDetail.title", {
            name: article.title,
          })}
        </title>
        <meta name="description" content={article.title} />
        <meta
          property="og:title"
          content={t("seo.articleDetail.title", {
            name: article.title,
          })}
          key="og-title"
        />
        <meta
          property="og:image"
          content={`${publicRuntimeConfig.backendUrl}${article.pic}`}
          key="og-image"
        />
        <meta property="og:description" content={article.title} key="og-desc" />
      </Head>

      <HeaderBannerNew1
        title={t("selections.headline")}
        navList={[
          {
            text: t("selections.headline"),
            link: "/selections",
          },
          {
            text: article.title,
          },
        ]}
      />
      <div className={styles.head}>
        <img src={article.pic} alt={article.title} />
        <ParallaxBanner
          layers={[
            {
              image: "/images/card_default.png",
              amount: 0.2,
            },
          ]}
          style={{
            height: "auto",
          }}
        >
          <div className={styles.headContent}>
            <div className={styles.tag}>{article.datest}</div>

            <div className={styles.contentInfo}>
              <h3>{article.title}</h3>

              <div className={styles.subtitle}>{article.subtitle}</div>
            </div>
          </div>
        </ParallaxBanner>
      </div>

      {subDataList.map((e, i) => (
        <div key={i}>
          <div className={styles.mainContainer}>
            {e.slice(0, 2).map((f, fIndex) => (
              <div key={`${i} + ${fIndex}`}>
                <div className="contentContainer">
                  <div className={classNames(styles.mainTitle, styles.title)}>
                    {f.title}
                  </div>
                </div>
                <CommonSwiperList
                  showFull={true}
                  slidesPerView={1.2}
                  spaceBetween={30}
                  breakpoints={{
                    960: {
                      slidesPerView: 3,
                      slidesPerGroup: 4,
                      spaceBetween: "3.1%",
                    },
                  }}
                  prefix={`sub${i + `${fIndex}`}`}
                  dataList={[{}, ...f.data]}
                  Element={({ data, index }) => {
                    if (index === 0) {
                      return (
                        <div
                          className={classNames(
                            styles.cardFirst,
                            styles[`cardFirst${f.category}`]
                          )}
                        >
                          {f.subtitle}
                        </div>
                      );
                    } else {
                      const Element =
                        f.category === "1" ? UnitFavCard : WorkFavCard;
                      return Element({
                        data,
                        collectmMapping,
                        setCollectmMapping,
                        formItems,
                        isLogin,
                        authKey,
                        memberId,
                      });
                    }
                  }}
                />
              </div>
            ))}
          </div>

          {e[2] && (
            <div className={classNames("bgRed", styles.block3)}>
              <ParallaxBanner
                layers={[
                  {
                    image: "/images/headerBG.png",
                    amount: 0.2,
                  },
                ]}
                style={{
                  height: "auto",
                }}
              >
                <div className={styles.content}>
                  <div className="contentContainer">
                    <div className={classNames(styles.mainTitle, styles.title)}>
                      {e[2].title}
                    </div>
                  </div>

                  <CommonSwiperList
                    showFull={true}
                    slidesPerView={1.2}
                    spaceBetween={30}
                    breakpoints={{
                      960: {
                        slidesPerView: 3,
                        slidesPerGroup: 4,
                        spaceBetween: "3.1%",
                      },
                    }}
                    prefix={`sub${i + "2"}`}
                    dataList={[{}, ...e[2].data]}
                    Element={({ data, index }) => {
                      if (index === 0) {
                        return (
                          <div
                            className={classNames(
                              styles.cardFirst,
                              styles[`cardFirst${e[2].category}`]
                            )}
                          >
                            {e[2].subtitle}
                          </div>
                        );
                      } else {
                        const Element =
                          e[2].category === "1" ? UnitFavCard : WorkFavCard;
                        return Element({
                          data,
                          collectmMapping,
                          setCollectmMapping,
                          formItems,
                          isLogin,
                          authKey,
                          memberId,
                        });
                      }
                    }}
                  />
                </div>
              </ParallaxBanner>
            </div>
          )}
        </div>
      ))}

      <Footer />
    </>
  );
}

export async function getServerSideProps(context) {
  const cookies = context.req.cookies;
  const locale = context.locale;
  const slug = context.query.slug;

  if (!slug) {
    return {
      notFound: true,
    };
  }

  const { isLogin } = auth(cookies);
  let authKey = "";
  let memberId = "";

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  const handleSourceItem = async () => {
    let formItems = {};
    const { resu, data } = await sourceItem({
      locale: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const handleArticleContent = async () => {
    let article = {};
    let collectmDefaultMapping = {};
    const { data, collectm } = await themeContent({
      lang: locale,
      slug,
      auth_key: authKey,
      memberm1_no: memberId,
    });
    article = data;
    collectmDefaultMapping = collectm;
    return {
      article,
      collectmDefaultMapping,
    };
  };

  const [{ article, collectmDefaultMapping }, formItems] = await Promise.all([
    handleArticleContent(),
    handleSourceItem(),
  ]);

  if (Array.isArray(article) && article[0] === null) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
      article,
      collectmDefaultMapping,
      formItems,
      isLogin,
      authKey,
      memberId,
    }, // will be passed to the page component as props
  };
}
