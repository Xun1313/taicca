import Head from "next/head";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import styles from "../styles/Programs.module.scss";
import { Footer } from "@/components/Footer/Footer";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import { planIndex, sourceItem } from "@/api";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { TLink } from "@/components/TLink";
import { CommonSwiperList } from "@/components/CommonSwiperList/CommonSwiperList";
import { ProgramsCard } from "@/components/ProgramsCard/ProgramsCard";

export default function Programs({
  newsListRes1,
  newsListRes2,
  newsListRes3,
  formItems,
}) {
  const { t } = useTranslation("common");

  return (
    <>
      <Head>
        <title>{t("seo.programs.title")}</title>
        <meta name="description" content={t("seo.programs.desc")} />
      </Head>
      <h1 className="seoH1">{t("h1.programs")}</h1>
      <HeaderBannerNew1
        title={t("news.headline")}
        navList={[
          {
            text: t("news.headline"),
            link: "/news",
          },
          {
            text: t("news.tab4"),
          },
        ]}
        desc={t("news.desc1")}
      />
      <div className={styles.categoryList}>
        <TLink href={`/news`}>
          <FancyButton>{t("news.tab")}</FancyButton>
        </TLink>
        <TLink href={`/publications`}>
          <FancyButton>{t("news.tab1")}</FancyButton>
        </TLink>
        <a href="javascript:void 0;">
          <FancyButton selected>{t("news.tab4")}</FancyButton>
        </a>
        <TLink href={`/events`}>
          <FancyButton>{t("news.tab2")}</FancyButton>
        </TLink>
        <TLink href={`/highlights`}>
          <FancyButton>{t("news.tab3")}</FancyButton>
        </TLink>
      </div>

      <div className={styles.list1}>
        <div className="contentContainer">
          <div className={styles.mainTitle}>{formItems.plan["1"]}</div>
        </div>

        <CommonSwiperList
          dataList={newsListRes1}
          Element={({ data }) => ProgramsCard({ data, formItems })}
          prefix="newsListRes1"
          showFull={true}
          slidesPerView={1.2}
          spaceBetween={30}
          breakpoints={{
            960: {
              slidesPerView: 3,
              slidesPerGroup: 4,
              spaceBetween: "3.1%",
            },
          }}
        />
      </div>

      <div className={styles.list2}>
        <div className="contentContainer">
          <div className={styles.mainTitle}>{formItems.plan["2"]}</div>
        </div>

        <CommonSwiperList
          dataList={newsListRes2}
          Element={({ data }) => ProgramsCard({ data, formItems })}
          prefix="newsListRes2"
          showFull={true}
          slidesPerView={1.2}
          spaceBetween={30}
          breakpoints={{
            960: {
              slidesPerView: 3,
              slidesPerGroup: 4,
              spaceBetween: "3.1%",
            },
          }}
        />
      </div>

      <div className={styles.list3}>
        <div className="contentContainer">
          <div className={styles.mainTitle}>{formItems.plan["3"]}</div>
        </div>

        <CommonSwiperList
          dataList={newsListRes3}
          Element={({ data }) => ProgramsCard({ data, formItems })}
          prefix="newsListRes3"
          showFull={true}
          slidesPerView={1.2}
          spaceBetween={30}
          breakpoints={{
            960: {
              slidesPerView: 3,
              slidesPerGroup: 4,
              spaceBetween: "3.1%",
            },
          }}
        />
      </div>

      <Footer />
    </>
  );
}
Programs.headerProps = {};
export async function getServerSideProps(context) {
  const locale = context.locale;

  const handleSourceItem = async () => {
    let formItems = {};
    const { resu, data } = await sourceItem({
      locale: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const handlePlanIndex = async (subCateid) => {
    let planIndexList = [];
    const { resu, data } = await planIndex({
      pagesize: 10,
      ispages: 0,
      page: 0,
      lang: locale,
      subCateid,
    });
    if (resu === 1) {
      planIndexList = data;
    }
    return planIndexList;
  };

  const [newsListRes1, newsListRes2, newsListRes3, formItems] =
    await Promise.all([
      handlePlanIndex(1),
      handlePlanIndex(2),
      handlePlanIndex(3),
      handleSourceItem(),
    ]);

  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
      newsListRes1,
      newsListRes2,
      newsListRes3,
      formItems,
    }, // will be passed to the page component as props
  };
}
