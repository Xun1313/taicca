import Head from "next/head";
// import { HeaderBannerLight } from "@/components/HeaderBanner/HeaderBanner";
import { PageInfoBasic } from "@/components/PageInfo/PageInfo";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import React from "react";
import styles from "@/styles/ArticleContent.module.scss";

import { Footer } from "@/components/Footer/Footer";
import classNames from "classnames";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { Trans, useTranslation } from 'next-i18next';
import { useRWD } from "@/hooks/useRwd";
import Link from 'next/link';
import { useRouter } from "next/router";
export default function Privacy({ article }) {
    const { t } = useTranslation("common");
    const rwd = useRWD();
    const { locale } = useRouter();
    const CustomLink = ({ children, href }) => (
        <a href={`${locale === "zh" ? "" : "/en"}${href}`} target="_blank" rel="noreferrer">
            {children}
        </a>
    );

    return (
        <>
            <Head>
                <title>{t("seo.privacy.title")}</title>
                <meta name="description" content={t("seo.privacy.desc")} />
                <meta property="og:description" content={t("seo.privacy.desc")} key="og-desc" />
            </Head>
            <h1 className="seoH1">{t("h1.privacy")}</h1>
            {/* <HeaderBannerLight title={t("privacy.headline")} numberText={""} />
            <PageInfoBasic /> */}

            <HeaderBannerNew1
                title={t("privacy.headline")}
                navList={[
                {
                    text: t("privacy.headline")
                }
                ]}
                desc={t("privacy.introduction")}
            />

            <div className={styles.contentContainer}>
                {/* <div
          className={"article"}
          dangerouslySetInnerHTML={{ __html: article.content }}
        ></div> */}
                <div className={"article-simple"}>
                    <Trans
                        i18nKey={t("privacy.content")}
                        transKeepBasicHtmlNodesFor={["li", "ul", "b", "h3", "a"]}
                        components={{
                            link_terms: <CustomLink href="/terms" />,
                        }}
                    />
                </div>
            </div>

            <Footer />
        </>
    );
}
Privacy.headerProps = {
    // red: true,
    // title: "活動公告",
    // numberText: "02",
};
export async function getServerSideProps(context) {
    const locale = context.locale;

    return {
        props: {
            ...(await serverSideTranslations(context.locale, ["common"])),
        }, // will be passed to the page component as props
    };
}
