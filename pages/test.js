import { Select } from "antd";
import React, { useState } from "react";
const OPTIONS = ["你好", "今天", "明天", "天氣好"];
const App = () => {
  const [selectedItems, setSelectedItems] = useState([]);
  const filteredOptions = OPTIONS.filter((o) => !selectedItems.includes(o));
  return (
    <div style={{
        marginTop: "200px"
    }}>
      <Select
        mode="multiple"
        placeholder="Inserted are removed"
        value={selectedItems}
        onChange={setSelectedItems}
        style={{
          width: "100%",
        }}
        options={filteredOptions.map((item) => ({
          value: item,
          label: item,
        }))}
      />
    </div>
  );
};
export default App;
