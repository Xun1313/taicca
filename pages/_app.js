import "../styles/ant.less";
import "../styles/calender.less";
import "../styles/globals.scss";
import "../styles/companion.scss";
import "../styles/favorite.scss";

import { useRouter } from "next/router";
import Head from "next/head";
import { ParallaxProvider } from "react-scroll-parallax";
import { Header } from "@/components/Header/Header";
import { Cursor } from "@/components/Cursor/Cursor";
import { useEffect, useMemo, useState } from "react";
import { appWithTranslation } from "next-i18next";
import { RWD, RWDContextProvider } from "@/context/RWDContext";
import { CursorStateContextPrivider } from "@/context/CursorStateContext";
import { LoadingContextProvider } from "@/context/LoadingContext";
import NextNProgress from "@/components/NextNProgress";
import getConfig from "next/config";
import * as gtag from "../lib/gtag";
import Script from "next/script";
// import { IntlProvider } from "react-intl";
import nextI18nextConfig from '../next-i18next.config';

function MyApp({ Component, pageProps }) {

    const jsonLdData = {
        "@@context":"http://schema.org",
        "@@type":"ProfessionalService",
        "@@id":"https://culturetech.taicca.tw",
        "name": "臺灣文化科技內容",
        "address":{
        "@@type":"PostalAddress",
        "streetAddress":"民生東路三段158號5樓",
        "addressLocality":"台北市松山區",
        "postalCode":"105402",
        "addressCountry":"TW"
        },
        "url": "https://culturetech.taicca.tw",
        "logo": "https://culturetech.taicca.tw/images/logo.png",
        "image": "https://culturetech.taicca.tw/images/logo.png",
        "description": "臺灣的文化科技內容及產業人士，為臺灣好IP與技術應用找新藍海，促進國際人才交流與合製，尋找有投資潛力的新商業模式。",
        "email": "futurecontent@taicca.tw",
        "telephone":  "+88627458166",
    };
    
  const { publicRuntimeConfig } = useMemo(getConfig, []);
  const router = useRouter();
  const { locale } = router;
  
  //const headerProps = Component?.headerProps || {};
  const [headerProps, setHeaderProps] = useState({});
  useEffect(() => {
    setHeaderProps(Component?.headerProps || {})
  }, [Component, locale])

  useEffect(() => {
    const handleRouteChange = (url) => {
      gtag.pageview(url);
    };
    router.events.on("routeChangeComplete", handleRouteChange);
    router.events.on("hashChangeComplete", handleRouteChange);
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
      router.events.off("hashChangeComplete", handleRouteChange);
    };
  }, [router.events]);

  const seoContent =
    locale === "zh"
      ? {
          title: "臺灣文化科技內容",
          desc: "臺灣的文化科技內容及產業人士，為臺灣好IP與技術應用找新藍海，促進國際人才交流與合製，尋找有投資潛力的新商業模式。",
        }
      : {
          title: "Taiwan Culture x Tech Content",
          desc: "Taiwan Culture x Tech Content website presents a complete panorama and builds up networks with cultural creativity, technology, and more resources of the ecosystems.",
        };

  return (
    <>
      <NextNProgress
        color="#E53C2B"
        startPosition={0.3}
        stopDelayMs={200}
        height="2"
        options={{ showSpinner: false }}
      />
      <Head>
        <script
          dangerouslySetInnerHTML={{
            __html: `history.scrollRestoration = "manual"`,
          }}
        />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1"
        ></meta>
        {
          publicRuntimeConfig.isTestingMachine && <meta
            name="robots"
            content="NOINDEX, NOFOLLOW"
          ></meta>
        }
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png"
        />
        <link rel="manifest" href="/site.webmanifest" />

        <title>{seoContent.title}</title>
        <meta name="description" content={seoContent.desc} />
        <meta property="og:title" content={seoContent.title} key="og-title" />
        <meta property="og:description" content={seoContent.desc} key="og-desc" />
        <meta
          property="og:image"
          content={`${publicRuntimeConfig.shareBaseUrl}/images/TAICCA-FUTURE-CONTENT.png`}
          key="og-image"
        />
      </Head>
      <Script
        strategy="afterInteractive"
        src={`https://www.googletagmanager.com/gtag/js?id=${gtag.GA_TRACKING_ID}`}
      />
      <Script
        id="gtag-init"
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', '${gtag.GA_TRACKING_ID}', {
                page_path: window.location.pathname,
              });
              gtag('config', '${gtag.GA_TRACKING_ID_2}', {
                page_path: window.location.pathname,
              });
              gtag('config', '${gtag.GA_TRACKING_ID_3}', {
                page_path: window.location.pathname,
              });
            `,
        }}
      />
      <script type="application/ld+json" dangerouslySetInnerHTML={{ __html: JSON.stringify(jsonLdData) }} />
      <RWDContextProvider>
        <LoadingContextProvider>
          <Header {...headerProps}></Header>
          <ParallaxProvider>
            <Component {...pageProps} />
          </ParallaxProvider>
        </LoadingContextProvider>
      </RWDContextProvider>
    </>
  );
}

export default appWithTranslation(MyApp, nextI18nextConfig);
