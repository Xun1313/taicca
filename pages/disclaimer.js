import Head from "next/head";
import { HeaderBannerLight } from "@/components/HeaderBanner/HeaderBanner";
import { PageInfoBasic } from "@/components/PageInfo/PageInfo";
import React from "react";
import styles from "@/styles/ArticleContent.module.scss";

import { Footer } from "@/components/Footer/Footer";
import classNames from "classnames";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { Trans, useTranslation } from 'next-i18next';
import { useRWD } from "@/hooks/useRwd";

export default function Disclaimer({ article }) {
    const { t } = useTranslation("common");
    const rwd = useRWD();

    return (
        <>
            <Head>
                <title>{t("seo.events.title")}</title>
                <meta name="description" content={t("seo.events.desc")} />
            </Head>
            <h1 className="seoH1">{t("h1.disclaimer")}</h1>
            <HeaderBannerLight title={t("Disclaimer.headline")} numberText={""} />
            <PageInfoBasic />

            <div className={styles.contentContainer}>
                {/* <div
          className={"article"}
          dangerouslySetInnerHTML={{ __html: article.content }}
        ></div> */}
                <div className={"article-simple"}>
                    <Trans i18nKey={t("Disclaimer.content")} />
                </div>
            </div>

            <Footer />
        </>
    );
}
Disclaimer.headerProps = {
    // red: true,
    // title: "活動公告",
    // numberText: "02",
};

export async function getServerSideProps(context) {
    const locale = context.locale;

    return {
        props: {
            ...(await serverSideTranslations(context.locale, ["common"])),
        }, // will be passed to the page component as props
    };
}
