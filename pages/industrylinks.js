import Head from "next/head";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import React, { useCallback, useEffect, useRef, useState } from "react";
import styles from "@/styles/IndustryLinks.module.scss";
import FilterIcon from "@/svgs/filter.svg";
import { Collapse } from "react-collapse";
import FilterOpenIcon from "@/svgs/filterOpen.svg";
import FilterCloseIcon from "@/svgs/filterClose.svg";
import { CardContent } from "@/components/CardContent/CardContent";
import classNames from "classnames";
import { FilterTree } from "@/components/FilterTree/FilterTree";
import {
  sourceItem,
  sourceIndexV3,
  getSsoursphereV3,
  sourceItemSource,
} from "@/api";
import Form, { Field } from "rc-field-form";
import { useAsyncFn, usePrevious } from "react-use";
import { useRouter } from "next/router";
import { useLoadMoreApiCommon } from "@/utilits/useLoadMoreApi";
import ReactPaginate from "react-paginate";
import moment from "moment";
import { Loading } from "@/components/Loading/Loading";
import { LoadMoreGroup } from "@/components/LoadMoreGroup/LoadMoreGroup";
import { useRWD } from "@/hooks/useRwd";
import { Modal } from "antd";
import WebsiteIcon from "@/svgs/website.svg";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import SVGLBX from "@/svgs/close.svg";
import { Footer } from "@/components/Footer/Footer";
import { useImage } from "react-image";
import { TLink } from "@/components/TLink";

const pageSize = 12;

const FilterOption = ({
  spec,
  value = [],
  onChange,
  isCollapsed,
  open,
  close,
}) => {
  const headerTitle = spec.title;
  // const [isCollapsed, setIsCollapsed] = useState(false);

  return (
    <div className={styles.bodyBox}>
      <div onClick={isCollapsed ? close : open} className={styles.optionHeader}>
        <div>{headerTitle}</div>
        {isCollapsed ? <FilterOpenIcon /> : <FilterCloseIcon />}
      </div>
      <Collapse isOpened={isCollapsed}>
        <FilterTree spec={spec.options} value={value} onChange={onChange} />
      </Collapse>
    </div>
  );
};

const Filter = ({ filterSprcs, onReset }) => {
  const { t } = useTranslation("common");
  const [filterOpenControl, setFilterOpenControl] = useState(null);
  return (
    <div className={styles.filter}>
      <div className={styles.header}>
        <div>
          <FilterIcon />
          {t("industrylinks.filter")}
        </div>
        <div
          onClick={() => {
            onReset();
          }}
        >
          {t("industrylinks.clearFilter")}
        </div>
      </div>
      <div className={styles.body}>
        {filterSprcs.map((spec) => (
          <Field name={spec.key} key={spec.key}>
            <FilterOption
              spec={spec}
              isCollapsed={filterOpenControl === spec.key}
              open={() => setFilterOpenControl(spec.key)}
              close={() => setFilterOpenControl(null)}
            />
          </Field>
        ))}
      </div>
    </div>
  );
};

const usePageApi = ({
  initPage,
  initAllPage,
  initData,
  initCount,
  pageSize,
  otherQuery = {},
}) => {
  const otherQueryRef = useRef(otherQuery);
  otherQueryRef.current = otherQuery;
  const [state, setState] = useState({
    data: initData,
    page: initPage,
    allPage: initAllPage,
    count: initCount,
  });
  const { locale } = useRouter();

  const [apiState, fetch] = useAsyncFn(async (page) => {
    const res = await sourceIndexV3({
      page: page,
      pagesize: pageSize,
      locale,
      ...otherQueryRef.current,
    });

    setState({
      data: res.data,
      page: page,
      allPage: res.pages.totalPages,
      count: res.pages.totalCount,
    });
  }, []);

  const handlePageClick = useCallback(
    (val) => {
      setState((prev) => ({
        ...prev,
        page: val.selected + 1,
      }));
      fetch(val.selected + 1);
    },
    [fetch]
  );

  const handleQueryChange = useCallback(() => {
    setState({
      data: [],
      page: 1,
      allPage: 1,
      count: 0,
    });

    fetch(1);
  }, [fetch]);

  const prevQuery = usePrevious(otherQuery);

  useEffect(() => {
    if (typeof prevQuery !== "undefined" && prevQuery !== otherQuery) {
      handleQueryChange();
    }
  }, [otherQuery]);

  return {
    handlePageClick,
    currentPage: state.page,
    allPage: state.allPage,
    data: state.data,
    isLoading: apiState.loading,
    count: state.count,
    handleQueryChange,
  };
};

const getDateString = (r) =>
  !r.date
    ? null
    : r.dateEd
    ? `${moment(r.date).format("YYYY/MM/DD")} - ${moment(r.dateEd).format(
        "YYYY/MM/DD"
      )}`
    : `${moment(r.date).format("YYYY/MM/DD")}`;

const Image = ({ src, ...rest }) => {
  const imageDara = useImage({ srcList: src, useSuspense: false });
  return <img src={imageDara.src} {...rest} alt="icon" />;
};

function IndustryLinks({
  formItems,
  soursphere,
  resourcesRes,
  sourceItemSourceData,
}) {
  const { t } = useTranslation("common");
  const [form] = Form.useForm();
  const { locale } = useRouter();

  const rwd = useRWD();
  const continentsList = Object.entries(formItems.continents_position)
    .map(([key, title]) => {
      return {
        key,
        title,
        amount: sourceItemSourceData.continents[key],
      };
    })
    .filter((e) => e.amount > 0);

  const industryList = Object.entries(formItems.industry)
    .map(([key, title]) => {
      return {
        key,
        title,
        amount: sourceItemSourceData.industry[key],
      };
    })
    .filter((e) => e.amount > 0);

  const categoryList = Object.entries(formItems.source_category)
    .map(([key, title]) => {
      return {
        key,
        title,
        amount: sourceItemSourceData.category[key],
      };
    })
    .filter((e) => e.amount > 0);

  const filterSprcs = [
    {
      title: t("industrylinks.area"),
      key: "continents",
      options: continentsList,
    },
    {
      title: t("industrylinks.industry"),
      key: "industry",
      options: industryList,
    },
    {
      title: t("industrylinks.category"),
      key: "category",
      options: categoryList,
    },
    {
      title: t("industrylinks.soursphere"),
      key: "sphere",
      options: soursphere,
    },
  ];

  const [filterQuery, setFilterQuery] = useState({});

  const pageControl = usePageApi({
    initAllPage: resourcesRes.pages.totalPages,
    initPage: 1,
    initData: resourcesRes.data,
    pageSize: pageSize,
    otherQuery: filterQuery,
    initCount: resourcesRes.pages.totalCount,
  });
  const loadMoreState = useLoadMoreApiCommon({
    initAllPage: resourcesRes.pages.totalPages,
    initPage: 1,
    initData: resourcesRes.data,
    pageSize: pageSize,
    api: sourceIndexV3,
    otherQuery: filterQuery,
    initCount: resourcesRes.pages.totalCount,
  });

  const [selectedData, setSelectedData] = useState(null);
  const [isLightboxOepn, setIsLightboxOepn] = useState(null);

  const control = rwd.device === "mobile" ? loadMoreState : pageControl;

  return (
    <>
      <Head>
        <title>{t("seo.industrylinks.title")}</title>
        <meta name="description" content={t("seo.industrylinks.desc")} />
      </Head>
      <h1 className="seoH1">{t("h1.industrylinks")}</h1>
      <HeaderBannerNew1
        title={t("resources.headline")}
        navList={[
          {
            text: t("resources.headline"),
          },
          {
            text: t(`resources.tab1`),
          },
        ]}
        desc={t("resources.desc1")}
      />
      <div className={styles.categoryList}>
        <TLink href="/resources">
          <FancyButton>{t("resources.tab")}</FancyButton>
        </TLink>
        <TLink href="/articles">
          <FancyButton>{t("resources.tab002")}</FancyButton>
        </TLink>
        <TLink href="/opencall">
          <FancyButton>{t("resources.tab004")}</FancyButton>
        </TLink>
        <TLink href="/industrylinks">
          <FancyButton selected>{t("resources.tab1")}</FancyButton>
        </TLink>
      </div>

      <div className={classNames("contentContainer", styles.content)}>
        <Form
          form={form}
          onValuesChange={(changedValues, values) => {
            setFilterQuery(values);
          }}
        >
          <input type="hidden" name="csrf_token" value="no_token" />
          <Filter
            filterSprcs={filterSprcs}
            onReset={() => {
              form.resetFields();
              setFilterQuery({});
            }}
          />
        </Form>
        <div className={styles.contentInner}>
          {control.isLoading && control.data.length === 0 && (
            <div className={styles.loading}>
              <Loading />
            </div>
          )}
          {!(control.isLoading && control.data.length === 0) && (
            <div className={styles.count}>
              {t("totalCount", {
                num: control.count,
              })}
            </div>
          )}

          <div className={styles.grid}>
            {control.data.map((d) => (
              <CardContent
                key={d.id}
                type="resources"
                title={d.title_zh}
                subTitle={d.title_en}
                tags={d.category.map((d) => Object.values(d)[0])}
                image={[d.pic, d.pic2, "/images/card_default.png"]}
                rangeText={getDateString(d)}
                category="news"
                onClick={() => {
                  setIsLightboxOepn(true);
                  setSelectedData(d);
                }}
              />
            ))}
          </div>
          <Modal
            visible={isLightboxOepn}
            footer={null}
            onCancel={() => {
              setIsLightboxOepn(false);
            }}
            width={860}
            closeIcon={<SVGLBX className={styles.lightboxClose} />}
          >
            {selectedData && (
              <div className={styles.lightbox}>
                <div className={styles.header}>
                  <div>
                    <Image
                      src={[
                        selectedData.pic,
                        selectedData.pic2,
                        "/images/card_default.png",
                      ]}
                    />
                  </div>
                  <div>
                    {selectedData.category
                      .map((d) => Object.values(d)[0])
                      .map((t) => (
                        <sapn key={t} className={styles.tag}>
                          {t}
                        </sapn>
                      ))}
                    <h3>{selectedData.title_zh}</h3>
                    <p className={styles.subTitle}>{selectedData.title_en}</p>
                    <p className={styles.date}>{getDateString(selectedData)}</p>
                    {selectedData.url && (
                      <a
                        href={selectedData.url}
                        target="_blank"
                        rel="noreferrer"
                      >
                        <WebsiteIcon />
                        {t("industrylinks.website")}
                      </a>
                    )}
                  </div>
                </div>
                <div className={styles.features}>{selectedData.features}</div>
                <div
                  className={styles.lightboxContent}
                  dangerouslySetInnerHTML={{ __html: selectedData.content }}
                ></div>
                {selectedData.history_url && (
                  <div className={styles.lightboxAction}>
                    <FancyButton
                      onClick={() => {
                        window.open(selectedData.history_url, "_blank");
                      }}
                    >
                      歷年作品
                    </FancyButton>
                  </div>
                )}
              </div>
            )}
          </Modal>
          <div className={classNames(styles.paginate, "desktop-only")}>
            <ReactPaginate
              previousLabel={"<"}
              nextLabel={">"}
              breakLabel={"..."}
              breakClassName={"break-me"}
              // initialPage={state.page - 1}
              initialPage={0}
              pageCount={pageControl.allPage}
              // pageCount={state.allPage}
              marginPagesDisplayed={2}
              pageRangeDisplayed={4}
              onPageChange={pageControl.handlePageClick}
              containerClassName={classNames("pagination")}
              activeClassName={"active"}
            />
          </div>
          {loadMoreState.isLoading && (
            <div style={{ textAlign: "center", marginBottom: 30 }}>
              <Loading />
            </div>
          )}
          {loadMoreState.showLoadMore && (
            <LoadMoreGroup
              actionText={t("event.showmore.button")}
              leftText={t("event.showmore")}
              className={classNames("mobile-only", styles.loadMoreGroup)}
              onLoadmore={loadMoreState.handleNext}
            />
          )}
        </div>
      </div>

      <Footer />
    </>
  );
}

export default IndustryLinks;
export async function getServerSideProps(context) {
  const handleSourceItem = async () => {
    let formItems = {};
    const { resu, data } = await sourceItem({
      locale: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const handleSourceItemSource = async () => {
    let sourceItemSourceData = {};
    const { resu, data } = await sourceItemSource();
    if (resu === 1) {
      sourceItemSourceData = data;
    }
    return sourceItemSourceData;
  };

  const [formItems, soursphereRes, resourcesRes, sourceItemSourceData] =
    await Promise.all([
      handleSourceItem(),
      await getSsoursphereV3(context.locale),
      await sourceIndexV3({
        locale: context.locale,
        page: 1,
        pagesize: pageSize,
      }),
      handleSourceItemSource(),
    ]);

  const soursphere = soursphereRes.data
    .map((e) => {
      return {
        title: e.title,
        key: e.slug,
        amount: sourceItemSourceData.sphere[e.slug],
      };
    })
    .filter((e) => e.amount > 0);
  console.log("---------");
  console.log(soursphere);
  return {
    props: {
      formItems,
      soursphere,
      resourcesRes,
      sourceItemSourceData,
      ...(await serverSideTranslations(context.locale, ["common"])),
    },
  };
}
