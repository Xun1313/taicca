import Head from "next/head";
import React, { useCallback, useState, useContext } from "react";

import styles from "@/styles/ForgotPassword.module.scss";
import { Recaptcha } from "@/components/Recaptcha/Recaptcha";
import { Footer } from "@/components/Footer/Footer";
import { FormSuccess } from "@/components/FormSuccess/FormSuccess";
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";
import SVGJoinBig from "@/svgs/joinBig.svg";

import { Form, Input } from "antd";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from 'next-i18next';
import { memberForget } from "@/api";
import { useRouter } from "next/router";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { LoadingContext } from "@/context/LoadingContext";

export default function ForgotPassword() {
  const { locale } = useRouter();
  const { t } = useTranslation("common");
  const rwd = useRWD();
  const { setShowLoading } = useContext(LoadingContext);

  const [error, setError] = useState("");
  const [isSuccess, setIsSuccess] = useState(false);
  const handleFinish = useCallback(
    (data) => {
      setShowLoading(true);
      memberForget({ ...data }).then((res) => {
        if (res.resu === 1) {
          setIsSuccess(true);
        } else {
          setError(t(`forgotPassword.error.apiMsg${res.error}`));
        }
        setShowLoading(false);
      });
    },
    [locale]
  );

  const FormSuccessTitle = () => {
    return (
      <>
        <div>{t("forgotPassword.popup.title1")}</div>
        <div>{t("forgotPassword.popup.title2")}</div>
      </>
    );
  };

  return (
    <>
      <Head>
        <title>{t("seo.forgotPassword.title")}</title>
        {/* <meta name="description" content={t("seo.forgotPassword.desc")} /> */}
      </Head>
      <h1 className="seoH1">{t("h1.forgotPassword")}</h1>
      <HeaderBannerNew1 title={t("meun.memberCenter")} showBack={true} />

      {isSuccess ? (
        <FormSuccess
          title={FormSuccessTitle()}
          desc={t("forgotPassword.popup.desc")}
        />
      ) : (
        <>
          {rwd.device === "mobile" && (
            <h3 className={classNames("customTitle1", styles.titlePc)}>
              會員登入
            </h3>
          )}
          <section className={"basic-section"}>
            <div>
              <SVGJoinBig className={styles.logo} />
            </div>

            <div className={styles.formContainer}>
              <Form
                layout="vertical"
                className={styles.form}
                onFinish={handleFinish}
              >
                <input type="hidden" name="csrf_token" value="no_token" />
                <h3 className={classNames("customTitle1", styles.titleMobile)}>
                  {t("forgotPassword.title")}
                </h3>
                <div className={classNames(styles.desc)}>
                  {t("forgotPassword.desc")}
                </div>
                <Form.Item
                  required
                  label={t("forgotPassword.email")}
                  name="username"
                  rules={[
                    {
                      required: true,
                      message: t("signin.email.required", {
                        label: t("forgotPassword.email"),
                      }),
                    },
                    { type: "email", message: t("signin.error.msg7") },
                  ]}
                >
                  <Input placeholder="XXX@xxx.com" />
                </Form.Item>

                <div style={{ marginTop: "34px" }}>
                  {error && <div style={{ color: "red" }}>{error}</div>}
                  <FancyButton className={classNames(styles.button)}>
                    {t("forgotPassword.submit")}
                  </FancyButton>
                </div>
              </Form>
            </div>
          </section>
        </>
      )}

      <Footer />
      <Recaptcha />
    </>
  );
}

ForgotPassword.headerProps = {};
export async function getStaticProps(context) {
  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
