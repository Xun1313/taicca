import Resources, {
  getServerSideProps as getServerSidePropsC,
} from "@/pages/resources";

export default Resources;

export const getServerSideProps = (context) =>
  getServerSidePropsC({ ...context, subCateid: "004" });
