/* eslint-disable @next/next/no-img-element */
import React, {
  useCallback,
  useMemo,
  useRef,
  useState,
  useEffect,
  useContext,
} from "react";
import dynamic from "next/dynamic";
import Link from "next/link";
import styles from "@/styles/QrActivity.module.scss";
import { Footer } from "@/components/Footer/Footer";
import {
  Form,
  Input,
  Radio,
  Row,
  Col,
  Checkbox,
  Select,
  Upload,
  InputNumber,
  Tabs,
  Tag,
  Button,
  message,
  Alert,
} from "antd";

const { Option } = Select;
const { TextArea } = Input;
import classNames from "classnames";

import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from 'next-i18next';
import { liveEventIndex, liveEventAdd, sourceItem, getCountry } from "@/api";
import { useRouter } from "next/router";
import { parseCookies } from "@/utilits/parseCookies";
import { LoadingContext } from "@/context/LoadingContext";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { parseItems } from "@/utilits/parseItems";
import { auth } from "@/utilits/auth";

export default function QrActivity({
  liveEventIndexData,
  slug,
  country,
  formItems,
  authKey,
  memberId,
  isLogin,
}) {
  const router = useRouter();
  const locale = router.locale;
  const { t } = useTranslation("common");
  const { setShowLoading } = useContext(LoadingContext);
  const [step, setStep] = useState(1);
  const [apiError1, setApiError1] = useState("");
  const [apiError2, setApiError2] = useState("");

  const acIdentityOptions = parseItems(formItems.acIdentity);

  const handleLiveEventAdd = async () => {
    // type: 1 -> is a member, 2 -> not a member yet
    if (isLogin) {
      setShowLoading(true);
      const { resu, error } = await liveEventAdd({
        slug,
        type: "1",
        memberm1_no: memberId,
        auth_key: authKey,
      });
      if (resu === 1) {
        setStep(3);
      } else {
        setApiError1(t(`qrActivity.form.error.apiMsg${error}`));
      }
      setShowLoading(false);
    } else {
      // 做 redirectUrl
      router.push(`/signin?redirectUrl=${router.asPath}`);
    }
  };

  const handleCliclNext = (values) => {
    console.log(values);
    setShowLoading(true);

    liveEventAdd({
      slug,
      lang: locale,
      type: "2",
      ...values,
    }).then((res) => {
      if (res.resu === 1) {
        setStep(3);
      } else {
        setApiError2(t(`qrActivity.form.error.apiMsg${res.error}`));
      }

      setShowLoading(false);
    });
  };

  return (
    <>
      <HeaderBannerNew1
        title={t("meun.memberCenter")}
        showBack={true}
        backLink="/user/myAccount"
        backText={t("myAccount.title")}
      />
      <div className={styles.contentContainer}>
        {(step === 1 || step === 2) && (
          <div className={styles.title}>
            {locale === "zh"
              ? liveEventIndexData.title_zh
              : liveEventIndexData.title_en}
          </div>
        )}

        {step === 1 && (
          <>
            <div
              className={styles.desc}
              dangerouslySetInnerHTML={{
                __html:
                  locale === "zh"
                    ? liveEventIndexData.content_zh
                    : liveEventIndexData.content_en,
              }}
            ></div>

            {apiError1 && (
              <div style={{ color: "red", textAlign: "center" }}>
                {apiError1}
              </div>
            )}
            <div className={styles.btnGroup}>
              <FancyButton selected onClick={handleLiveEventAdd}>
                {t("qrActivity.btn1")}
              </FancyButton>
              <FancyButton onClick={() => setStep(2)}>
                {t("qrActivity.btn2")}
              </FancyButton>
            </div>
          </>
        )}
        {step === 2 && (
          <Form
            className={styles.form}
            layout="vertical"
            onFinish={handleCliclNext}
            scrollToFirstError={true}
            name="step1"
          >
            <input type="hidden" name="csrf_token" value="no_token" />
            <div className={styles.formTitle}>{t("qrActivity.form.title")}</div>

            <Form.Item
              name="name_zh"
              label={t("qrActivity.form.field1")}
              rules={[]}
            >
              <Input placeholder={t("qrActivity.form.field1.placeholder")} />
            </Form.Item>

            <Form.Item
              required
              name="name_en"
              label={t("qrActivity.form.field2")}
              rules={[
                {
                  required: true,
                  message: t("qrActivity.form.required", {
                    label: t("qrActivity.form.field2.placeholder1"),
                  }),
                },
              ]}
            >
              <Input placeholder={t("qrActivity.form.field2.placeholder1")} />
            </Form.Item>

            <Form.Item
              required
              name="lastname_en"
              rules={[
                {
                  required: true,
                  message: t("qrActivity.form.required", {
                    label: t("qrActivity.form.field2.placeholder2"),
                  }),
                },
              ]}
            >
              <Input placeholder={t("qrActivity.form.field2.placeholder2")} />
            </Form.Item>

            <Form.Item
              required
              name="email"
              label={t("qrActivity.form.field3")}
              rules={[
                {
                  required: true,
                  message: t("qrActivity.form.required", {
                    label: t("qrActivity.form.field3"),
                  }),
                },
                { type: "email", message: t("signin.error.msg7") },
              ]}
            >
              <Input placeholder={t("qrActivity.form.field3.placeholder")} />
            </Form.Item>

            <Form.Item
              required
              name="nation"
              label={t("qrActivity.form.field4")}
              rules={[
                {
                  required: true,
                  message: t("qrActivity.form.required", {
                    label: t("qrActivity.form.field4.placeholder"),
                  }),
                },
              ]}
            >
              <Select
                placeholder={t("qrActivity.form.field4.placeholder")}
                showSearch
                optionFilterProp="title"
              >
                {country.map((c) => (
                  <Option
                    key={c.name}
                    value={c.name}
                    title={`${c.name} ${c.traditionalName}`}
                  >
                    {c.name} {c.traditionalName}
                  </Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item
              required
              label={t("qrActivity.form.field5")}
              name="identity"
              rules={[
                {
                  required: true,
                  message: t("register.required", {
                    label: t("qrActivity.form.field5"),
                  }),
                },
              ]}
            >
              <Radio.Group className={classNames("customRadioGroup")}>
                <Radio value={acIdentityOptions[0].value}>
                  {acIdentityOptions[0].label}
                  {locale === "en" && (
                    <div style={{ fontSize: 14, color: "#A0A6AD" }}>
                      {t("register.field1.desc1")}
                    </div>
                  )}
                </Radio>
                <Radio value={acIdentityOptions[1].value}>
                  {acIdentityOptions[1].label}
                </Radio>
              </Radio.Group>
            </Form.Item>

            <div className={styles.formBtnGroup}>
              {apiError2 && (
                <div style={{ color: "red", textAlign: "center" }}>
                  {apiError2}
                </div>
              )}
              <FancyButton selected>{t("qrActivity.form.btn1")}</FancyButton>
              <FancyButton htmlType="button" onClick={() => setStep(1)}>
                {t("qrActivity.form.btn2")}
              </FancyButton>
            </div>
          </Form>
        )}
        {step === 3 && (
          <div className={styles.successContainer}>
            <div
              className={classNames(styles.successTitle, styles.successTitle1)}
            >
              {t("qrActivity.success.title1")}
            </div>
            <div
              className={classNames(styles.successTitle, styles.successTitle2)}
            >
              {t("qrActivity.success.title2")}
            </div>

            <Link legacyBehavior href="/">
              <FancyButton>{t("qrActivity.success.btn1")}</FancyButton>
            </Link>
          </div>
        )}
      </div>

      <Footer />
    </>
  );
}
QrActivity.headerProps = {};
export async function getServerSideProps(context) {
  const cookies = context.req.cookies;
  const slug = context.query.slug;
  const { isLogin } = auth(cookies);

  let authKey = "";
  let memberId = "";

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  const handleLiveEventIndexData = async () => {
    let liveEventIndexData = [];

    const { resu, data } = await liveEventIndex({
      slug,
    });
    if (resu === 1) {
      liveEventIndexData = data;
    }
    return liveEventIndexData;
  };

  const handleCountry = async () => {
    let country = {};
    const { resu, data } = await getCountry({
      locale: context.locale,
    });
    if (resu === 1) {
      country = data.country;
    }
    return country;
  };

  const handleSourceItem = async () => {
    let formItems = {};
    const { resu, data } = await sourceItem({
      locale: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const [liveEventIndexData, country, formItems] = await Promise.all([
    handleLiveEventIndexData(),
    handleCountry(),
    handleSourceItem(),
  ]);

  return {
    props: {
      liveEventIndexData,
      slug,
      country,
      formItems,
      authKey,
      memberId,
      isLogin,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
