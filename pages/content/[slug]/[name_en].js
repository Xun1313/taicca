import Head from "next/head";
import React, { useMemo, useContext } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import {
  collectmIndex,
  getCountryV3,
  getSourceItemV3,
  getWorkV3,
  accWorksReview,
  memberWorkDownload,
} from "@/api";
import styles from "@/styles/Work.module.scss";
import { useWindowSize } from "react-use";
import { Footer } from "@/components/Footer/Footer";
import MemberCard from "@/components/MemberCard/MemberCard";
import moment from "moment";
import classNames from "classnames";
import WorkCardList from "@/components/WorkCardList/WorkCardList";
import IconDownload from "@/svgs/bx_download.svg";
import IconLink from "@/svgs/link.svg";
import ReactPlayer from "react-player";
import PlatformIcon from "@/components/PlatformIcon/PlatformIcon";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import { getLoginData } from "@/utilits/getLoginData";
import { useFavorite } from "@/hooks/useFavorite";
import FavoriteIcon from "@/components/FavoriteIcon/FavoriteIcon";
import Link from "next/link";
import { ApplyWorkPopup } from "@/components/ApplyPopup/ApplyPopup";
import { LoadingContext } from "@/context/LoadingContext";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import PreviewBanner from "@/components/PreviewBanner/PreviewBanner";
import IconLocked from "@/svgs/locked.svg";
import { Tooltip } from "antd";
import getConfig from "next/config";
import { workerCount } from "mapbox-gl";

const Unitm1Display = ({ id, name_en, lastname_en, name_zh }) => {
  const displayName = [name_en, lastname_en, name_zh]
    .filter((n) => n)
    .join(" ");

  return (
    <Link legacyBehavior href={`/professionals/${id}/${name_en}`}>
      <a>{displayName}</a>
    </Link>
  );
};

const NameList = ({ Unitm1_no, name, mark = "、" }) => {
  const list = [
    ...Unitm1_no.map((d) => <Unitm1Display key={d.id} {...d} />),
    ...name,
  ];

  return list.reduce((acc, val, index) => {
    const isLast = list.length - 1 === index;

    return isLast ? [...acc, val] : [...acc, val, mark];
  }, []);
};

export default function Content({
  sourceItems,
  work,
  country,
  memberFavorites,
  workFavorites,
  isLogin,
  authKey,
  memberId,
  preview,
}) {
  const { publicRuntimeConfig } = useMemo(getConfig, []);
  const { setShowLoading } = useContext(LoadingContext);
  const showVideo = useMemo(() => {
    return ReactPlayer.canPlay(work.youtube);
  }, [work]);

  const windowSize = useWindowSize();
  const isMobile = windowSize.width < 960;

  const { locale } = useRouter();
  const router = useRouter();
  const { t } = useTranslation("common");

  const memberFavoritesState = useFavorite({
    initFavoriteIds: memberFavorites.map((d) => d.id),
    authKey,
    memberId,
    type: 1,
  });

  const workFavoritesState = useFavorite({
    initFavoriteIds: workFavorites.map((d) => d.id),
    authKey,
    memberId,
    type: 2,
  });

  const handleSuccessApply = async ({ successCb }) => {
    setShowLoading(true);
    const { resu } = await accWorksReview({
      memberm1_no: memberId,
      auth_key: authKey,
      workm1_no: work.id,
    });

    if (resu === 1) {
      successCb(work.id);
    }
    setShowLoading(false);
  };

  return (
    <>
      <Head>
        <title>
          {t("seo.contentDetail.title", {
            name:
              locale === "zh"
                ? work.name_zh || work.name_en
                : work.name_en || work.name_zh,
          })}
        </title>
        <meta
          name="description"
          content={
            locale === "zh"
            ? work.intro_zh?.slice(0,80) || work.intro_en?.slice(0,80)
            : work.intro_en?.slice(0,150) || work.intro_zh?.slice(0,80)
          }
        />
        <meta
          property="og:title"
          content={t("seo.contentDetail.title", {
            name:
              locale === "zh"
                ? work.name_zh || work.name_en
                : work.name_en || work.name_zh,
          })}
          key="og-title"
        />
        <meta
          property="og:image"
          content={`${publicRuntimeConfig.backendUrl}${work.pic}`}
          key="og-image"
        />
        <meta
          property="og:description"
          content={
            locale === "zh"
              ? work.intro_zh?.slice(0,80) || work.intro_en?.slice(0,80)
              : work.intro_en?.slice(0,150) || work.intro_zh?.slice(0,80)
          }
          key="og-desc"
        />
      </Head>
      {preview && (
        <PreviewBanner
          previewTitle={t("works.step4.submit1")}
          backText={t("works.preview.back")}
          action={
            work.isfinish && (
              <ApplyWorkPopup
                onSuccess={handleSuccessApply}
                SubmitBtn={({ text }) => (
                  <FancyButton
                    selected
                    htmlType="button"
                    scaleSize={38}
                    className={classNames(styles.previewBtn)}
                  >
                    {text}
                  </FancyButton>
                )}
              />
            )
          }
        />
      )}

      <div className={styles.hero}>
        <div className={styles.imgContainer}>
          <img src={work.pic} alt="icon" />
        </div>
        <div className={styles.heroInfoContainer}>
          <div className={styles.heroInfo}>
            <FavoriteIcon
              tooltip={t("favorite.tooltip")}
              className={styles.favoriteIcon}
              isFavorite={workFavoritesState.idList.indexOf(work.id) !== -1}
              onClick={(e) => {
                e.stopPropagation();
                e.preventDefault();
                workFavoritesState.handleFavorite(work.id);
              }}
              isFavoriteColor="white"
              isLogin={isLogin}
            />
            <div className={styles.titleEng}>{work.name_en}</div>
            <h1 className={styles.titleZh}>{work.name_zh}</h1>
            <div className={styles.titleInfo}>
              {`${t("work.workct1")}： ${work.workct1.map(
                (c) =>
                  c.name ||
                  [
                    c.unit?.[0]?.name_en,
                    c.unit?.[0]?.lastname_en,
                    c.unit?.[0]?.name_zh,
                  ]
                    .filter((d) => d)
                    .join(" ")
              )}`}
            </div>
          </div>
          <div className={styles.heroInfoBottom}>
            <div className={styles.tags}>
              {work.gtheme?.map((t) => `#${sourceItems.artTags[t]}`).join(" ")}
            </div>
            <div className={styles.status}>
              {sourceItems.workType[work.work_status]}
            </div>
          </div>
        </div>
      </div>

      {/* <PageInfoBasic /> */}
      <div className={styles.container}>
        

        <div className={styles.gridContainer}>
          <div className={styles.infoLeft}>
            <div className={styles.infoSection1}>
              {work.work_status == "1" && (
                <div>
                  <span className={styles.title}>{`${t(
                    "work.estimated"
                  )} `}</span>{" "}
                  {locale === "en"
                    ? `${work.final_month.toString().padStart(2, "0")}/${
                        work.final_year
                      }`
                    : `${work.final_year}/${work.final_month
                        .toString()
                        .padStart(2, "0")}`}
                </div>
              )}
              {work.work_status == "2" && (
                <div>
                  <span className={styles.title}>{`${t("work.year")} `}</span>{" "}
                  {work.year}
                </div>
              )}

              {work.platform && (
                <div>
                  <span className={styles.title}>{t("work.platform")}</span>{" "}
                  <a href={work.platform} target="_blank" rel="noreferrer">
                    <PlatformIcon url={work.platform} />
                  </a>
                </div>
              )}
            </div>

            <div className={styles.infoSection2}>
              <div className={styles.title}>{t("work.gclass")}</div>

              <div className={styles.infoTagGroup}>
                {work.gclass.map((g) => (
                  <div key={g} className={styles.infoTag}>
                    {sourceItems.artType[g]}
                  </div>
                ))}
              </div>
            </div>

            <div className={classNames(styles.hr, "mobile-only")} />
            <div className={styles.infoSection3}>
              <div className={styles.title}>{t("work.artform")}</div>

              <div className={styles.infoTagGroup}>
                {work.workform.map((w) => (
                  <div className={styles.infoTag} key={w}>
                    {sourceItems.artform[w]}
                  </div>
                ))}
                {/* <div className={styles.infoTag}>多媒體裝置</div>
                <div className={styles.infoTag}>遊戲</div> */}
              </div>
            </div>
            <div className={classNames(styles.hr, "mobile-only")} />
            <div className={styles.infoSection4}>
              <div className={styles.group}>
                <span className={styles.title}>{t("work.mv_length")}</span>
                <br />
                {work.mv_length} {t("work.mv_length_unit")}
              </div>

              <div className={styles.group}>
                <span className={styles.title}>{t("work.language")}</span>
                <br />
                {work.language.map((l) => sourceItems.lang[l]).join("/")}
              </div>

              <div className={classNames(styles.hr, "mobile-only")} />

              <div className={styles.group}>
                <span className={styles.title}>{t("work.screentext")}</span>
                <br />
                {work.screentext.map((l) => sourceItems.lang[l]).join("/")}
              </div>

              <div className={styles.group}>
                <span className={styles.title}>{t("work.ex_method")}</span>
                <br />
                {work.ex_method.map((e) =>
                  e === "1" ? (
                    <span key={1}>{t("work.ex_method_1")}</span>
                  ) : e === "2" ? (
                    <span key={2}>{t("work.ex_method_2")}</span>
                  ) : (
                    e
                  )
                )}
              </div>
              <div className={classNames(styles.hr, "mobile-only")} />
              <div className={styles.group}>
                <span className={styles.title}>{t("work.ex_number")}</span>
                <br />
                {work.ex_number.map((e) =>
                  e === "1" ? (
                    <span key={3}>{t("work.ex_number_1")}</span>
                  ) : e === "2" ? (
                    <span key={4}>{t("work.ex_number_2")}</span>
                  ) : (
                    e
                  )
                )}
              </div>
            </div>
            <div className={classNames(styles.hr, "mobile-only")} />
            <div className={styles.infoSection5}>
              <div className={styles.title}>{t("work.workct1")}</div>
              <div className={styles.memberGroup}>
                {work.workct1.map((d, i) => {
                  const isUnit = !!d.unit?.[0];
                  const props = {
                    pic: d.unit?.[0]?.pic_min,
                    nameZh: d.unit?.[0]?.name_zh,
                    nameEng:
                      [d.unit?.[0]?.name_en, d.unit?.[0]?.lastname_en]
                        .filter((n) => n)
                        .join(" ") || d.name,
                    isFavorite: d.unit?.[0]
                      ? memberFavoritesState.idList.indexOf(d.unit[0].id) !== -1
                      : false,
                    onFavorite: (e) => {
                      e.preventDefault();
                      e.stopPropagation();
                      memberFavoritesState.handleFavorite(d.unit?.[0]?.id);
                    },
                    showFavorite: isLogin && d.unit?.[0],
                    favoriteTooltip: t("favorite.tooltip"),
                    unitText: d.unit?.[0]
                      ? sourceItems.mechanism[d.unit?.[0].unit]
                      : "",
                    isLogin,
                  };

                  return isUnit ? (
                    <Link
                      legacyBehavior
                      key={i}
                      href={`/professionals/${d.unit?.[0]?.id}/${d.unit?.[0]?.name_en}`}
                    >
                      <a>
                        <MemberCard key={i} {...props} />
                      </a>
                    </Link>
                  ) : (
                    <MemberCard key={i} {...props} />
                  );
                })}
              </div>
            </div>
            <div className={styles.infoSection6}>
              <div className={styles.title}>{t("work.madecoutry")}</div>
              {work.madecoutry.map((c) => (
                <>
                  {c}
                  <br />
                </>
              ))}
              <br className="desktop-only" />
              <div className={classNames(styles.hr, "mobile-only")} />
              <div className={styles.title}>{t("work.workct2")}</div>
              {work.workct2.map((w) =>
                w.Unitm1_no || w.name ? (
                  <>
                    {`${sourceItems.rolecreation[w.title]}： `}
                    <NameList Unitm1_no={w.Unitm1_no} name={w.name} />
                    <br />
                  </>
                ) : null
              )}
              <br className="desktop-only" />
              <br className="desktop-only" />
              <div className={classNames(styles.hr, "mobile-only")} />
              <div className={styles.title}>{t("work.workct3")}</div>
              {work.workct3.map((w) =>
                w.Unitm1_no || w.name ? (
                  <>
                    {`${sourceItems.roleproduction[w.title]}： `}
                    <NameList Unitm1_no={w.Unitm1_no} name={w.name} />
                    <br />
                  </>
                ) : null
              )}
              <br className="desktop-only" />
              <br className="desktop-only" />
              <div className={classNames(styles.hr, "mobile-only")} />
              {(work.pdf_zh || work.pdf_en) && (
                <>
                  <div className={styles.title}>{t("work.pdf")}</div>
                  {work.pdf_zh &&
                    (isLogin ? (
                      <a
                        href={work.pdf_zh}
                        target="_blank"
                        rel="noreferrer"
                        onClick={() => {
                          memberWorkDownload({ lang: "zh", id: work.id });
                        }}
                      >
                        <div className={styles.download}>
                          {work.pdf_title_zh}
                          <IconDownload />
                        </div>
                      </a>
                    ) : (
                      <div className={styles.download}>
                        {work.pdf_title_zh}
                        <Tooltip title={t("memberOnly")} placement="right">
                          <IconLocked />
                        </Tooltip>
                      </div>
                    ))}

                  {work.pdf_en &&
                    (isLogin ? (
                      <a
                        href={work.pdf_en}
                        target="_blank"
                        rel="noreferrer"
                        onClick={() => {
                          memberWorkDownload({ lang: "en", id: work.id });
                        }}
                      >
                        <div className={styles.download}>
                          {work.pdf_title_en}
                          <IconDownload />
                        </div>
                      </a>
                    ) : (
                      <div className={styles.download}>
                        {work.pdf_title_en}
                        <Tooltip title={t("memberOnly")} placement="right">
                          <IconLocked />
                        </Tooltip>
                      </div>
                    ))}
                </>
              )}
            </div>

            <br className="desktop-only" />
            <br />

            <div className={styles.contactCard}>
              <img src="/images/memberEmailIcon.png" alt="icon"></img>
              <div>
                <div className={styles.name}>
                  <span className={styles.eng}>{work.contact_en}</span>{" "}
                  <span className={styles.eng}>{work.lastname_en}</span>{" "}
                  {work.contact_zh}
                </div>
                <div
                  className={classNames(styles.email, [
                    { [styles.locked]: !isLogin },
                  ])}
                >
                  {isLogin ? (
                    work.email
                  ) : (
                    <>
                      ***@******{" "}
                      <Tooltip title={t("memberOnly")} placement="right">
                        <IconLocked />
                      </Tooltip>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className={styles.infoRight}>
            <div className={styles.info}>
              {locale === "en" ? work.intro_en : work.intro_zh}
            </div>

            <div className={styles.updateTime}>
              Update : {moment(new Date(work.edit_date)).format("L")}
            </div>

            <div className={styles.section}>
              <div className={styles.title}>{t("work.workt1")}</div>
              <div className={styles.info}>
                {work.workt1.map((w, index) => (
                  <div key={index}>{`${w.year}/${
                    locale === "en" ? w.title_en : w.title_zh
                  }/${(locale === "en" ? w.unit_en : w.unit_zh) || ""}`}</div>
                ))}
              </div>
            </div>

            <div className={styles.section}>
              <div className={styles.title}>{t("work.workt2")}</div>
              <div className={styles.info}>
                {work.workt2.map((w, index) => {
                  return (
                    <div key={index}>{`${w.year}/${
                      country.find((c) => c.name === w.nation)?.[
                        locale === "en" ? "name" : "traditionalName"
                      ] || ""
                    }/${locale === "en" ? w.name_en : w.name_zh}`}</div>
                  );
                })}
              </div>
            </div>

            {work.youtube && (
              <div className={styles.section}>
                <div className={styles.title}>{t("work.youtube")}</div>
                <a href={work.youtube} target="_blank" rel="noreferrer">
                  <div className={classNames(styles.info, styles.link)}>
                    {work.youtube}
                    <IconLink />
                  </div>
                </a>
                {showVideo && (
                  <div className={styles.video}>
                    <ReactPlayer
                      url={work.youtube}
                      width="100%"
                      height="100%"
                    />
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
      {work.recent.length > 0 && (
        <div className={styles.recent}>
          <div className={classNames(styles.title, styles.sectionContainer)}>
            {t("content.works")}
          </div>
          <WorkCardList
            works={work.recent}
            sourceItems={sourceItems}
            favoriteIds={workFavoritesState.idList}
            onFavorite={workFavoritesState.handleFavorite}
            favoriteText={t("favorite.tooltip")}
            isLogin={isLogin}
          />
        </div>
      )}

      <Footer />
    </>
  );
}

export async function getServerSideProps(context) {
  const slug = context.query.slug;
  const preview = context.query.preview;

  const { authKey, memberId, isLogin } = getLoginData(context);

  const sourceItemsReq = getSourceItemV3({ locale: context.locale });

  const locale = context.locale;

  const workReq = getWorkV3({
    locale: locale,
    id: slug,
    ispublic: preview ? 0 : 1,
    auth_key: authKey,
    memberm1_no: memberId,
  });

  const countryReq = getCountryV3({
    locale: locale,
    id: slug,
  });

  const memberFavoritesReq = isLogin
    ? collectmIndex({
        auth_key: authKey,
        memberm1_no: memberId,
        ispages: 0,
        type: 1,
        lang: locale,
      }).then((d) => {
        if (d.resu === 1) {
          return d.data;
        }
        return Promise.reject(d);
      })
    : Promise.resolve([]);

  const workFavoritesReq = isLogin
    ? collectmIndex({
        auth_key: authKey,
        memberm1_no: memberId,
        ispages: 0,
        type: 2,
        lang: locale,
      }).then((d) => {
        if (d.resu === 1) {
          return d.data;
        }
        return Promise.reject(d);
      })
    : Promise.resolve([]);

  const [sourceItems, work, country, memberFavorites, workFavorites] =
    await Promise.all([
      sourceItemsReq,
      workReq,
      countryReq,
      memberFavoritesReq,
      workFavoritesReq,
    ]);

  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
      sourceItems: sourceItems.data,
      work: work.data[0],
      country: country.data?.country,
      memberFavorites,
      workFavorites,
      isLogin,
      authKey,
      memberId,
      preview: preview || "",
    },
  };
}
