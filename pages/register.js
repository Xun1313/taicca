import Head from "next/head";
import React, { useCallback, useMemo, useState, useContext } from "react";

import styles from "@/styles/Register.module.scss";
import { Recaptcha } from "@/components/Recaptcha/Recaptcha";
import { Footer } from "@/components/Footer/Footer";
import SVGLBX from "@/svgs/close.svg";
import SVGEmail from "@/svgs/emailRegister.svg";
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";
import SVGQustion2 from "@/svgs/question2.svg";
import { parseItems } from "@/utilits/parseItems";
import { auth } from "@/utilits/auth";

import { Form, Input, Modal, Row, Col, Checkbox, Radio } from "antd";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from 'next-i18next';
import { register, sourceItem } from "@/api";
import router, { useRouter } from "next/router";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { LoadingContext } from "@/context/LoadingContext";
import Link from 'next/link'

export default function Register({ acIdentity }) {
  const { locale } = useRouter();
  const { t } = useTranslation("common");
  const rwd = useRWD();
  const { setShowLoading } = useContext(LoadingContext);

  const [error, setError] = useState("");
  const [inputEmail, setInputEmail] = useState("");
  const [noticeModal, setNoticeModal] = useState(false);

  const acIdentityOptions = parseItems(acIdentity);

  const doubleColProps = useMemo(
    () => ({
      span: rwd.device === "mobile" ? 24 : 12,
    }),
    [rwd.device]
  );

  const tripleColProps = useMemo(
    () => ({
      span: rwd.device === "mobile" ? 24 : 8,
    }),
    [rwd.device]
  );

  const handleFinish = useCallback(
    (data) => {
      console.log(data);
      setShowLoading(true);
      register({ ...data, locale }).then((res) => {
        if (res.resu === 1) {
          setInputEmail(data.email);
          setNoticeModal(true);
        } else {
          setError(t(`register.error.apiMsg${res.error}`));
        }
        setShowLoading(false);
      });
    },
    [locale]
  );
  return (
    <>
      <Head>
        <title>{t("seo.register.title")}</title>
        <meta name="description" content={t("seo.register.desc")} />
      </Head>
      <h1 className="seoH1">{t("h1.register")}</h1>
      
      <HeaderBannerNew1 title={t("meun.memberCenter")} showBack={true} />

      <section className={classNames(styles.contentContainer)}>
        <h3 className={classNames("customTitle1", styles.title)}>
          {t("register.join")}
        </h3>
        <div className={classNames(styles.create)}>{t("register.create")}</div>
        <Form layout="vertical" onFinish={handleFinish}>
          <input type="hidden" name="csrf_token" value="no_token" />
          <Form.Item
            required
            label={t("register.field1")}
            name="identity"
            rules={[
              {
                required: true,
                message: t("register.required", {
                  label: t("register.field1"),
                }),
              },
            ]}
          >
            <Radio.Group className={classNames("customRadioGroup")}>
              <Radio value={acIdentityOptions[0].value}>
                {acIdentityOptions[0].label}
                {locale === "en" && (
                  <div style={{ fontSize: 14, color: "#A0A6AD" }}>
                    {t("register.field1.desc1")}
                  </div>
                )}
              </Radio>
              <Radio value={acIdentityOptions[1].value}>
                {acIdentityOptions[1].label}
              </Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item
            required
            label={t("register.field2")}
            name="email"
            rules={[
              {
                required: true,
                message: t("register.required", {
                  label: t("register.field2"),
                }),
              },
              { type: "email", message: t("signin.error.msg7") },
            ]}
          >
            <div>
              <Input placeholder={t("register.field2.placeholder")} />
              <div className={classNames(styles.emailNotice)}>
                <div>
                  <SVGQustion2 />
                </div>
                {t("register.field2.memo")}
              </div>
            </div>
          </Form.Item>

          <Row gutter={40}>
            <Col {...doubleColProps}>
              <Form.Item
                label={t("register.field3")}
                name="password"
                rules={[
                  {
                    required: true,
                    message: t("signin.error.msg6"),
                    pattern: new RegExp(
                      /^(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\`~!@#$%^&*_<>.,()\-+=|\\?\/{}\[\]]).{8,}$/
                    ),
                  },
                ]}
              >
                <Input.Password placeholder="***************" />
              </Form.Item>
            </Col>

            <Col {...doubleColProps}>
              <Form.Item
                label={t("register.field4")}
                name="confirmpassword"
                rules={[
                  {
                    required: true,
                    message: t("signin.error.msg6"),
                    pattern: new RegExp(
                      /^(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\`~!@#$%^&*_<>.,()\-+=|\\?\/{}\[\]]).{8,}$/
                    ),
                  },
                ]}
              >
                <Input.Password placeholder="***************" />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={40}>
            {locale === "zh" && (
              <Col {...tripleColProps}>
                <Form.Item
                  required
                  label={t("register.field5")}
                  name="name_zh"
                  rules={[
                    {
                      required: true,
                      message: t("register.required", {
                        label: t("register.field5"),
                      }),
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>
            )}

            <Col {...tripleColProps}>
              <Form.Item
                required
                label={t("register.field6")}
                name="name_en"
                rules={[
                  {
                    required: true,
                    message: t("register.required", {
                      label: t("register.field6.placeholder"),
                    }),
                  },
                ]}
              >
                <Input placeholder={t("register.field6.placeholder")} />
              </Form.Item>
            </Col>

            <Col {...tripleColProps}>
              <Form.Item
                required
                label={" "}
                name="lastname_en"
                rules={[
                  {
                    required: true,
                    message: t("register.required", {
                      label: t("register.field7.placeholder"),
                    }),
                  },
                ]}
              >
                <Input placeholder={t("register.field7.placeholder")} />
              </Form.Item>
            </Col>
          </Row>

          <Form.Item
            required
            name="privacy"
            valuePropName="checked"
            rules={[
              {
                required: true,
                message: t("register.desc1.error"),
              },
            ]}
          >
            <Checkbox>
                {t("register.desc1")}
                <Link legacyBehavior href="/privacy">{t("register.desc.privacy")}</Link>
                {t("register.desc.and")}
                <Link legacyBehavior href="/terms">{t("register.desc.terms")}</Link>
            </Checkbox>
            
          </Form.Item>

          <div className={classNames(styles.customSubmit)}>
            <div className={classNames(styles.submit)}>
              {error && <div style={{ color: "red" }}>{error}</div>}
              <FancyButton>{t("register.submit")}</FancyButton>
            </div>
          </div>
        </Form>

        <Modal
          visible={noticeModal}
          footer={null}
          onCancel={() => {
            setNoticeModal(false);
          }}
          width={800}
          closeIcon={<SVGLBX className={styles.lightboxClose} />}
        >
          <div className={styles.noticeContainer}>
            <div
              className={styles.noticeTitle}
              dangerouslySetInnerHTML={{
                __html: t("register.popup.title"),
              }}
            ></div>
            <div
              className={styles.desc}
              dangerouslySetInnerHTML={{
                __html: t("register.popup.desc1", {
                  email: inputEmail,
                }),
              }}
            ></div>
            <Row align="middle" className={styles.desc}>
              <SVGEmail />
              <a
                href="mailto: futurecontent@taicca.tw"
                style={{ marginLeft: 5, textDecoration: "underline" }}
                target="_blank"
                rel="noreferrer"
              >
                futurecontent@taicca.tw
              </a>
            </Row>

            <div className={classNames(styles.checkBtn)}>
              <FancyButton onClick={() => router.push("/professionals")}>
                {t("register.popup.desc2")}{" "}
              </FancyButton>
            </div>
          </div>
        </Modal>
      </section>
      <Footer />
      <Recaptcha />
    </>
  );
}
Register.headerProps = {};
export async function getServerSideProps(context) {
  const cookies = context.req.cookies;

  const { isLogin, redirect } = auth(cookies);
  if (isLogin) return redirect;

  let acIdentity = {};
  const { resu, data } = await sourceItem({ locale: context.locale });
  if (resu === 1) {
    acIdentity = data.acIdentity;
  }

  return {
    props: {
      acIdentity,
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
