import Head from "next/head";
import { Header } from "@/components/Header/Header";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import React, { useContext, useState, useMemo } from "react";
import dynamic from "next/dynamic";
import styles from "@/styles/Content2.module.scss";

import { Footer } from "@/components/Footer/Footer";
import classNames from "classnames";
import SVGScoialFB from "@/svgs/socialFB.svg";
import SVGScoialTwitter from "@/svgs/socialTwitter.svg";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { planContent, sourceItem, planJoinMember } from "@/api";
import getConfig from "next/config";
import { useTranslation } from "next-i18next";
import moment from "moment";
import { auth } from "@/utilits/auth";
import { parseCookies } from "@/utilits/parseCookies";

import { useRouter } from "next/router";
import { ParallaxBanner } from "react-scroll-parallax";
import { LoadingContext } from "@/context/LoadingContext";
import { CommonSwiperList } from "@/components/CommonSwiperList/CommonSwiperList";
import { ProgramsCard } from "@/components/ProgramsCard/ProgramsCard";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import { CommonLightBox } from "@/components/CommonLightBox/CommonLightBox";

export default function Content({
  article,
  formItems,
  isLogin,
  memberId,
  authKey,
  slug,
}) {
  const { publicRuntimeConfig } = useMemo(getConfig, []);
  const [showSignUpLightBox, setShowSignUpLightBox] = useState(false);
  const [signUpError, setSignUpError] = useState("");
  const { setShowLoading } = useContext(LoadingContext);

  const { t } = useTranslation("common");

  const router = useRouter();
  const { locale } = useRouter();
  const content = useMemo(() => {
    let res = article.content || "";

    res = res.replace(/<oembed/gm, "<o-embed");
    res = res.replace(/<\/oembed>/gm, "</o-embed>");

    return res;
  }, [article.content]);

  const handleSignUp = async () => {
    setShowLoading(true);
    const { resu, error } = await planJoinMember({
      auth_key: authKey,
      memberm1_no: memberId,
      slug,
    });

    if (resu === 1) {
      router.push(`/consentConfirm/${slug}`);
    } else {
      setSignUpError(t(`programs.error.apiMsg${error}`));
    }
    setShowLoading(false);
  };

  Content.headerProps = {
    customLangLink: () => {
      const toLocale = locale === "zh" ? "en" : "zh";
      if (article.langPlan) {
        router.push(`/programs/${article.langPlan}`, undefined, {
          locale: toLocale,
        });
      } else {
        router.push("/404", undefined, {
          locale: toLocale,
        });
      }
    },
  };

  return (
    <>
      <Head>
        <title>
          {article.seo_title ||
            t("seo.articleDetail.title", {
              name: article.title,
            })}
        </title>
        <meta name="description" content={article.seo_des || article.title} />
        <meta
          property="og:title"
          content={
            article.seo_title ||
            t("seo.articleDetail.title", {
              name: article.title,
            })
          }
          key="og-title"
        />
        <meta
          property="og:image"
          content={`${publicRuntimeConfig.backendUrl}${article.pic}`}
          key="og-image"
        />
        <meta
          property="og:description"
          content={article.seo_des || article.title}
          key="og-desc"
        />
      </Head>

      <HeaderBannerNew1
        title={t("news.headline")}
        navList={[
          {
            text: t("news.headline"),
            link: "/news",
          },
          {
            text: t("news.tab4"),
            link: "/programs",
          },
          {
            text: article.title,
          },
        ]}
      />
      <div className={styles.head}>
        <img src={article.pic} alt={article.title} />
        <ParallaxBanner
          layers={[
            {
              image: "/images/card_default.png",
              amount: 0.2,
            },
          ]}
          style={{
            height: "auto",
          }}
        >
          <div className={styles.headContent}>
            <div className={styles.tagList}>
              <div className={classNames(styles.tag, styles.area)}>
                {formItems.planArea[article.area]}
              </div>
              <div className={classNames(styles.tag, styles.category)}>
                {formItems.plan[article.category]}
              </div>
            </div>

            <div className={styles.contentInfo}>
              <div className={styles.time}>{article.datest}</div>
              <h3>{article.title}</h3>
              {article.receipt_date &&
                article.receipt_dateed &&
                article.status && (
                  <div
                    className={styles.contehtStatus}
                  >{`${article.status}`}</div>
                )}
              <div style={{ marginBottom: 20 }}>
                {article.receipt_date &&
                  article.receipt_dateed &&
                  `${moment(article.receipt_date).format(
                    "YYYY/MM/DD"
                  )} - ${moment(article.receipt_dateed).format("YYYY/MM/DD")}`}
              </div>

              {article.openjoin && (
                <div className={styles.signUp}>
                  <FancyButton
                    onClick={() => {
                      if (isLogin) {
                        setShowSignUpLightBox(true);
                      } else {
                        router.push(`/signin?redirectUrl=${router.asPath}`);
                      }
                    }}
                  >
                    {t("programs.signUp.btn1")}
                  </FancyButton>
                </div>
              )}
            </div>
          </div>
        </ParallaxBanner>
      </div>

      <div className={styles.contentContainer}>
        <div
          className={"article"}
          dangerouslySetInnerHTML={{ __html: content }}
        ></div>

        {article.openjoin && (
          <div className={styles.signUp}>
            <FancyButton
              onClick={() => {
                if (isLogin) {
                  setShowSignUpLightBox(true);
                } else {
                  router.push(`/signin?redirectUrl=${router.asPath}`);
                }
              }}
            >
              {t("programs.signUp.btn1")}
            </FancyButton>
          </div>
        )}

        <div className={styles.social}>
          Share
          <a
            href={`https://www.facebook.com/sharer/sharer.php?u=${publicRuntimeConfig.shareBaseUrl}${router.asPath}`}
            target="_blank"
            rel="noreferrer"
          >
            <SVGScoialFB />
          </a>
          <a
            href={`https://twitter.com/intent/tweet?url=https://${publicRuntimeConfig.shareBaseUrl}${router.asPath}`}
            target="_blank"
            rel="noreferrer"
          >
            <SVGScoialTwitter />
          </a>
          {/* <a
            href="https://twitter.com/TAICCA_Official"
            target="_blank"
            rel="noreferrer"
          >
            <img src={"/images/linkedin.png"} />
          </a>
          <a
            href="https://twitter.com/TAICCA_Official"
            target="_blank"
            rel="noreferrer"
          >
            <img src={"/images/IG.png"} />
          </a> */}
        </div>
      </div>

      <div className={styles.recentContainer}>
        <ParallaxBanner
          layers={[
            {
              image: "/images/logo_default.png",
              amount: 0.2,
            },
          ]}
          style={{
            height: "auto",
          }}
        >
          <div className={classNames("contentContainer", styles.content)}>
            <div className={styles.title}>{t("programs.recentTitle")}</div>
          </div>

          <CommonSwiperList
            dataList={article.Recent}
            Element={({ data }) => ProgramsCard({ data, formItems })}
            showFull={true}
            slidesPerView={1.2}
            spaceBetween={30}
            breakpoints={{
              960: {
                slidesPerView: 3,
                slidesPerGroup: 4,
                spaceBetween: "3.1%",
              },
            }}
          />
        </ParallaxBanner>
      </div>

      <Footer />

      <CommonLightBox
        className={classNames(styles.signUpLightBox)}
        containerClass={classNames(styles.container)}
        titleClass={classNames(styles.title)}
        descClass={classNames(styles.desc)}
        button1Class={classNames(styles.button1)}
        button2Class={classNames(styles.button2)}
        title={t("programs.signUp.title")}
        desc={
          <div>
            <div className={styles.descTitle}>
              {t("programs.signUp.desc", {
                title: article.title,
              })}
            </div>

            {signUpError && (
              <div className={styles.descError}>{signUpError}</div>
            )}
          </div>
        }
        button1={t("programs.signUp.btn2")}
        showButton2={true}
        button2={t("programs.signUp.btn3")}
        showModal={showSignUpLightBox}
        setShowModal={setShowSignUpLightBox}
        onClose2={handleSignUp}
      />
    </>
  );
}

export async function getServerSideProps(context) {
  const locale = context.locale;
  const slug = context.query.slug;
  const cookies = context.req.cookies;

  if (!slug) {
    return {
      notFound: true,
    };
  }

  const { isLogin } = auth(cookies);

  let authKey = "";
  let memberId = "";

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  const handleSourceItem = async () => {
    let formItems = {};
    const { resu, data } = await sourceItem({
      locale: context.locale,
    });
    if (resu === 1) {
      formItems = data;
    }
    return formItems;
  };

  const handleArticleContent = async () => {
    let article = {};
    const { data } = await planContent({
      locale,
      slug,
    });
    article = data;
    return article;
  };

  const [article, formItems] = await Promise.all([
    handleArticleContent(),
    handleSourceItem(),
  ]);

  if (Array.isArray(article) && article[0] === null) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
      article,
      formItems,
      isLogin,
      authKey,
      memberId,
      slug,
    }, // will be passed to the page component as props
  };
}
