import { PageInfoBasic } from "@/components/PageInfo/PageInfo";
import React from "react";

import styles from "../styles/message.module.scss";
import { Footer } from "@/components/Footer/Footer";
import SVG404 from "@/svgs/404.svg";
import Link from "next/link";
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";
import { HeaderBannerLight } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { TLink } from "@/components/TLink";
import { Trans, useTranslation } from 'next-i18next';
import SBGSuccess from "@/svgs/joinSuccess.svg";
import { useRouter } from "next/router";

const messages = {
  contactSuccess: {
    title: "contact.headline",
    message1: "contact.form.success.title",
    message2: "contact.form.success.desc",
  },
};

export default function Message() {
  const rwd = useRWD();
  const { t } = useTranslation("common");
  const router = useRouter();
  const queryA = router.query?.a;
  const message = messages[queryA] || {};

  return (
    <>
      <HeaderBannerLight title={t(message.title)} />
      <PageInfoBasic />
      <div className={"contentContainer"}>
        <section className={styles.section1}>
          <div>
            <SBGSuccess className={styles.successIcon} />
          </div>
          <div>
            <h3>
              <Trans i18nKey={message.message1} />
            </h3>
            <div className={styles.desc}>
              <Trans i18nKey={message.message2} />
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
}
Message.headerProps = {
  // red: true,
};
export async function getStaticProps(context) {
  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
