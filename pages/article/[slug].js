import Head from "next/head";
import { Header } from "@/components/Header/Header";
import { HeaderBannerNew1 } from "@/components/HeaderBanner/HeaderBanner";
import React, { useContext, useMemo } from "react";
import dynamic from "next/dynamic";
import styles from "@/styles/ArticleContent.module.scss";

import { Footer } from "@/components/Footer/Footer";
import SVGScoialFB from "@/svgs/socialFB.svg";
import SVGScoialTwitter from "@/svgs/socialTwitter.svg";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { getArticleContentv3, getArticleContent001v3 } from "@/api";
import getConfig from "next/config";
import { useTranslation } from 'next-i18next';
import { subCateidMap } from "@/utilits/subCateidMap";
// import { GoogleSlider } from "@/components/GoogleSlider";
import { useRWD } from "@/hooks/useRwd";
import moment from "moment";

import { TLink } from "@/components/TLink";
import { ParallaxContext } from "react-scroll-parallax";
import { useRouter } from "next/router";
import ReactDOM from "react-dom/client";
import ReactPlayer from "react-player";

if (typeof window !== "undefined") {
  class OembedVideo extends HTMLElement {
    connectedCallback() {
      const mountPoint = document.createElement("div");
      this.attachShadow({ mode: "open" }).appendChild(mountPoint);

      mountPoint.style.aspectRatio = 960 / 540;
      const url = this.getAttribute("url");

      ReactDOM.createRoot(mountPoint).render(
        <ReactPlayer width="100%" height="100%" url={url} />
      );
    }
  }
  customElements.define("o-embed", OembedVideo);
}

export default function Content({ article, cateId }) {
  const { publicRuntimeConfig } = useMemo(getConfig, []);
  const parallaxController = useContext(ParallaxContext);
  const { t } = useTranslation("common");
  const { locale } = useRouter();
  const rwd = useRWD();
  const cateIdMapping = {
    "02": {
      headerTitle: t("news.headline"),
      langSlug: article.langArticlenews,
      path: article.subCateid === "2" ? "/events" : "/news",
      navPath: "/news",
    },
    "05": {
      headerTitle: t("resources.headline"),
      langSlug: article.langArticle,
      path: "/resources",
      navPath: "/resources",
    },
  };
  const headerTitle = cateIdMapping[cateId].headerTitle;

  const router = useRouter();
  console.log(router);
  // 001:新聞 002:趨勢報告 003:活動
  // const subCateidData = subCateidMap[article.subCateid];
  const dateString = article.dateEd
    ? `${moment(article.date).format("YYYY/MM/DD")} - ${moment(
        article.dateEd
      ).format("YYYY/MM/DD")}`
    : `${moment(article.date).format("YYYY/MM/DD")}`;

  // const showDateTitle = subCateidData.showDateTitle;
  const showDateTitle = false;

  const content = useMemo(() => {
    let res = article.content || "";

    res = res.replace(/<oembed/gm, "<o-embed");
    res = res.replace(/<\/oembed>/gm, "</o-embed>");

    return res;
  }, [article.content]);

  Content.headerProps = {
    customLangLink: () => {
      const toLocale = locale === "zh" ? "en" : "zh";
      let langSlug = cateIdMapping[cateId].langSlug;
      let path = cateIdMapping[cateId].path;

      if (langSlug) {
        router.push(`${path}/${langSlug}`, undefined, {
          locale: toLocale,
        });
      } else {
        router.push("/404", undefined, {
          locale: toLocale,
        });
      }
    },
  };

  return (
    <>
      <Head>
        <title>
          {article.seo_title ||
            t("seo.articleDetail.title", {
              name: article.title,
            })}
        </title>
        <meta name="description" content={article.seo_des || article.title} />
        <meta
          property="og:title"
          content={
            article.seo_title ||
            t("seo.articleDetail.title", {
              name: article.title,
            })
          }
          key="og-title"
        />
        <meta
          property="og:image"
          content={`${publicRuntimeConfig.backendUrl}${article.pic}`}
          key="og-image"
        />
        <meta
          property="og:description"
          content={article.seo_des || article.title}
          key="og-desc"
        />
      </Head>

      <HeaderBannerNew1
        title={headerTitle}
        navList={[
          {
            text: headerTitle,
            link: cateIdMapping[cateId].navPath,
          },
          {
            text: article.title,
          },
        ]}
      />

      <div className={styles.contentContainer}>
        <h1>{article.title}</h1>
        <div className={styles.contentInfo}>
          {article.status && (
            <div className={styles.contehtStatus}>{`${article.status}`}</div>
          )}
          {showDateTitle ? (
            <div style={{ whiteSpace: "pre-line" }}>{`${t(
              "event.date.tittle"
            )}：${dateString}`}</div>
          ) : (
            <div style={{ whiteSpace: "pre-line" }}>{`${dateString}`}</div>
          )}
        </div>
        <div
          className={"article"}
          dangerouslySetInnerHTML={{ __html: content }}
        ></div>

        <div className={styles.social}>
          Share
          <a
            href={`https://www.facebook.com/sharer/sharer.php?u=${publicRuntimeConfig.shareBaseUrl}${router.asPath}`}
            target="_blank"
            rel="noreferrer"
          >
            <SVGScoialFB />
          </a>
          <a
            href={`https://twitter.com/intent/tweet?url=https://${publicRuntimeConfig.shareBaseUrl}${router.asPath}`}
            target="_blank"
            rel="noreferrer"
          >
            <SVGScoialTwitter />
          </a>
          {/* <a
            href="https://twitter.com/TAICCA_Official"
            target="_blank"
            rel="noreferrer"
          >
            <img src={"/images/linkedin.png"} />
          </a>
          <a
            href="https://twitter.com/TAICCA_Official"
            target="_blank"
            rel="noreferrer"
          >
            <img src={"/images/IG.png"} />
          </a> */}
        </div>
      </div>

      <Footer />
    </>
  );
}
// Content.headerProps = {
//   // red: true,
//   // title: "活動公告",
//   // numberText: "02",
// };
export async function getServerSideProps(context) {
  const locale = context.locale;
  const slug = context.query.slug;
  const cateId = context.cateId;

  if (!slug) {
    return {
      notFound: true,
    };
  }

  const apiFunc = () => {
    if (cateId === "02") {
      return getArticleContentv3;
    } else if (cateId === "05") {
      return getArticleContent001v3;
    }
  };

  const articleRes = await apiFunc()({
    locale,
    slug,
  });

  // console.log(articleRes.data);
  if (Array.isArray(articleRes.data) && articleRes.data[0] === null) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
      article: articleRes.data,
      cateId,
    }, // will be passed to the page component as props
  };
}
