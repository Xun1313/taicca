import { PageInfoBasic } from "@/components/PageInfo/PageInfo";
import React from "react";

import styles from "../styles/404.module.scss";
import { Footer } from "@/components/Footer/Footer";
import SVG404 from "@/svgs/404.svg";
import Link from "next/link";
import classNames from "classnames";
import { useRWD } from "@/hooks/useRwd";
import { HeaderBannerLight } from "@/components/HeaderBanner/HeaderBanner";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { TLink } from "@/components/TLink";
import { Trans, useTranslation } from 'next-i18next';

export default function Custom404() {
  const rwd = useRWD();
  const { t } = useTranslation("common");
  return (
    <>
      <HeaderBannerLight />
      <PageInfoBasic />
      <div className={"contentContainer"}>
        <section className={styles.section1}>
          <div>
            <SVG404 className={styles.joinBig} />
          </div>
          <div>
            <h1>
              <Trans i18nKey={t("error404.page.reminder")} />
            </h1>
            <div className={styles.desc}>
              <Trans i18nKey={"error404.page.instructions"}>
                <TLink href="/">TAICCA首頁</TLink>
                <TLink href="/contact">聯絡我們</TLink>
              </Trans>
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
}
Custom404.headerProps = {
  // red: true,
};
export async function getStaticProps(context) {
  return {
    props: {
      ...(await serverSideTranslations(context.locale, ["common"])),
    }, // will be passed to the page component as props
  };
}
