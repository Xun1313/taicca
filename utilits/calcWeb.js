export const calcWeb = (num) => {
  return `clamp(0px ,calc(${num} * 100vw / 1920), ${num}px)`;
};
export const calcWeb1440 = (num) => {
  return `clamp(0px ,calc(${num} * 100vw / 1440), ${num}px)`;
};

export const calcWeb1440Count = (num, windowSize) =>
  Math.min(num * (windowSize.width / 1440), num);
