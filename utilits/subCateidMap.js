// 001:新聞 002:趨勢報告 003:活動

export const subCateidMap = {
  "001": {
    numberText: "04",
    name: "news",
    titleKey: "news.headline",
    showDateTitle: false,
  },
  "002": {
    numberText: "04",
    name: "reports",
    titleKey: "reports.headline",
    showDateTitle: false,
  },
  "003": {
    numberText: "02",
    name: "events",
    titleKey: "event.headline",
    showDateTitle: true,
  },
};
