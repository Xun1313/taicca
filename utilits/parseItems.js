export const parseItems = (obj) => {
  // "industry": {
  //   "VisualArt": "視覺藝術",
  // }
  // to
  // [{ value: "VisualArt", label: "視覺藝術" }]
  const keys = Object.keys(obj).sort(
    (a, b) => parseInt(a, 10) - parseInt(b, 10)
  );

  return keys.reduce((acc, key) => {
    acc.push({
      label: obj[key],
      value: key,
    });
    return acc;
  }, []);
};

export const parseItems2 = (arr, sourceItem) => {
  // ["VisualArt"] -> [{ value: "VisualArt", label: "視覺藝術" }]
  if (arr) {
    return arr.map((e) => {
      return {
        value: e,
        label: sourceItem[e],
      };
    });
  } else {
    return [];
  }
};
