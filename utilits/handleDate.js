import moment from "moment";

export const getFullRangeText = (article) => {
  const start = article.date ? moment(article.date) : null;
  const end = article.dateEd ? moment(article.dateEd) : null;
  
  if(!start){
    return null
  }

  const full = end
    ? `${start.format("YYYY.M.DD")} - ${end.format("YYYY.M.DD")}`
    : start.format("YYYY.M.DD");

  return full;
};
export const getShortDateText = (article) => {
  const start = moment(article.date);

  return `${start.format("M/D")}`;
};
