import { LoadingContext } from "@/context/LoadingContext";
import { collectmJoin } from "@/api";
import { useMemo, useContext } from "react";

const useFavorite = ({
  tb,
  id,
  collectmMapping,
  setCollectmMapping,
  isLogin,
  authKey,
  memberId,
}) => {
  const { setShowLoading } = useContext(LoadingContext);

  const isFavorite = useMemo(
    () => collectmMapping[tb]?.includes(id + ""),
    [collectmMapping, tb, id]
  );

  const handleFavorite = async () => {
    if (!isLogin) return;

    setShowLoading(true);
    const { resu } = await collectmJoin({
      auth_key: authKey,
      memberm1_no: memberId,
      id: id,
      join: isFavorite ? 0 : 1,
      type: tb === "unit" ? 1 : 2,
    });

    if (resu === 1) {
      setCollectmMapping((prev) => {
        return {
          ...prev,
          [tb]: isFavorite
            ? prev[tb].filter((e) => e !== id + "")
            : [...prev[tb], id + ""],
        };
      });
    }
    setShowLoading(false);
  };

  return {
    isFavorite,
    handleFavorite,
  };
};

export default useFavorite;
