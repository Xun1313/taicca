import Axios from "axios";
import getConfig from "next/config";

const instance = Axios.create({
  baseURL:
    typeof window !== "undefined"
      ? ""
      : getConfig().publicRuntimeConfig.backendUrl,
  withCredentials: true,
});

export const axios = instance;
