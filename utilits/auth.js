import { parseCookies } from "@/utilits/parseCookies";

export const auth = (cookies) => {
  const redirect = {
    redirect: {
      destination: "/user/myAccount",
      permanent: false,
    },
  };
  try {
    return {
      isLogin: !!parseCookies(cookies, "taiccaAuth"),
      redirect,
    };
  } catch (error) {
    return {
      isLogin: false,
      redirect,
    };
  }
};
