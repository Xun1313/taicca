import { auth } from "./auth";
import { parseCookies } from "./parseCookies";

export const getLoginData = (context) => {
  const cookies = context.req.cookies;
  const { isLogin } = auth(cookies);

  let authKey = "";
  let memberId = "";

  if (!isLogin) {
    return {
      authKey,
      memberId,
      isLogin,
    };
  }

  try {
    const authData = JSON.parse(parseCookies(cookies, "taiccaAuth"));
    authKey = authData.auth_key;
    memberId = authData.id;
  } catch (error) {}

  return {
    authKey,
    memberId,
    isLogin,
  };
};
