import { getArticlev3 } from "@/api";
import { useRouter } from "next/router";
import { useCallback, useRef, useState, useEffect } from "react";
import { useAsyncFn, usePrevious, useUpdateEffect } from "react-use";

export const useLoadMoreApi = ({
  initPage,
  initAllPage,
  initData,
  pageSize,
  subCateid,
  otherQuery = {},
}) => {
  const otherQueryRef = useRef(otherQuery);
  otherQueryRef.current = otherQuery;
  const { locale } = useRouter();
  const [state, setState] = useState({
    data: initData,
    page: initPage,
    allPage: initAllPage,
  });

  const [apiState, fetch] = useAsyncFn(async (page) => {
    const res = await getArticlev3({
      subCateid: subCateid,
      page: page,
      locale,
      pagesize: pageSize,
      ispages: 1,
      ...otherQueryRef.current,
    });

    // console.log(res);

    setState((prev) => ({
      data: [...prev.data, ...res.data],
      page: page,
      allPage: res.pages.totalPages,
    }));
  }, []);

  const handleNext = useCallback(
    (val) => {
      const currentPage = state.page;
      setState((prev) => ({
        ...prev,
        page: currentPage + 1,
      }));
      fetch(currentPage + 1);
    },
    [fetch, state.page]
  );

  const handleQueryChange = useCallback(() => {
    setState({
      data: [],
      page: 1,
      allPage: 1,
    });

    fetch(1);
  }, [fetch]);

  return {
    handleNext,
    showLoadMore: state.page < state.allPage,
    currentPage: state.page,
    allPage: state.allPage,
    data: state.data,
    handleQueryChange,
    isLoading: apiState.loading,
  };
};

export const useLoadMoreApiCommon = ({
  initPage,
  initAllPage,
  initData,
  initCollectm,
  pageSize,
  otherQuery = {},
  api,
  initCount,
}) => {
  const otherQueryRef = useRef(otherQuery);
  otherQueryRef.current = otherQuery;
  const { locale } = useRouter();
  const [state, setState] = useState({
    collectm: initCollectm,
    data: initData,
    page: initPage,
    allPage: initAllPage,
    count: initCount,
  });

  const [apiState, fetch] = useAsyncFn(async (page) => {
    const res = await api({
      page: page,
      locale,
      pagesize: pageSize,
      ...otherQueryRef.current,
    });


    setState((prev) => ({
      data: [...prev.data, ...res.data],
      page: page,
      allPage: res.pages.totalPages,
      count: res.pages.totalCount,
      collectm: res.collectm
    }));
  }, []);

  const handleNext = useCallback(
    (val) => {
      const currentPage = state.page;
      setState((prev) => ({
        ...prev,
        page: currentPage + 1,
      }));
      fetch(currentPage + 1);
    },
    [fetch, state.page]
  );

  const handleQueryChange = useCallback(() => {
    setState({
      data: [],
      page: 1,
      allPage: 1,
      count: 0,
    });

    fetch(1);
  }, [fetch]);

  const handleFavoriteChange = useCallback((func) => {
    return setState((prev) => {
      return {
        ...prev,
        collectm: func(prev.collectm),
      };
    });
  }, []);

  const prevQuery = usePrevious(otherQuery);

  useEffect(() => {
    if (typeof prevQuery !== "undefined" && prevQuery !== otherQuery) {
      handleQueryChange();
    }
  }, [otherQuery]);
  return {
    handleNext,
    showLoadMore: state.page < state.allPage,
    currentPage: state.page,
    allPage: state.allPage,
    data: state.data,
    handleQueryChange,
    handleFavoriteChange,
    isLoading: apiState.loading,
    count: state.count,
    collectm: state.collectm
  };
};

export const useLoadMoreApi2 = ({
  initPage,
  initAllPage,
  initData,
  initTotalCount,
  api,
}) => {
  const [state, setState] = useState({
    data: initData,
    page: initPage,
    totalCount: initTotalCount,
    allPage: initAllPage,
  });

  const [apiState, fetch] = useAsyncFn(
    async (page) => {
      const res = await api({
        page: page,
      });

      setState((prev) => ({
        data: [...prev.data, ...res.data],
        page: page,
        allPage: res.pages.totalPages,
        totalCount: res.pages.totalCount,
      }));
    },
    [api]
  );

  const handleNext = useCallback(
    (val) => {
      const currentPage = state.page;
      setState((prev) => ({
        ...prev,
        page: currentPage + 1,
      }));
      fetch(currentPage + 1);
    },
    [fetch, state.page]
  );

  useUpdateEffect(() => {
    setState({
      data: [],
      page: 0,
      totalCount: 0,
      allPage: 0,
    });

    fetch(1);
  }, [api]);

  return {
    handleNext,
    showLoadMore: state.page < state.allPage,
    currentPage: state.page,
    allPage: state.allPage,
    data: state.data,
    totalCount: state.totalCount,
    isLoading: apiState.loading,
  };
};
