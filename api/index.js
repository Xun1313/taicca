import { axios } from "@/utilits/axios";

export const getBanner = ({ locale = "zh" }) =>
  axios.post("/apiv3/banner/homeindex", { lang: locale }).then(({ data }) => data);

// 001:新聞 002:趨勢報告 003:活動
export const getArticales = ({
  locale = "zh",
  subCateid = "001",
  page,
  ...rest
}) => {
  return axios
    .post(
      "/api/article/index",
      { lang: locale, subCateid, ...rest },
      { params: { ...(typeof page !== "undefined" ? { page } : {}) } }
    )
    .then(({ data }) => data);
};

export const getFormItems = ({ locale = "en" }) => {
  return axios
    .post("/apiv2/member/item", { lang: locale })
    .then(({ data }) => data.data);
};

export const getMembers = ({ locale = "en" }) => {
  return axios
    .post("/apiv2/member/index", { lang: locale })
    .then(({ data }) => data.data);
};

export const getArticalesSoon = ({
  locale = "zh",
  subCateid = "001",
  date,
}) => {
  return axios
    .post("/api/article/soon", { lang: locale, subCateid, date })
    .then(({ data }) => data);
};

export const getArticalesHot = ({ locale = "zh", subCateid = "001" }) => {
  return axios
    .post("/api/article/hot", { lang: locale, subCateid })
    .then(({ data }) => data);
};

export const getArticle = ({ locale, slug }) => {
  return axios
    .post("/api/article/content", { lang: locale, slug })
    .then(({ data }) => data);
};

export const insertMember = (data) => {
  const formData = new FormData();

  const keys = Object.keys(data);

  keys.forEach((key) => {
    if (Array.isArray(data[key])) {
      formData.append(key, data[key].join(","));
    } else if (typeof data[key] === "boolean") {
      formData.append(key, data[key] ? 1 : 0);
    } else if (typeof data[key] === "object" && data[key].file) {
      formData.append(key, data[key].file.originFileObj);
    } else {
      if (data[key]) {
        formData.append(key, data[key]);
      }
    }
  });

  return axios.post("/api/member/insert", formData).then(({ data }) => data);
};

export const sendContact = (data, lang) => {
  return axios
    .post("/api/contact/index", { ...data, lang })
    .then(({ data }) => data);
};

export const searchApi = ({ text, lang, page, ...rest }) => {
  return axios
    .post(
      "/apiv3/search/index",
      { text, lang, ...rest },
      { params: { ...(typeof page !== "undefined" ? { page } : {}) } }
    )
    .then(({ data }) => data);
};

export const mailConfirm = ({ mail, token }) => {
  return axios
    .post("/api/member/mailconfirm", { mail, token })
    .then(({ data }) => data);
};

export const getSourceItem = (lang) => {
  return axios.post("/apiv2/source/item", { lang }).then(({ data }) => data);
};

export const getPageData = ({ type, lang }) => {
  return axios
    .post("/apiv2/pages/index", { lang, type })
    .then(({ data }) => data.data);
};

export const getSsoursphere = (lang) => {
  return axios
    .post("/apiv2/soursphere/index", { lang })
    .then(({ data }) => data);
};

export const getSsoursphereV3 = (lang) => {
  return axios
    .post("/apiv3/soursphere/index", { lang })
    .then(({ data }) => data);
};

export const getSoureces = ({
  page,
  locale,
  nation,
  category,
  sphere,
  ...rest
}) => {
  return axios
    .post(
      "/apiv2/source/index",
      {
        lang: locale,
        ...rest,
        ...(nation && nation.length !== 0 ? { nation: nation.join(",") } : {}),
        ...(category ? { category: category.join(",") } : {}),
        ...(sphere ? { sphere: sphere.join(",") } : {}),
      },
      { params: { ...(typeof page !== "undefined" ? { page } : {}) } }
    )
    .then(({ data }) => data);
};

export const getCountry = ({ locale }) => {
  return axios
    .post("/apiv2/member/country", {
      lang: locale,
    })
    .then(({ data }) => data);
};

export const login = ({ username, password, locale }) => {
  return axios
    .post("/apiv3/memberm/login", {
      username,
      password,
      lang: locale,
    })
    .then(({ data }) => data);
};

export const logout = () => {
  return axios.post("/apiv3/memberm/logout").then(({ data }) => data);
};

export const register = ({
  email,
  password,
  confirmpassword,
  name_zh,
  name_en,
  lastname_en,
  identity,
  locale,
}) => {
  return axios
    .post("/apiv3/memberm/register", {
      email,
      password,
      confirmpassword,
      name_zh,
      name_en,
      lastname_en,
      identity,
      lang: locale,
    })
    .then(({ data }) => data);
};

export const memberVerify = ({ mail, token }) => {
  return axios
    .post("/apiv3/memberm/verify", {
      mail,
      token,
    })
    .then(({ data }) => data);
};

export const memberForget = ({ username }) => {
  return axios
    .post("/apiv3/memberm/forget", {
      username,
    })
    .then(({ data }) => data);
};

export const memberModify = ({ ...data }) => {
  return axios
    .post("/apiv3/memberm/modify", {
      ...data,
    })
    .then(({ data }) => data);
};

export const memberResetpwd = ({ ...data }) => {
  return axios
    .post("/apiv3/memberm/resetpwd", {
      ...data,
    })
    .then(({ data }) => data);
};

export const memberReemail = ({ ...data }) => {
  return axios
    .post("/apiv3/memberm/reemail", {
      ...data,
    })
    .then(({ data }) => data);
};

export const sourceItem = ({ locale }) => {
  return axios
    .post("/apiv3/source/item", {
      lang: locale,
    })
    .then(({ data }) => data);
};

export const sourceIndex = ({ ...data }) => {
  return axios
    .post("/apiv3/source/index", {
      ...data
    })
    .then(({ data }) => data);
};

export const sourceIndexV3 = ({
  page,
  locale,
  nation,
  category,
  sphere,
  industry,
  ...rest
}) => {
  return axios
    .post(
      "/apiv3/source/index",
      {
        lang: locale,
        ...rest,
        ...(nation && nation.length !== 0 ? { nation: nation.join(",") } : {}),
        ...(category ? { category: category.join(",") } : {}),
        ...(sphere ? { sphere: sphere.join(",") } : {}),
        ...(industry ? { industry: industry.join(",") } : {}),
      },
      { params: { ...(typeof page !== "undefined" ? { page } : {}) } }
    )
    .then(({ data }) => data);
};

export const soursphereIndex = ({ page, lang }) => {
  return axios
    .post("/api/soursphere/index", {
      page,
      lang,
    })
    .then(({ data }) => data);
};

export const sourceItemSource = () => {
  return axios.post("/apiv3/source/item-source", {}).then(({ data }) => data);
};

export const unitList = ({ auth_key, memberm1_no, unit, type }) => {
  return axios
    .post("/apiv3/unit/list", {
      auth_key,
      memberm1_no,
      unit,
      type,
    })
    .then(({ data }) => data);
};

export const unitNew = ({ auth_key, memberm1_no, ...rest }) => {
  return axios
    .post("/apiv3/unit/new", {
      auth_key,
      memberm1_no,
      ...rest,
    })
    .then(({ data }) => data);
};

export const unitOne = ({ auth_key, memberm1_no, Unitm1_no }) => {
  return axios
    .post("/apiv3/unit/one", {
      auth_key,
      memberm1_no,
      Unitm1_no,
    })
    .then(({ data }) => data);
};

export const unitIsshow = ({ auth_key, memberm1_no, Unitm1_no, isshow }) => {
  return axios
    .post("/apiv3/unit/isshow", {
      auth_key,
      memberm1_no,
      Unitm1_no,
      isshow,
    })
    .then(({ data }) => data);
};

export const unitDelete = ({ auth_key, memberm1_no, Unitm1_no }) => {
  return axios
    .post("/apiv3/unit/delete", {
      auth_key,
      memberm1_no,
      Unitm1_no,
    })
    .then(({ data }) => data);
};

export const unitModify = ({ auth_key, memberm1_no, Unitm1_no, ...data }) => {
  return axios
    .post("/apiv3/unit/modify", {
      auth_key,
      memberm1_no,
      Unitm1_no,
      ...data,
    })
    .then(({ data }) => data);
};

export const unitModify2 = ({ auth_key, memberm1_no, Unitm1_no, ...data }) => {
  return axios
    .post("/apiv3/unit/modify2", {
      auth_key,
      memberm1_no,
      Unitm1_no,
      ...data,
    })
    .then(({ data }) => data);
};

export const unitAddpartner = ({
  auth_key,
  memberm1_no,
  Unitm1_no,
  id,
  step3,
}) => {
  return axios
    .post("/apiv3/unit/addpartner", {
      auth_key,
      memberm1_no,
      Unitm1_no,
      id,
      step3,
    })
    .then(({ data }) => data);
};

export const unitWorks = ({ auth_key, memberm1_no, Unitm1_no }) => {
  return axios
    .post("/apiv3/unit/works", {
      auth_key,
      memberm1_no,
      Unitm1_no,
    })
    .then(({ data }) => data);
};

export const unitPickworks = ({
  auth_key,
  memberm1_no,
  Unitm1_no,
  workm1_no,
  review,
  step4,
}) => {
  return axios
    .post("/apiv3/unit/pickworks", {
      auth_key,
      memberm1_no,
      Unitm1_no,
      workm1_no,
      step4,
      review,
    })
    .then(({ data }) => data);
};

export const accWorksNew = ({ auth_key, memberm1_no, name_zh, name_en }) => {
  return axios
    .post("/apiv3/acc-works/new", {
      auth_key,
      memberm1_no,
      name_zh,
      name_en,
    })
    .then(({ data }) => data);
};

export const accWorksList = ({ auth_key, memberm1_no }) => {
  return axios
    .post("/apiv3/acc-works/list", {
      auth_key,
      memberm1_no,
    })
    .then(({ data }) => data);
};

export const accWorksRename = ({
  auth_key,
  memberm1_no,
  workm1_no,
  name_en,
  name_zh,
}) => {
  return axios
    .post("/apiv3/acc-works/rename", {
      auth_key,
      memberm1_no,
      workm1_no,
      name_en,
      name_zh,
    })
    .then(({ data }) => data);
};

export const accWorksIsshow = ({
  auth_key,
  memberm1_no,
  workm1_no,
  isshow,
}) => {
  return axios
    .post("/apiv3/acc-works/isshow", {
      auth_key,
      memberm1_no,
      workm1_no,
      isshow,
    })
    .then(({ data }) => data);
};

export const accWorksDelete = ({ auth_key, memberm1_no, workm1_no }) => {
  return axios
    .post("/apiv3/acc-works/delete", {
      auth_key,
      memberm1_no,
      workm1_no,
    })
    .then(({ data }) => data);
};

export const accWorksOne = ({ auth_key, memberm1_no, workm1_no }) => {
  return axios
    .post("/apiv3/acc-works/one", {
      auth_key,
      memberm1_no,
      workm1_no,
    })
    .then(({ data }) => data);
};

export const accWorksModifyst2 = ({
  auth_key,
  memberm1_no,
  workm1_no,
  ...data
}) => {
  return axios
    .post("/apiv3/acc-works/modifyst2", {
      auth_key,
      memberm1_no,
      workm1_no,
      ...data,
    })
    .then(({ data }) => data);
};

export const accWorksModifyst3 = ({
  auth_key,
  memberm1_no,
  workm1_no,
  ...data
}) => {
  return axios
    .post("/apiv3/acc-works/modifyst3", {
      auth_key,
      memberm1_no,
      workm1_no,
      ...data,
    })
    .then(({ data }) => data);
};

export const accWorksModifyst4 = ({ ...data }) => {
  const formData = new FormData();

  for (const key in data) {
    if (Array.isArray(data[key])) {
      data[key].forEach((e, i) => {
        for (const key2 in e) {
          formData.append(`${key}[${i}][${key2}]`, e[key2]);
        }
      });
    } else {
      formData.append(key, data[key]);
    }
  }

  return axios
    .post("/apiv3/acc-works/modifyst4", formData)
    .then(({ data }) => data);
};

export const accWorksModifyst5 = ({
  auth_key,
  memberm1_no,
  workm1_no,
  ...data
}) => {
  return axios
    .post("/apiv3/acc-works/modifyst5", {
      auth_key,
      memberm1_no,
      workm1_no,
      ...data,
    })
    .then(({ data }) => data);
};

export const getArticlev3 = ({ locale = "zh", page, ...rest }) => {
  return axios
    .post(
      "/apiv3/article/index",
      { lang: locale, ...rest },
      { params: { ...(typeof page !== "undefined" ? { page } : {}) } }
    )
    .then(({ data }) => data);
};

export const getArticleContentv3 = ({ locale, slug }) => {
  return axios
    .post("/apiv3/article/content", { lang: locale, slug })
    .then(({ data }) => data);
};

export const getArticle001Hotv3 = ({ locale }) => {
  return axios
    .post("/apiv3/article001/hot", { lang: locale })
    .then(({ data }) => data);
};

export const getArticle001v3 = ({ locale = "zh", page, ...rest }) => {
  return axios
    .post(
      "/apiv3/article001/index",
      { lang: locale, ...rest },
      { params: { ...(typeof page !== "undefined" ? { page } : {}) } }
    )
    .then(({ data }) => data);
};

export const getArticleContent001v3 = ({ locale, slug }) => {
  return axios
    .post("/apiv3/article001/content", { lang: locale, slug })
    .then(({ data }) => data);
};

export const getMemberWork = ({ page, locale, ...rest }) => {
  return axios
    .post(
      "/apiv3/member/work",
      {
        lang: locale,
        ...rest,
      },
      { params: { ...(typeof page !== "undefined" ? { page } : {}) } }
    )
    .then(({ data }) => data);
};

export const sourceItemFeature = () => {
  return axios.post("/apiv3/source/item-feature").then(({ data }) => data);
};

export const getMembersV3 = ({ locale = "en", id }) => {
  return axios
    .post("/apiv3/member/unit", {
      lang: locale,
      memberm1_no: 5,
      auth_key: "3Pd4FmNnHW9mo_2KRCplmO0E-XjBkXTE",
    })
    .then(({ data }) => data.data);
};

export const getMemberV3 = ({
  locale = "en",
  id,
  memberm1_no,
  auth_key,
  ispublic,
}) => {
  return axios
    .post("/apiv3/member/unit-one", {
      lang: locale,
      memberm1_no,
      auth_key,
      id,
      ispublic,
    })
    .then(({ data }) => data.data);
};

export const getSourceItemV3 = ({ locale }) => {
  return axios
    .post("/apiv3/source/item", { lang: locale })
    .then(({ data }) => data);
};

export const collectmJoin = ({ memberm1_no, auth_key, type, id, join }) => {
  return axios
    .post("/apiv3/collectm/join", { memberm1_no, auth_key, type, id, join })
    .then(({ data }) => data);
};

export const matchIndex = ({ memberm1_no, auth_key }) => {
  return axios
    .post("/apiv3/match/index", { memberm1_no, auth_key })
    .then(({ data }) => data);
};

export const matchModify = ({ memberm1_no, auth_key, ...data }) => {
  return axios
    .post("/apiv3/match/modify", { memberm1_no, auth_key, ...data })
    .then(({ data }) => data);
};

export const sourceMatch = ({ lang }) => {
  return axios.post("/apiv3/source/match", { lang }).then(({ data }) => data);
};

export const collectmIndex = ({ memberm1_no, auth_key, ...data }) => {
  return axios
    .post("/apiv3/collectm/index", { memberm1_no, auth_key, ...data })
    .then(({ data }) => data);
};

export const getWorkV3 = ({
  locale = "en",
  id,
  memberm1_no,
  auth_key,
  ispublic,
}) => {
  return axios
    .post("/apiv3/member/work-one", {
      lang: locale,
      id,
      memberm1_no,
      auth_key,
      ispublic,
    })
    .then(({ data }) => data);
};

export const getCountryV3 = (lang) => {
  return axios.post("/apiv3/member/country", { lang }).then(({ data }) => data);
};

export const memNewsIndex = ({ memberm1_no, auth_key }) => {
  return axios
    .post("/apiv3/mem-news/index", { memberm1_no, auth_key })
    .then(({ data }) => data);
};

export const memNewsAdd = ({ memberm1_no, auth_key, ...data }) => {
  return axios
    .post("/apiv3/mem-news/add", { memberm1_no, auth_key, ...data })
    .then(({ data }) => data);
};

export const planIndex = ({ locale = "zh", page, ...rest }) => {
  return axios
    .post(
      "/apiv3/plan/index",
      { lang: locale, ...rest },
      { params: { ...(typeof page !== "undefined" ? { page } : {}) } }
    )
    .then(({ data }) => data);
};

export const planContent = ({ locale, slug }) => {
  return axios
    .post("/apiv3/plan/content", { lang: locale, slug })
    .then(({ data }) => data);
};

export const memberDataCount = () => {
  return axios.post("/apiv3/member/data-count").then(({ data }) => data);
};

export const themeIndex = ({ ...data }) => {
  return axios.post("/apiv3/theme/index", data).then(({ data }) => data);
};

export const themeContent = ({ ...data }) => {
  return axios.post("/apiv3/theme/content", data).then(({ data }) => data);
};

export const unitReview = ({ ...data }) => {
  return axios.post("/apiv3/unit/review", data).then(({ data }) => data);
};

export const accWorksReview = ({ ...data }) => {
  return axios.post("/apiv3/acc-works/review", data).then(({ data }) => data);
};

export const planJoin = ({ ...data }) => {
  return axios.post("/apiv3/plan/join", data).then(({ data }) => data);
};

export const planAgree = ({ ...data }) => {
  return axios.post("/apiv3/plan/agree", data).then(({ data }) => data);
};

export const memberLiveEvent = ({ ...data }) => {
  return axios.post("/apiv3/member/live-event", data).then(({ data }) => data);
};

export const memberTheme = ({ ...data }) => {
  return axios.post("/apiv3/member/theme", data).then(({ data }) => data);
};

export const liveEventIndex = ({ ...data }) => {
  return axios.post("/apiv3/live-event/index", data).then(({ data }) => data);
};

export const liveEventAdd = ({ ...data }) => {
  return axios.post("/apiv3/live-event/add", data).then(({ data }) => data);
};

export const memberWorkDownload = ({ ...data }) => {
  return axios.post("/apiv3/member/work-download", data).then(({ data }) => data);
};

export const planJoinMember = ({ ...data }) => {
  return axios.post("/apiv3/plan/join-member", data).then(({ data }) => data);
};