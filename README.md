## 簡易部署方式

1.確認環境包含 node / yarn

2.安裝套件 (本機端/server 端都要)

```
yarn
```

3.build (本機端)

```
npm run build
```

4.run (本機端/sever 端)

```
npm run start
```

成功後可以測試 http://localhost:3000/

本機端測試成功，則可將.next 資料夾搬移到 sever 上，並在 server 上執行步驟 2 跟 4

## PM2 部署

1.安裝 pm2

```
yarn global add pm2
```

2.使用 pm2 啟動 next

```
pm2 start npm --name next -- run start
```

3.檢查狀態

```
pm2 list
```

## 檔案更新

＊正式機路徑：/srv/future.taicca.tw/taicca

1.更新原始碼 (server 及本地端都要)

```
git pull
```

2.於本地端 build

```
npm run build
```

3.搬移.next 資料夾到 server

3.server pm2 重啟

```
pm2 restart next
```

## 修改 api 位置

打改 next.config.js
修改 backendUrl
