export const GA_TRACKING_ID = 'G-4C0T0ZE3FN'
export const GA_TRACKING_ID_2 = 'UA-205963181-1'
export const GA_TRACKING_ID_3 = 'G-0YE719EXDC'

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export const pageview = (url) => {
  window.gtag('config', GA_TRACKING_ID, {
    page_path: url,
  })
}

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const event = ({ action, category, label, value }) => {
  window.gtag('event', action, {
    event_category: category,
    event_label: label,
    value: value,
  })
}