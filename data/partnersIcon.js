import SVGIcon01 from "@/svgs/partners/icon01.svg";
import SVGIcon02 from "@/svgs/partners/icon02.svg";
import SVGIcon03 from "@/svgs/partners/icon03.svg";
import SVGIcon04 from "@/svgs/partners/icon04.svg";
import SVGIcon05 from "@/svgs/partners/icon05.svg";

export const partnersIcons = {
  icon01: SVGIcon01,
  icon02: SVGIcon02,
  icon03: SVGIcon03,
  icon04: SVGIcon04,
  icon05: SVGIcon05,
};
