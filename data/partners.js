export const categorys = [
  {
    title: "professionals.looking.content.tittle1",
    color: "#E53C2B",
    text: `professionals.looking.content.tittle1.content`,
    key: "創作方",
  },
  {
    title: "professionals.looking.content.tittle2",
    color: "#1D8B63",
    text: `professionals.looking.content.tittle2.content`,
    key: "技術方",
  },
  {
    title: "professionals.looking.content.tittle3",
    color: "#55ACCD",
    text: `professionals.looking.content.tittle3.content`,
    key: "場域展會",
  },
  {
    title: "professionals.looking.content.tittle4",
    color: "#EBC945",
    text: `professionals.looking.content.tittle4.content`,
    key: "組織",
  },
  {
    title: "professionals.looking.content.tittle5",
    color: "#DA57D8",
    text: `professionals.looking.content.tittle5.content`,
    key: "新創投資",
  },
];
