export const places = [
  {
    title: "map.continents.list.1",
    key: "Asia",
    coordinates: [25.068769426969883, 121.57983521712785],
    // coordinates: [31.9679992, 91.167997],
  },
  {
    title: "map.continents.list.2",
    key: "Europe",
    coordinates: [48.1284101, 4.1482421],
  },
  {
    title: "map.continents.list.3",
    key: "North America",
    coordinates: [40.873978, -103.833818],
  },
  {
    title: "map.continents.list.4",
    key: "South America",
    coordinates: [-14.999788, -59.925606],
  },
  {
    title: "map.continents.list.5",
    key: "Oceania",
    coordinates: [-23.190695, 133.411514],
  },
];
