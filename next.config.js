const withLess = require("next-with-less");
const { i18n } = require("./next-i18next.config");

const apiBaseUrl = process.env.API_BASE_URL || "https://taicca.hantw.tw";
const baseUrl = process.env.BASE_URL || "https://taicca.felixchen.dev";
const isTestingMachine = !!process.env.TESTING_MACHINE || false;

console.log({
  apiBaseUrl,
  baseUrl,
  isTestingMachine
});

module.exports = withLess({
  i18n,
  publicRuntimeConfig: {
    backendUrl: apiBaseUrl,
    shareBaseUrl: baseUrl,
    isTestingMachine: isTestingMachine,
  },
  headers: async () => [
    {
      source: '/(.*)',
      headers: [
        // {
        //   key: 'Content-Security-Policy',
        //   value: "policy",
        // },
        {
          key: 'X-Frame-Options',
          value: "DENY",
        },
      ],
    },
  ],
  async rewrites() {
    return [
      {
        source: "/api/:path*",
        destination: `${apiBaseUrl}/api/:path*`, // Proxy to Backend
      },
      {
        source: "/apiv2/:path*",
        destination: `${apiBaseUrl}/apiv2/:path*`, // Proxy to Backend
      },
      {
        source: "/apiv3/:path*",
        destination: `${apiBaseUrl}/apiv3/:path*`, // Proxy to Backend
      },
      {
        source: "/uploads/:path*",
        destination: `${apiBaseUrl}/uploads/:path*`, // Proxy to Backend
      },
    ];
  },
  experimental: { scrollRestoration: false },

  reactStrictMode: true,
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"],
    });

    return config;
  },
  transpilePackages: [ "antd", "@ant-design", "rc-util", "rc-pagination", "rc-picker", "rc-notification", "rc-tooltip", "rc-tree", "rc-table" ],

  lessLoaderOptions: {
    /* ... */

    /* ... */
    lessOptions: {
      /* ... */
      modifyVars: {
        "primary-color": "#353435",
        // "primary-color": "#E63C2B",
        // "primary-color-hover": "#353435",
        // "primary-color-outline": "#353435",
        "error-color": "#e63c2b",
        "primary-5": "#adadad",
        "outline-color": "#d9d9d9",
        "input-hover-border-color": "#d9d9d9",
        "control-padding-horizontal": "17px",
        "font-family": `'Microsoft JhengHei','Heiti TC', 'PingFang TC','Apple LiGothic', 'AppleGothic', 
        'Noto Sans TC', 'Noto Sans CJK TC', 'WenQuanYi Micro Hei', 'SimHei', -apple-system, BlinkMacSystemFont, 
        'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol',
        'Noto Color Emoji';`,
        "border-radius-base": "25px",
        "font-size-base": "15px",
        "border-color-base": "#F2F2F2",
        "border-width-base": "2px",
        "heading-color": "unset",
        "height-base": "50px",
        "checkbox-color": "#E63C2B",
        "radio-dot-color": "#E63C2B",
        "checkbox-size": "25px",
        "radio-size": "25px",
        /* ... */
      },
    },
  },
  sassOptions: {
    // includePaths: [path.join(__dirname, "styles")],
    prependData: `
    @use "sass:string";
    @use "sass:math";


    
    @function strip-unit($number) {
      @if type-of($number) == 'number' and not unitless($number) {
        @return math.div($number, ($number * 0 + 1));
      }
    
      @return $number;
    }

    @function calc-web($num) {
      $num-without-px: strip-unit($num);
      @return clamp(0px ,calc(#{$num-without-px} *  100vw / 1920), #{$num});
    }

    @function calc-web-1440($num) {
      $num-without-px: strip-unit($num);
      @return clamp(0px ,calc(#{$num-without-px} *  100vw / 1440), #{$num});
    }


    @function calc-web-n($num) {
      $num-without-px: strip-unit($num);
      @return clamp(#{$num} ,calc(#{$num-without-px} *  100vw / 1920), 5000px);
    }

    @function calc-web-o($num) {
      $num-without-px: strip-unit($num);
      @return calc(#{$num-without-px} * 100vw / 1920);
    }

    @function calc-web-1440-o($num) {
      $num-without-px: strip-unit($num);
      @return calc(#{$num-without-px} * 100vw / 1440);
    }
    
    $mobile-media: 959px;
    $mobile-media-m: 960px;
    $pad-landscape: 1024px;
    $pad-portrait: 768px;

    @mixin pad-landscape() {
      @media screen and (min-device-width : $pad-portrait) and (max-device-width : $pad-landscape) and (orientation : landscape) {
        @content;
      }
    }
    @mixin pad-portrait() {
      @media screen and (min-device-width : $pad-portrait) and (max-device-width : $pad-landscape) and (orientation : portrait) {
        @content;
      }
    }

    @mixin mobile-only() {
      @media all and (max-width: $mobile-media) {
          @content;
      }
    }

    @mixin not-mobile() {
      @media all and (min-width: $mobile-media-m) {
          @content;
      }
    }

    @mixin not-mobile-u1440() {
      @media all and (min-width: $mobile-media-m) and (max-width: 1440px) {
          @content;
      }
    }



    @mixin fix-desktop-margin($name) {
      #{$name}: calc(50vw - 655px);
      @include not-mobile-u1440 {
        #{$name}: calc-web-1440(62.703px);
      }
    }

    $main-color: #E53C2B;
    $base-margin: 58px;
    `,
  },
});
