const fs = require("fs");

const enRaw = require("./public/locales/en.json");

const zhRaw = require("./public/locales/zh.json");

const handler = (raw, name) => {
  const keys = Object.keys(raw);

  const mergeStrings = keys.reduce((acc, val) => {
    acc = { ...acc, ...raw[val] };
    return acc;
  }, {});

  // const destructKeys = Object.keys(mergeStrings).reduce((acc, key) => {
  //   const path = key.split(".");

  //   let cutsor = acc;
  //   path.forEach((pathKey, index) => {
  //     if (index === path.length - 1) {
  //       cutsor[pathKey] = mergeStrings[key];
  //     } else {
  //       if (!cutsor[pathKey]) {
  //         cutsor[pathKey] = {};
  //       }
  //       cutsor = cutsor[pathKey];
  //     }
  //   });

  //   return acc;
  // }, {});
  fs.writeFileSync(
    `public/locales/${name}/common.json`,
    JSON.stringify(mergeStrings, null, 2)
  );
};

handler(enRaw, "en");
handler(zhRaw, "zh");
