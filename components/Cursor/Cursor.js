import { useEffect, useState, useRef, useContext } from "react";
import styles from "./Cursor.module.scss";
import { useSpring } from "@react-spring/core";
import { animated } from "@react-spring/web";
import classNames from "classnames";
import { CursorStateContext } from "@/context/CursorStateContext";
import { Router } from "next/router";

export const Cursor = () => {
  const cursorState = useContext(CursorStateContext);
  const [style, api] = useSpring(() => ({
    x: 100,
    y: 100,
    config: {
      mass: 5,
      friction: 40,
      tension: 150,
    },
  }));

  // const [isHover, setIsHover] = useState("");
  // const isHeverRef = useRef(isHover);
  // isHeverRef.current = isHover;

  useEffect(() => {
    const handler = (e) => {
      // console.log(e.target);
      // if (
      //   e.target.classList.contains("button") &&
      //   isHeverRef.current !== "button"
      // ) {
      //   setIsHover("button");
      // }
      // if (
      //   !e.target.classList.contains("button") &&
      //   isHeverRef.current == "button"
      // ) {
      //   setIsHover("");
      // }

      // if (
      //   e.target.classList.contains("cursor-video") &&
      //   isHeverRef.current !== "video"
      // ) {
      //   setIsHover("video");
      // }
      // if (
      //   !e.target.classList.contains("cursor-video") &&
      //   isHeverRef.current == "video"
      // ) {
      //   setIsHover("");
      // }

      // console.log(e);
      api.start({
        to: {
          x: e.clientX,
          y: e.clientY,
        },
      });
    };

    window.addEventListener("mousemove", handler);
    const handleRouterChangeComplete = () => {
      cursorState.setState("");
    };
    const routerListener = Router.events.on(
      "routeChangeComplete",
      handleRouterChangeComplete
    );

    return () => {
      window.removeEventListener("mousemove", handler);
      Router.events.off("routeChangeComplete", handleRouterChangeComplete);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <animated.div
      style={style}
      className={classNames(styles.cursor, {
        [styles.hover]: cursorState.state === "button",
        [styles.video]: cursorState.state === "video",
        [styles.next]: cursorState.state === "next",
        [styles.link]: cursorState.state === "link",
        [styles.fancyButton]: cursorState.state === "fancyButton",
      })}
    ></animated.div>
  );
};
