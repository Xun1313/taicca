import classNames from "classnames";
import styles from "./FormSuccess.module.scss";
import SBGSuccess from "@/svgs/joinSuccess.svg";

export const FormSuccess = ({ title, desc }) => {
  return (
    <section
      className={classNames("basic-section reverse", styles.sectionSeccuess)}
    >
      <div>
        <SBGSuccess className={styles.successIcon} />
      </div>
      <div>
        <h3>{title}</h3>
        <div className={classNames("desc", styles.desc)}>
          {desc}
        </div>
      </div>
    </section>
  );
};