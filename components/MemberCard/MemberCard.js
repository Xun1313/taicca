import React from "react";
import styles from "./MemberCard.module.scss";
import FavoriteIcon from "../FavoriteIcon/FavoriteIcon";

export default function MemberCard({
  pic,
  nameEng,
  nameZh,
  showFavorite,
  favoriteTooltip,
  onFavorite,
  isFavorite,
  unitText,
  isLogin
}) {
  return (
    <div className={styles.container}>
      <img src={pic || "/images/memberBlank.png"} className={styles.pic} alt="icon" />
      <div>
        <div className={styles.title}>{unitText}</div>
        <div className={styles.name}>
          <span className={styles.eng}>{nameEng}</span> {nameZh}
        </div>
      </div>
      {showFavorite && (
        <FavoriteIcon
          tooltip={favoriteTooltip}
          className={styles.favoriteIcon}
          onClick={onFavorite}
          isFavorite={isFavorite}
          isFavoriteColor="#444444"
          isLogin={isLogin}
        />
      )}
    </div>
  );
}
