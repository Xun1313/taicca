import classNames from "classnames";
import styles from "./CommonSwiperList.module.scss";
import { useRWD } from "@/hooks/useRwd";
import { Navigation, Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

const breakpointsDefault = {
  960: { slidesPerView: 3, spaceBetween: "3.1%" },
};

export const CommonSwiperList = ({
  prefix = "default",
  dataList,
  Element,
  slidesPerView = 1,
  slidesPerGroup = 1,
  spaceBetween = 0,
  breakpoints = breakpointsDefault,
  showFull = false,
}) => {
  const rwd = useRWD();
  return (
    <div className={styles.swiperList}>
      <div
        className={classNames("contentContainer", {
          [styles.full]: showFull,
        })}
      >
        <Swiper
          className={styles.swiper}
          modules={[Navigation, Pagination]}
          slidesPerView={slidesPerView}
          slidesPerGroup={slidesPerGroup}
          spaceBetween={spaceBetween}
          breakpoints={breakpoints}
          navigation={{
            prevEl: `.swiper-button-prev-${prefix}`,
            nextEl: `.swiper-button-next-${prefix}`,
          }}
          pagination={{
            el: `.swiper-pagination-${prefix}`,
            type: "bullets",
            clickable: true,
          }}
        >
          {dataList.map((u, i) => (
            <SwiperSlide className={styles.slide} key={i}>
              <Element data={u} index={i}></Element>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>

      <div className="contentContainer">
        <div
          className={classNames(styles.actionGroup, {
            [styles.actionFull]: showFull,
          })}
        >
          <div
            className={classNames(
              "swiper-button-prev",
              `swiper-button-prev-${prefix}`,
              styles.prev
            )}
          ></div>
          <div
            className={classNames(
              "swiper-button-next",
              `swiper-button-next-${prefix}`,
              styles.next
            )}
          ></div>
          <div
            className={classNames(
              "swiper-pagination",
              `swiper-pagination-${prefix}`,
              styles.pagination
            )}
          ></div>
        </div>
      </div>
    </div>
  );
};
