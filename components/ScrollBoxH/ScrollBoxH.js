import { useRWD } from "@/hooks/useRwd";
import classNames from "classnames";
import React, { useEffect, useRef } from "react";
import { useScrollBoost } from "react-scrollbooster";
import styles from "./ScrollBoxH.module.scss";

const ScrollBoxHMobile = ({ children, className, innerClassName }) => {
  return (
    <div className={className}>
      <div className={innerClassName}>{children}</div>
    </div>
  );
};

const ScrollBoxHDesktop = ({ children, className, innerClassName }) => {
  const [viewport, scrollbooster] = useScrollBoost({
    direction: "horizontal",
    // friction: 0.2,
    scrollMode: "transform",
    // bounce: true,

    emulateScroll: true,
    preventDefaultOnEmulateScroll: true,

    // pointerMode: "mouse",
    // ...optional options
    // shouldScroll: () => false,

    onUpdate: (e) => {},
  });

  useEffect(() => {
    const target = scrollbooster?.props.content;
    if (target) {
      const addLock = () => {
        document.body.classList.add("lock");
      };
      const removeLock = () => {
        document.body.classList.remove("lock");
      };
      target.addEventListener("mouseover", addLock);
      target.addEventListener("mouseout", removeLock);

      return () => {
        target.removeEventListener("mouseover", addLock);
        target.removeEventListener("mouseout", removeLock);
      };
    }
  }, [scrollbooster]);

  return (
    <div
      ref={viewport}
      // onScroll={(e) => {
      //   e.preventDefault();
      //   e.stopPropagation();
      // }}
      // mousewheel={(e) => {
      //   e.preventDefault();
      //   e.stopPropagation();
      // }}
      className={className}
    >
      <div className={innerClassName}>{children}</div>
    </div>
  );
};

export const ScrollBoxH = ({ children, className, innerClassName }) => {
  const rwd = useRWD();
  const Comp = rwd.device === "mobile" ? ScrollBoxHMobile : ScrollBoxHDesktop;

  return (
    <Comp
      className={classNames(styles.scrollboxOuter, className)}
      innerClassName={classNames(styles.scrollboxInner, innerClassName)}
    >
      {children}
    </Comp>
  );
};
