import Link from 'next/link'
import React from 'react'
import { ParallaxBanner } from 'react-scroll-parallax'
import { FancyButton } from '../FancyButton/FancyButton'
import styles from './CTAContainer.module.scss'
import classNames from 'classnames'
import { Trans, useTranslation } from 'next-i18next';
import { useRouter } from "next/router";

export default function CTAContainer({
  href,
  desc1,
  desc2,
  ctaText,
}) {
    const { t } = useTranslation("common");
    const { locale } = useRouter();
    const CustomLink = ({ children, href }) => (
        <a href={`${locale === "zh" ? "" : "/en"}${href}`} target="_blank" rel="noreferrer">
            {children}
        </a>
    );
  return (
    <div className={styles.ctaContainer}>
        <ParallaxBanner
          layers={[
            {
              image: "/images/headerBG.png",
              amount: 0.2,
            },
          ]}
          style={{
            height: "auto",
          }}
        >
          <div className={classNames("contentContainer", styles.content)}>
            <div className={styles.desc}>
                <div className={classNames("homeNone", styles.desc1)}><Trans
                    i18nKey={t("why.about")}
                    transKeepBasicHtmlNodesFor={["li", "ul", "b", "h3", "a"]}
                    components={{
                        link_about: <CustomLink href="/about" />,
                    }}
                /></div>
              <div className={styles.desc1}>{desc1}</div>
              <div className={styles.desc2}>{desc2}</div>
            </div>

            <Link legacyBehavior href={href}>
              <a className={styles.btn}>
                <FancyButton>{ctaText}</FancyButton>
              </a>
            </Link>
          </div>
        </ParallaxBanner>
      </div>
  )
}
