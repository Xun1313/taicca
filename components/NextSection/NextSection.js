import { useCursorControl } from "@/context/CursorStateContext";
import Link from "next/link";
import { useRouter } from "next/router";
import { memo } from "react";
import { TLink } from "../TLink";
import styles from "./NextSection.module.scss";
export const NextSectionRaw = ({ title, prefix, path = "/", image }) => {
  const cursorControl = useCursorControl("next");
  const router = useRouter();
  return (
    <div
      className={styles.container}
      onClick={() => {
        router.push(path);
      }}
      {...cursorControl.props}
    >
      <div className={styles.dash}></div>
      <div
        className={styles.image}
        style={{ backgroundImage: `url(${encodeURI(image)})` }}
      ></div>
      <div className={styles.next}>NEXT</div>
      {/* <TLink href={path}> */}
      <a>
        <h2 data-prefix={prefix}>{title}</h2>
      </a>
      {/* </TLink> */}
    </div>
  );
};
export const NextSection = memo(NextSectionRaw);
