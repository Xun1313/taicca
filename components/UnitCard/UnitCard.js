import FavoriteIcon from "@/components/FavoriteIcon/FavoriteIcon";
import styles from "./UnitCard.module.scss";
import Link from "next/link";
import useFavorite from "@/utilits/useFavorite";
const colorList = {
  1: "#E53C2B",
  2: "#1D8B63",
  3: "#55ACCD",
  4: "#EBC945",
  5: "#DA57D8",
};

export const UnitDefaultCard = ({ data, formItems, customChildren1 }) => {
  return (
    <Link
      legacyBehavior
      key={data.id}
      href={`/professionals/${data.id}/${data.name_en}`}
    >
      <a className={styles.unitCard}>
        {customChildren1}
        <div className={styles.memberImgContainer}>
          <img src={data.pic} alt={data.title} />
        </div>
        <div>
          <div className={styles.memberRegistration}>
            {data.registration.map((e, i) => (
              <span className={styles.sphere} key={i}>
                <dot style={{ backgroundColor: colorList[e] }} />
                {formItems.clientele[e]}
              </span>
            ))}
          </div>
          <div className={styles.memberName}>
            {data.name_en && <span>{data.name_en}</span>}
            {data.lastname_en && <span>{data.lastname_en}</span>}
            {data.name_zh && <span>{data.name_zh}</span>}
          </div>
        </div>
      </a>
    </Link>
  );
};

export const UnitCustomCard1 = ({ data, formItems, customChildren1 }) => {
  return (
    <UnitDefaultCard
      data={data}
      formItems={formItems}
      customChildren1={customChildren1}
    />
  );
};

export const UnitFavCard = ({
  data,
  collectmMapping,
  setCollectmMapping,
  formItems,
  isLogin,
  authKey,
  memberId,
}) => {
  const { isFavorite, handleFavorite } = useFavorite({
    tb: data.tb,
    id: data.id,
    collectmMapping,
    setCollectmMapping,
    isLogin,
    authKey,
    memberId,
  });

  return (
    <UnitDefaultCard
      data={data}
      formItems={formItems}
      customChildren1={
        <FavoriteIcon
          className={styles.favoriteIcon}
          isFavorite={isFavorite}
          isLogin={isLogin}
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            handleFavorite();
          }}
        />
      }
    />
  );
};
