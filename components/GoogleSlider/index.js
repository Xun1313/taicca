import dynamic from "next/dynamic";

export const GoogleSlider = dynamic(() => import("react-google-slides"), {
  ssr: false,
});
