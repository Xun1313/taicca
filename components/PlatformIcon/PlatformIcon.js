import React from "react";

import IconSteam from "@/svgs/platform/steam.svg";
import IconMeta from "@/svgs/platform/meta.svg";
import IconViveport from "@/svgs/platform/viveport.svg";
import IconSparkAR from "@/svgs/platform/sparkAR.svg";
import IconAppleStore from "@/svgs/platform/applestore.svg";
import IconPlayStore from "@/svgs/platform/playstore.svg";
import IconLink from "@/svgs/platform/link.svg";

/*

link refs

Steam
https://store.steampowered.com/app/582660/Black_Desert/


Meta
https://www.meta.com/zh-tw/experiences/4979055762136823#?

vive
https://www.viveport.com/apps/85e0279a-a9ec-4c48-a638-b6bf558c34b5

SparkAR
https://www.instagram.com/ar/437231326935400


iOS / App Store 
https://apps.apple.com/tw/app/preview-planner-for-instagram/id1126609754


Android / Google Play
https://play.google.com/store/apps/details?id=com.nianticlabs.pokemongo&hl=zh-TW


*/

export default function PlatformIcon({ url, ...rest }) {
  if (url.indexOf("store.steampowered.com/app") !== -1) {
    return <IconSteam {...rest} />;
  }
  if (url.indexOf("www.meta.com") !== -1) {
    return <IconMeta {...rest} />;
  }

  if (url.indexOf("www.viveport.com/apps") !== -1) {
    return <IconViveport {...rest} />;
  }

  if (url.indexOf("www.instagram.com/ar") !== -1) {
    return <IconSparkAR {...rest} />;
  }

  if (url.indexOf("apps.apple.com") !== -1) {
    return <IconAppleStore {...rest} />;
  }

  if (url.indexOf("play.google.com/store/apps/") !== -1) {
    return <IconPlayStore {...rest} />;
  }

  return <IconLink {...rest} />;
}
