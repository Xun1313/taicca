import classNames from "classnames";
import { useRouter } from "next/router";
import styles from "./CardContent.module.scss";
import { useCursorControl } from "@/context/CursorStateContext";
import { memo } from "react";
import { useImage } from "react-image";

export const CardContentRaw = ({
  image = "./pics/demo.png",
  rangeText,
  tags,
  title = "新加坡亞洲電視論壇及內容交易市場展（線上展）",
  subTitle,
  status = "",
  type,
  slug,
  category = "article",
  clamp = 3,
  sphere,
  onClick,
  statusColor,
  subCate,
  id,
}) => {
  const router = useRouter();
  const cursorControl = useCursorControl("link");
  let oType = type;
  if (type === "partner") {
    oType = "resources";
  }

  const { src } = useImage({
    srcList: image,
    useSuspense: false,
  });

  return (
    <div
      id={id}
      className={classNames(styles.container, {
        [styles.newsBig]: type === "newsBig",
        [styles.newsSmall]: type === "newsSmall" || oType === "resources",
        [styles.reportSmall]: type === "reportSmall",
        [styles.resources]: oType === "resources",
        [styles.partner]: type === "partner",
      })}
      onClick={() => {
        if (onClick) {
          onClick();
          return;
        }
        router.push(`/${category}/${slug}`);
      }}
      {...cursorControl.props}
    >
      <div className={styles.img}>
        <div
          style={{
            backgroundImage: `url(${src})`,
          }}
        ></div>
      </div>
      <div
        className={classNames(
          {
            "desktop-only": oType !== "resources",
          },
          styles.range,
          styles.tags
        )}
      >
        {status && (
          <span
            className={classNames(styles.status)}
            {...(statusColor
              ? { style: { color: statusColor, borderColor: statusColor } }
              : {})}
          >
            {status}
          </span>
        )}
        {subCate && (
          <span
            className={classNames(styles.tag)}
            {...(statusColor
              ? { style: { color: statusColor, borderColor: statusColor } }
              : {})}
          >
            {subCate}
          </span>
        )}
        {tags &&
          tags.map((t, i) => (
            <span className={styles.tag} key={i}>
              {t}
            </span>
          ))}
        {sphere}
        {oType !== "resources" && rangeText}
      </div>
      <div className={styles.textContainer}>
        <div
          className={classNames(styles.title, {
            [styles.clamp2]: clamp === 2,
            [styles.clamp3]: clamp === 3,
          })}
        >
          {title}
        </div>
        {subTitle && <div className={styles.subTitle}>{subTitle}</div>}
      </div>
      <div
        className={classNames(
          { "mobile-only": oType !== "resources" },
          styles.range
        )}
      >
        {status && (
          <span
            {...(statusColor
              ? { style: { color: statusColor, borderColor: statusColor } }
              : {})}
          >
            {status}
          </span>
        )}
        {rangeText}
      </div>
    </div>
  );
};
export const CardContent = memo(CardContentRaw);
