import { Tooltip } from "antd";
import React from "react";
import SVGFavoriteIcon from "@/svgs/favoriteIcon.svg";
import styles from "./FavoriteIcon.module.scss";
import classNames from "classnames";
import { useTranslation } from 'next-i18next';
import { useRouter } from "next/router";

export default function FavoriteIcon({
  isFavorite,
  onClick,
  className,
  isFavoriteColor = "black",
  isLogin
}) {
  const { t } = useTranslation("common");
  const router = useRouter();

  return (
    <Tooltip title={!isFavorite && t("favorite.tooltip")}>
      <SVGFavoriteIcon
        className={classNames(styles.favoriteIcon, className)}
        style={{
          fill: isFavorite ? isFavoriteColor : "transparent",
        }}
        onClick={e => {
          if (isLogin) {
            onClick(e);
          } else {
            e.preventDefault();
            router.push("/signin");
          }
        }}
      />
    </Tooltip>
  );
}
