import React, { useCallback, useState } from "react";
import styles from "./Droupdown.module.scss";
import ArrowSVG from "./arrow.svg";
import { Popover } from "antd";
import classNames from "classnames";
import { FilterTree } from "../FilterTree/FilterTree";
import { useTranslation } from "next-i18next";
import { FancyButton } from "../FancyButton/FancyButton";
const filterSprc = [
  {
    title: "North America",
    key: "North America",
    options: [
      {
        title: "Canada",
        key: "Canada",
      },
      {
        title: "US",
        key: "US",
      },
    ],
  },
  {
    title: "Southeast Asia",
    key: "Southeast Asia",
    options: [
      {
        title: "Vietnam",
        key: "Vietnam",
      },
      {
        title: "Philippines",
        key: "Philippines",
      },
      {
        title: "Indonesia",
        key: "Indonesia",
      },
    ],
  },
];

export default function Droupdown({
  placeholder = "所在地區",
  spec = filterSprc,
  double,
  value = [],
  onChange,
  inputClassName,
  idPrefix,
}) {
  const [visible, setVisible] = useState(false);
  const [localVal, setLocakValue] = useState([]);
  const hide = () => {
    setVisible(false);
  };

  const handleVisibleChange = (newVisible) => {
    setVisible(newVisible);
    if (!newVisible) {
      setLocakValue(value);
    }
  };

  const clean = useCallback(() => {
    setLocakValue([]);
    onChange?.([]);
    setVisible(false);
  }, []);

  const save = useCallback(() => {
    onChange?.(localVal);
    setVisible(false);
  }, [localVal]);
  const { t } = useTranslation("common");
  return (
    <Popover
      content={
        <div className={styles.popcontainer}>
          <div className={classNames({ [styles.doubleGrid]: double })}>
            <FilterTree
              spec={spec}
              value={localVal}
              onChange={setLocakValue}
              subClassName={styles.doubleGrid}
              idPrefix={idPrefix}
            />
          </div>

          <div className={styles.action}>
            <div onClick={clean}>
                {t("Droupdown.clear")}
            </div>
            <button onClick={save}>
                {t("Droupdown.save")}
            </button>
          </div>
        </div>
      }
      trigger="click"
      visible={visible}
      onVisibleChange={handleVisibleChange}
      placement="bottomLeft"
    >
      <div
        className={classNames(styles.container, inputClassName, {
          [styles.open]: value && value.length,
        })}
      >
        <div className={styles.content}>{placeholder}</div>
        <ArrowSVG
          className={classNames(styles.arrow, {
            [styles.open]: visible,
          })}
        />
      </div>
    </Popover>
  );
}
