import SVGInfo from "@/svgs/info.svg";
import styles from "@/components/EmptyDataBlock/EmptyDataBlock.module.scss";
import { useTranslation } from "next-i18next";

export const EmptyDataBlock = () => {
  const { t } = useTranslation("common");

  return (
    <div className={styles.emptyDataBlock}>
      <div>
        <SVGInfo />
      </div>
      <div>{t("emptyData")}</div>
    </div>
  );
};
