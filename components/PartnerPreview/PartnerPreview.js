import React from "react";
import styles from "./PartnerPreview.module.scss";
import { FancyButton } from "../FancyButton/FancyButton";
import SVGClose from "@/svgs/close.svg";
import Link from "next/link";
import Dotdotdot from "react-dotdotdot";

function PartnerPreview({
  data,
  registration,
  onClose,
  sourceItems,
  locale,
  textEstablish,
  textMore,
}) {
  return (
    <div className={styles.container}>
      <div className={styles.imageContainer}>
        <img className={styles.image} src={data.pic} alt={data.pic}></img>
      </div>
      <div className={styles.content}>
        {registration}
        <h4 className={styles.titleEng}>{data.name_en} {data.lastname_en}</h4>
        <h3 className={styles.titleZh}>{data.name_zh}</h3>
        <div className={styles.establish}>
          {`${sourceItems.mechanism[data.unit]}`}
          {data.year && data.unit !== "4" && ` ${textEstablish}：${data.year}`}
        </div>

        <div className={styles.hopeSphere}>
          {data.hopeSphere?.map?.((h) => (
            <div key={h}>{sourceItems.sector[h]}</div>
          ))}
        </div>
        <div className={styles.industry}>
          {data.industry?.map?.((h) => (
            <div key={h}>{sourceItems.industry[h]}</div>
          ))}
        </div>

        <Dotdotdot clamp={10}>
          <p className={styles.intro}>
            {locale === "en" ? data.intro_en : data.intro_zh}
          </p>
        </Dotdotdot>
        <Link legacyBehavior href={`/professionals/${data.id}/${data.name_en}`}>
          <FancyButton>{textMore}</FancyButton>
        </Link>
      </div>
      <SVGClose className={styles.close} onClick={onClose} />
    </div>
  );
}

export default PartnerPreview;
