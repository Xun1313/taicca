import classNames from "classnames";

export const RWDSelect = ({ web, mobile, className }) => {
  const Big = web;
  const Small = mobile;
  return (
    <>
      <Big className={classNames("desktop-only", className)} />
      <Small className={classNames("mobile-only", className)} />
    </>
  );
};
