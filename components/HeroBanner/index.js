import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import styles from "./style.module.scss";
import { Pagination, Autoplay, Navigation } from "swiper";
import classNames from "classnames";

export default function HeroBanner({ data, onItemClick }) {
  return (
    <Swiper
      spaceBetween={50}
      slidesPerView={1}
      className={classNames(styles.swiper, "heroSwiper")}
      pagination={true}
      navigation={true}
      modules={[Pagination, Autoplay, Navigation]}
      autoplay={{
        delay: 3000,
        disableOnInteraction: false,
      }}
      loop={true}
    >
      {data.map((d, i) => (
        <SwiperSlide
          key={i}
          className={styles.slide}
          style={{
            cursor: d.url ? "pointer" : "auto",
          }}
          onClick={() => onItemClick(d)}
        >
          <div
            className={classNames(styles.image, "desktop-only")}
            style={{ backgroundImage: `url('${d.image}')` }}
          />
          <div
            className={classNames(styles.image, "mobile-only")}
            style={{ backgroundImage: `url('${d.imageM}')` }}
          />
          <div className={styles.textContainer}>
            {d.titleZh}
            <br />
            <span className={styles.textEn}>{d.titleEn}</span>
          </div>
        </SwiperSlide>
      ))}
    </Swiper>
  );
}
