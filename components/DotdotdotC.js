import Dotdotdot from "react-dotdotdot";

export const DotdotdotC = ({ children, clamp = 3 }) => {
  return <Dotdotdot clamp={3}>{children}</Dotdotdot>;
};
