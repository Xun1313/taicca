import classNames from "classnames";
import { CommonLightBox } from "@/components/CommonLightBox/CommonLightBox";
import { Row, Checkbox } from "antd";
import { useState } from "react";
import styles from "./ApplyPopup.module.scss";
import { Trans, useTranslation } from 'next-i18next';
import { useRouter } from "next/router";

export const ApplyUnitPopup = ({ SubmitBtn, onSuccess }) => {
  const { t } = useTranslation("common");
  const router = useRouter();
  const { locale } = useRouter();
  const [showSubmitLightBox, setShowSubmitLightBox] = useState(false);
  const [agreeError, setAgreeError] = useState(false);
  const [agree, setAgree] = useState(false);
  const CustomLink = ({ children, href }) => (
    <a
      href={`${locale === "zh" ? "" : "/en"}${href}`}
      target="_blank"
      rel="noreferrer"
    >
      {children}
    </a>
  );

  return (
    <div>
      <div onClick={() => setShowSubmitLightBox(true)}>
        <SubmitBtn text={t("companion.step4.btn4")} />
      </div>

      <CommonLightBox
        className={classNames(styles.submitLightBox)}
        containerClass={classNames(styles.container)}
        titleClass={classNames(styles.title)}
        descClass={classNames(styles.desc)}
        button1Class={classNames(styles.button1)}
        button2Class={classNames(styles.button2)}
        title={t("companion.submit.title")}
        desc={
          <div>
            <div className={styles.content}>
              <div className={styles.desc1}>{t("companion.submit.desc1")}</div>
              <Row wrap={false}>
                <Checkbox
                  checked={agree}
                  onChange={(e) => setAgree(e.target.checked)}
                />
                <div>
                  <Trans
                    i18nKey="companion.submit.desc2"
                    t={t}
                    components={{
                      link_privacy: <CustomLink href="/privacy" />,
                      link_terms: <CustomLink href="/terms" />,
                    }}
                  />
                </div>
              </Row>
            </div>

            {agreeError && (
              <div className={styles.error}>{t("companion.apply.error1")}</div>
            )}
          </div>
        }
        button1={t("companion.submit.btn1")}
        showButton2={true}
        button2={t("companion.submit.btn2")}
        showModal={showSubmitLightBox}
        setShowModal={setShowSubmitLightBox}
        onClose2={() => {
          if (agree) {
            const successCb = (id) =>
              router.push(`/user/companion/${id}?step=5`);

            onSuccess({ successCb });
          } else {
            setAgreeError(true);
          }
        }}
      />
    </div>
  );
};

export const ApplyWorkPopup = ({ SubmitBtn, onSuccess }) => {
  const { t } = useTranslation("common");
  const router = useRouter();
  const { locale } = useRouter();
  const [showSubmitLightBox, setShowSubmitLightBox] = useState(false);
  const [agreeError, setAgreeError] = useState(false);
  const [agree, setAgree] = useState(false);
  const CustomLink = ({ children, href }) => (
    <a
      href={`${locale === "zh" ? "" : "/en"}${href}`}
      target="_blank"
      rel="noreferrer"
    >
      {children}
    </a>
  );

  return (
    <div>
      <div onClick={() => setShowSubmitLightBox(true)}>
        <SubmitBtn text={t("works.step4.submit3")} />
      </div>

      <CommonLightBox
        className={classNames(styles.submitLightBox)}
        containerClass={classNames(styles.container)}
        titleClass={classNames(styles.title)}
        descClass={classNames(styles.desc)}
        button1Class={classNames(styles.button1)}
        button2Class={classNames(styles.button2)}
        title={t("works.submit.title")}
        desc={
          <div>
            <div className={styles.content}>
              <div className={styles.desc1}>{t("works.submit.desc1")}</div>
              <Row wrap={false}>
                <Checkbox
                  checked={agree}
                  onChange={(e) => setAgree(e.target.checked)}
                />
                <div className={styles.desc2}>
                  <Trans
                    i18nKey="works.submit.desc2"
                    t={t}
                    components={{
                      link_license: <CustomLink href="/license" />,
                    }}
                  />
                </div>
              </Row>
            </div>

            {agreeError && (
              <div className={styles.error}>{t("works.apply.error1")}</div>
            )}
          </div>
        }
        button1={t("works.submit.btn1")}
        showButton2={true}
        button2={t("works.submit.btn2")}
        showModal={showSubmitLightBox}
        setShowModal={setShowSubmitLightBox}
        onClose2={() => {
          if (agree) {
            const successCb = (id) => router.push(`/user/works/${id}?step=5`);

            onSuccess({ successCb });
          } else {
            setAgreeError(true);
          }
        }}
      />
    </div>
  );
};
