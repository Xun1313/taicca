import styles from "./ComingSoon.module.scss";

export const ComingSoon = () => {
  return (
    <div className="contentContainer">
      <div className={styles.comingSoon}>
        <div className={styles.content}>
          <div>即將上線，敬請期待</div>
          <div>Coming soon</div>
        </div>
      </div>
    </div>
  );
};
