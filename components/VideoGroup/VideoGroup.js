import React, { useContext } from "react";
import styles from "./VideoGroup.module.scss";
import Slider from "react-slick";
import classNames from "classnames";
import SVGArrow from "@/svgs/arrow.svg";
import { useCursorControl } from "@/context/CursorStateContext";
import { VideoLightboxContext } from "../VideoLightbox/VideoLightbox";

const mock = [
  {
    id: 1,
    cover: "/images/video_01.png",
    videoURL: "https://www.youtube.com/watch?v=s_2oDe0FFnw",
  },
  {
    id: 2,
    cover: "/images/video_02.png",
    videoURL: "https://fb.watch/8KhcCeGZ1j/",
  },
];

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "red", right: 0 }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "green", left: 0 }}
      onClick={onClick}
    />
  );
}

export const VideoGroup = ({ list = mock }) => {
  const settings = {
    nextArrow: <SVGArrow />,
    prevArrow: <SVGArrow />,
    dots: true,
  };
  const videoControl = useContext(VideoLightboxContext);

  const curosrControl = useCursorControl("video");
  return (
    <div className={classNames(styles.container, "videoGroup")}>
      <Slider {...settings}>
        {list.map((video) => (
          <div key={video.id} className={styles.videoContainer}>
            <div
              onClick={() => videoControl.open(video)}
              className={classNames(styles.videoCover)}
              {...curosrControl.props}
            >
              <div
                className={classNames(styles.bg)}
                style={{ backgroundImage: `url(${video.cover})` }}
              ></div>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
};
