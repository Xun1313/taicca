import { useCallback, useState, useRef, useEffect } from "react";
import Calendar from "react-calendar";
import styles from "./RangePicker.module.scss";
import { Popover } from "antd";
import moment from "moment";
import SVGDayPicker from "@/svgs/dayPickerIcon.svg";
import classNames from "classnames";
import { FancyButton } from "../FancyButton/FancyButton";
import { useCursorControl } from "@/context/CursorStateContext";

const WeekdayMap = {
  Sun: "週日",
  Mon: "週一",
  Tue: "週二",
  Wed: "週三",
  Thu: "週四",
  Fri: "週五",
  Sat: "週六",
};

function useOnClickOutside(ref, handler, enable) {
  useEffect(
    () => {
      if (!enable) {
        return;
      }
      const listener = (event) => {
        // Do nothing if clicking ref's element or descendent elements
        if (!ref.current || ref.current.contains(event.target)) {
          return;
        }
        // event.stopPropagation();
        // event.preventDefault();

        handler(event);
      };
      setTimeout(() => {
        document.addEventListener("click", listener);
        document.addEventListener("touchstart", listener);
      }, 0);

      return () => {
        document.removeEventListener("click", listener);
        document.removeEventListener("touchstart", listener);
      };
    },
    // Add ref and handler to effect dependencies
    // It's worth noting that because passed in handler is a new ...
    // ... function on every render that will cause this effect ...
    // ... callback/cleanup to run every render. It's not a big deal ...
    // ... but to optimize you can wrap handler in useCallback before ...
    // ... passing it into this hook.
    [ref, handler, enable]
  );
}

const Inner = ({
  value,
  onChange,
  setIsOpen,
  isOpen,
  onRangeChange,
  locale,
  t,
}) => {
  const handleClickOutside = useCallback(() => {
    setIsOpen(false);
    onChange([null, null]);
    onRangeChange(null);
  }, [onChange, onRangeChange, setIsOpen]);
  const ref = useRef();

  useOnClickOutside(ref, handleClickOutside, isOpen);

  // const ref = useDetectClickOutside({
  //   onTriggered: handleClickOutside,
  //   disableClick: true,
  // });
  return (
    <div ref={ref}>
      <Calendar
        selectRange
        allowPartialRange={true}
        showNeighboringMonth={true}
        locale="en-US"
        formatMonthYear={(_, date) =>
          moment(date).format(locale === "zh" ? "M月 YYYY" : "MM / YYYY")
        }
        formatShortWeekday={(_, date) => {
          const text = moment(date).format("ddd");
          return locale === "zh" ? WeekdayMap[text] : text;
        }}
        onChange={onChange}
        value={value}
        formatDay={(locale, date) => date.getDate()}
      />
      <FancyButton
        onClick={() => {
          setIsOpen(false);

          if (value[0] && !value[1]) {
            onChange([value[0], value[0]]);
            onRangeChange([value[0], value[0]]);
          } else {
            onRangeChange(value);
          }
        }}
        className={classNames(styles.confirm)}
      >
        {t("confirm")}
      </FancyButton>
    </div>
  );
};

const RangePicker = ({ onRangeChange = () => {}, t, locale, className }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [value, onChange] = useState([null, null]);
  const isSelected = value[0] && value[1];
  const c = useCursorControl("link");

  return (
    <Popover
      content={
        <Inner
          value={value}
          onChange={onChange}
          isOpen={isOpen}
          setIsOpen={setIsOpen}
          onRangeChange={onRangeChange}
          locale={locale}
          t={t}
        />
      }
      // title="Title"
      destroyTooltipOnHide={true}
      trigger=""
      placement="bottomLeft"
      overlayInnerStyle={{ padding: 0 }}
      overlayClassName={"range-picker-overlay"}
      visible={isOpen}
      // visible={this.state.visible}
      // onVisibleChange={this.handleVisibleChange}
    >
      <div
        className={classNames(
          styles.input,
          { [styles.active]: isSelected },
          className
        )}
        onClick={() => {
          if (!isSelected) {
            setIsOpen(true);
          }
        }}
        {...c.props}
      >
        <div
          onClick={() => {
            setIsOpen(true);
          }}
        >
          {isSelected
            ? `${moment(value[0]).format("YYYY/M/D")} - ${moment(
                value[1]
              ).format("YYYY/M/D")}`
            : t("event.tittle3")}
        </div>
        {isSelected ? (
          <div
            onClick={() => {
              onChange([null, null]);
              onRangeChange(null);
            }}
            className={styles.x}
          >
            X
          </div>
        ) : (
          <SVGDayPicker
            className={styles.icon}
            onClick={() => {
              // this.from.getInput().focus();
            }}
          />
        )}
      </div>
    </Popover>
  );
};

export default RangePicker;
