import styles from "./RangePicker.module.scss";
import DayPickerInput from "react-day-picker/DayPickerInput";
import DayPicker from "react-day-picker";

import "react-day-picker/lib/style.css";
import React, { useCallback, useState } from "react";

import "react-day-picker/lib/style.css";
import moment from "moment";
import SVGDayPicker from "@/svgs/dayPickerIcon.svg";

import { formatDate, parseDate } from "react-day-picker/moment";
import classNames from "classnames";

const WEEKDAYS_SHORT = {
  zh: ["週日", "週一", "週二", "週三", "週四", "週五", "週六"],
};
const MONTHS = {
  zh: [
    "1月",
    "2月",
    "3月",
    "4月",
    "5月",
    "6月",
    "7月",
    "8月",
    "9月",
    "10月",
    "11月",
    "12月",
  ],
};

const WEEKDAYS_LONG = {
  zh: ["週日", "週一", "週二", "週三", "週四", "週五", "週六"],
};

const FIRST_DAY_OF_WEEK = {
  zh: 0,
};
// Translate aria-labels
const LABELS = {
  zh: { nextMonth: "下個月", previousMonth: "上個月" },
};

export default class RangePicker extends React.Component {
  constructor(props) {
    super(props);
    this.handleFromChange = this.handleFromChange.bind(this);
    this.handleToChange = this.handleToChange.bind(this);
    this.state = {
      from: undefined,
      to: undefined,
    };
  }

  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), "months") < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }

  handleFromChange(from) {
    // Change the from date and focus the "to" input field
    this.setState({ from });
  }

  handleToChange(to) {
    this.setState({ to }, this.showFromMonth);
  }

  render() {
    const { from, to } = this.state;
    const modifiers = { start: from, end: to };
    const locale = "zh";
    return (
      <>
        <div className={classNames("rangePicker", this.props.className)}>
          <div>
            <DayPickerInput
              value={from}
              ref={(el) => (this.from = el)}
              placeholder={this.props.t("event.tittle3")}
              format="YYYY/MM/DD"
              formatDate={formatDate}
              parseDate={parseDate}
              // showOverlay={true}
              dayPickerProps={{
                selectedDays: [from, { from, to }],
                disabledDays: { after: to },
                toMonth: to,
                modifiers,
                numberOfMonths: 1,
                onDayClick: () => {
                  setTimeout(() => {
                    this.to.getInput().focus();
                  }, 200);
                },
                showOutsideDays: true,
              }}
              onDayChange={this.handleFromChange}
            />
            <span style={{ visibility: from ? "visible" : "hidden" }}>-</span>

            <span
              className="InputFromTo-to"
              style={{ display: from ? "inline" : "none" }}
            >
              <DayPickerInput
                ref={(el) => (this.to = el)}
                value={to}
                placeholder=""
                format="YYYY/MM/DD"
                formatDate={formatDate}
                parseDate={parseDate}
                // showOverlay={true}
                dayPickerProps={{
                  selectedDays: [from, { from, to }],
                  disabledDays: { before: from },
                  modifiers,
                  month: from,
                  fromMonth: from,
                  numberOfMonths: 1,
                  showOutsideDays: true,
                }}
                onDayChange={this.handleToChange}
              />
            </span>
          </div>
          <SVGDayPicker
            className={"dayPickerIcon"}
            onClick={() => {
              this.from.getInput().focus();
            }}
          />
        </div>
        {/* <div className={"DayPickerInput-Overlay"}>
          <DayPicker
            fromMonth={from}
            selectedDays={[from, { from, to }]}
            disabledDays={{ before: from }}
            modifiers={modifiers}
            month={from}
            showOutsideDays
            locale={locale}
            months={MONTHS[locale]}
            weekdaysLong={WEEKDAYS_LONG[locale]}
            weekdaysShort={WEEKDAYS_SHORT[locale]}
            firstDayOfWeek={FIRST_DAY_OF_WEEK[locale]}
            labels={LABELS[locale]}
            // onDayClick={this.handleDayClick}
            // onDayMouseEnter={this.handleDayMouseEnter}
          />
        </div> */}
      </>
    );
  }
}
