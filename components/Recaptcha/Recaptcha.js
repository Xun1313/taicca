import Script from "next/script";

export const Recaptcha = () => {
  return (
    <Script src="https://www.google.com/recaptcha/api.js?render=6LcJq1QpAAAAADEGyHrv3AO-z9yBIiJPBlKgILgR" />
  );
};
