import classNames from "classnames";
import styles from "./CardContentNews.module.scss";

export const CardContentNews = ({
  image = "./pics/demo.png",
  rangeText = "2021.9.15 - 2021.9.30",
  title = "新加坡亞洲電視論壇及內容交易市場展（線上展）",
  status = "進行中",
}) => {
  return (
    <div className={styles.container}>
      <div
        className={styles.img}
        style={{
          backgroundImage: `url(${image})`,
        }}
      ></div>
      <div className={classNames("desktop-only", styles.range)}>
        {status && <span>{status}</span>}
        {rangeText}
      </div>
      <div className={styles.title}>{title}</div>
      <div className={classNames("mobile-only", styles.range)}>
        {status && <span>{status}</span>}
        {rangeText}
      </div>
    </div>
  );
};
