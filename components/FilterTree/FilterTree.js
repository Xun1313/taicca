import React, { useState } from "react";
import styles from "./FilterTree.module.scss";
const filterSprcs = [
  {
    title: "Area",
    key: "Area",
    options: [
      {
        title: "North America",
        key: "North America",
        options: [
          {
            title: "Canada",
            key: "Canada",
          },
          {
            title: "US",
            key: "US",
          },
        ],
      },
      {
        title: "Southeast Asia",
        key: "Southeast Asia",
        options: [
          {
            title: "Vietnam",
            key: "Vietnam",
          },
          {
            title: "Philippines",
            key: "Philippines",
          },
          {
            title: "Indonesia",
            key: "Indonesia",
          },
        ],
      },
    ],
  },
  {
    title: "類別",
    key: "類別",
    options: [
      {
        title: "影展/藝術節",
        key: "影展/藝術節",
      },
      {
        title: "場會",
        key: "場會",
      },
    ],
  },
];

export const FilterTree = ({
  spec,
  value = [],
  onChange,
  subClassName,
  idPrefix = "",
}) => {
  const [selectStateS, setSelectStateS] = useState([]);
  const selectState = typeof onChange === "function" ? value : selectStateS;

  const setSelectState =
    typeof onChange === "function" ? onChange : setSelectStateS;

  return (
    <>
      {spec.map((option, i) => (
        <div key={`${option.key}`} className={styles.filterContnet}>
          <input
            id={`filter_${idPrefix}_${option.key}`}
            type="checkbox"
            checked={selectState.indexOf(`${option.key}`) !== -1}
            onChange={() => {
              if (option.options) {
                const o = value;
                const filterIds = [
                  option.key,
                  ...option.options.map(({ key }, j) => key),
                ];
                const clean = o.filter((ids) => filterIds.indexOf(ids) === -1);
                if (o.indexOf(option.key) !== -1) {
                  setSelectState(clean);
                  return;
                }

                setSelectState([...clean, ...filterIds]);
                return;
              } else {
                setSelectState(
                  value.indexOf(option.key) !== -1
                    ? value.filter((ids) => ids !== option.key)
                    : [...value, option.key]
                );
                return;
              }
            }}
          ></input>
          <label htmlFor={`filter_${idPrefix}_${option.key}`}>
            {option.title}
          </label>
          <span>{option.amount}</span>
          <div className={subClassName}>
            {option.options?.map((io, j) => (
              <div key={io.key} className={styles.filterContnet}>
                <input
                  id={`${`filter_${idPrefix}_${io.key}`}`}
                  type="checkbox"
                  checked={selectState.indexOf(io.key) !== -1}
                  onChange={() => {
                    const o = value;
                    const allOptions = option.options.map(({ key }) => key);
                    const newData =
                      o.indexOf(io.key) !== -1
                        ? o.filter((ids) => ids !== io.key)
                        : [...o, io.key];

                    if (
                      allOptions.every((id) => newData.indexOf(id) !== -1) &&
                      newData.indexOf(option.key) == -1
                    ) {
                      setSelectState([...newData, option.key]);
                      return;
                    }

                    if (
                      newData.indexOf(option.key) !== -1 &&
                      !allOptions.every((id) => newData.indexOf(id) !== -1)
                    ) {
                      setSelectState(newData.filter((id) => id !== option.key));
                      return;
                    }

                    setSelectState(newData);
                    return;
                  }}
                ></input>
                <label htmlFor={`filter_${idPrefix}_${io.key}`}>
                  {io.title}
                </label>
              </div>
            ))}
          </div>
        </div>
      ))}
    </>
  );
};
