import Dialog from "rc-dialog";
import SVGLBX from "@/svgs/lightbox-close.svg";
import { createContext, useCallback, useState } from "react";
import styles from "./VideoLightbox.module.scss";
import ReactPlayer from "react-player/lazy";

export const VideoLightboxContext = createContext({
  isOpen: false,
  open: () => {},
  close: () => {},
  data: {},
});

export const VideoLightbox = ({ children }) => {
  const [state, setState] = useState({
    isOpen: false,
    data: null,
  });

  const handleOpen = useCallback((data) => {
    setState({
      isOpen: true,
      data: data,
    });
  }, []);
  const handleClose = useCallback(() => {
    setState((p) => ({
      ...p,
      isOpen: false,
    }));
  }, []);

  return (
    <VideoLightboxContext.Provider
      value={{
        isOpen: state.isOpen,
        open: handleOpen,
        close: handleClose,
        data: state.data,
      }}
    >
      {children}
      <Dialog
        animation="fade"
        maskAnimation="fade"
        onClose={handleClose}
        visible={state.isOpen}
        destroyOnClose={true}
        closeIcon={<SVGLBX />}
        prefixCls={"lightbox"}
        light
      >
        <div className={styles.videoCover}>
          {state.data?.videoURL && (
            <ReactPlayer
              width="100%"
              height="100%"
              style={{ background: "rgba(0,0,0, 0.7)" }}
              url={state.data?.videoURL}
            />
          )}

          <div className={styles.playButton}>Play Video</div>
        </div>
      </Dialog>
    </VideoLightboxContext.Provider>
  );
};
