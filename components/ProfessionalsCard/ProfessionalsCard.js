/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
import classNames from "classnames";
import React from "react";
import { Join } from "../Join/Join";
import styles from "./ProfessionalsCard.module.scss";
import SVGClose from "@/svgs/close.svg";
import TelIcon from "./phone.svg";
import Mail from "./mail.svg";
// import FB from "./fb.svg";
// import Twitter from "./twitter.svg";
// import Linkedin from "./linkedin.svg";
import iconFB from "./iconFb.svg";
import iconTwitter from "./iconTwitter.svg";
import iconLinkedin from "./iconLinkedin.svg";
import { useDetectClickOutside } from "react-detect-click-outside";
import { useTranslation } from "next-i18next";

const contacts = [
  {
    key: "tel",
    title: "tel",
  },
  {
    key: "email",
    title: "email",
  },
  {
    key: "facebook",
    title: "facebook",
    icon: iconFB,
  },
  {
    key: "twitter",
    title: "twitter",
    icon: iconTwitter,
  },
  {
    key: "linkedin",
    title: "linkedin",
    icon: iconLinkedin,
  },
];

const renderContact = (partner, key, title) => {
  if (!partner[key] || partner[key] === "undefined") return null;
  if (key === "tel") {
    return (
      <div className={styles.tel} key={key}>
        <TelIcon />
        <span>{partner[key]}</span>
        <br />
      </div>
    );
  }
  if (key === "email") {
    return (
      <div className={styles.mail} key={key}>
        <Mail viewBox="0 0 22 18" />
        <span>{partner[key]}</span>
        <br />
      </div>
    );
  }

  const Icon = contacts.find((c) => c.key === key)?.icon;
  return (
    <a
      href={partner[key]}
      target="_blank"
      rel="noreferrer"
      className={styles.linkIcon}
    >
      {Icon ? <Icon /> : partner[key]}
      {/* {partner[key]} */}
    </a>
  );
};

export default function ProfessionalsCard({
  className,
  onClose,
  intro,
  name,
  pic,
  partner,
  sphere,
  registration,
  isCardAnimationPlayingRef,
}) {
  const cardRef = useDetectClickOutside({
    onTriggered: () => {
      console.log(isCardAnimationPlayingRef.current);
      if (!isCardAnimationPlayingRef.current) {
        onClose();
      }

      // setSelectPartner(null);
    },
  });
  // console.log(partner);
  const portfolio1 =
    partner.portfolio1 && partner.portfolio1 !== "undefined"
      ? partner.portfolio1
      : "";

  const portfolio2 =
    partner.portfolio2 && partner.portfolio2 !== "undefined"
      ? partner.portfolio2
      : "";
  const { t } = useTranslation("common");
  const showWorks = !!portfolio1 || !!portfolio2;
  return (
    <div ref={cardRef} className={classNames(styles.container, className)}>
      <div>
        <img src={pic} alt="icon"></img>
        {registration}

        <div className={styles.content}>
          <h3>{name}</h3>
          <p>{`${intro}`}</p>

          <p>
            {sphere && (
              <>
                <span>{t("professionals.unit")}</span>
                <span>{sphere}</span>
                <br />
              </>
            )}
            {!!portfolio1 ||
              (!!portfolio2 && (
                <>
                  <span>{t("professionals.works")}</span>
                  <span>
                    {portfolio1 && (
                      <a href={portfolio1} target={"_blank"} rel="noreferrer">
                        {portfolio1}
                      </a>
                    )}
                    {portfolio1 && portfolio2 && <br />}
                    {portfolio2 && (
                      <a href={portfolio2} target={"_blank"} rel="noreferrer">
                        {portfolio2}
                      </a>
                    )}
                  </span>
                </>
              ))}
          </p>
          <p>
            {/* <span>聯繫方式：</span>
            <br /> */}
            <span>
              {contacts.map(({ key, title }) =>
                renderContact(partner, key, title)
              )}
            </span>
          </p>
        </div>
        <Join />
      </div>
      <SVGClose className={styles.close} onClick={onClose} />
    </div>
  );
}
