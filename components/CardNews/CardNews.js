import classNames from "classnames";
import { useRouter } from "next/router";
import styles from "./CardNews.module.scss";
import { memo } from "react";
import { useCursorControl } from "@/context/CursorStateContext";
import { DotdotdotC } from "../DotdotdotC";
export const CardNewsRaw = ({
  image = "./pics/demo.png",
  title = "翻開下一頁，與故事一起去更遠的地方 文策院於台北國際書展期間，力...",
  state = "進行中",
  rangeText = "9/15 - 9 /30",
  slug,
  clamp,
}) => {
  const router = useRouter();

  const c = useCursorControl("link");
  return (
    <div
      className={styles.outer}
      onClick={() => router.push(`/article/${slug}`)}
      {...c.props}
    >
      <div
        className={classNames(styles.container, styles.news)}
        // style={{
        //   backgroundImage: `url(${encodeURI(image)})`,
        // }}
      >
        <div
          className={styles.bg}
          style={{
            backgroundImage: `url(${image})`,
          }}
        ></div>
        <div className={classNames(styles.content, "desktop-only")}>
          <div className={styles.maskContainer}>
            <div className={styles.mask}>
              <div
                className={classNames(styles.title, {
                  [styles.clamp2]: clamp === 2,
                  [styles.clamp3]: clamp === 3,
                })}
              >
                {title}
              </div>
              <div className={styles.title}>
                {state && <span className={styles.tag}>{state}</span>}
                {rangeText}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={"mobile-only"}>
        <div
          className={classNames(styles.title, {
            [styles.clamp2]: clamp === 2,
            [styles.clamp3]: clamp === 3,
          })}
        >
          {title}
        </div>
        <div className={styles.subTitle}>
          {state && <span className={styles.tag}>{state}</span>}
          {rangeText}
        </div>
      </div>
    </div>
  );
};
export const CardNews = memo(CardNewsRaw);
