import { useCursorControl } from "@/context/CursorStateContext";
import classNames from "classnames";
import styles from "./LoadMoreGroup.module.scss";

export const LoadMoreGroup = ({
  className,
  actionText,
  leftText,
  onLoadmore,
}) => {
  const c = useCursorControl();
  return (
    <div className={classNames(styles.container, className)}>
      <div>
        {leftText}
        {/* <br />
        或立即點選下方加入共創圈夥伴，與我們一起為台灣的文創盡一分力！ */}
      </div>
      <button className={"button"} {...c.props} onClick={onLoadmore}>
        {actionText}
      </button>
    </div>
  );
};
