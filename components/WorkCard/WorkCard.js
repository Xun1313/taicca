import classNames from "classnames";
import FavoriteIcon from "@/components/FavoriteIcon/FavoriteIcon";
import styles from "./WorkCard.module.scss";
import Link from "next/link";
import useFavorite from "@/utilits/useFavorite";

export const WorkDefaultCard = ({ data, formItems, customChildren1 }) => {
  return (
    <Link legacyBehavior key={data.id} href={`/content/${data.id}/${data.name_en}`}>
      <a className={styles.workCard}>
        <img src={data.pic} className={styles.workCardImg} alt="icon" />
        <div className={styles.workCardContainer}>
          <div className={styles.workCardName}>
            <div className={styles.workCardNameEng}>{data.name_en}</div>
            <div className={styles.workCardNameZh}>{data.name_zh}</div>
          </div>
          {customChildren1}
        </div>

        <div className={styles.tagList}>
          {data.gclass?.map?.((h) => (
            <div key={h} className={classNames(styles.tag, styles.gclassTag)}>
              {formItems.artType[h]}
            </div>
          ))}
        </div>

        <div className={styles.tagList}>
          {data.workform?.map?.((h) => (
            <div key={h} className={classNames(styles.tag, styles.workformTag)}>
              {formItems.artform[h]}
            </div>
          ))}
        </div>
      </a>
    </Link>
  );
};

export const WorkCustomCard1 = ({ data, formItems, customChildren1 }) => {
  return (
    <WorkDefaultCard
      data={data}
      formItems={formItems}
      customChildren1={customChildren1}
    />
  );
};

export const WorkFavCard = ({
  data,
  collectmMapping,
  setCollectmMapping,
  formItems,
  isLogin,
  authKey,
  memberId,
}) => {
  const { isFavorite, handleFavorite } = useFavorite({
    tb: data.tb,
    id: data.id,
    collectmMapping,
    setCollectmMapping,
    isLogin,
    authKey,
    memberId,
  });

  return (
    <WorkDefaultCard
      data={data}
      formItems={formItems}
      customChildren1={
        <FavoriteIcon
          className={styles.favoriteIcon}
          isFavorite={isFavorite}
          isLogin={isLogin}
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            handleFavorite();
          }}
        />
      }
    />
  );
};
