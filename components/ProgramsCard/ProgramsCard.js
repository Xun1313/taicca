import Link from "next/link";
import classNames from "classnames";
import styles from "./ProgramsCard.module.scss";

export const ProgramsCard = ({ data, formItems }) => {
  return (
    <Link legacyBehavior href={`/programs/${data.slug}`}>
      <a className={styles.programsCard}>
        <img src={data.pic} alt={data.title} />
        <div className={styles.content}>
          <div className={styles.tagList}>
            <div className={classNames(styles.tag, styles.category)}>
              {formItems.plan[data.category]}
            </div>
            <div className={classNames(styles.tag, styles.area)}>
              {formItems.planArea[data.area]}
            </div>
          </div>

          <div className={styles.title}>{data.title}</div>
          {data.receipt_date && (
            <div className={styles.time}>
              {data.receipt_date} - {data.receipt_dateed}
            </div>
          )}
        </div>
      </a>
    </Link>
  );
};
