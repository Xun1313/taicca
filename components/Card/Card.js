import { useCursorControl } from "@/context/CursorStateContext";
import classNames from "classnames";
import { useRouter } from "next/router";
import styles from "./Card.module.scss";
import { memo } from "react";
import { FancyButton } from "../FancyButton/FancyButton";
import { useRWD } from "@/hooks/useRwd";
import { DotdotdotC } from "../DotdotdotC";
export const CardRaw = ({
  image = "./pics/demo.png",
  title = "TAICCA X Midem 國際音樂合作音樂祭",
  year = "2021",
  rangeText = "9/15 - 9 /30",
  square,
  slug,
  category = "article",
  clamp = 3,
}) => {
  const router = useRouter();
  const c = useCursorControl();
  const rwd = useRWD();
  return (
    <div
      className={classNames(styles.outer, { [styles.square]: square })}
      onClick={() => {
        rwd.device === "mobile" && router.push(`/${category}/${slug}`);
      }}
    >
      <div className={classNames(styles.container)} style={{}}>
        <div
          className={styles.bg}
          style={{
            backgroundImage: `url(${image})`,
          }}
        ></div>
        <div className={classNames(styles.content, "desktop-only")}>
          <div className={styles.maskContainer}>
            <div className={styles.mask}>
              <div
                className={classNames(styles.title, {
                  [styles.clamp2]: clamp === 2,
                  [styles.clamp3]: clamp === 3,
                })}
              >
                {title}
              </div>

              <div data-year={year} className={styles.subTitle}>
                {rangeText}
              </div>
            </div>
          </div>
          <FancyButton
            onClick={() => {
              router.push(`/${category}/${slug}`);
            }}
            className={"desktop-only light"}
            light
            // {...c.props}
          >
            more
          </FancyButton>
        </div>
      </div>
      <div className={"mobile-only"}>
        <div
          className={classNames(styles.title, {
            [styles.clamp2]: clamp === 2,
            [styles.clamp3]: clamp === 3,
          })}
        >
          {title}
        </div>
        <div data-year={year} className={styles.subTitle}>
          {rangeText}
        </div>
      </div>
    </div>
  );
};
export const Card = memo(CardRaw);
