import SVGJoinStep1 from "@/svgs/joinStep1.svg";
import SVGJoinStep2 from "@/svgs/joinStep2.svg";
import SVGJoinStep3 from "@/svgs/joinStep3.svg";
import SVGJoinStep4 from "@/svgs/joinStep4.svg";

import SVGJoinStep1m from "@/svgs/joinStep1M.svg";
import SVGJoinStep2m from "@/svgs/joinStep2M.svg";
import SVGJoinStep3m from "@/svgs/joinStep3M.svg";
import SVGJoinStep4m from "@/svgs/joinStep4M.svg";
import styles from "./JoinStep.module.scss";
import classNames from "classnames";

const steps = [SVGJoinStep1, SVGJoinStep2, SVGJoinStep3, SVGJoinStep4];
const stepsM = [SVGJoinStep1m, SVGJoinStep2m, SVGJoinStep3m, SVGJoinStep4m];

// export const JoinStep = ({ isMoble, step, ...rest }) => {
//   const Comp = isMoble ? stepsM[step - 1] : steps[step - 1];
//   return <Comp {...rest} />;
// };

const stepList = [
  {
    title: "join.form.step1",
    info: "join.form.introduction.step1",
  },
  {
    title: "join.form.step2",
    info: "join.form.introduction.step2",
  },
  {
    title: "join.form.step3",
    info: "join.form.introduction.step3",
  },
  {
    title: "join.form.step4",
    info: "join.form.introduction.step4",
  },
];

export const JoinStep = ({ isMoble, step, className, t, ...rest }) => {
  return (
    <div className={classNames(styles.container, className)}>
      <div className={classNames(styles.bg, className)}>
        <div className={styles.progressOut}>
          <div
            className={classNames(styles.progressInner, {
              [styles.step1]: 1 === step,
              [styles.step2]: 2 === step,
              [styles.step3]: 3 === step,
              [styles.step4]: 4 === step,
            })}
          >
            <div className={classNames(styles.dot, styles.active, {})}></div>
          </div>
        </div>
        {stepList.map((d, i) => (
          <div
            data-text={`${t(d.title)}\n${t(d.info)}`}
            className={classNames(styles.dot, {
              [styles.activeStatic]: i + 1 === step,
              [styles.finish]: i + 1 < step,
            })}
            key={i}
          ></div>
        ))}
      </div>
    </div>
  );
};
