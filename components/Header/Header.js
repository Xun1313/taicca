import React, { useRef, useState, useEffect } from "react";
import styles from "./Header.module.scss";
import SVGLogoDC from "@/svgs/logoDC.svg";
import SVGLogoDCMobile from "@/svgs/logoDCMobile.svg";
import SVGSeaechDC from "@/svgs/searchDC.svg";
import SVGMember from "@/svgs/member.svg";
import SVGMenuDC from "@/svgs/menuDC.svg";
import classNames from "classnames";
import { Menu } from "../Menu/menu";

import { SearchInputA } from "../SearchInput/SearchInput";
import Link from "next/link";
import router, { useRouter } from "next/router";
import { useRWD } from "@/hooks/useRwd";
import { useCursorControl } from "@/context/CursorStateContext";

const languages = [
  {
    id: "zh",
    title: "CN",
  },
  {
    id: "en",
    title: "EN",
  },
];

const LanguaueSelecter = ({ currentLangId = "cn", customLangLink }) => {
  const router = useRouter();
  const c = useCursorControl("link");
  return (
    <div className={styles.languaueSelecter}>
      {languages.map(({ title, id }) => {
        return (
          <React.Fragment key={id}>
            <input
              checked={router.locale === id}
              type="radio"
              id={id}
              name="language"
              onChange={() => {}}
              onClick={() => {
                if (customLangLink) {
                  customLangLink();
                } else {
                  if (router.pathname === "/search") {
                    router.push(router.asPath, null, {
                      locale: id,
                      query: router.query,
                    });
                  } else {
                    router.push(router.asPath, undefined, {
                      locale: id,
                      query: router.query,
                    })
                    // router.push(router.pathname, router.asPath, {
                    //   locale: id,
                    //   query: router.query,
                    // });
                  }
                }
              }}
            />
            <label {...c.props} htmlFor={id}>
              {title}
            </label>
          </React.Fragment>
        );
      })}
    </div>
  );
};

const useScorllHeaderControl = ({ startY = 100 }) => {
  const [state, setState] = useState({
    float: false,
    show: true,
  });
  const stateRef = useRef(state);
  stateRef.current = state;

  useEffect(() => {
    let prevY = window.scrollY;
    const handelScroll = (e) => {
      const newY = window.scrollY;
      if (newY !== prevY) {
        setState({
          float: newY > startY + 50,
          show: newY < prevY || newY < startY,
        });
      }
      prevY = newY;
    };
    window.addEventListener("scroll", handelScroll);

    return () => {
      window.removeEventListener("scroll", handelScroll);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return state;
};

export const Header = ({ title, red, numberText, search = true, customLangLink }) => {
  const rwd = useRWD();
  const { float: flatRaw, show } = useScorllHeaderControl({
    startY: rwd.device === "mobile" ? 100 : red ? 150 : 100,
  });

  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const float = isSearchOpen || flatRaw;
  const c = useCursorControl("link");


  useEffect(() => {
    if (isMenuOpen) {
      const func = (e) => {
        let target = e.target;
        const body = document.getElementsByTagName("body")[0];

        while (target !== body) {
          if (target.classList.contains("menu-root")) {
            return;
          } else {
            target = target.parentElement;
          }
        }

        setIsMenuOpen(false);
      };
      window.addEventListener("mousedown", func);
      return () => {
        window.removeEventListener("mousedown", func);
      };
    }
  }, [isMenuOpen]);

  // const [shortLocale] = locale ? locale.split("-") : ["en"];
  return (
    <>
      <div
        className={classNames(styles.container, {
          [styles.float]: true,// float
          [styles.hide]: false,// !show
          [styles.red]: red,
          [styles.fixed]: flatRaw,
        })}
      >
        <div className={styles.inner}>
          <Link legacyBehavior href={"/"}>
            <a {...c.props}>
              {rwd.device === "mobile" ? <SVGLogoDCMobile className={styles.logo} /> : <SVGLogoDC className={styles.logo} />}
            </a>
          </Link>
          <div className={styles.actions}>
            {/* <SVGSeaechDC className={"desktop-only"} /> */}
            <SearchInputA
              className={"desktop-only"}
              float={float}
              onSeachOpenChange={(val) => {
                setIsSearchOpen(val);
              }}
              {...c.props}
            />
            {/* onAction={() => {
                router.push(`/search?text=${value}`);
              }} */}

            <Link legacyBehavior href={"/signin"}>
              <a className={`${styles.member}`}>
                <SVGMember />
              </a>
            </Link>

            <LanguaueSelecter customLangLink={customLangLink} />

            <SVGMenuDC
              onClick={() => {
                setIsMenuOpen(true);
              }}
              className={`${styles.menu}`}
              {...c.props}
            />
          </div>
        </div>

        {title && (
          <div
            data-prefix={numberText ? "PART" : undefined}
            className={classNames(styles.titleContainer, {
              [styles.titleContainerNoPrefix]: !numberText,
            })}
          >
            {numberText && <span>{`${numberText}.`}</span>}
            <h3>{title}</h3>
          </div>
        )}
      </div>
      <Menu isMenuOpen={isMenuOpen} onClose={() => setIsMenuOpen(false)} />
    </>
  );
};
