import { CursorStateContext } from "@/context/CursorStateContext";
import classNames from "classnames";
import { useContext, useEffect, useRef, useState } from "react";
import { useSpring, animated } from "react-spring";
import { useUpdateEffect } from "react-use";
import styles from "./FancyButton.module.scss";

export const FancyButton = ({
  htmlType,
  children,
  className,
  innerClassName,
  onClick,
  scaleSize = 20,
  light,
  selected
}) => {
  const [isActive, setIsActive] = useState(false);
  const buttonRef = useRef();
  // const scaleSize = 20;
  const [bgStyles, api] = useSpring(() => ({
    x: 0,
    y: 0,
    scale: 0,
  }));

  const cursorStateContext = useContext(CursorStateContext);
  const lastDataRef = useRef({
    x: 0,
    y: 0,
    dx: 0,
    dy: 0,
  });

  useUpdateEffect(() => {
    const button = buttonRef.current;
    if (!isActive) {
      if (button) {
        const lastData = lastDataRef.current;

        api.start({
          x: lastData.x + 2 * lastData.dx,
          y: lastData.y + 2 * lastData.dy,
        });
      }

      return;
    }

    cursorStateContext.setState("fancyButton");
    if (button) {
      let lastData = {};
      const handleMove = (e) => {
        api.start({
          x: e.offsetX,
          y: e.offsetY,
        });
        lastDataRef.current = {
          x: e.offsetX,
          y: e.offsetY,
          dx: e.offsetX - lastDataRef.current.x,
          dy: e.offsetY - lastDataRef.current.y,
        };
      };
      api.start({
        scale: scaleSize,
      });
      button.addEventListener("mousemove", handleMove);

      return () => {
        cursorStateContext.setState("");
        api.start({
          scale: 0,
        });
        button.removeEventListener("mousemove", handleMove);
      };
    }
  }, [isActive]);

  return (
    <button
      type={htmlType}
      ref={buttonRef}
      onClick={onClick}
      className={classNames("button", styles.button, className, {
        [styles.light]: light,
        [styles.selected]: selected,
      })}
      onMouseEnter={() => setIsActive(true)}
      onMouseLeave={() => setIsActive(false)}
    >
      <div
        style={{ transform: "translateZ(0px)", pointerEvents: "none" }}
        className={innerClassName}
      >
        {children}
      </div>
      <animated.div
        style={bgStyles}
        className={styles["button-bg"]}
      ></animated.div>
    </button>
  );
};
