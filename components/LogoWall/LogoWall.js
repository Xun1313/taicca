import { usePageParallaxUpdate } from "@/hooks/usePageParallaxUpdate";
import { useRWD } from "@/hooks/useRwd";
import { calcWeb } from "@/utilits/calcWeb";
import React from "react";
import { ParallaxBanner } from "react-scroll-parallax";
import styles from "./LogoWall.module.scss";

export const LogoWall = ({ deskHeight = 460 }) => {
  const rwd = useRWD();
  usePageParallaxUpdate();

  return (
    <ParallaxBanner
      className={styles.container}
      layers={[
        {
          image: "/images/logoWall.png",
          amount: 0.3,
        },
        // {
        //   image: "https://foo.com/bar.png",
        //   amount: 0.2,
        // },
      ]}
      style={{
        height: rwd.device === "desktop" ? calcWeb(deskHeight) : "459px",
      }}
    ></ParallaxBanner>
  );
};
