import { message, Alert } from "antd";

export const CommonMsg = ({ type = "success", msg = "" }) => {
  const colorList = {
    success: "#2B641E",
    error: "#71192F",
  }

  message[type]({
    content: (
      <Alert
        message={
          <div
            style={{
              color: colorList[type],
            }}
          >
            {msg}
          </div>
        }
        type={type}
        showIcon
        closable
      />
    ),
    icon: <div></div>,
    style: {
      marginTop: "40vh",
    },
  });
};
