import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build";
import { useRouter } from "next/router";

const editorConfiguration = {
  language: "zh",
  toolbar: [
    "heading",
    "|",
    "bold",
    "italic",
    "link",
    "bulletedList",
    "numberedList",
    "|",
    "outdent",
    "indent",
    "|",
    "blockQuote",
    "insertTable",
    "mediaEmbed",
    "undo",
    "redo",
  ],
};

const CustomEditor = ({ initialData, setData }) => {
  const router = useRouter();
  const locale = router.locale;

  return (
    <CKEditor
      editor={Editor}
      config={editorConfiguration}
      data={initialData}
      onReady={(editor) => {
        // You can store the "editor" and use when it is needed.
        // console.log("Editor is ready to use!", editor);
      }}
      onBlur={(event, editor) => {
        // console.log("Blur.", editor);
      }}
      onFocus={(event, editor) => {
        // console.log("Focus.", editor);
      }}
      onChange={(event, editor) => {
        const data = editor.getData();
        setData(data);
        console.log({ event, editor, data });
      }}
    />
  );
};

export { CustomEditor };
