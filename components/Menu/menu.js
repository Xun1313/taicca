import styles from "./menu.module.scss";

import SVGClose from "@/svgs/close.svg";
import SVGMember2 from "@/svgs/member2.svg";
import classNames from "classnames";
import { motion, AnimatePresence } from "framer-motion";
import Link from "next/link";
import { useRouter } from "next/router";
import { Join } from "../Join/Join";
import SVGNanoLogo from "@/svgs/nanoLogo.svg";
import { SearchInput } from "../SearchInput/SearchInput";
import { useTranslation } from 'next-i18next';
import { TLink } from "../TLink";
import { useCursorControl } from "@/context/CursorStateContext";

const links = [
  {
    title: "menu.home",
    number: "00",
    path: "/",
  },
  {
    title: "meun.professionals",
    number: "01",
    path: "/professionals",
  },
  {
    title: "meun.content",
    number: "02",
    path: "/content",
  },
  {
    title: "meun.selections",
    number: "03",
    path: "/selections",
  },
  {
    title: "meun.news",
    number: "04",
    path: "/news",
  },
  {
    title: "meun.resources",
    number: "05",
    path: "/resources",
  },
  {
    title: "meun.about",
    number: "06",
    path: "/about",
  },
  {
    title: "meun.memberCenter",
    number: "07",
    path: "/signin",
  },
];

const variantsRoot = {
  open: {
    opacity: 1,
    translateX: "0%",
    transition: {
      ease: "easeOut",
    },
  },
  closed: {
    opacity: 0,
    translateX: "100%",
    transition: {
      ease: "easeOut",
      delay: 0.1,
    },
  },
};
export const Menu = ({ onClose, isMenuOpen }) => {
  const router = useRouter();
  const { t } = useTranslation();
  const c = useCursorControl("link");
  return (
    <motion.div
      className={classNames(styles.container, 'menu-root')}
      variants={variantsRoot}
      initial={"closed"}
      animate={isMenuOpen ? "open" : "closed"}
    >
      <div className={classNames("mobile-only", styles.mobileTop)}>
        <span>
          <Link legacyBehavior href="/signin">
            <a onClick={onClose}>
              <SVGMember2 />
            </a>
          </Link>
        </span>
      </div>
      <SearchInput
        className={classNames(styles.searchMobile, "mobile-only")}
        onAction={(value) => {
          router.push(`/search?text=${value}`);
          onClose();
        }}
      />
      <nav className={"enMenuLi"}>
        <ul>
          {links.map((link) => {
            return (
              <Link legacyBehavior key={link.number} href={link.path} passHref>
                <li
                  data-num={`${link.number}.`}
                  onClick={onClose}
                  className={classNames({
                    [styles.active]: router.pathname == link.path,
                  })}
                  {...c.props}
                >
                  {t(link.title)}
                </li>
              </Link>
            );
          })}
        </ul>
      </nav>
      <a onClick={onClose} {...c.props} href="javascript:void(0);">
        <SVGClose className={styles.close} />
      </a>
    </motion.div>
  );
};
