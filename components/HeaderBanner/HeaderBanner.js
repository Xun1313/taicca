import { useRWD } from "@/hooks/useRwd";
import { calcWeb } from "@/utilits/calcWeb";
import classNames from "classnames";
import React from "react";
import { useRouter } from "next/router";
import { ParallaxBanner } from "react-scroll-parallax";
import styles from "./HeaderBanner.module.scss";
import { TLink } from "@/components/TLink";
import { useTranslation } from 'next-i18next';

export default function HeaderBanner({
  numberText,
  title,
  image = "/images/bannerBG01.png",
}) {
  const rwd = useRWD();
  return (
    <ParallaxBanner
      layers={[
        {
          image: image,
          amount: 0.1,
        },
      ]}
      style={{
        height: rwd.device === "mobile" ? 300 : calcWeb(500),
      }}
    >
      <div className={styles.content}>
        <div>
          <div>{numberText}</div>
          <h1>{title}</h1>
        </div>
      </div>
    </ParallaxBanner>
  );
}

export const HeaderBannerLight = ({ numberText, title }) => {
  const rwd = useRWD();
  return (
    <div className={styles.contentLight}>
      <div
        data-prefix={numberText ? "PART" : undefined}
        className={classNames(styles.titleContainer, {
          [styles.titleContainerNoPrefix]: !numberText,
        })}
      >
        {numberText && <span>{`${numberText}.`}</span>}
        <h3>{title}</h3>
      </div>
    </div>
  );
};

export const HeaderBannerNew1 = ({
  title,
  desc,
  navList = [],
  backLink,
  backText,
  showBack = false,
  action,
  descClass
}) => {
  // const rwd = useRWD();
  const router = useRouter();
  const { t } = useTranslation("common");
  backText = backText ? backText : t("previous.button");

  return (
    <ParallaxBanner
      layers={[
        {
          image: "/images/headerBG.png",
          amount: 0.2,
        },
      ]}
      style={{
        height: "auto",
        // height: rwd.device === "mobile" ? 150 : calcWeb(150),
      }}
    >
      <div
        className={classNames("contentContainer", styles.contentNew1, {
          [styles.isDesc]: desc,
        })}
      >
        {navList.length > 0 && (
          <div className={styles.navList}>
            <TLink href={"/"}>{t("menu.home")}</TLink>
            {navList.map((e, i) => {
              if (e.link) {
                return <TLink key={i} href={e.link}>{` > ${e.text}`}</TLink>
              } else {
                return <span key={i}>{` > ${e.text}`}</span>
              }
            })}
          </div>
        )}
        {showBack && (
          <div
            className={styles.back}
            onClick={() => {
              backLink ? router.replace(backLink) : router.back();
            }}
          >
            {backText && `< ${backText}`}
          </div>
        )}

        {title && <div className={classNames("enMainTitle", styles.title)}>{title}</div>}
        {desc && <div className={classNames(styles.desc, descClass)}>{desc}</div>}
        {action && <div className={styles.action}>{action}</div>}
      </div>
    </ParallaxBanner>
  );
};
