import classNames from "classnames";
import styles from "./CommonLightBox.module.scss";
import { Modal, Row } from "antd";
import { FancyButton } from "@/components/FancyButton/FancyButton";
import SVGLBX from "@/svgs/close.svg";

export const CommonLightBox = ({
  className,
  titleClass,
  descClass,
  button1Class,
  button2Class,
  containerClass,
  title,
  desc,
  button1,
  button2,
  showModal,
  setShowModal,
  showButton1 = true,
  showButton2 = false,
  onClose1 = () => setShowModal(false),
  onClose2 = () => setShowModal(false),
}) => {
  return (
    <Modal
      className={classNames(className)}
      visible={showModal}
      footer={null}
      onCancel={() => {
        setShowModal(false)
      }}
      width={600}
      closeIcon={<SVGLBX className={styles.lightboxClose} />}
      maskClosable={false}
    >
      <div className={classNames(styles.noticeContainer, containerClass)}>
        <div className={classNames(styles.noticeTitle, titleClass)}>{title}</div>
        <div className={classNames(styles.noticeDesc, descClass)}>{desc}</div>
        <div className={styles.buttonGroup}>
          {showButton1 && <div
            className={classNames(styles.submit, button1Class)}
            onClick={() => onClose1()}
          >
            <FancyButton style={{ width: "100%" }}>{button1}</FancyButton>
          </div>}
          {showButton2 && <div
            className={classNames(styles.submit, button2Class)}
            onClick={() => onClose2()}
          >
            <FancyButton style={{ width: "100%" }}>{button2}</FancyButton>
          </div>}
        </div>
      </div>
    </Modal>
  );
};
