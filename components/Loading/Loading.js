import classNames from "classnames";
import styles from "./Loading.module.scss";

export const Loading = () => {
  return (
    <div className={styles.container}>
      <div className={styles.loadingText}>LOADING</div>
      <div className={styles.loader}>
        <span className={classNames(styles.dot, styles.dot_1)}></span>
        <span className={classNames(styles.dot, styles.dot_2)}></span>
        <span className={classNames(styles.dot, styles.dot_3)}></span>

        <span className={classNames(styles.dot, styles.dot_1_animation)}></span>
      </div>
    </div>
  );
};
