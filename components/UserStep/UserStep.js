import styles from "./UserStep.module.scss";
import classNames from "classnames";
import SVGTick from "@/svgs/tick.svg";
import SVGTickInactive from "@/svgs/tick-inactive.svg";
import Link from "next/link";
import router, { useRouter } from "next/router";

export const UserStep = ({
  isMoble,
  step,
  className,
  t,
  stepList,
  ...rest
}) => {
  return (
    <div className={classNames(styles.container)}>
      {stepList[0].show && (
        <div
          className={classNames(styles.step, {
            [styles.activeStatic]: step === 1,
          }, "enStyle")}
        >
          <Link legacyBehavior href={stepList[0].link}>
            <a className={classNames(styles.title)}>{stepList[0].title}</a>
          </Link>
          <div className={classNames(styles.progressBg)}>
            <div
              className={classNames(styles.progress)}
              style={{ width: `${stepList[0].progress}%` }}
            ></div>
          </div>
          <div className={classNames(styles.amount)}>
            {stepList[0].progress === 100 ? (
              step === 1 ? (
                <SVGTick />
              ) : (
                <SVGTickInactive />
              )
            ) : (
              `${stepList[0].progress}%`
            )}
          </div>
        </div>
      )}

      {stepList[1].show && (
        <div
          className={classNames(styles.step, {
            [styles.activeStatic]: step === 2,
          }, "enStyle")}
        >
          <Link legacyBehavior href={stepList[1].link}>
            <a className={classNames(styles.title)}>{stepList[1].title}</a>
          </Link>
          <div className={classNames(styles.progressBg)}>
            <div
              className={classNames(styles.progress)}
              style={{ width: `${stepList[1].progress}%` }}
            ></div>
          </div>
          <div className={classNames(styles.amount)}>
            {stepList[1].progress === 100 ? (
              step === 2 ? (
                <SVGTick />
              ) : (
                <SVGTickInactive />
              )
            ) : (
              `${stepList[1].progress}%`
            )}
          </div>
        </div>
      )}

      {stepList[2].show && (
        <div
          className={classNames(styles.step, {
            [styles.activeStatic]: step === 3,
          }, "enStyle")}
        >
          <Link legacyBehavior href={stepList[2].link}>
            <a className={classNames(styles.title)}>{stepList[2].title}</a>
          </Link>
          <div className={classNames(styles.progressBg)}>
            <div
              className={classNames(styles.progress)}
              style={{ width: `${stepList[2].progress}%` }}
            ></div>
          </div>
          <div className={classNames(styles.amount)}>
            {stepList[2].progress === 100 ? (
              step === 3 ? (
                <SVGTick />
              ) : (
                <SVGTickInactive />
              )
            ) : (
              `${stepList[2].progress}%`
            )}
          </div>
        </div>
      )}

      {stepList[3].show && (
        <div
          className={classNames(styles.step, {
            [styles.activeStatic]: step === 4,
          }, "enStyle")}
        >
          <Link legacyBehavior href={stepList[3].link}>
            <a className={classNames(styles.title)}>{stepList[3].title}</a>
          </Link>
          <div className={classNames(styles.progressBg)}>
            <div
              className={classNames(styles.progress)}
              style={{ width: `${stepList[3].progress}%` }}
            ></div>
          </div>
          <div className={classNames(styles.amount)}>
            {stepList[3].progress === 100 ? (
              step === 4 ? (
                <SVGTick />
              ) : (
                <SVGTickInactive />
              )
            ) : (
              `${stepList[3].progress}%`
            )}
          </div>
        </div>
      )}
    </div>
  );
};
