import classNames from "classnames";
import { Loading } from "@/components/Loading/Loading";
import styles from "./GlobalLoading.module.scss";

export const GlobalLoading = () => {
  return (
    <div className={styles.container}>
      <Loading />
    </div>
  );
};
