import React from "react";
import { useWindowSize } from "react-use";

import { Swiper, SwiperRef, SwiperSlide } from "swiper/react";
import styles from "./WorkCardList.module.scss";
import { calcWeb1440Count } from "@/utilits/calcWeb";
import Link from "next/link";
import FavoriteIcon from "../FavoriteIcon/FavoriteIcon";

export default function WorkCardList({
  works,
  sourceItems,
  showFavorites,
  favoriteIds,
  onFavorite,
  favoriteText,
  isLogin
}) {
  const windowSize = useWindowSize();
  const isMobile = windowSize.width < 960;
  const marginH = (windowSize.width - calcWeb1440Count(1280, windowSize)) / 2;

  return (
    <Swiper
      slidesPerView={"auto"}
      spaceBetween={50}
      slidesOffsetAfter={isMobile ? 18 : marginH}
      slidesOffsetBefore={isMobile ? 18 : marginH}
      slidesPerGroup={isMobile ? 1 : 4}
      updateOnWindowResize={true}
    >
      {works.map((u) => (
        <SwiperSlide key={u.id} className={styles.workCardContainer}>
          <Link legacyBehavior href={`/content/${u.id}/${u.name_en}`}>
            <a className={styles.workCard}>
              <img src={u.pic_min} className={styles.workCardImg} alt="icon" />
              <div className={styles.workTitleGroup}>
                <div className={styles.workCardName}>
                  <div className={styles.workCardNameEng}>{u.name_en}</div>
                  <div className={styles.workCardNameZh}>{u.name_zh}</div>
                </div>
                <FavoriteIcon
                  tooltip={favoriteText}
                  className={styles.favoriteIcon}
                  isFavorite={favoriteIds.indexOf(u.id) !== -1}
                  onClick={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    onFavorite(u.id);
                  }}
                  isLogin={isLogin}
                />
              </div>

              <div className={styles.workCardWorkform}>
                {u.workform?.map?.((h) => (
                  <div key={h}>{sourceItems.artform[h]}</div>
                ))}
              </div>
              <div className={styles.workCardArtType}>
                {u.gclass?.map?.((h) => (
                  <div key={h}>{sourceItems.artType[h]}</div>
                ))}
              </div>
            </a>
          </Link>
        </SwiperSlide>
      ))}
    </Swiper>
  );
}
