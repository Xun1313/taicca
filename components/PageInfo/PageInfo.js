import { useRWD } from "@/hooks/useRwd";
import { calcWeb, calcWeb1440 } from "@/utilits/calcWeb";
import classNames from "classnames";
import { useRouter } from "next/router";
import React from "react";
import { useTranslation } from 'next-i18next';
import { ParallaxBanner } from "react-scroll-parallax";
import styles from "./PageInfo.module.scss";
import Link from "next/link";
import { ALink, TLink } from "../TLink";

export default function PageInfo({
  title = "活動公告",
  navTitle = "共創夥伴",
  desc = "後疫情時代，內容產業全面進入虛實整合的全新型態！帶來前所未有的嶄新挑戰與機會。與我們一起合作，在創作的路上發光發熱！",
  children,
}) {
  const rwd = useRWD();
  const router = useRouter();
  const { t } = useTranslation("common");
  return (
    <ParallaxBanner
      layers={[
        {
          image: "/images/infoBG.png",
          amount: 0.2,
        },
      ]}
      style={{
        height: rwd.device === "mobile" ? 360 : calcWeb(360),
      }}
    >
      <div className={styles.container}>
        <div className={styles.top}>
          <div className={styles.nav}>
            <TLink href={"/"}>{`${t("menu.home")} > `}</TLink>
            <span>{navTitle}</span>
          </div>
          <h3>{title}</h3>
          <div className={classNames("mobile-only", styles.info)}>{desc}</div>
        </div>
        <div className={styles.bottom}>
          {children}
          <div className={classNames("desktop-only", styles.info)}>
            <p>{desc}</p>
          </div>
        </div>
      </div>
    </ParallaxBanner>
  );
}

export const PageInfoBasic = ({
  isCustom = false,
  text = "",
  link = "",

  backText,
  title,
  action,
}) => {
  const { t } = useTranslation("common");
  const rwd = useRWD();
  const router = useRouter();
  return (
    <ParallaxBanner
      layers={[
        {
          image: "/images/infoBG.png",
          amount: 0.2,
        },
      ]}
      style={{
        height: rwd.device === "mobile" ? 80 : calcWeb(120),
      }}
    >
      <div className={styles.simpleContainer}>
        <div className={styles.simple}>
          {isCustom ? (
            <div
              className={styles.back}
              onClick={() => {
                router.push(link);
              }}
            >
              <ALink>{`< ${text}`}</ALink>
            </div>
          ) : (
            <div
              className={styles.back}
              onClick={() => {
                router.back();
              }}
            >
              <ALink>{`< ${backText || t("previous.button")}`}</ALink>
            </div>
          )}
        </div>
        <div className={styles.title}>{title}</div>
        <div>{action}</div>
      </div>
    </ParallaxBanner>
  );
};

export const PageInfoSearch = ({ children }) => {
  const { t } = useTranslation("common");
  const rwd = useRWD();
  const router = useRouter();
  return (
    <ParallaxBanner
      layers={[
        {
          image: "/images/infoBG.png",
          amount: 0.2,
        },
      ]}
      style={{
        height: rwd.device === "mobile" ? 150 : calcWeb(210),
      }}
    >
      <div className={styles.searchContainer}>{children}</div>
    </ParallaxBanner>
  );
};
