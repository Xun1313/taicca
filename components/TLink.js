import { useCursorControl } from "@/context/CursorStateContext";
import Link from "next/link";

export const TLink = ({ children, href, target, onClick, rel, ...rest }) => {
  const cursorControl = useCursorControl("link");
  return (
    <Link legacyBehavior href={href} {...rest}>
      <a
        rel={rel}
        onClick={onClick}
        target={target}
        onMouseOver={() => cursorControl.props.onMouseEnter()}
        onMouseOut={() => cursorControl.props.onMouseLeave()}
        // {...cursorControl.props}
      >
        {children}
      </a>
    </Link>
  );
};

export const ALink = ({ children, href, target, onClick, rel, ...rest }) => {
  const cursorControl = useCursorControl("link");
  return (
    <a
      rel={rel}
      onClick={onClick}
      target={target}
      onMouseOver={() => cursorControl.props.onMouseEnter()}
      onMouseOut={() => cursorControl.props.onMouseLeave()}
      {...rest}
      // {...cursorControl.props}
    >
      {children}
    </a>
  );
};
