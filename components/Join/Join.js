import SVGJoin from "@/svgs/join.svg";
import SVGJoinM from "@/svgs/joinM.svg";
import classNames from "classnames";
import styles from "./Join.module.scss";
import AIcon from "@/svgs/AIcon.svg";
import Link from "next/link";
import { useRouter } from "next/router";
import { useTranslation } from 'next-i18next';
import { useCursorControl } from "@/context/CursorStateContext";
import { FancyButton } from "../FancyButton/FancyButton";

export const Join = ({ showBg, onClick, className }) => {
  const router = useRouter();
  // const c = useCursorControl();
  const { t } = useTranslation("common");
  return (
    <div
      className={classNames(styles.wraper, { [styles.bg]: showBg }, className)}
    >
      <div>
        {t("professionals.join")}
        <FancyButton
          scaleSize={32}
          onClick={() => {
            router.push("/join");
            onClick?.();
          }}
          className={classNames(styles.button)}
          innerClassName={styles.buttonInner}
          // {...c.props}
        >
          {t("professionals.join.button")}
          <AIcon />
        </FancyButton>
      </div>

      {/* <Link href={"/join"}>
        <a onClick={onClick}>
          <div
            className={classNames(styles.container, { [styles.bg]: showBg })}
          >
            <SVGJoin className={classNames("desktop-only", "joinIcon")} />
            <SVGJoinM className={classNames("mobile-only", "joinIcon")} />
          </div>
        </a>
      </Link> */}
    </div>
  );
};
