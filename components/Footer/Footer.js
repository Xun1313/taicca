import styles from "./Footer.module.scss";
import SVGLogoDC from "@/svgs/logoDC_footer.svg";
import { useTranslation } from 'next-i18next';
import Link from "next/link";
import { useRouter } from "next/router";

export const Footer = () => {
  const { t } = useTranslation("common");
  const { locale } = useRouter();
  return (
    <>
      <div className={styles.container}>
        <div className={styles.topArea}>
          <SVGLogoDC className={styles.logo} />
          {/* <img className={styles.logo2} src={"/images/logo2.png"} />
          <img className={styles.logo2} src={"/images/logo3.png"} /> */}
        </div>
        <div className={styles.middleArea}>
          <a
            href={
              locale === "zh"
                ? "https://www.facebook.com/taicca.tw"
                : "https://www.facebook.com/TAICCA.Global/"
            }
            target="_blank"
            rel="noreferrer"
          >
            <div className={styles.socialIcon}>
              <img src={"/images/fb.png"} alt="facebook"></img>
            </div>
          </a>
          <a
            href="https://twitter.com/TAICCA_Official"
            target="_blank"
            rel="noreferrer"
          >
            <div className={styles.socialIcon}>
              <img src={"/images/twitter.png"} alt="twitter"></img>
            </div>
          </a>
          <a
            href="https://twitter.com/TAICCA_Official"
            target="_blank"
            rel="noreferrer"
          >
            <div className={styles.socialIcon}>
              <img src={"/images/linkedin.png"} alt="linkedin"></img>
            </div>
          </a>
          <a
            href="https://twitter.com/TAICCA_Official"
            target="_blank"
            rel="noreferrer"
          >
            <div className={styles.socialIcon}>
              <img src={"/images/IG.png"} alt="IG"></img>
            </div>
          </a>
        </div>
        <div className={styles.bottomArea}>
          <div className={styles.info}>
            {t("footer.tel")}
            <br />
            {t("footer.address")}
          </div>
          <div className={styles.links}>
            <div className={styles.socialIconGroup}>
              <a
                href={
                  locale === "zh"
                    ? "https://www.facebook.com/taicca.tw"
                    : "https://www.facebook.com/TAICCA.Global/"
                }
                target="_blank"
                rel="noreferrer"
              >
                <div className={styles.socialIcon}>
                  <img src={"/images/fb.png"} alt="facebook"></img>
                </div>
              </a>
              <a
                href="https://twitter.com/TAICCA_Official"
                target="_blank"
                rel="noreferrer"
              >
                <div className={styles.socialIcon}>
                  <img src={"/images/twitter.png"} alt="twitter"></img>
                </div>
              </a>
              <a
                href="https://www.linkedin.com/company/taicca-taiwan/"
                target="_blank"
                rel="noreferrer"
              >
                <div className={styles.socialIcon}>
                  <img src={"/images/linkedin.png"} alt="linkedin"></img>
                </div>
              </a>
              <a
                href="https://www.instagram.com/taicca.tw/"
                target="_blank"
                rel="noreferrer"
              >
                <div className={styles.socialIcon}>
                  <img src={"/images/IG.png"} alt="IG"></img>
                </div>
              </a>
            </div>
            <a href="mailto:futurecontent@taicca.tw">
              {t("footer.link.contact")}
            </a>
            {` | `}
            <Link legacyBehavior href="/terms">{t("footer.link.terms")}</Link>
            {` | `}
            <Link legacyBehavior href="/privacy">{t("footer.link.privacy")}</Link>
          </div>
        </div>
      </div>
      <div className={styles.copyright}>
        <div>
          Copyright © {new Date().getFullYear()} TAICCA (Taiwan Creative Content Agency). All rights
          reserved.
        </div>
      </div>
    </>
  );
};
