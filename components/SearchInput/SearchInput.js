import { Input } from "antd";
import SVGSearch from "@/svgs/searchDC.svg";
import styles from "./SearchInput.module.scss";
import classNames from "classnames";
import { useCallback, useMemo, useRef, useState } from "react";
import { motion } from "framer-motion";
import { useOnClickOutside } from "@/hooks/useOnClickOutSide";
import { useWindowSize } from "react-use";
import { useRouter } from "next/router";

export const SearchInput = ({ className, onAction, onChange }) => {
  const router = useRouter();
  const inputRef = useRef();

  const action = useCallback(() => {
    const value = inputRef.current?.state?.value;

    onAction?.(value);
    onChange?.(value);
  }, []);

  return (
    <div className={classNames(styles.searchInput, className)}>
      <Input
        ref={inputRef}
        placeholder="Search"
        onKeyPress={(e) => {
          if (e.key === "Enter") {
            action();
          }
        }}
      />
      <SVGSearch onClick={action} />
    </div>
  );
};

const containerVariants = {
  close: { width: 49 },
  open: { width: 400 },
};

const inputVariants = {
  close: { opacity: 0 },
  open: { opacity: 1 },
};

export const SearchInputA = ({
  className,
  float,
  onSeachOpenChange,
  ...rest
}) => {
  // const windowSize = useWindowSize();
  // const targetSize =
  //   windowSize.width > 1600 ? 400 : (windowSize.width / 1600) * 400;
  // const containerVariants = useMemo(
  //   () => ({
  //     close: { width: 49 },
  //     open: { width: targetSize },
  //   }),
  //   [targetSize]
  // );
  const [isOpen, setIsOpen] = useState(false);
  const inputRef = useRef();
  const [inputValue, setInputValue] = useState("");
  const router = useRouter();
  const containerRef = useRef();

  const isOpenRef = useRef(isOpen);
  isOpenRef.current = isOpen;
  useOnClickOutside(containerRef, () => {
    if (isOpenRef.current) {
      setInputValue("");

      setIsOpen(false);
      onSeachOpenChange(false);
    }
  });

  const action = () => {
    setIsOpen((p) => true);
    onSeachOpenChange(true);
    if (inputValue) {
      router.push(`/search?text=${inputValue}`);
      setIsOpen(false);
      onSeachOpenChange(false);
      setInputValue("");
    }
  };

  return (
    <motion.div
      initial="close"
      animate={isOpen ? "open" : "close"}
      transition={{
        type: "spring",
        duration: 0.4,
        bounce: 0,
      }}
      variants={containerVariants}
      onAnimationComplete={() => {
        if (isOpen) {
          inputRef.current.focus();
        }
      }}
      ref={containerRef}
      className={classNames(
        styles.searchInputA,
        {
          [styles.float]: float,
        },
        className
      )}
      {...rest}
    >
      <motion.div variants={inputVariants}>
        <Input
          ref={inputRef}
          value={inputValue}
          onChange={e => setInputValue(e.target.value)}
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              action();
            }
          }}
        />
      </motion.div>
      <SVGSearch onClick={action} />
    </motion.div>
  );
};
