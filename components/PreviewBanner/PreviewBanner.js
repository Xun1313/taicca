import React from "react";
import { ParallaxBanner } from "react-scroll-parallax";
import { useRouter } from "next/router";
import styles from './PreviewBanner.module.scss'

export default function PreviewBanner({ backText, previewTitle, action }) {
  const router = useRouter();
  return (
    <ParallaxBanner
      layers={[
        {
          image: "/images/headerBG.png",
          amount: 0.2,
        },
      ]}
      style={{
        height: "auto",
      }}
    >
      <div className={styles.previewContainer}>
        <div className="contentContainer">
          <div className={styles.previewBlock}>
            <div className={styles.back} onClick={() => router.back()}>
              {"<"} {backText}
            </div>
            <div className={styles.preview}>{previewTitle}</div>
            <div className={styles.action}>{action}</div>
          </div>
        </div>
      </div>
    </ParallaxBanner>
  );
}
