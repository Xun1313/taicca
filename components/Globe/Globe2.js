import { useCursorControl } from "@/context/CursorStateContext";
import { categorys } from "@/data/partners";
import { useRWD } from "@/hooks/useRwd";
import { calcWeb } from "@/utilits/calcWeb";
import classNames from "classnames";
import { AnimatePresence, motion } from "framer-motion";
import { useCallback, useMemo, useState } from "react";
import ProfessionalsCard from "../ProfessionalsCard/ProfessionalsCard";
import styles from "./Globe.module.scss";
import Globe from "react-globe.gl";

const plces = [
  {
    title: "map.continents.list.1",
    coordinates: [31.9679992, 91.167997],
  },
  {
    title: "map.continents.list.2",
    coordinates: [48.1284101, 4.1482421],
  },
  {
    title: "map.continents.list.3",
    coordinates: [40.873978, -103.833818],
  },
  {
    title: "map.continents.list.4",
    coordinates: [-14.999788, -59.925606],
  },
  {
    title: "map.continents.list.5",
    coordinates: [-23.190695, 133.411514],
  },
];

const size = 60;

const markersDemo = [
  {
    id: "marker0",
    city: "Taipei",
    color: categorys[0].color,
    coordinates: [25.138948, 121.502828],
    value: size,
  },
  {
    id: "marker1",
    city: "Singapore",
    color: categorys[0].color,
    coordinates: [1.3521, 103.8198],
    value: 54,
  },
  {
    id: "marker2",
    city: "New York",
    color: categorys[2].color,
    coordinates: [40.73061, -73.935242],
    value: 80,
  },
  {
    id: "marker3",
    city: "San Francisco",
    color: categorys[3].color,
    coordinates: [37.773972, -122.431297],
    value: 50,
  },
  {
    id: "marker4",
    city: "Beijing",
    color: categorys[3].color,
    coordinates: [39.9042, 116.4074],
    value: 25,
  },
  {
    id: "marker5",
    city: "London",
    color: categorys[1].color,
    coordinates: [51.5074, 0.1278],
    value: 35,
  },
  {
    id: "marker6",
    city: "Los Angeles",
    color: categorys[4].color,
    coordinates: [29.7604, -95.3698],
    value: 435,
  },
];

export function Globe2({ t, members, locale }) {
  const [globe, setGlobe] = useState();

  const markers = useMemo(
    () =>
      members.map((m) => ({
        id: m.id,
        color: categorys[m.registration - 1].color,
        data: {
          ...m,
          name: m[`name_${locale}`],
          intro: m[`intro_${locale}`],
          color: categorys[m.registration - 1].color,
          type: t(categorys[m.registration - 1].title),
        },
        coordinates: [m.longitude - 0, m.latitude - 0],
        value: size,
      })),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [members]
  );

  const handleZoomEnc = useCallback(() => {
    console.log(globe);
    // const orbitControls = globe.orbitControls;
    // console.log(orbitControls.dollyIn(10));

    // const camera = globe.camera;
    // camera.zoom = 1.2;
    // camera.updateProjectionMatrix();

    // const zoomDistance = 0.5;
    // const currDistance = camera.position.length();
    // const factor = zoomDistance / currDistance;

    // camera.position.x *= factor;
    // camera.position.y *= factor;
    // camera.position.z *= factor;
  }, [globe]);
  const handleZoomDec = useCallback(() => {}, []);
  const [focus, setFocus] = useState(null);
  const rwd = useRWD();
  const c = useCursorControl();
  const [selectPartner, setSelectPartner] = useState(null);

  const pointsData = useMemo(
    () =>
      members.map((m) => ({
        id: m.id,
        color: categorys[m.registration - 1].color,
        data: {
          ...m,
          name: m[`name_${locale}`],
          intro: m[`intro_${locale}`],
          color: categorys[m.registration - 1].color,
          type: t(categorys[m.registration - 1].title),
        },
        coordinates: [m.longitude - 0, m.latitude - 0],
        value: size,
        lat: m.latitude - 0,
        lng: m.longitude - 0,
        size: 0.3,
      })),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [members]
  );

  return (
    <div className={styles.wraper}>
      <div className={styles.container}>
        {/* <ReactGlobe
          markers={markers}
          options={{
            pointLightIntensity: 0,
            enableMarkerGlow: false,
            enableCameraZoom: false,
            focusDistanceRadiusScale: 2.4,
            enableMarkerTooltip: !selectPartner,
            focusOnMarkerClick: false,
            // enableMarkerTooltip: false,
            markerRadiusScaleRange: [0.005, 0.03],
            markerTooltipRenderer: (marker) => marker.data[`name_${locale}`],
          }}
          onDefocus={() => {
            setFocus(null);
          }}
          onClickMarker={(c) => {
            console.log(c);
            setSelectPartner(c.data);
          }}
          onTouchMarker={() => {
            alert();
          }}
          initialCameraDistanceRadiusScale={2.8}
          focus={focus}
          height={rwd.device === "mobile" ? 470 : calcWeb(900)}
          globeTexture={"/images/gobe_from_ioo@6.jpg"}
          globeBackgroundTexture={null}
          globeCloudsTexture={null}
          onGetGlobe={setGlobe}
          width={"100vw"}
        /> */}
        <Globe
          backgroundColor={"#FFFFFF"}
          showAtmosphere={false}
          polygonSideColor={() => "rgba(0, 0, 0, 0)"}
          globeImageUrl={"/images/gobe_from_ioo@6.jpg"}
          pointsData={pointsData}
          pointAltitude="size"
          pointColor="color"
          onPointClick={() => {
            alert();
          }}
          onPoint
        />
        <div className={styles.overlay}>
          <div className={styles.title}>Who are we looking for.</div>
        </div>
        <AnimatePresence>
          {selectPartner && (
            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
            >
              <ProfessionalsCard
                className={styles.professionalsCard}
                name={selectPartner.name}
                intro={selectPartner.intro}
                pic={selectPartner.pic}
                type={selectPartner.type}
                color={selectPartner.color}
                onClose={() => {
                  setSelectPartner(null);
                }}
              />
            </motion.div>
          )}
        </AnimatePresence>
      </div>
      <div className={classNames("droupdown", styles.droupdown)}>
        <select
          value={
            focus === null
              ? ""
              : plces.find((d) => d.coordinates === focus)?.title
          }
          onChange={(e) => {
            if (e.target.value) {
              const newSelection = plces.find(
                (d) => d.title === e.target.value
              );
              if (newSelection) {
                setFocus(newSelection.coordinates);
              }
            } else {
              setFocus(null);
            }
          }}
          defaultValue=""
        >
          <option value="" disabled>
            區域
          </option>
          {plces.map(({ title, coordinates }) => {
            return (
              <option key={title} value={title}>
                {t(title)}
              </option>
            );
          })}
        </select>
      </div>
      <div className={styles.categorys}>
        {categorys.map(({ title, color }) => (
          <div key={title} className={styles.category}>
            <div
              className={styles.dot}
              style={{ backgroundColor: color }}
            ></div>
            {t(title)}
          </div>
        ))}
      </div>
      <div className={classNames("desktop-only", styles.buttons)}>
        {plces.map(({ title, coordinates }) => {
          return (
            <button
              onClick={() => {
                if (coordinates === focus) {
                  setFocus(null);
                } else {
                  setFocus(coordinates);
                }
              }}
              key={title}
              className={classNames("button", {
                active: coordinates === focus,
              })}
              {...c.props}
            >
              {t(title)}
            </button>
          );
        })}
      </div>
    </div>
  );
}
