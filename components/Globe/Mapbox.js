import mapboxgl from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import { useCallback, useEffect, useRef, useState } from "react";
import styles from "./Mapbox.module.scss";
mapboxgl.accessToken =
  "pk.eyJ1IjoiamltbXljaHUiLCJhIjoiY2w1ajNldjNtMGUxbDNicGw0dDkwdnN5MiJ9.DDVf27zwhWURD0-jOdQ_Sg";

export const Mapbox = ({
  markers,
  onClickMarker,
  mapboxActionRef,
  height,
  focusPartner,
}) => {
  const mapContainer = useRef(null);
  const map = useRef(null);
  const [lng, setLng] = useState(121.547752);
  const [lat, setLat] = useState(25.0573607);
  const [zoom, setZoom] = useState(6);
  const markersRef = useRef([]);

  useEffect(() => {
    if (map.current) return; // initialize map only once

    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/jimmychu/cl5j3g1a8000814pauk37hgcp",
      // style: "mapbox://styles/mapbox/streets-v11",
      center: [lng, lat],
      zoom: zoom,
      // projection: "globe",
      attributionControl: false,
    });
  }, []);
  useEffect(() => {
    markersRef.current.forEach((element) => {
      element.remove();
    });
    markersRef.current = [
      ...markers.filter((d) => d.id !== focusPartner?.id),
      ...markers.filter((d) => d.id == focusPartner?.id),
    ]
      .map((m) => {
        const el = document.createElement("div");
        el.className = m.id === focusPartner?.id ? "marker focus" : "marker";
        // el.style.backgroundColor = m.color;
        el.addEventListener("click", function (e) {
          // Prevent the `map.on('click')` from being triggered
          e.stopPropagation();
          onClickMarker(m);
        });

        try {
          return new mapboxgl.Marker(el, { anchor: "bottom" })
            .setLngLat([m.coordinates[1], m.coordinates[0]])
            .addTo(map.current);
        } catch (e) {}
      })
      .filter((d) => d);
  }, [markers, focusPartner]);

  useEffect(() => {
    if (!map.current) return; // wait for map to initialize
    map.current.on("move", () => {
      setLng(map.current.getCenter().lng.toFixed(4));
      setLat(map.current.getCenter().lat.toFixed(4));
      setZoom(map.current.getZoom().toFixed(2));
    });
  });

  if (mapboxActionRef) {
    mapboxActionRef.current = {
      setFocus: (cord, zoom) => {
        try {
          map.current.flyTo({
            center: [cord[1], cord[0]],
            zoom: zoom || map.current.getZoom(),
            essential: true, // this animation is considered essential with respect to prefers-reduced-motion
          });
        } catch (e) {}
      },
    };
  }
  useEffect(() => {
    setTimeout(() => {
      map.current.flyTo({
        center: [121.547752, 25.0573607],
        zoom: 9,
        essential: true, // this animation is considered essential with respect to prefers-reduced-motion
      });
    }, 1000);
  }, []);
  const zoomInc = useCallback(() => {
    map.current.flyTo({
      zoom: map.current.getZoom() + 1,
      essential: true,
    });
  }, []);
  const zoomDec = useCallback(() => {
    map.current.flyTo({
      zoom: map.current.getZoom() - 1,
      essential: true,
    });
  }, []);
  return (
    <div
      style={{
        width: "100%",
        height: height,
      }}
    >
      <div
        style={{
          width: "100%",
          height: "100%",
        }}
        ref={mapContainer}
        className="map-container"
      />
      <div className={styles.zoom}>
        <div onClick={zoomInc}>+</div>
        <div onClick={zoomDec}>−</div>
      </div>
    </div>
  );
};
