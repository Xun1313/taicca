
import { categorys } from "@/data/partners";
import { useRWD } from "@/hooks/useRwd";
import { AnimatePresence, motion } from "framer-motion";
import { useMemo, useRef } from "react";
import styles from "./Globe.module.scss";
import { Mapbox } from "./Mapbox";

const size = 60;

const markersDemo = [
  {
    id: "marker0",
    city: "Taipei",
    color: categorys[0].color,
    coordinates: [25.138948, 121.502828],
    value: size,
  },
  {
    id: "marker1",
    city: "Singapore",
    color: categorys[0].color,
    coordinates: [1.3521, 103.8198],
    value: 54,
  },
  {
    id: "marker2",
    city: "New York",
    color: categorys[2].color,
    coordinates: [40.73061, -73.935242],
    value: 80,
  },
  {
    id: "marker3",
    city: "San Francisco",
    color: categorys[3].color,
    coordinates: [37.773972, -122.431297],
    value: 50,
  },
  {
    id: "marker4",
    city: "Beijing",
    color: categorys[3].color,
    coordinates: [39.9042, 116.4074],
    value: 25,
  },
  {
    id: "marker5",
    city: "London",
    color: categorys[1].color,
    coordinates: [51.5074, 0.1278],
    value: 35,
  },
  {
    id: "marker6",
    city: "Los Angeles",
    color: categorys[4].color,
    coordinates: [29.7604, -95.3698],
    value: 435,
  },
];

export function Globe({
  t,
  members,
  locale,
  selectPartner,
  setSelectPartner,
  showBigTitle,
  actionRef,
  focusPartner,
}) {
  const markers = useMemo(
    () =>
      members.map((m) => ({
        id: m.id,
        color: m.registration
          ? categorys[m.registration[0] - 1].color
          : "#E53C2B",
        data: {
          ...m,

          color: m.registration
            ? categorys[m.registration[0] - 1].color
            : "#E53C2B",
          type: m.registration ? t(categorys[m.registration[0] - 1].title) : "",
        },
        coordinates: [m.latitude - 0, m.longitude - 0],
        value: size,
      })),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [members]
  );

  const rwd = useRWD();

  const mapboxActionRef = useRef();

  if (actionRef) {
    actionRef.current = {
      setFocus: mapboxActionRef.current?.setFocus,
    };
  }
  return (
    <div className={styles.wraper}>
      <div className={styles.container}>
        <Mapbox
          markers={markers}
          onClickMarker={(c) => {
            setSelectPartner(c.data);
          }}
          mapboxActionRef={mapboxActionRef}
          focusPartner={focusPartner}
          height={rwd.device === "mobile" ? 470 : 820}
        />
        <AnimatePresence>
          {showBigTitle && (
            <motion.div
              className={styles.overlay}
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
            >
              <div className={styles.title}>Who are we looking for.</div>
            </motion.div>
          )}
        </AnimatePresence>
      </div>
      {/* <div className={classNames("mobile-only", "droupdown", styles.droupdown)}>
        <select
          value={
            focus === null
              ? ""
              : places.find((d) => d.coordinates === focus)?.title
          }
          onChange={(e) => {
            if (e.target.value) {
              const newSelection = places.find(
                (d) => d.title === e.target.value
              );
              if (newSelection) {
                setFocus(newSelection.coordinates);
              }
            } else {
              setFocus(null);
            }
          }}
          defaultValue=""
        >
          <option value="" disabled>
            區域
          </option>
          {places.map(({ title, coordinates }) => {
            return (
              <option key={title} value={title}>
                {t(title)}
              </option>
            );
          })}
        </select>
      </div> */}
      {/* <div
        className={classNames(styles.categorys, {
          [styles.en]: locale === "en",
        })}
      >
        {categorys.map(({ title, color }) => (
          <div key={title} className={classNames(styles.category)}>
            <div
              className={styles.dot}
              style={{ backgroundColor: color }}
            ></div>
            {t(title)}
          </div>
        ))}
      </div> */}
      {/* <div className={classNames("desktop-only", styles.buttons)}>
        {places.map(({ title, coordinates }) => {
          return (
            <FancyButton
              onClick={() => {
                if (coordinates === focus) {
                  setFocus(null);
                } else {
                  setFocus(coordinates);
                }
              }}
              key={title}
              className={classNames({
                active: coordinates === focus,
              })}
              {...c.props}
            >
              {t(title)}
            </FancyButton>
          );
        })}
      </div> */}
    </div>
  );
}
