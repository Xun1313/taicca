import React from 'react'
import { categorys as categories } from "@/data/partners";
import styles from './Registrations.module.scss'

export default function Registrations({registration, t}) {
  return (
    registration.map((d) => {
      const category = categories[d - 1];

      return (
        <span className={styles.sphere} key={category.title}>
          <dot style={{ backgroundColor: category.color }} />
          {t(category.title)}
        </span>
      );
    })
  )
}
