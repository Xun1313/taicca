import Link from "next/link";
import styles from "./SelectionsCard.module.scss";

export const SelectionsCard = ({ data }) => {
  return (
    <Link legacyBehavior href={`/selections/${data.slug}`}>
      <a className={styles.selectionsCard}>
        <img src={data.pic} alt={data.title} />
        <div className={styles.content}>
          <div className={styles.tag}>{data.datest}</div>

          <div className={styles.title}>{data.title}</div>
        </div>
      </a>
    </Link>
  );
};
