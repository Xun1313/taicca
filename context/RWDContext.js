import { useRWD, useRWDRaw } from "@/hooks/useRwd";
import { createContext } from "react";

export const RWDContext = createContext({ isSSR: true, device: "" });

export const RWDContextProvider = ({ children }) => {
  const rwd = useRWDRaw();
  return <RWDContext.Provider value={rwd}>{children}</RWDContext.Provider>;
};

export const RWD = ({ children }) => {
  const rwd = useRWD();
  return children(rwd);
};
