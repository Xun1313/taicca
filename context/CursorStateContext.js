import React, {
  createContext,
  useCallback,
  useContext,
  useRef,
  useState,
} from "react";

export const CursorStateContext = createContext({
  state: "",
  setState: () => {},
  stateRef: React.createRef(),
});

const useCursorState = () => {
  const [state, setState] = useState("");
  const stateRef = useRef(state);
  stateRef.current = state;

  return {
    state,
    setState,
    stateRef,
  };
};

export const CursorStateContextPrivider = ({ children }) => {
  const stateData = useCursorState();
  return (
    <CursorStateContext.Provider value={stateData}>
      {children}
    </CursorStateContext.Provider>
  );
};

export const useCursorControl = (type = "button") => {
  const cursorData = useContext(CursorStateContext);
  const handleEnter = useCallback(() => {
    cursorData.setState(type);
  }, [cursorData, type]);
  const handleLeave = useCallback(() => {
    cursorData.setState("");
  }, [cursorData]);

  return {
    props: {
      onMouseEnter: handleEnter,
      onMouseLeave: handleLeave,
    },
  };
};
