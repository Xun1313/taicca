import { useState, createContext } from "react";
import { GlobalLoading } from "@/components/GlobalLoading/GlobalLoading";

export const LoadingContext = createContext();

export const LoadingContextProvider = ({ children }) => {
  const [showLoading, setShowLoading] = useState(false);

  const value = {
    showLoading,
    setShowLoading,
  };
  return (
    <LoadingContext.Provider value={value}>
      {showLoading && <GlobalLoading />}
      {children}
    </LoadingContext.Provider>
  );
};
