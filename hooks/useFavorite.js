import { collectmJoin } from "@/api";
import { LoadingContext } from "@/context/LoadingContext";
import { useCallback, useContext, useState } from "react";

export const useFavorite = ({ initFavoriteIds, type, authKey, memberId }) => {
  const { setShowLoading } = useContext(LoadingContext);
  const [favoriteIds, setFavoriteIds] = useState(initFavoriteIds);

  const handleFavorite = useCallback(
    async (id) => {
      setShowLoading(true);
      const { resu } = await collectmJoin({
        auth_key: authKey,
        memberm1_no: memberId,
        id,
        join: favoriteIds.indexOf(id) !== -1 ? 0 : 1,
        type,
      });

      if (resu === 1) {
        setFavoriteIds((prev) =>
          prev.indexOf(id) !== -1 ? prev.filter((v) => v !== id) : [...prev, id]
        );
      }
      setShowLoading(false);
    },
    [setShowLoading]
  );

  return {
    idList: favoriteIds,
    handleFavorite,
  };
};
