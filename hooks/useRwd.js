import { RWDContext } from "@/context/RWDContext";
import { useEffect, useLayoutEffect, useState, useContext } from "react";
import { useMedia, useWindowSize } from "react-use";

const isBrowser = typeof window !== "undefined";

const useMedia2 = (query, defaultState = false) => {
  const [state, setState] = useState(defaultState);

  useEffect(() => {
    let mounted = true;
    const mql = window.matchMedia(query);
    const onChange = () => {
      if (!mounted) {
        return;
      }
      setState(!!mql.matches);
    };

    mql.addListener(onChange);
    setState(mql.matches);

    return () => {
      mounted = false;
      mql.removeListener(onChange);
    };
  }, [query]);

  return state;
};

export const useRWDRaw = () => {
  const m = useMedia2("(min-width: 960px)", -1);
  return {
    isSSR: m === -1,
    device: m ? "desktop" : "mobile",
  };
};

export const useRWD = () => {
  const rwd = useContext(RWDContext);
  return rwd;
};
