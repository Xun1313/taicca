import { useContext, useEffect } from "react";
import { ParallaxContext } from "react-scroll-parallax";

export const usePageParallaxUpdate = () => {
  const parallaxController = useContext(ParallaxContext);
  useEffect(() => {
    setTimeout(() => {
      parallaxController.update();
    }, 100);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
