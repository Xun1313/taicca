import { useRouter } from "next/router";
import { useCallback, useState, useRef } from "react";
import { useAsyncFn, useUpdateEffect } from "react-use";

export const usePageApi = ({
  initPage,
  initAllPage,
  initTotalCount,
  initData,
  api,
}) => {
  const [state, setState] = useState({
    data: initData,
    page: initPage,
    totalCount: initTotalCount,
    allPage: initAllPage,
  });

  const stateRef = useRef(state);
  stateRef.current = state;

  const { locale } = useRouter();

  const [apiState, fetch] = useAsyncFn(
    async (page) => {
      const res = await api({
        page,
        locale,
      });

      setState({
        data: res.data,
        page: page,
        totalCount: res.pages.totalCount,
        allPage: res.pages.totalPages,
      });
    },
    [api]
  );

  const handlePageClick = useCallback(
    (val) => {
      console.log(val.selected + 1, stateRef.current.page);
      if (val.selected + 1 === stateRef.current.page) {
        return;
      }
      setState((prev) => ({
        ...prev,
        page: val.selected + 1,
      }));
      fetch(val.selected + 1);
    },
    [fetch]
  );

  useUpdateEffect(() => {
    fetch(1);
  }, [api]);

  return {
    handlePageClick,
    currentPage: state.page,
    allPage: state.allPage,
    data: state.data,
    totalCount: state.totalCount,
    isLoading: apiState.loading,
  };
};
